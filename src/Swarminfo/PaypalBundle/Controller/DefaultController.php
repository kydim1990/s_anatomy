<?php

namespace Swarminfo\PaypalBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    //propose the client to pay
    public function paypaltestAction()
    {
        $paypal = $this->container->get('swarminfo_paypal.paypal');
        //set parameters to perform a paypal redirection
        $cancelurl = "http://localhost:8000/profile";
        $returnurl = "http://localhost:8000/paypalresult";
        $amount = $this->container->getParameter('paypal.amount');
        $description = "buy a premium membership to access full functionnalities";
        $custom= array("user"=>"cuicuicui");
        $paypal->redirectPaypal($cancelurl, $returnurl, $amount, $description , $custom );
        
        //return $this->render('SwarminfoPaypalBundle:Default:test.html.twig', array('name' => $text));
    }
    
    //called after redirection from paypal : perform the payment and output the result
    public function paypalResultAction(){
        $paypal = $this->container->get('swarminfo_paypal.paypal');
        var_dump($_GET);
        $token = $_GET["token"];
        $PayerId = $_GET["PayerID"];
        $amount = $this->container->getParameter('paypal.amount');
        $result = $paypal->checkPayment( $amount , $token , $PayerId);
        
        return $this->render('SwarminfoPaypalBundle:Default:index.html.twig', array('name' => (string)$result));
    }
}

