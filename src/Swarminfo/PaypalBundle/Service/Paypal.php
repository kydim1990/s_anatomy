<?php
// src/OC/PlatformBundle/Antispam/OCAntispam.php
namespace Swarminfo\PaypalBundle\Service;

class Paypal
{
    
    private $basePaypalUrl = array( 'https://www.sandbox.paypal.com/webscr?' , 'https://www.paypal.com/webscr?');
    private $basePaypalApiUrl = array('https://api-3t.sandbox.paypal.com/nvp?' , 'https://api-3t.paypal.com/nvp?' );
    private $sandbox = true;
    private $version = 124.0;
    private $user = "";
    private $userpass = "";
    private $signature = "";
    private $currencyCode = "USD";
//    private $currencyCode = "EUR";

    /**
     * @return string
     */
    public function getCurrencyCode()
    {
        return $this->currencyCode;
    }

    /**
     * @param string $currencyCode
     */
    public function setCurrencyCode($currencyCode)
    {
        $this->currencyCode = $currencyCode;
    }
    
    public function __construct($sandbox, $version, $user, $userpass, $signature, $currency){
        $this->sandbox = $sandbox;
        $this->version = $version;
        $this->user = $user;
        $this->userpass = $userpass;
        $this->signature = $signature;
        $this->currencyCode = $currency;
    }

    
    
    //build the url for the paypal requests
    public function buildPaypalUrl()
    {
        $url = $this->getBasePaypalApiUrl().'VERSION='.$this->version.'&USER='.$this->user.'&PWD='.$this->userpass.'&SIGNATURE='.$this->signature; 
        return $url; 
    }

    
    //function to redirect the user to a paypal payment window
    public function redirectPaypal($cancelurl, $returnurl, $amount, $description , $custom=array() ){
        //build the request to access the paypal api, and access it :
        $request = $this->setExpressCheckoutUrl($cancelurl, $returnurl, $amount, $description , $custom );
        var_dump($request);
        $requestCreationResult = $this->paypalCurl($request);
        
        var_dump($requestCreationResult);
        //die();
        //die in case of failure
        if (!$this->isSuccess($requestCreationResult)) die( isset($requestCreationResult['L_LONGMESSAGE0']) ?  $requestCreationResult['L_LONGMESSAGE0'] : "couldn't contact paypal");
        // redirect the user toward paypal
	header("Location: ".$this->getBasePaypalUrl()."cmd=_express-checkout&token=".$requestCreationResult['TOKEN']);
        exit();
    }
    
    //function to perform the payment
    //return an error message in case of error or true in case of success
    public function checkPayment( $amount , $token , $payerId){
        $request = $this->buildPaypalUrl(); //build the base url
        $request = $request."&METHOD=DoExpressCheckoutPayment".
			"&TOKEN=".htmlentities($token, ENT_QUOTES). 
			"&AMT=".$amount.
			"&CURRENCYCODE=".$this->currencyCode.
			"&PayerID=".htmlentities($payerId, ENT_QUOTES). 
			"&PAYMENTACTION=sale";

        $requestPaymentResult = $this->paypalCurl($request);

//        print_r($requestPaymentResult);
//        print_r($this->currencyCode);
//        die();

        if (!$this->isSuccess($requestPaymentResult)) return( isset($requestCreationResult['L_LONGMESSAGE0']) ?  $requestCreationResult['L_LONGMESSAGE0'] : "something failed, the payment didn't occur");
        return true;
    }
    
    //build the url for the sell
    public function setExpressCheckoutUrl($cancelurl, $returnurl, $amount, $description , $custom ){
        $request = $this->buildPaypalUrl(); //build the base url
        
        // La fonction urlencode permet d'encoder au format URL les espaces, slash, deux points, etc.)
        $request = $request."&METHOD=SetExpressCheckout".
                                "&CANCELURL=".urlencode($cancelurl).
                                "&RETURNURL=".urlencode($returnurl).
                                "&AMT=".urlencode($amount).
                                "&CURRENCYCODE=".urlencode($this->currencyCode).
                                "&DESC=".urlencode($description);
        if(!empty($custom)){
            $request .= "&CUSTOM=";
            foreach($custom as $key=>$value){
                $request .= urlencode($key.":".$value.";");
            }
        }
        
        return $request;
    }

    //launch a curl to contact paypal api; return the output as an array of parameters
    //return false in case of error
    public function paypalCurl($url){
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $curlResult = curl_exec($ch);
        // S'il y a une erreur, on affiche "Erreur", suivi du détail de l'erreur.
        if (!$curlResult) return false;
        
        // retrieve all paramaters in an array
        $results = array();
        $parameters = explode("&",$curlResult);
        foreach($parameters as $param)
        {
            list($key, $value) = explode("=", $param);
            $results[$key]=urldecode($value); 
        }
        
        // close the curl session
        curl_close($ch);
        
        return $results;
    }
    
    //check if the output of a paypal request is a success
    //output is the return value of the paypalCurl function
    public function isSuccess($output){
        return $output !== false && $output["ACK"] == "Success";
    }
    
    public function getBasePaypalUrl(){
        return $this->sandbox ? $this->basePaypalUrl[0] : $this->basePaypalUrl[1];
    }
    
    public function getBasePaypalApiUrl(){
        return $this->sandbox ? $this->basePaypalApiUrl[0] : $this->basePaypalApiUrl[1];
    }
    
    
};

//
//
//https://api-3t.sandbox.paypal.com/nvp?VERSION=122.0&USER=paypalimagescript_api1.yopmail.com&PWD=4Q4B4MUW3CQQ3645&SIGNATURE=AFcWxV21C7fd0v3bYYYRCpSSRl31AhsbIplB46npsDeWpfRBH8ac.Wat&METHOD=SetExpressCheckout&AMT=10.00&RETURNURL=http://www.siteduzero.com/return.php&CANCELURL=http://www.siteduzero.com/cancel.php
//
//https://www.sandbox.paypal.com/webscr&cmd=_express-checkout&token=EC-53717334P4656731C
//
//
//TOKEN=EC%2d53717334P4656731C&TIMESTAMP=2015%2d11%2d12T12%3a57%3a25Z&CORRELATIONID=6d7c6be63cba&ACK=Success&VERSION=122%2e0&BUILD=18308778