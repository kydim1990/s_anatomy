<?php

namespace Swarminfo\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Orderlist
{
  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="bigint")
   * @ORM\Id
   */
  protected $id;

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param int $userid
     */
    public function setUserid($userid)
    {
        $this->userid = $userid;
    }

    /**
     * @param \DateTime $paydate
     */
    public function setPaydate($paydate)
    {
        $this->paydate = $paydate;
    }

    /**
     * @param float $totalexvat
     */
    public function setTotalexvat($totalexvat)
    {
        $this->totalexvat = $totalexvat;
    }

    /**
     * @param float $totalinvat
     */
    public function setTotalinvat($totalinvat)
    {
        $this->totalinvat = $totalinvat;
    }

  /**
   * @var integer
   *
   * @ORM\Column(name="userid", type="bigint")
   * @ORM\Id
   */
  protected $userid;

    /**
     * @var integer
     *
     * @ORM\Column(name="plan", type="bigint")
     */
    protected $plan;

  /**
   * @var \DateTime
   *
   * @ORM\Column(name="paydate", type="datetime")
   */
  protected $paydate;

    /**
     * @return int
     */
    public function getPlan()
    {
        return $this->plan;
    }

    /**
     * @param int $plan
     */
    public function setPlan($plan)
    {
        $this->plan = $plan;
    }

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="pendingdate", type="datetime")
     */
    protected $pendingdate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="comletedgdate", type="datetime")
     */
    protected $comletedgdate;

  /**
   * @var float
   *
   * @ORM\Column(name="totalexvat", type="float")
   */
  protected $totalexvat;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getUserid()
    {
        return $this->userid;
    }

    /**
     * @return \DateTime
     */
    public function getPaydate()
    {
        return $this->paydate;
    }

    /**
     * @return float
     */
    public function getTotalexvat()
    {
        return $this->totalexvat;
    }

    /**
     * @return float
     */
    public function getTotalinvat()
    {
        return $this->totalinvat;
    }

    /**
     * @return \DateTime
     */
    public function getPendingdate()
    {
        return $this->pendingdate;
    }

    /**
     * @return \DateTime
     */
    public function getComletedgdate()
    {
        return $this->comletedgdate;
    }

  /**
   * @var float
   *
   * @ORM\Column(name="totalinvat", type="float")
   */
  protected $totalinvat;

    /**
     * @var string
     *
     * @ORM\Column(name="lang", type="text")
     */
    protected $lang;

    /**
     * @return string
     */
    public function getLang()
    {
        return $this->lang;
    }

    /**
     * @param string $lang
     */
    public function setLang($lang)
    {
        $this->lang = $lang;
    }

  public function __construct($id,$userid,$amount,$pendate,$compldate,$plan,$lang='en'){
      $this->id=$id;
      $this->userid=$userid;
      $this->paydate=new \DateTime("now");
      $this->totalexvat=$amount;
      $this->totalinvat=$amount;
      $this->pendingdate=$pendate;
      $this->comletedgdate=$compldate;
      $this->plan=$plan;
      $this->lang=$lang;
  }



}




