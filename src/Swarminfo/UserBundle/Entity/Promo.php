<?php

namespace Swarminfo\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Promo
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="created", type="datetime")
     */
    protected $created;

    /**
     * @ORM\Column(name="isused", type="integer")
     */
    protected $isused=0;

    /**
     * @ORM\Column(name="isunlimited", type="integer")
     */
    protected $isunlimited=0;

    /**
     * @return mixed
     */
    public function getIsused()
    {
        return $this->isused;
    }

    /**
     * @param mixed $isused
     */
    public function setIsused($isused)
    {
        $this->isused = $isused;
    }

    /**
     * @return mixed
     */
    public function getPromocode()
    {
        return $this->promocode;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getPrefix()
    {
        return $this->prefix;
    }

    /**
     * @param string $prefix
     */
    public function setPrefix($prefix)
    {
        $this->prefix = $prefix;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return mixed
     */
    public function getIsunlimited()
    {
        return $this->isunlimited;
    }

    /**
     * @param mixed $isunlimited
     */
    public function setIsunlimited($isunlimited)
    {
        $this->isunlimited = $isunlimited;
    }

    /**
     * @param mixed $promocode
     */
    public function setPromocode($promocode)
    {
        $this->promocode = $promocode;
    }

    /**
     * @return mixed
     */
    public function getExperied()
    {
        return $this->experied;
    }

    /**
     * @param mixed $experied
     */
    public function setExperied($experied)
    {
        $this->experied = $experied;
    }

    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param mixed $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="prefix", type="text")
     */
    protected $prefix;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="text")
     */
    protected $code;

    /**
     * @var string
     *
     * @ORM\Column(name="promocode", type="text")
     */
    protected $promocode;

    /**
     * @ORM\Column(name="experied", type="integer")
     */
    protected $experied;

    /**
     * @ORM\Column(name="startfrom", type="integer")
     */
    protected $startfrom;

    /**
     * @return mixed
     */
    public function getStartfrom()
    {
        return $this->startfrom;
    }

    /**
     * @param mixed $startfrom
     */
    public function setStartfrom($startfrom)
    {
        $this->startfrom = $startfrom;
    }



    public function __construct($prefix,$code,$experied, $isunlimited, $startfrom=null){
      $this->created = new \DateTime("now");
      $this->prefix = $prefix;
      $this->code = $code;
      $this->experied = $experied;
      $this->promocode = $this->prefix.$this->code;
      $this->isunlimited=$isunlimited;
      $this->startfrom = $startfrom;
  }



}




