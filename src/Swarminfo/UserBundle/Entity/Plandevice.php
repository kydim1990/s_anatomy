<?php

namespace Swarminfo\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Plandevice
{
  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="bigint")
   * @ORM\Id
   */
  protected $id;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getCountdevice()
    {
        return $this->countdevice;
    }

    /**
     * @param int $countdevice
     */
    public function setCountdevice($countdevice)
    {
        $this->countdevice = $countdevice;
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="countdevice", type="bigint")
     */
    protected $countdevice;

    /**
     * @var string
     *
     * @ORM\Column(name="lang", type="text")
     */
    protected $lang;

    /**
     * @return string
     */
    public function getLang()
    {
        return $this->lang; 
    }

    /**
     * @param string $lang
     */
    public function setLang($lang)
    {
        $this->lang = $lang;
    }

  public function __construct($id,$countdevice,$lang='en'){
      $this->id=$id;
      $this->countdevice=$countdevice;
      $this->lang=$lang;
  }



}




