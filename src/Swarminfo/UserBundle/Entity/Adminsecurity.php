<?php
/**
 * Created by PhpStorm.
 * User: Diman
 * Date: 02.03.2017
 * Time: 16:16
 */

namespace Swarminfo\UserBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Adminsecurity
{
    /**
     * @ORM\Column(name="id", type="text")
     * @ORM\Id
     */
    protected $id=0;

    /**
     * @ORM\Column(name="code", type="integer")
     */
    protected $code=0;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $ip
     */
    public function setIp($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    public function __construct($id,$code)
    {
        $this->id = $id;
        $this->code = $code;
    }
}