<?php

namespace Swarminfo\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\Null;

/**
 * @ORM\Entity
 */
class Fieldsregister
{
  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="bigint")
   * @ORM\Id
   */
  protected $id;
  /**
   * @var \DateTime
   *
   * @ORM\Column(name="createdate", type="datetime")
   */
  protected $createdate;

  /**
   * @return mixed
   */
  public function getCreatedate()
  {
    return $this->createdate->format('m/d/Y');
  }

  /**
   * @param mixed $createdate
   */
  public function setCreatedate($createdate)
  {
    $this->createdate = $createdate;
  }

  /**
   * @return string
   */
  public function getPromocode()
  {
    return $this->promocode;
  }

  /**
   * @param string $promocode
   */
  public function setPromocode($promocode)
  {
    $this->promocode = $promocode;
  }
  /**
   * @var integer
   *
   * @ORM\Column(name="userid", type="bigint")
   */
  protected $userid;
  /**
   * @var string
   *
   * @ORM\Column(name="title", type="text")
   */
  protected $title;

  /**
   * @var string
   *
   * @ORM\Column(name="promocode", type="text")
   */
  protected $promocode;

  /**
   * @var string
   *
   * @ORM\Column(name="firstname", type="text")
   */
  protected $firstname;

  /**
   * @var string
   *
   * @ORM\Column(name="lastname", type="text")
   */
  protected $lastname;

  /**
   * @var string
   *
   * @ORM\Column(name="profession", type="text")
   */
  protected $profession;

  /**
   * @return string
   */
  public function getCountry()
  {
    return $this->country;
  }

  /**
   * @param string $country
   */
  public function setCountry($country)
  {
    $this->country = $country;
  }

  /**
   * @return string
   */
  public function getAddressLine1()
  {
    return $this->address_line_1;
  }

  /**
   * @param string $address_line_1
   */
  public function setAddressLine1($address_line_1)
  {
    $this->address_line_1 = $address_line_1;
  }

  /**
   * @return string
   */
  public function getAddressLine2()
  {
    return $this->address_line_2;
  }

  /**
   * @param string $address_line_2
   */
  public function setAddressLine2($address_line_2)
  {
    $this->address_line_2 = $address_line_2;
  }

  /**
   * @return string
   */
  public function getCity()
  {
    return $this->city;
  }

  /**
   * @param string $city
   */
  public function setCity($city)
  {
    $this->city = $city;
  }

  /**
   * @return int
   */
  public function getPostcode()
  {
    return $this->postcode;
  }

  /**
   * @param int $postcode
   */
  public function setPostcode($postcode)
  {
    $this->postcode = $postcode;
  }

  /**
   * @return string
   */
  public function getRegion()
  {
    return $this->region;
  }

  /**
   * @param string $region
   */
  public function setRegion($region)
  {
    $this->region = $region;
  }

  /**
   * @return string
   */
  public function getPhone()
  {
    return $this->phone;
  }

  /**
   * @param string $phone
   */
  public function setPhone($phone)
  {
    $this->phone = $phone;
  }

  /**
   * @return boolean
   */
  public function isComPref1()
  {
    return $this->com_pref_1;
  }

  /**
   * @param boolean $com_pref_1
   */
  public function setComPref1($com_pref_1)
  {
    $this->com_pref_1 = $com_pref_1;
  }

  /**
   * @return boolean
   */
  public function isComPref2()
  {
    return $this->com_pref_2;
  }

  /**
   * @param boolean $com_pref_2
   */
  public function setComPref2($com_pref_2)
  {
    $this->com_pref_2 = $com_pref_2;
  }

  /**
   * @return string
   */
  public function getLastname()
  {
    return $this->lastname;
  }

  /**
   * @param string $lastname
   */
  public function setLastname($lastname)
  {
    $this->lastname = $lastname;
  }

  /**
   * @return string
   */
  public function getProfession()
  {
    return $this->profession;
  }

  /**
   * @param string $profession
   */
  public function setProfession($profession)
  {
    $this->profession = $profession;
  }

  /**
   * @return boolean
   */
  public function isIsphisican()
  {
    return $this->isphisican;
  }

  /**
   * @param boolean $isphisican
   */
  public function setIsphisican($isphisican)
  {
    $this->isphisican = $isphisican;
  }

  /**
   * @return string
   */
  public function getOrganization()
  {
    return $this->organization;
  }

  /**
   * @param string $organization
   */
  public function setOrganization($organization)
  {
    $this->organization = $organization;
  }

  /**
   * @return string
   */
  public function getOrganizationType()
  {
    return $this->organization_type;
  }

  /**
   * @param string $organization_type
   */
  public function setOrganizationType($organization_type)
  {
    $this->organization_type = $organization_type;
  }

  /**
   * @var boolean
   *
   * @ORM\Column(name="isphisican", type="boolean")
   */
  protected $isphisican;

  /**
   * @var string
   *
   * @ORM\Column(name="organization", type="text")
   */
  protected $organization;
  /**
   * @var string
   *
   * @ORM\Column(name="organization_type", type="text")
   */
  protected $organization_type;

  /**
   * @var string
   *
   * @ORM\Column(name="country", type="text")
   */
  protected $country;

  /**
   * @var string
   *
   * @ORM\Column(name="address_line_1", type="text")
   */
  protected $address_line_1;

  /**
   * @var string
   *
   * @ORM\Column(name="address_line_2", type="text")
   */
  protected $address_line_2;

  /**
   * @var string
   *
   * @ORM\Column(name="city", type="text")
   */
  protected $city;

  /**
   * @var int
   *
   * @ORM\Column(name="postcode", type="text")
   */
  protected $postcode;

  /**
   * @var string
   *
   * @ORM\Column(name="region", type="string")
   */
  protected $region;
  /**
   * @var string
   *
   * @ORM\Column(name="phone", type="text")
   */
  protected $phone;

  /**
   * @var boolean
   *
   * @ORM\Column(name="com_pref_1", type="boolean")
   */
  protected $com_pref_1;

  /**
   * @var boolean
   *
   * @ORM\Column(name="com_pref_2", type="boolean")
   */
  protected $com_pref_2;

  /**
   * @return int
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * @param int $id
   */
  public function setId($id)
  {
    $this->id = $id;
  }

  /**
   * @return mixed
   */
  public function getUserid()
  {
    return $this->userid;
  }

  /**
   * @param mixed $userid
   */
  public function setUserid($userid)
  {
    $this->userid = $userid;
  }

  /**
   * @return mixed
   */
  public function getTitle()
  {
    return $this->title;
  }

  /**
   * @param mixed $title
   */
  public function setTitle($title)
  {
    $this->title = $title;
  }

  /**
   * @return string
   */
  public function getFirstname()
  {
    return $this->firstname;
  }

  /**
   * @param string $firstname
   */
  public function setFirstname($firstname)
  {
    $this->firstname = $firstname;
  }

  public function __construct($_userid,$_title,$_firsname,$lastname,
                              $profession,$isphisican,$organization,$organization_type,
                              $country,$address_line_1,$address_line_2,$city,$postcode,$region,
                              $phone,$com_pref_1,$com_pref_2, $promocode=null){
    $this->userid=$_userid;
    $this->title=$_title;
    $this->firstname=$_firsname;
    $this->id=$_userid;
    $this->lastname=$lastname;
    $this->profession=$profession;
    $this->isphisican=$isphisican;
    $this->organization=$organization;
    $this->organization_type=$organization_type;
    $this->country=$country;
    $this->address_line_1=$address_line_1;
    $this->address_line_2=$address_line_2;
    $this->city=$city;
    $this->postcode=$postcode;
    $this->region=$region;
    $this->phone=$phone;
    $this->com_pref_1=$com_pref_1;
    $this->com_pref_2=$com_pref_2;
    $this->createdate=new \DateTime("now");
    $this->promocode = $promocode;
  }



}




