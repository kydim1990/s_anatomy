<?php

namespace Swarminfo\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Devices
{
  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="bigint")
   * @ORM\Id
   */
  protected $id;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="device", type="text")
     */
    protected $device; 

    /**
     * @return string
     */
    public function getDevice()
    {
        return $this->device;
    }

    /**
     * @param string $device
     */
    public function setDevice($device)
    {
        $this->device = $device;
    }


    /**
     * @var string
     *
     * @ORM\Column(name="lang", type="text")
     */
    protected $lang;

    /**
     * @return string
     */
    public function getLang()
    {
        return $this->lang;
    }

    /**
     * @param string $lang
     */
    public function setLang($lang)
    {
        $this->lang = $lang;
    }


  public function __construct($id,$device,$lang='en'){
      $this->id=$id;
      $this->device=$device;
      $this->lang=$lang;
  }



}




