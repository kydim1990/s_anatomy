<?php

namespace Swarminfo\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM; 

/**
 * @ORM\Entity
 */
class Subscribelanguage
{
  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="bigint")
   * @ORM\Id
   */
  protected $id;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param string $language
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    }

   /**
    * @var string
    *
    * @ORM\Column(name="language", type="text")
    */
  protected $language;



  public function __construct($id,$language){
      $this->id=$id;
      $this->language=$language;
  }



}




