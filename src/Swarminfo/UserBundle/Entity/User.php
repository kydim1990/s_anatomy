<?php

namespace Swarminfo\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * @ORM\Entity
 */
class User extends BaseUser
{
  /**
   * @ORM\Column(name="id", type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  protected $id;
  
  /**
   * @ORM\Column(name="expire", type="integer")
   */
  protected $expire=0;

  /**
   * @ORM\Column(name="locked_at", type="integer")
   */
  protected $locked_at=0;

  /**
   * @var string
   *
   * @ORM\Column(name="status", type="text")
   */
  protected $status;

  
  /**
   * @return mixed
   */
  public function getLockedAt()
  {
    return $this->locked_at;
  }

  /**
   * @param mixed $locked_at
   */
  public function setLockedAt($locked_at)
  {
    $this->locked_at = $locked_at;
  }

  /**
   * @ORM\Column(name="pass_left_attempt", type="text")
   */
  protected $pass_left_attempt;

  /**
   * @ORM\Column(name="user_locked_at", type="datetime")
   */
  protected $user_locked_at;

  /**
   * @return mixed
   */
  public function getStatus()
  {
    return $this->status;
  }

  /**
   * @param mixed $status
   */
  public function setStatus($status)
  {
    $this->status = $status;
  }


  
  public function setExpire($v){
    $this->expire = $v;
  }

  /**
   * @return mixed
   */
  public function getPassLeftAttempt()
  {
    return $this->pass_left_attempt;
  }

  /**
   * @param mixed $pass_left_attempt
   */
  public function setPassLeftAttempt($pass_left_attempt)
  {
    $this->pass_left_attempt = $pass_left_attempt;
  }

  /**
   * @return mixed
   */
  public function getUserLockedAt()
  {
    return $this->user_locked_at;
  }
  

  /**
   * @param mixed $user_locked_at
   */
  public function setUserLockedAt($user_locked_at)
  {
    $this->user_locked_at = $user_locked_at;
  }
  
  public function getExpire(){ return $this->expire; }
  
}




