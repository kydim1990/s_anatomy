<?php
/**
 * Created by PhpStorm.
 * User: Diman
 * Date: 10.10.2016
 * Time: 20:23
 */

namespace Swarminfo\UserBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;


class SendemailCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('app:send-email-users')

            // the short description shown while running "php bin/console list"
            ->setDescription('Send email')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp("This command allows you to send email...");
    }

    protected function sendemail($address, $body)
    {
        $emailFrom=$this->getContainer()->getParameter('mailer_user');
        $emailFromName=$this->getContainer()->getParameter('mailer_name');
        $subscription_subject=$this->getContainer()->getParameter('subscription_subject');
        $message = \Swift_Message::newInstance()
            ->setSubject($subscription_subject)
            ->setFrom(array($emailFrom => $emailFromName))
            ->setTo($address)
            ->setBody($body)
        ;
        $this->getContainer()->get('mailer')->send($message);
        $res = 'Email has been sent to '.$address;
        return $res;

    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
//        Test send email command
//        $emailFrom=$this->getContainer()->getParameter('mailer_user');
//        $message = \Swift_Message::newInstance()
//            ->setSubject('Email from command')
//            ->setFrom($emailFrom)
//
//            ->setTo('kydim1990@rambler.ru')
//            ->setBody('Email from command')
//
//        ;
//        $this->getContainer()->get('mailer')->send($message);
//        $output->writeln('Email has been sent');
//        --------------------------
        
        $em = $this->getContainer()->get('doctrine')->getManager();
        $query = $em->createQuery(
            'SELECT u.email, u.expire, u.username
            FROM SwarminfoUserBundle:User u
            WHERE u.expire != :time
            ORDER BY u.id ASC'
        )->setParameter('time', time());

        $premium_users = $query->getResult();

        $send_days = $this->getContainer()->getParameter('send_email_experied');
        $subscription_expered=$this->getContainer()->getParameter('subscription_expered');

        foreach ($premium_users as $pu){
            $diff = round(($pu['expire']-time())/(3600*24));
            if (array_search($diff, $send_days)){
                print_r($pu['username']." ");
                print_r($pu['email']." ");
                $text = $subscription_expered[0].' '.$diff.' '.$subscription_expered[1];
                print_r(" ");
                $this->sendemail($pu['email'],$text);
                print_r($text);
            }
        }

//        $output->writeln($this->sendemail('kydim1990@rambler.ru','Hello man'));

    }
}