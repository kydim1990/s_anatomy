<?php

namespace Swarminfo\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class SwarminfoUserBundle extends Bundle
{
	public function getParent(){
		return "FOSUserBundle";
	}
}
