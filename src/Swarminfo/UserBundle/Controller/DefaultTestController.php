<?php

namespace Swarminfo\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Swarminfo\UserBundle\Entity\Devices;
use FOS\UserBundle\Mailer;
use Swarminfo\UserBundle\Entity\Subscribelanguage;
use Symfony\Component\Validator\Constraints\Null;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Util\SecureRandom;
use Swarminfo\UserBundle\Entity\Adminsecurity;

class DefaultTestController extends Controller
{
    private $languageName=array('en'=>'English','sc'=>'中文(简体)','tc'=>'中文(繁體)','fr'=>'Français','es'=>'Español','de'=>'Deutsch',
        'ja'=>'日本語','pt'=>'Português','ru'=>'Русский','ar'=>'اللُغَة العَرَبِيَّة'
    );

    private $currencycode=array('en'=>'USD','sc'=>'HKD','tc'=>'HKD','fr'=>'EUR','es'=>'EUR','de'=>'EUR',
        'ja'=>'JPY','pt'=>'EUR','ru'=>'RUB','ar'=>'USD'
    );

    private function getamounttolang($locale)
    {
        $res=array();
        for ($i=1;$i<4;$i+=1)
        {
            if ($locale == null) $locale = 'en';
            //print_r('locale= '.$locale);
            $tmp=$this->container->getParameter('paypal_'.$locale.'_'.(string)$i);
            array_push($res,$tmp);
        }
        return $res;
    }

    //get language of curren usrl
    private function getlanguageUrl(){
        preg_match('/\/[a-z][a-z]\//',$_SERVER['REQUEST_URI'], $lanshort);
        return substr($lanshort[0], 1, -1);
    }
    
    public function indexAction($name)
    {
        return $this->render('SwarminfoUserBundle:Default:index.html.twig', array('name' => $name));
    }
    
    //display the pannel for the user (depending on authentication, display login, registration, premium membership)
    public function userpannelAction()
    {

//        print_r('userpannelAction');
        //send email if admin
        $isadmin = $this->isAdmin();
        if ($isadmin){
            $body = $this->container->getParameter('message_admin_logged_body');
            $subject = $this->container->getParameter('message_admin_logged_subject');
            $emailFrom=$this->container->getParameter('mailer_user');
            $usr= $this->get('security.context')->getToken()->getUser();
            $message = \Swift_Message::newInstance()
//                ->setSubject('Admin is logged!')
                ->setSubject($subject)
                ->setFrom($emailFrom)
                ->setTo($usr->getEmail())
//                ->setTo('kydim1990@rambler.ru')
//                ->setBody('Admin '.$usr->getUsername().' is logged!')
                ->setBody($body, 'text/html')
//                ->setBody(
//                    $this->renderView(
//                        'HelloBundle:Hello:email.txt.twig',
//                        array('name' => $name)
//                    )
//                )
            ;
            $this->get('mailer')->send($message);
        }

        //check devices
        $usr= $this->get('security.context')->getToken()->getUser();

        $repository_devices = $this->getDoctrine()->getManager()->getRepository('SwarminfoUserBundle:Devices');
        $repository_plan = $this->getDoctrine()->getManager()->getRepository('SwarminfoUserBundle:Plandevice');
        $locale = $this->get('request')->getLocale();
        $userplan=$repository_plan->findOneBy(array('id' => $usr->getId(),'lang'=>$locale));
        print_r($userplan);
//        die();

        $session = $this->container->get('session');
        $t=$this->get('security.context')->getToken();
        $t->setAttribute("sesid",$session->getId() );
        $session = $this->get('session');
        $userdevices=$repository_devices->findBy(array('id' => $usr->getId(),'lang'=>$locale));
        $em = $this->getDoctrine()->getManager();
        
        if ($userplan and $userplan->getCountdevice()!=0)
//        if ($userplan)
        {
//            delete died session
            $openses=scandir(session_save_path());
            $arr_sess=array();
            foreach ($openses as $name => $value)
            {
                array_push($arr_sess,str_replace('sess_','',$value)); 
            }

//            print_r($userdevices);
//            print_r($arr_sess);
//            die();

            $cur_sess_count=0;

            foreach ($userdevices as $ses)
            {
//                print_r($ses->getDevice());
                if (!in_array($ses->getDevice(), $arr_sess))
                {
//                    print_r($ses->getDevice());
                    $em->remove($ses);
                    $em->flush();
                    $cur_sess_count+=1;
//                    die();
                }
            }


            if (count($userdevices)-$cur_sess_count>=$userplan->getCountdevice() ) {
                $usersession=$repository_devices->findBy(array('device' => $session->getId(),'lang'=>$locale));
                if (!$usersession)
                {
//                    print_r($usersession);
//                    die();
                    $this->get('security.context')->setToken(null);
                    $message="The account is full of users now. please try again later";
                    return $this->redirect( $this->generateUrl('fos_user_security_login', array( "message"=>$message)) );
                }

                
            }
            else{
                $sesdev=$repository_devices->findOneBy(array('device' => $session->getId()));
                if (!$sesdev)
                {
                    //insert device
                    $userdevice=new Devices($usr->getId(),$session->getId(),$locale);
                    $em->persist($userdevice);
                    $em->flush();
                }

            }
        }
//        ----------------
//      pass_countpossible to default
        if ($usr->getPassLeftAttempt() < $this->container->getParameter('pass_countpossible')){
            $usr->setPassLeftAttempt($this->container->getParameter('pass_countpossible'));
            $em->persist($usr);
            $em->flush();
        }

        $securityContext = $this->container->get('security.authorization_checker');
        if ($securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $usr= $this->get('security.context')->getToken()->getUser();
//            $expire = time() > $usr->getExpire() ? "expired" : date( "F j o" , $usr->getExpire() );
            $diff = ($usr->getExpire()-time());
            $expire = time() > $usr->getExpire() ? "expired" : floor($diff / (60 * 60 * 24))." days left expired";


            if ($expire == "expired"){
                //delete all user plan devices
                $delete_userplans = $repository_plan->findOneBy(array('id' => $usr->getId()));
                if ($delete_userplans)
                {
                    foreach ($delete_userplans as $del_plan){
                        $em->remove($del_plan);
                        $em->flush();
                    }
                }

            }

            //recapcha
            //-------
            if (strpos($_SERVER['HTTP_REFERER'], '/login') === false)
                return $this->render('SwarminfoUserBundle::userpannel.html.twig', array( "expire"=>$expire));

//            print_r($_SERVER['HTTP_REFERER']);
//            die();
            //redirect after login
            $ispremium = $this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY') && isset($usr) && $usr->getExpire() > time() && $this->getlanguageUrl()!='en';

            if ($ispremium )
            {
                $curlang=$this->getlanguageUrl();
                $lang=$this->languageName[$curlang];
                $codecurrency=$this->currencycode[$curlang];
                $amounts=$this->getamounttolang($curlang);
            }
            else
            {
                $codecurrency=null;
                $lang=null;
            }
            //Redirect after login
            //print_r($this->container->getParameter('is_use_paypall'));
            //die(' userpannelAction Redirect after login');
	   
            if ($this->container->getParameter('is_use_paypall') == 0 )
                return $this->redirect($this->generateUrl('swarminfo_imagescript_page1')); 
//                return $this->render('SwarminfoUserBundle::userpannel.html.twig', array( "expire"=>$expire)); 

            return $this->redirect($this->generateUrl('swarminfo_imagescript_page1', array('user'=>$usr, "ispremium"=>$ispremium,
                "language"=>$lang, "currency"=>$codecurrency, "amounts"=>$amounts,"curlang"=>$curlang)));
        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login', array( )) );
        }

        
    }

    private function isAdmin(){
        $adminusers = $this->container->getParameter('adminusers');
        $usr = $this->get('security.context')->getToken()->getUser();
        $security = $this->get('security.context');
        $isauthenticated = $security->isGranted('IS_AUTHENTICATED_FULLY') || $security->isGranted('IS_AUTHENTICATED_REMEMBERED');
        $isadmin = $isauthenticated && isset($usr) && in_array($usr->getUsername(), $adminusers) ;
        return $isadmin;
    }

    public function usertestAction(){
        $emailFrom=$this->container->getParameter('mailer_user');
        $usr= $this->get('security.context')->getToken()->getUser();
        $body = $this->container->getParameter('message_admin_logged_body');
        $subject = $this->container->getParameter('message_admin_logged_subject');
        $message = \Swift_Message::newInstance()
//            ->setSubject('Admin is logged!')
            ->setSubject($subject)
            ->setFrom($emailFrom)
//                ->setTo($usr->getEmail())
            ->setTo('kydim1990@rambler.ru')
//            ->setBody('Admin dsfdsfsdfdf is logged!')
            ->setBody($body)
//                ->setBody(
//                    $this->renderView(
//                        'HelloBundle:Hello:email.txt.twig',
//                        array('name' => $name)
//                    )
//                )
        ;
        $this->get('mailer')->send($message);
        return $this->redirect( $this->generateUrl('fos_user_security_login', array( )) );
    }

    protected function sendemail($address, $subject,$body)
    {
        $emailFrom=$this->container->getParameter('mailer_user');
        $emailFromName=$this->container->getParameter('mailer_name');
        $message = \Swift_Message::newInstance()
            ->setSubject($subject)
            ->setFrom(array($emailFrom => $emailFromName))
            ->setTo($address)
            ->setBody($body)
        ;
        $this->container->get('mailer')->send($message);

    }

    private function getip(){
        return getenv('HTTP_CLIENT_IP')?:
            getenv('HTTP_X_FORWARDED_FOR')?:
                getenv('HTTP_X_FORWARDED')?:
                    getenv('HTTP_FORWARDED_FOR')?:
                        getenv('HTTP_FORWARDED')?:
                            getenv('REMOTE_ADDR');
    }


    public function isadminAction(){
        $result=null;
        $adminusers = $this->container->getParameter('adminusers');
        $ip=$this->getip();
        $adminsecuriy = $this->getDoctrine()->getManager()->getRepository('SwarminfoUserBundle:Adminsecurity');
        $securiy_code = $adminsecuriy->findOneBy(array('id'=> $ip));
        if (isset($_POST["_code"]) && !empty($_POST["_code"])){
            $result=null;
            if ($securiy_code->getCode()== $_POST["_code"]){
                $result='ok';
            }

            $response = new Response(json_encode(array('result' => $result)));
            return $response;
        }
        
       

        $code=null;
        if (in_array($_POST['username'], $adminusers)){
            $result='ok';

            $generator = new SecureRandom();
            $random = $generator->nextBytes(2048);
            $decEncoded = bindec($random);
            
//            print_r($securiy_code);
//            echo('<br>');
            if (!is_null($securiy_code)){
                $securiy_code->setCode($decEncoded);
            }
            else{
                $securiy_code = new Adminsecurity($this->getip(), $decEncoded);
            }
//            print_r($securiy_code);
            $em = $this->getDoctrine()->getManager();
            $em->persist($securiy_code);
            $em->flush();


            $receive_admin = $this->container->getParameter('mailer_receives_security_admin_user');
            $subject = $this->container->getParameter('mailer_receives_security_admin_subject');
            $body = $this->container->getParameter('mailer_receives_security_admin_body');
            $this->sendemail($receive_admin,  $subject , $body.$decEncoded);
            $code=$decEncoded;
        }

//        $generator = new SecureRandom();
//        $random = $generator->nextBytes(2048);
//        $decEncoded = bindec($random);
//        print_r($decEncoded);
//        die();
//        exit();
        $response = new Response(json_encode(array('result' => $result,'code'=>$code))); 
        return $response;
    }
}
