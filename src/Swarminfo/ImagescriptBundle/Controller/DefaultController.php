<?php

namespace Swarminfo\ImagescriptBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Swarminfo\ImagescriptBundle\Entity\Project;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Translation\MessageSelector;
use Symfony\Component\Translation\Loader\XliffFileLoader;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Swarminfo\UserBundle\Entity\Orderlist;
use Swarminfo\UserBundle\Entity\Plandevice;
use Swarminfo\UserBundle\Entity\Devices;
use Swarminfo\UserBundle\Entity\Fieldsregister;
use Swarminfo\UserBundle\Entity\Subscribelanguage;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;

class DefaultController extends Controller
{
    //current amiunt for paypall
    private $curamount;
    //count of devices, 0 -all
    private $plan;

    private $Devices_plan=array('A'=>1,'B'=>3,'C'=>0);

    private $languageName=array('en'=>'English','sc'=>'中文(简体)','tc'=>'中文(繁體)','fr'=>'Français','es'=>'Español','de'=>'Deutsch',
        'ja'=>'日本語','pt'=>'Português','ru'=>'Русский','ar'=>'اللُغَة العَرَبِيَّة'
        );

    private $currencycode=array('en'=>'USD','sc'=>'HKD','tc'=>'HKD','fr'=>'EUR','es'=>'EUR','de'=>'EUR',
        'ja'=>'JPY','pt'=>'EUR','ru'=>'RUB','ar'=>'The page still not appear.'
    );



    //action to open a paypal purchase order
    public function paypalgetpremiumAction($test=0){
        //if not logged, register
//        $repository_plan = $this->getDoctrine()->getManager()->getRepository('SwarminfoUserBundle:Plandevice');
//        $userplan=$repository_plan->findOneBy(array('id' => 7,'lang'=>$_GET['curlang']));
//
//        print_r($userplan);
//        die();

        $securityContext = $this->container->get('security.authorization_checker');
        if ( ! $securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
//            return $this->redirect( $this->generateUrl('fos_user_security_login', array( )) );
            return $this->redirect( $this->generateUrl('swarminfo_imagescript_page5', array("plan"=>$_GET['plan'],"amount"=>$_GET['amount'] )) );
        }
        $paypal = $this->container->get('swarminfo_paypal.paypal');

//        print_r($paypal->getCurrencyCode());
//        if ($_GET['currency'])
//        {
//            $replang = $this->getDoctrine()->getManager()->getRepository('SwarminfoUserBundle:Subscribelanguage');
//            $usr= $this->get('security.context')->getToken()->getUser();
//            $langfound=$replang->findOneBy(array('id' => $usr->getId(), "language"=>$_GET['curlang']));
//            if ($langfound) //if exist
//            {
//                $message = "You are already subscribed to ".$this->languageName[$_GET['curlang']];
//                return $this->render('SwarminfoImagescriptBundle::simplemessage.html.twig', array('message' => $message));
//            }
//            else{
//                $paypal->setCurrencyCode($_GET['currency']);
////                $paypal->setCurrencyCode("USD");
//            }
//
//        }
//        $paypal->setCurrencyCode($_GET['currency']);
//        die();

        //set parameters to perform a paypal redirection
        $amount = $_GET['amount'];

        $this->curamount=$amount;

        $cancelurl = $this->generateUrl('swarminfo_imagescript_cancelpaypalorder', array( ) , true);
        $returnurl = $this->generateUrl('swarminfo_imagescript_paypalmakepremium', array("amount"=>$amount,
            "plan"=>$_GET['plan'],"curlang"=>$_GET['curlang'] ), true);;
//        $amount = $this->container->getParameter('paypal.amount');

        $this->plan=$_GET['plan'];

        $description = "buy a premium membership to access full functionnalities";
        $custom= array();
        var_dump($returnurl);
        var_dump("$cancelurl");


        $result = $paypal->redirectPaypal($cancelurl, $returnurl, $amount, $description , $custom );
        
        //return $this->render('SwarminfoPaypalBundle:Default:test.html.twig', array('name' => $text));
    }
    
    //action to cancel the paypal order
    public function cancelpaypalorderAction(){
        $message = "the purchase of the premium option has been canceled";
        return $this->render('SwarminfoImagescriptBundle::simplemessage.html.twig', array('message' => $message));
    }


    //change language
    public function changelanguageAction()
    {
        $_locale = $_GET['_locale'];
//        print_r($_locale);
        $request = $this->get('request');
//        $request->getSession()->set('_locale', $_locale);

        $referer = $request->request->get('referer', $request->headers->get('referer'));
        if ($_locale=='en') {
            $request->getSession()->set('_locale', $_locale);
            return $this->redirect($referer);
        }

//        check subscribe for each language
        $usr = $this->get('security.context')->getToken()->getUser();
        $ispremium = $this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY') && isset($usr) && $usr->getExpire() > time();


//        print_r($_locale);
//        die();

        if ($ispremium){
//                die('test1');
            $repository_plan = $this->getDoctrine()->getManager()->getRepository('SwarminfoUserBundle:Plandevice');
            $repository_devices = $this->getDoctrine()->getManager()->getRepository('SwarminfoUserBundle:Devices');
            $userplan=$repository_plan->findOneBy(array('id' => $usr->getId(),'lang'=>$_locale));
//            print_r($_GET['_locale']);
//            die();
            if ($userplan) {
                print_r($userplan);
                $session = $this->container->get('session');
                $t = $this->get('security.context')->getToken();
                $t->setAttribute("sesid", $session->getId());
                $session = $this->get('session');
                $userdevices = $repository_devices->findBy(array('id' => $usr->getId(), 'lang' => $_locale));
                $em = $this->getDoctrine()->getManager();
                print_r('da1');
                if ($userplan and $userplan->getCountdevice() != 0) {
//                    die('test2');
//                  delete died session
                    $openses = scandir(session_save_path());
                    $arr_sess = array();
                    foreach ($openses as $name => $value) {
                        array_push($arr_sess, str_replace('sess_', '', $value));
                    }

                    $cur_sess_count = 0;

                    foreach ($userdevices as $ses) {
                        //                print_r($ses->getDevice());
                        if (!in_array($ses->getDevice(), $arr_sess)) {
                            //                    print_r($ses->getDevice());
                            $em->remove($ses);
                            $em->flush();
                            $cur_sess_count += 1;
                            //                    die();
                        }
                    }

                    if (count($userdevices) - $cur_sess_count >= $userplan->getCountdevice()) {
//                        die('test3');
                        $usersession = $repository_devices->findOneBy(array('device' => $session->getId(), 'lang' => $_locale));
//                        print_r($usersession);

                        if (!$usersession) {
//                            print_r($usersession);
                            $message = "The account is full of users now of the language. please try again later ";
                            return $this->render('SwarminfoImagescriptBundle::simplemessage.html.twig', array('message' => $message));
//                            die("The account is full of users now. please try again later ");
//                            $this->get('security.context')->setToken(null);
//                            $message="The account is full of users now. please try again later";
//                            return $this->redirect( $this->generateUrl('fos_user_security_login', array( "message"=>$message)) );
                        }

                    } else {
                        $sesdev = $repository_devices->findOneBy(array('device' => $session->getId()));
//                        print_r($session->getId());
                        if ($sesdev) {
                            //update current device
                            $sesdev->setLang($_locale);
                            $em->persist($sesdev);
                            $em->flush();
                        }

                    }
                }
//                die('test4');
//                print_r('da');
                $request->getSession()->set('_locale', $_locale);
            }
            else
            {
//                die('test');
                $message="Please sign out and subscribe the selected language to sign in again.";
//                    die('test');
                return $this->render('SwarminfoImagescriptBundle::not_subscripting.html.twig', array('message' => $message));
            }



        }

        $request->getSession()->set('_locale', $_locale);

        return $this->redirect($referer);
    }


    //action to make a user premium after the purchase through paypal
    public function paypalmakepremiumAction(){
        $securityContext = $this->container->get('security.authorization_checker');
        //check the user is authenticated before purchasing
        if ( ! $securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $message = "the purchase of the premium option has been canceled because no user is authenticated";
            return $this->render('SwarminfoImagescriptBundle::simplemessage.html.twig', array('message' => $message));
        }
        //get the current user
        $em = $this->getDoctrine()->getEntityManager();
        $usr= $this->get('security.context')->getToken()->getUser();

        //update the expiration time of the premium access
        $currentTime = time();
        $expirationTime = $usr->getExpire();
        $additionalTime = $this->container->getParameter('paypal.premiumtime')*86400;
        $newExpirationTime =  $expirationTime < $currentTime
                        ? $currentTime + $additionalTime
                        : $expirationTime + $additionalTime;
        
        $paypal = $this->container->get('swarminfo_paypal.paypal');

       /* if ($_GET['currency']){
            $paypal->setCurrencyCode($_GET['currency']);
//            $paypal->setCurrencyCode("USD");
//            print_r($paypal->getCurrencyCode());
//            die();
        }*/


        $token = $_GET["token"];
        $PayerId = $_GET["PayerID"];

//        $amount = $this->container->getParameter('paypal.amount');
//        $amount = $this->curamount;
        $amount = $_GET['amount'];

        //pendingdate
        $pendate=new \DateTime("now");

        $result = $paypal->checkPayment( $amount , $token , $PayerId);
//        print_r($result);
//        die();
        //in case of success, upgrade to premium
        if($result === true){
            $usr->setExpire($newExpirationTime);
            $usr->setStatus('');
            $em->persist($usr);
            $em->flush();

            //send email success registraction
            $this->sendemailregistration($usr);

            //          insert in orderlist
            $repository = $this->getDoctrine()->getManager()->getRepository('SwarminfoUserBundle:Orderlist');
            $all=$repository->findAll();
            $copleteddate=new \DateTime("now");
            $neworder=new Orderlist(count($all)+1,$usr->getId(),$amount,$pendate,$copleteddate,$this->Devices_plan[$_GET['plan']],$_GET['curlang'] );
            $em = $this->getDoctrine()->getManager();
            $em->persist($neworder);
            $em->flush();

            //plandevice
            $repository_plan = $this->getDoctrine()->getManager()->getRepository('SwarminfoUserBundle:Plandevice');
            $userplan=$repository_plan->findOneBy(array('id' => $usr->getId(),'lang'=>$_GET['curlang']));
            if ($userplan)
            {
                $userplan->setCountdevice($this->Devices_plan[$_GET['plan']]);
            }
            else
                $userplan=new Plandevice($usr->getId(),$this->Devices_plan[$_GET['plan']],$_GET['curlang']);
            $em->persist($userplan);
            $em->flush();

            $_SESSION['USER_FAIL']=false;
            
            $message = "your order was successfull : you  have a premium account until ".date("F j o", $usr->getExpire() );
            return $this->render('SwarminfoImagescriptBundle::simplemessage.html.twig', array('message' => $message));
        }
        else{
            var_dump($result);
            $message = "the purchase of the premium option has been canceled because of an error during the transaction";
            return $this->render('SwarminfoImagescriptBundle::simplemessage.html.twig', array('message' => $message));
        }
        return $this->render('SwarminfoPaypalBundle:Default:index.html.twig', array('name' => (string)$result));
    }
    
    public function page1Action()
    {
      
        return $this->render('SwarminfoImagescriptBundle::page1.html.twig', array());
    }
    
    public function page2Action()
    {
        if ($_SESSION['USER_END']!='')
        {
            $sesid=$_SESSION['USER_END'];
//            print_r($sesid);
//            die();
            $repository_devices = $this->getDoctrine()->getManager()->getRepository('SwarminfoUserBundle:Devices');
            $sessforremove=$repository_devices->findOneBy(array('device' => $sesid));
            if ($sessforremove)
            {
//                print_r($sessforremove);
                $em = $this->getDoctrine()->getManager();
                $em->remove($sessforremove);
                $em->flush();

            }
            $_SESSION['USER_END']='';

        }
        $search = null;
        if (isset($_POST['search']) and  !empty($_POST['search']))
        {
            $search = $_POST['search'];
        }
        elseif (isset($_GET['search']) and  !empty($_GET['search']))
            $search = $_GET['search'];

//        $search = str_replace($search, ' ', '_');

        $displayedProjects = $this->getAllProjectsData($search);
        //var_dump($displayedProjects);
        //die();
        $usr = $this->get('security.context')->getToken()->getUser();

        $usr = $this->cleare_remveduser($usr);
        /*if (!is_null($usr)  and $usr != 'anon.' and   $usr->getstatus() == 'removed')
        {
            $this->get('session')->clear();
            $repository = $this->getDoctrine()->getManager()->getRepository('SwarminfoUserBundle:Fieldsregister');
            $fieldsreg=$repository->findOneBy(array('id' => $usr->getId()));

            $em = $this->getDoctrine()->getManager();

            if ($fieldsreg){
                $em->remove($fieldsreg);
            }

            $em->remove($usr);
            $em->flush();

            $usr = 'anon.';
        }*/ 



        return $this->render('SwarminfoImagescriptBundle::page2.html.twig', array("projects"=>$displayedProjects,
            'user'=>$usr, 'search'=>$search));
    }

    private function cleare_remveduser($usr){
        if (!is_null($usr)  and $usr != 'anon.' and   $usr->getstatus() == 'removed')
        {
            $this->get('session')->clear();
            $repository = $this->getDoctrine()->getManager()->getRepository('SwarminfoUserBundle:Fieldsregister');
            $fieldsreg=$repository->findOneBy(array('id' => $usr->getId()));

            $em = $this->getDoctrine()->getManager();

            if ($fieldsreg){
                $em->remove($fieldsreg);
            }

            $em->remove($usr);
            $em->flush();

            return 'anon.';
        }
        return $usr;
    }

    //get language of curren usrl
    private function getlanguageUrl(){
        preg_match('/\/[a-z][a-z]\//',$_SERVER['REQUEST_URI'], $lanshort);
        return substr($lanshort[0], 1, -1);
    }

    private function getamounttolang($locale='en')
    {
        $res=array();
        $locale = ($locale == null ? 'en' : $locale);

        for ($i=1;$i<4;$i+=1)
        {
            $tmp=$this->container->getParameter('paypal_'.$locale.'_'.(string)$i);
            array_push($res,$tmp);
        }
        return $res;
    }

    public function page3Action()
    {
        if ($this->container->getParameter('is_use_paypall') == 0 )
        {
            return $this->redirect( $this->generateUrl('swarminfo_imagescript_page5'));
//            if ($this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY'))
//                return $this->redirect( $this->generateUrl('swarminfo_imagescript_page5'));
//            else
//                die('page3');
        }


        $usr = $this->get('security.context')->getToken()->getUser();
        $usr = $this->cleare_remveduser($usr);

//        for additionla language
        $ispremium = false;
        if ($usr != 'anon.'){
            $ispremium = $this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY') && isset($usr) && $usr->getExpire() > time();
        }

        if ($ispremium)
        {
            $curlang=$this->getlanguageUrl();
            $lang=$this->languageName[$curlang];
//            $codecurrency=$this->currencycode[$curlang];
            $codecurrency="USD";
            $amounts=$this->getamounttolang($curlang);
        }
        else{
            $curlang=$lang=$codecurrency=$amounts=null;
        }
//        print_r($amounts);
//        die();


        return $this->render('SwarminfoImagescriptBundle::page3.html.twig', array('user'=>$usr,
            "language"=>$lang, "currency"=>$codecurrency, "amounts"=>$amounts,"curlang"=>$curlang,"ispremium"=>$ispremium
            ));
    }
    
    public function page4Action()
    {
        $id=isset($_GET["id"]) ? $_GET["id"] : null;
        $search=isset($_GET["search"]) ? $_GET["search"] : null;
        $usr = $this->get('security.context')->getToken()->getUser();

        //check subcribe to differ language
//        print_r($this->getlanguageUrl());
//        die();

//        $ispremium_lang = $this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY') && isset($usr) && $usr->getExpire() > time() ;
//        $curlang=$this->getlanguageUrl();
//        if ($ispremium_lang){
//            if ($curlang=='en') $ispremiumlanguage=true;
//            else{
//                $curlang=$this->getlanguageUrl();
//                $replang = $this->getDoctrine()->getManager()->getRepository('SwarminfoUserBundle:Subscribelanguage');
//                $usr= $this->get('security.context')->getToken()->getUser();
//                $langfound=$replang->findOneBy(array('id' => $usr->getId(), "language"=>$curlang));
//                if ($langfound)
//                    $ispremiumlanguage=true;
//                else
//                    $ispremiumlanguage=false;
//            }
//        }
//        else
//            $ispremiumlanguage=false;

//        print_r($ispremiumlanguage);
//        die();

        $ispremium = $this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY') && isset($usr) && $usr->getExpire() > time();
        $isadmin = $this->isAdmin();
        $project = $this->getProject($id);

        $isshow=$ispremium || $isadmin || $project->isFree();
//        $isshow=true; //test
//        if(! $project) return "{}";
//        else return $ispremium || $isadmin || $project->isFree() ? $project->getData() : $project->getLimitedData();

        return $this->render('SwarminfoImagescriptBundle::page4.html.twig', array("id"=>$id,'user'=>$usr, 'isshow'=>$isshow, "search"=>$search));
    }
    
    
    public function indexAction($name)
    {
        
        return $this->render('SwarminfoImagescriptBundle:Default:index.html.twig', array('name' => $name));
    }
    
    public function iframeAction($id)
    {
        if($this->isAdmin()) return $this->render('SwarminfoImagescriptBundle::adminIframe.html.twig', array("id"=>$id ));
        else return $this->render('SwarminfoImagescriptBundle::iframe.html.twig', array("id"=>$id ));
    }

    public function projects_groupAction($id)
    {
        $displayedProjects = $this->getAllProjectsData(null, $id);
        $usr = $this->get('security.context')->getToken()->getUser();

        return $this->render('SwarminfoImagescriptBundle::project_group.html.twig', array("projects"=>$displayedProjects,
            'user'=>$usr, 'search'=>null));
    }

    public function frontpageiframeAction()
    {
        return $this->render('SwarminfoImagescriptBundle::front_page_iframe.html.twig');
    }
    
    //default controller to view the the main page
    public function mainAction(){
        $id=isset($_GET["id"]) ? $_GET["id"] : null;
        if(!isset($id)) return $this->redirect( $this->generateUrl('swarminfo_imagescript_edit', array('id' => rand(10000000000,99999999999) )) );
        return $this->render('SwarminfoImagescriptBundle::index.html.twig', array('id' => $id));
    }
    
    //default controller to edit a project
    public function editAction(){
        $id=isset($_GET["id"]) ? $_GET["id"] : null;
        if(!isset($id)) return $this->redirect( $this->generateUrl('swarminfo_imagescript_edit', array('id' => rand(10000000000,99999999999) )) );
        return $this->render('SwarminfoImagescriptBundle::edit.html.twig', array('id' => $id));
    }
    
    //function to load a project json
    public function loadAction(){
        try{
            $id=$_POST["id"];
            $data = $this->getData($id);
            
        }
        catch(Exception $e){
            $data = "{}";
        }

        header('Content-Type: application/json');
        header("Cache-control: public");
        echo $data;
        exit();
    }
    
    //function to check if the currentuser is an admin
    public function isAdmin(){
        $adminusers = $this->container->getParameter('adminusers');
        $usr = $this->get('security.context')->getToken()->getUser();
        $security = $this->get('security.context');
        $isauthenticated = $security->isGranted('IS_AUTHENTICATED_FULLY') || $security->isGranted('IS_AUTHENTICATED_REMEMBERED');
        $isadmin = $isauthenticated && isset($usr) && in_array($usr->getUsername(), $adminusers) ;
        return $isadmin;
    }
    
    //function to delete a project
    public function deleteAction(){
        //if the user is not an admin, just exit :
        $isadmin = $this->isAdmin();
        if(!$isadmin) exit();
        
        //retrieve the entity managaer
        $em = $this->getDoctrine()->getManager(); 
        $id=$_POST["id"];
        $project = $this->getProject($id);
        //create the project if it doesn't exist yet
        if (!$project) {
            $project = new Project();
            $project->setId($id);
        }
        
        $em->remove($project);
        $em->flush();
        
        //delete the directory of the project
        $projectDir = $this->getProjectsDirectory($id);
        $this->rrmdir($projectDir);
        
        //insert a log line
        $text = date("Y-m-d H:i:s")." ".$this->getUsername()." ".$id." project deleted \n";
        file_put_contents($this->generalLogPath(),$text);
            
        //don't return anything since it is an ajax request
        exit();
        
    }
    
    public function rrmdir($dir) {
        if (is_dir($dir)) {
          $objects = scandir($dir);
          foreach ($objects as $object) {
            if ($object != "." && $object != "..") {
              if (filetype($dir."/".$object) == "dir") $this->rrmdir($dir."/".$object); else unlink($dir."/".$object);
            }
          }
          reset($objects);
          rmdir($dir);
        }
      }
      
    
    //function to update a project
    public function saveAction(){
        //if the user is not an admin, just exit :
        $isadmin = $this->isAdmin();
        if(!$isadmin) throw $this->createNotFoundException('you are not a logged admin.');
        //exit();
        
        //retrieve the entity managaer
        $em = $this->getDoctrine()->getManager(); 
        $id=$_POST["id"];
        $data = $_POST["data"];
        $project = $this->getProject($id);
        //create the project if it doesn't exist yet
        if (!$project) {
            $project = new Project();
            $project->setId($id);
        }
        //save the json data
        $project->setData($data);
        //persist the project
        $em->persist($project);
        $em->flush($project);
        
        //create a repository for the image if it doesn't exists yet
        $projectDir = $this->getProjectsDirectory($id);
        //create a directory if it is a new project
        if(!file_exists($projectDir)){
            mkdir($projectDir,0755);
        }
        
        $i = 0;
        //if an image has been given, save it
        while(isset($_POST["imagename$i"]) && isset($_FILES["image$i"]) ){
            move_uploaded_file( $_FILES["image$i"]["tmp_name"], $projectDir . $_POST["imagename$i"] );
            $i++;
        }
        
        //if a log has been ordered, add it
        if(isset($_POST["log"])){
            $this->addLog($id,$_POST["log"]);
        }

        //if a deletion has been ordered, do it
        if( isset($_POST["delete"])  ){
            $basename = basename($_POST["delete"]);
            unlink("$projectDir$basename");
            
        }
        
        //don't return anything since it is an ajax request
        exit();
    }
    
    //function to add a log entry
    public function addLog($projectId,$logline){
        $logdir = $this->getLogDir($projectId);
        $logpath = $this->getLogFile($projectId);
        //create a directory if it is a new project
        if(!file_exists($logdir)){
            mkdir($logdir,0755);
            file_put_contents($logpath,"");
        }
        
        //move the files if they are too big
        $this->checkMaxLogSize($projectId);
        //create the line to add and push it
        
        $text = date("Y-m-d H:i:s")." ".$this->getUsername()." ".$projectId." ".$logline."\n";
        var_dump(date("Y-m-d H:i:s"));
        $previouslogs = file_get_contents($logpath);
        file_put_contents($logpath, $text.$previouslogs);
    }
    
    //get the username of the current user
    public function getUsername(){
        try{
            $usr = $this->get('security.context')->getToken()->getUser();
            return $usr->getUsername();
        }
        catch(Exception $e){return "unknownuser";}
    }
    
    //check the current log file isn't too big
    public function checkMaxLogSize($projectid){
        $maxsize = 1000000;
        $logfile = $this->getLogFile($projectid);
        $logdir = $this->getLogDir($projectid);
        $size = filesize($logfile);
        if($size>$maxsize){
            $files = glob($logdir."/*" );
            $number = count($files);
            //assuming there are the log files : log, log, log2 ang log3
            //move log3 t log4, log2 to log3 log1 to log2 log to log1 and recreate log
            for($i=$number-1;$i>=1;$i--){
                if(file_exists($logfile.$i)) rename($logfile.$i, $logfile.($i+1));
            }
            rename($logfile,$logfile."1");
            file_put_contents($logfile,"");
        }
    }
    
    //get the path of the file storing all the generals logs
    public function generalLogPath(){
        return $this->getProjectsDirectory()."generalLogs";
    }

    //get the path of the log directory
    public function getLogDir($projectid){
        return $this->getProjectsDirectory($projectid)."logs/";
    }
    
    //get the path of the log file
    public function getLogFile($projectid){
        return $this->getLogDir($projectid)."log";
    }
    
    //function to prepare an array with data about all the projects
    public function getAllProjectsData($search=null, $tag_id=null, $isonlylegends=false){
        if ($isonlylegends){
            $alllegends = array();
//            return 'isonlylegends';
        }

        $projects = $this->getAllProjects();
        $displayedProjects = array();

        foreach($projects as $project){
            if ($project->isPublish() or $this->isAdmin())
            {
                $isShow = True;
                $data = json_decode($project->getData(),true);
                if (!is_null($search)){
                    $legends=$data["legends"];
                    $is_find =false;
//                    echo '<br>';
//                    print_r("search=$search");
                    foreach ($legends as $l) {
                        $text = $l['text']['english'];
//                        echo '<br>';
//                        print_r($text);
                        if (strpos(strtoupper($text), strtoupper($search)) !== false) {
//                            echo '<br>';
//                            print_r($data['id']);
//                            echo '<br>';
//                            print_r($text);
                            $is_find = true;
                            break;
                        }
                    }
                    if (!$is_find) $isShow = False;
                }



                $name = isset($data["general"]["name"]) ? $data["general"]["name"] : "";
                $tag = isset($data["general"]["tag"]) ? $data["general"]["tag"] : "";
                $find_tag = str_replace(' ', '', $tag);
                if (!is_null($tag_id) and strpos(strtoupper($find_tag), strtoupper($tag_id)) === false ){
//
//                    print_r('continue '.$tag);
//                    echo '<br>';
//                    print_r('continue pos '.strpos(strtoupper($tag), strtoupper($tag_id)));
//                    echo '<br>';
                    continue;
                }

                if ($isonlylegends){
                    $legends = $name = isset($data["legends"]) ? $data["legends"]: "";
                    foreach ($legends as $l)
                    {
                        $alllegends[$l['text']['english']] = $l['text']['english'];
//                        print_r('legends '.$l['text']['english']);
//                        echo '<br>';
                    }
                    continue;
                }

//                print_r('tag ID '.$tag_id);
//                echo '<br>';


                //isFree : if true, can accesslegends without premium account
                $isFree = $project->isFree();
                if( empty($tag) ) $tag="_";
                if(!isset($displayedProjects[$tag])) $displayedProjects[$tag] = array();
                $logo = isset($data["general"]["logo"]) ? $data["general"]["logo"] : "";
                //$url = $logo != "" && isset($data["images"][$logo]["url"]) ? $data["images"][$logo]["url"] : "";
                $url = $logo != "" ? "/bundles/swarminfoimagescript/ressources/" . $project->getId() ."/".$logo : "";
                
                $isonfront = $project->isOnfront();
                $weight = $project->getweight();

                $displayedProjects[$tag][] = array("name"=>$name, "url"=>$url , "id"=>$project->getId(), "images"=>count($data["images"]),
                    "free"=>$isFree, 'onfront'=>$isonfront, "isshow"=>$isShow, "weight"=>$weight);


            }

            //var_dump($displayedProjects);
        }

        if ($isonlylegends) return $alllegends;

//        sort tags of display projects
        $keys = array_keys($displayedProjects);
//        print_r($keys);

        foreach ($keys as $k){
            $displayedProjects[$k] = $this->sortin_tag($displayedProjects, $k);
        }


        return $displayedProjects;
    }


    private function sortin_tag($dispproj, $tag, $val='weight'){
        $a = $dispproj[$tag];
        foreach ($a as $key => $row) {
            $volume[$key]  = $row[$val];
        }
        array_multisort($volume,  SORT_ASC, $a);
        return $a;

// Сортируем данные по volume по убыванию и по edition по возрастанию
        array_multisort($volume,  SORT_ASC, $a);
    }

    //action to display the amin pannel :
    public function adminpannelAction(){
        if(! $this->isAdmin() ){
            //$message = "you should be an admin to access this page";
            //return $this->render('SwarminfoImagescriptBundle::simplemessage.html.twig', array('message' => $message));
            //if not admin, return an empty view
            return new Response("");
        }
        $displayedProjects = $this->getAllProjectsData();

        return $this->render('SwarminfoImagescriptBundle::adminpannel.html.twig', array('projects' => $displayedProjects));
        
    }
    
    //function to get all the projects
    private function getAllProjects(){
        $repository = $this
        ->getDoctrine()
        ->getManager()
        ->getRepository('SwarminfoImagescriptBundle:Project');
        
//        return $repository->findBy(array('id'=>93922769878));
        return $repository->findAll();
    }
    
    //function to get a project
    private function getProject($id){
        $repository = $this
        ->getDoctrine()
        ->getManager()
        ->getRepository('SwarminfoImagescriptBundle:Project');
        
        return $repository->find($id);
    }
    
    //function to get the data of a project
    private function getData($id){
        $usr= $this->get('security.context')->getToken()->getUser();
        //update the expiration time of the premium access
        $currentTime = time();
        //var_dump($usr);
        $ispremium = $this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY') && isset($usr) && $usr->getExpire() > $currentTime;
        $isadmin = $this->isAdmin();
        $project = $this->getProject($id);

        if(! $project) return "{}";
        else return $ispremium || $isadmin || $project->isFree() ? $project->getData() : $project->getLimitedData();
//        else return ($ispremium && $ispremiumlanguage) || $isadmin || $project->isFree() ? $project->getData() : $project->getLimitedData();
    }
    
    //get the directory for project ressources
    public function getProjectsDirectory( $id = null ){
        $dir =  __DIR__.'/../../../../web/bundles/swarminfoimagescript/ressources/';
        if(isset($id)) $dir .= "$id/";
        return $dir;
    }

//    //add in order list
//    function addorder($userid,$amount)
//    {
//        $neworder=new Orderlist($userid,$amount);
//        $em = $this->getDoctrine()->getManager();
//        $em->persist($neworder);
//        $em->flush();
//    }

    //order history
    public function orderhistoryAction()
    {
//        print_r(time()+30*86400);
//        die();

        $usr= $this->get('security.context')->getToken()->getUser();

        $repository = $this->getDoctrine()->getManager()->getRepository('SwarminfoUserBundle:Orderlist');
        $orlists = $repository->findBy(
            array('userid' => $usr->getId())
        );



        usort($orlists, array($this, "cmpdate"));

        return $this->render('SwarminfoImagescriptBundle::orderhistory.html.twig', array("data"=>$orlists));
    }

    //for sort order history by date
    function cmpdate($a, $b)
    {
        if ($a->getPaydate() == $b->getPaydate() ) {
            return 0;
        }
        return ($a->getPaydate() > $b->getPaydate()) ? -1 : 1;
    }

    //detect device
    public function getdevise()
    {
        $agent=$_SERVER['HTTP_USER_AGENT'];
        $pos_start = strpos($agent, '(');
        $pos_end = strpos($agent, ')');
        $result=substr($agent,$pos_start+1,$pos_end -$pos_start-1);
        return $result;
    }

    public function orderviewAction()
    {
        $repository = $this->getDoctrine()->getManager()->getRepository('SwarminfoUserBundle:Orderlist');
        $order = $repository->findOneBy(
            array('id' => $_GET["id"])
        );

        return $this->render('SwarminfoImagescriptBundle::orderview.html.twig',array("order"=>$order));
    }

    public function orderprintAction()
    {
        $repository = $this->getDoctrine()->getManager()->getRepository('SwarminfoUserBundle:Orderlist');
        $order = $repository->findOneBy(
            array('id' => $_GET["id"])
        );

        $userManager = $this->get('fos_user.user_manager');

        $usr=$userManager->findUserBy(array("id"=>$order->getUserid()));

        $html = $this->renderView('SwarminfoImagescriptBundle::orderprint.html.twig',array("order"=>$order, "user"=>$usr));
        return new Response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
            200,
            array(
                'Content-Type'          => 'application/pdf',
                'Content-Disposition'   => 'attachment; filename="file.pdf"'
            )
        );
//
//        return $this->render('SwarminfoImagescriptBundle::orderprint.html.twig',array("order"=>$order, "user"=>$usr));
    }


    //user member list
    public function usermemberlistAction()
    {
        if (!$this->isAdmin()){
            return $this->page2Action();
        }
        $userManager = $this->getDoctrine()->getManager()->getRepository('SwarminfoUserBundle:User');
//        $fusr=$userManager->findBy(array('status'=>null));

        if ($_POST['_searchvalue'] && !empty($_POST['_searchvalue']))
        {
            $tag=$_POST['_filtertag'];
            $val='%'.$_POST['_searchvalue'].'%';
            $em = $this->getDoctrine();
            $connection = $em->getConnection();
            //filter
            if ($_POST['_filtertag']=="username" or $_POST['_filtertag']=="email")
            {
                $table='user';
            }
            else
            {
                $table='fieldsregister';
            }
            $statement = $connection->prepare("SELECT DISTINCT `id` FROM `$table` WHERE `$tag` like  '$val'");
            $statement->execute();
            $results = $statement->fetchAll();
            $fusr=[];
            foreach ($results as $re)
            {
                $usrone=$userManager->findOneBy(array("id"=>$re));
                array_push($fusr, $usrone);
//                    print_r($re);
            }
//            print_r($_POST['_filtertag']);
//            die();
        }
        else{
            $fusr=$userManager->findAll();
        }


        $repository = $this->getDoctrine()->getManager()->getRepository('SwarminfoUserBundle:Fieldsregister');
        $freg=$repository->findAll();

//        sort status
        $status_sort=$_GET['sortpremium'];
        if ($status_sort)
            usort($fusr, array($this, "cmpStatusMember"));
        else
            usort($fusr, array($this, "cmpid"));


        $professions=$this->getextarafield("prof");
        $organizationtype=$this->getextarafield("ortype");
        $countries=$this->getextarafield("country");
        $regions=$this->getextarafield("regions");

        //users plans
        $repository_plan = $this->getDoctrine()->getManager()->getRepository('SwarminfoUserBundle:Plandevice');
        $plans=$repository_plan->findAll();

        $message=null;
        if ($_GET["message"] && !empty($_GET["message"])) $message=$_GET["message"];

        $filterlist=array("username",'email',"firstname","lastname","phone");

        return $this->render('SwarminfoImagescriptBundle::usermemberlist.html.twig',
            array("users"=>$fusr, "message"=>$message,"freg"=>$freg,"professions"=>$professions,"organizationtype"=>$organizationtype,
                "searchvalue"=>$_POST['_searchvalue'],"filtertag"=>$_POST['_filtertag'],
                "countries"=>$countries, "regions"=>$regions, "plans"=>$plans,'status_sort'=>$status_sort,"filterlist"=>$filterlist));
    }
    
    private function getextarafield($key)
    {
        if ($key=="prof")
            return
            $professions=array("Radiologist","Radiology trainee","Radiographer",
                "Nuclear Medicine Physician","Radiation oncologist",
                "Orthopaedics surgeon","Clinical oncologist","Rheumatologist","General practitioner","Junior doctor",
                "Other medical specialist","Chiropractor","Nursing","Physiotherapist","Other Therapist/technologist",
                "Other healthcare professional","Student","Professor/lecturer/other educator",
                "Managerial/administrative staff","Others");
        if ($key=="ortype")
            return $organizationty=array("Hospital","Imaging center","Group practice","Healthcare network"
                            ,"College/University","Vocational-technical institute","Healthcare consulting",
                                "Group purchasing organization","Imaging vendor","Rehab facility","Pharmaceutical","Other");
        if ($key=="country")
            return $countries=array("AF"=>"Afghanistan","AL"=>"Albania","DZ"=>"Algeria","AS"=>"American Samoa","AD"=>"Andorra","AO"=>"Angola","AI"=>"Anguilla","AQ"=>"Antarctica","AG"=>"Antigua and Barbuda","AR"=>"Argentina",
                "AM"=>"Armenia","AW"=>"Aruba","AU"=>"Australia","AT"=>"Austria","AZ"=>"Azerbaijan","BS"=>"Bahamas","BH"=>"Bahrain","BD"=>"Bangladesh","BB"=>"Barbados","BY"=>"Belarus","BE"=>"Belgium","BZ"=>"Belize",
                "BJ"=>"Benin","BM"=>"Bermuda","BT"=>"Bhutan","BO"=>"Bolivia","BA"=>"Bosnia and Herzegovina","BW"=>"Botswana","BV"=>"Bouvet Island","BR"=>"Brazil","IO"=>"British Indian Ocean Territory",
                "BN"=>"Brunei Darussalam","BG"=>"Bulgaria","BF"=>"Burkina Faso","BI"=>"Burundi","KH"=>"Cambodia","CM"=>"Cameroon","CA"=>"Canada","CV"=>"Cape Verde","KY"=>"Cayman Islands","CF"=>"Central African Republic",
                "TD"=>"Chad","CL"=>"Chile","CN"=>"China","CX"=>"Christmas Island","CC"=>"Cocos (Keeling) Islands","CO"=>"Colombia","KM"=>"Comoros","CG"=>"Congo","CD"=>"Congo, The Democratic Republic Of The",
                "CK"=>"Cook Islands","CR"=>"Costa Rica","HR"=>"Croatia","CU"=>"Cuba","CY"=>"Cyprus","CZ"=>"Czech Republic","CI"=>"Côte d'Ivoire","DK"=>"Denmark","DJ"=>"Djibouti","DM"=>"Dominica","DO"=>"Dominican Republic",
                "EC"=>"Ecuador","EG"=>"Egypt","SV"=>"El Salvador","GQ"=>"Equatorial Guinea","ER"=>"Eritrea","EE"=>"Estonia","ET"=>"Ethiopia","FK"=>"Falkland Islands (Malvinas)","FO"=>"Faroe Islands","FJ"=>"Fiji",
                "FI"=>"Finland","FR"=>"France","GF"=>"French Guiana","PF"=>"French Polynesia","TF"=>"French Southern Territories","GA"=>"Gabon","GM"=>"Gambia","GE"=>"Georgia","DE"=>"Germany","GH"=>"Ghana","GI"=>"Gibraltar",
                "GR"=>"Greece","GL"=>"Greenland","GD"=>"Grenada","GP"=>"Guadeloupe","GU"=>"Guam","GT"=>"Guatemala","GG"=>"Guernsey","GN"=>"Guinea","GW"=>"Guinea-Bissau","GY"=>"Guyana","HT"=>"Haiti",
                "HM"=>"Heard Island and McDonald Islands","VA"=>"Holy See (Vatican City State)","HN"=>"Honduras","HK"=>"Hong Kong","HU"=>"Hungary","IS"=>"Iceland","IN"=>"India","ID"=>"Indonesia",
                "IR"=>"Iran, Islamic Republic of","IQ"=>"Iraq","IE"=>"Ireland","IM"=>"Isle of Man","IL"=>"Israel","IT"=>"Italy","JM"=>"Jamaica","JP"=>"Japan","JE"=>"Jersey","JO"=>"Jordan","KZ"=>"Kazakhstan","KE"=>"Kenya",
                "KI"=>"Kiribati","KP"=>"Korea, Democratic People's Republic of","KR"=>"Korea, Republic of","KW"=>"Kuwait","KG"=>"Kyrgyzstan","LA"=>"Lao People's Democratic Republic","LV"=>"Latvia","LB"=>"Lebanon",
                "LS"=>"Lesotho","LR"=>"Liberia","LY"=>"Libyan Arab Jamahiriya","LI"=>"Liechtenstein","LT"=>"Lithuania","LU"=>"Luxembourg","MO"=>"Macau","MK"=>"Macedonia, The Former Yugoslav Republic of","MG"=>"Madagascar",
                "MW"=>"Malawi","MY"=>"Malaysia","MV"=>"Maldives","ML"=>"Mali","MT"=>"Malta","MH"=>"Marshall Islands","MQ"=>"Martinique","MR"=>"Mauritania","MU"=>"Mauritius","YT"=>"Mayotte","MX"=>"Mexico","FM"=>"Micronesia, 
            Federated States of","MD"=>"Moldova, Republic of","MC"=>"Monaco","MN"=>"Mongolia","ME"=>"Montenegro","MS"=>"Montserrat","MA"=>"Morocco","MZ"=>"Mozambique","MM"=>"Myanmar","NA"=>"Namibia","NR"=>"Nauru",
                "NP"=>"Nepal","NL"=>"Netherlands","AN"=>"Netherlands Antilles","NC"=>"New Caledonia","NZ"=>"New Zealand","NI"=>"Nicaragua","NE"=>"Niger","NG"=>"Nigeria","NU"=>"Niue","NF"=>"Norfolk Island",
                "MP"=>"Northern Mariana Islands","NO"=>"Norway","OM"=>"Oman","PK"=>"Pakistan","PW"=>"Palau","PS"=>"Palestinian Territory, Occupied","PA"=>"Panama","PG"=>"Papua New Guinea","PY"=>"Paraguay","PE"=>"Peru",
                "PH"=>"Philippines","PN"=>"Pitcairn","PL"=>"Poland","PT"=>"Portugal","PR"=>"Puerto Rico","QA"=>"Qatar","RE"=>"Reunion","RO"=>"Romania","RU"=>"Russian Federation","RW"=>"Rwanda","BL"=>"Saint Barthélemy",
                "SH"=>"Saint Helena","KN"=>"Saint Kitts and Nevis","LC"=>"Saint Lucia","MF"=>"Saint Martin","PM"=>"Saint Pierre and Miquelon","VC"=>"Saint Vincent and The Grenadines","WS"=>"Samoa","SM"=>"San Marino",
                "ST"=>"Sao Tome and Principe","SA"=>"Saudi Arabia","SN"=>"Senegal","RS"=>"Serbia","SC"=>"Seychelles","SL"=>"Sierra Leone","SG"=>"Singapore","SK"=>"Slovakia","SI"=>"Slovenia","SB"=>"Solomon Islands",
                "SO"=>"Somalia","ZA"=>"South Africa","GS"=>"South Georgia and The South Sandwich Islands","ES"=>"Spain","LK"=>"Sri Lanka","SD"=>"Sudan","SR"=>"Suriname","SJ"=>"Svalbard and Jan Mayen","SZ"=>"Swaziland",
                "SE"=>"Sweden","CH"=>"Switzerland","SY"=>"Syrian Arab Republic","TW"=>"Taiwan","TJ"=>"Tajikistan","TZ"=>"Tanzania, United Republic of","TH"=>"Thailand","TL"=>"Timor-Leste","TG"=>"Togo","TK"=>"Tokelau",
                "TO"=>"Tonga","TT"=>"Trinidad and Tobago","TN"=>"Tunisia","TR"=>"Turkey","TM"=>"Turkmenistan","TC"=>"Turks and Caicos Islands","TV"=>"Tuvalu","UG"=>"Uganda","UA"=>"Ukraine","AE"=>"United Arab Emirates",
                "GB"=>"United Kingdom","UM"=>"United States Minor Outlying Islands","US"=>"United States of America","UY"=>"Uruguay","UZ"=>"Uzbekistan","VU"=>"Vanuatu","VE"=>"Venezuela","VN"=>"Viet Nam",
                "VG"=>"Virgin Islands, British","VI"=>"Virgin Islands, U.S.","WF"=>"Wallis and Futuna","EH"=>"Western Sahara","YE"=>"Yemen","ZM"=>"Zambia","ZW"=>"Zimbabwe","AX"=>"Åland");
        if ($key=="regions")
            return $regions=array("AL"=>"Alabama","AK"=>"Alaska","AZ"=>"Arizona","AR"=>"Arkansas","CA"=>"California","CO"=>"Colorado","CT"=>"Connecticut","DE"=>"Delaware",
                "DC"=>"District Of Columbia","FL"=>"Florida","GA"=>"Georgia","HI"=>"Hawaii","ID"=>"Idaho","IL"=>"Illinois","IN"=>"Indiana","IA"=>"Iowa",
                "KS"=>"Kansas","KY"=>"Kentucky","LA"=>"Louisiana","ME"=>"Maine","MD"=>"Maryland","MA"=>"Massachusetts","MI"=>"Michigan","MN"=>"Minnesota",
                "MS"=>"Mississippi","MO"=>"Missouri","MT"=>"Montana","NE"=>"Nebraska","NV"=>"Nevada","NH"=>"New Hampshire","NJ"=>"New Jersey","NM"=>"New Mexico",
                "NY"=>"New York","NC"=>"North Carolina","ND"=>"North Dakota","OH"=>"Ohio","OK"=>"Oklahoma","OR"=>"Oregon","PA"=>"Pennsylvania","RI"=>"Rhode Island",
                "SC"=>"South Carolina","SD"=>"South Dakota","TN"=>"Tennessee","TX"=>"Texas","UT"=>"Utah","VT"=>"Vermont","VA"=>"Virginia","WA"=>"Washington",
                "WV"=>"West Virginia","WI"=>"Wisconsin","WY"=>"Wyoming");
    }

    public function updateuser_from_adminAction()
    {
        if (!$this->isAdmin()){
            return $this->page2Action();
        }

        $userManager = $this->get('fos_user.user_manager');

        $usr=$userManager->findUserBy(array("id"=>$_POST["_id"]));

//        print_r($_POST["_id"]);
//        die();

        $repository = $this->getDoctrine()->getManager()->getRepository('SwarminfoUserBundle:Fieldsregister');
        $freg=$repository->find($usr->getId());
        if ($usr)
        {
            $isupdate=false;
            //pass
            if (isset($_POST["_userpass"]) && !empty($_POST["_userpass"]))
            {
//                print_r($_POST["_userpass"]);
//                print_r($usr->getPassword());
//                die('ravno pass');
                //update pass
                $usr->setPlainPassword($_POST["_userpass"]);
                $isupdate=true;
                //----
            }
            //username
            if (isset($_POST["_username"]) && $_POST["_username"]!=$usr->getUsername())
            {
                $usr->setUsername($_POST["_username"]);
                $isupdate=true;
            }

            //email
            if (isset($_POST["_email"]) && $_POST["_email"]!=$usr->getExpire())
            {
                $usr->setEmail($_POST["_email"]);
                $isupdate=true;
            }

            //eexperied date
            if (isset($_POST["_expired"]) && strtotime($_POST['_expired'])!=$usr->getUsername())
            {
                $usr->setExpire(strtotime($_POST['_expired']));
                $isupdate=true;
            }

            //member
//            if (isset($_POST["_status"]) && ($_POST["_status"]=="Member" && time()<=$usr->getExpire()  ||
//                    $_POST["_status"]!="Member" && time()>$usr->getExpire() ))
//            {
//                $currentTime = time();
//                $expirationTime = $usr->getExpire();
//                $additionalTime = $this->container->getParameter('paypal.premiumtime')*86400;
//                $newExpirationTime =  $expirationTime < $currentTime
//                    ? $currentTime + $additionalTime
//                    : $expirationTime + $additionalTime;
//                $usr->setExpire($newExpirationTime);
//                $isupdate=true;
//            }

            //fieldsregister
            $repository = $this->getDoctrine()->getManager()->getRepository('SwarminfoUserBundle:Fieldsregister');
            $freg=$repository->find($usr->getId());

//            print_r($isupdate);
//            $userManager->updateUser($usr);
            if ($isupdate) {
//                print_r($usr);
//                die();
              $userManager->updateUser($usr);
              $message='User '.$usr->getUsername().' was edited';
            }
            $repository = $this->getDoctrine()->getManager()->getRepository('SwarminfoUserBundle:Fieldsregister');
            $freg=$repository->find($usr->getId());
            if ($freg)
            {
                $freg->setTitle($_POST["_title"]);
                $freg->setFirstname($_POST["_firstname"]);
                $freg->setLastname($_POST["_lastname"]);
                $freg->setProfession($_POST["_profession"]);
                $isphis=$_POST["_isphysician"]=="Yes" ? true:false;
                $freg->setIsphisican($isphis);
                $freg->setOrganization($_POST["_organization"]);
                $freg->setOrganizationType($_POST["_organizationtype"]);
                $freg->setCountry($_POST["_country"]);
                $freg->setAddressLine1($_POST["_addresline1"]);
                $freg->setAddressLine2($_POST["_addresline2"]);
                $freg->setCity($_POST["_city"]);
                $freg->setPostcode($_POST["_postcode"]);
                $freg->setRegion($_POST["_region"]);
                $freg->setPhone($_POST["_phone"]);
                $com_pref1=$_POST["_receive1"]=="on" ? true:false;
                $freg->setComPref1($com_pref1);
                $com_pref2=$_POST["_receive2"]=="on" ? true:false;
                $freg->setComPref2($com_pref2);

                //create date
//                if (isset($_POST["_createdate"]) && !empty($_POST["_createdate"]))
//                {
//                    $freg->setCreatedate(new \DateTime($_POST["_createdate"]));
////                    $freg->setCreatedate(new \DateTime("2016-06-10"));
//                }
//
                $em = $this->getDoctrine()->getManager();
                $em->persist($freg);
                $em->flush();


                $message='User '.$usr->getUsername().' was edited';
            }

            else #create new fieldregistr
            {
                $isphys=$_POST["_isphysician"]=="Yes" ? true:false;
                $res1=$_POST["_receive1"]=="on" ? true:false;
                $res2=$_POST["_receive2"]=="on" ? true:false;

                $fieldreg=new Fieldsregister($usr->getId(),$_POST["_title"],$_POST["_firstname"],$_POST["_lastname"],
                    $_POST["_profession"], $isphys,$_POST["_organization"],$_POST["_organizationtype"],
                    $_POST["_country"],$_POST["_addresline1"],$_POST["_addresline2"],$_POST["_city"],
                    $_POST["_postcode"],$_POST["_region"],$_POST["_phone"],$res1,$res2, null
                );
                $em = $this->getDoctrine()->getManager();
                $em->persist($fieldreg);
                // actually executes the queries (i.e. the INSERT query)
                $em->flush();

                $message='User '.$usr->getUsername().' was edited';
            }

        }

        $title=array("","Dr.","Prof.","Mr.","Mrs.","Ms.");
        $professions=$this->getextarafield("prof");
        $organizationtype=$this->getextarafield("ortype");
        $countries=$this->getextarafield("country");
        $regions=$this->getextarafield("regions");

        return $this->render('SwarminfoImagescriptBundle::admin_edit_user.html.twig',
            array("user"=>$usr,"message"=>$message,"fieldsreg"=>$freg,"title"=>$title,"professions"=>$professions,"organizationtype"=>$organizationtype,
                "countries"=>$countries, "regions"=>$regions));

    }

    //update user from user member list
    public function updateuserAction()
    {
        if (!$this->isAdmin()){
            return $this->page2Action();
        }

        $userManager = $this->get('fos_user.user_manager');
        $usr=$userManager->findUserBy(array("id"=>$_POST["_id"]));

//        print_r($usr->getStatus());

        $repository = $this->getDoctrine()->getManager()->getRepository('SwarminfoUserBundle:Fieldsregister');
        $fieldsreg=$repository->findOneBy(array('id' => $usr->getId()));

        $title=array("","Dr.","Prof.","Mr.","Mrs.","Ms.");
        $professions=$this->getextarafield("prof");
        $organizationtype=$this->getextarafield("ortype");
        $countries=$this->getextarafield("country");
        $regions=$this->getextarafield("regions");

//        print_r($fieldsreg->getFirstname());
//        die();

        $message=null;

        return $this->render('SwarminfoImagescriptBundle::admin_edit_user.html.twig', 
            array("user"=>$usr,"message"=>$message, "fieldsreg"=>$fieldsreg,"title"=>$title,"professions"=>$professions,"organizationtype"=>$organizationtype,
                "countries"=>$countries, "regions"=>$regions));

    }

    public function delete_user_from_adminAction()
    {
        if (!$this->isAdmin()){
            return $this->page2Action();
        }

        //delete user
        $userManager = $this->get('fos_user.user_manager');
        $deluser=$userManager->findUserBy(array("id"=>$_GET["id"]));

//        $deluser->setStatus('removed');
        if (is_null($deluser) )
            return $this->redirect( $this->generateUrl('swarminfo_imagescript_usermemberlist', array("message"=>null )) );

        $repository = $this->getDoctrine()->getManager()->getRepository('SwarminfoUserBundle:Fieldsregister');
        $fieldsreg=$repository->findOneBy(array('id' => $deluser->getId()));

        $em = $this->getDoctrine()->getManager();

        if ($fieldsreg){
            $em->remove($fieldsreg);
        }

        $em->remove($deluser);
        $em->flush(); 

//        $userManager = $this->get('fos_user.user_manager');
        $userManager = $this->getDoctrine()->getManager()->getRepository('SwarminfoUserBundle:User');
//        $fusr=$userManager->findUsers();

        $fusr=$userManager->findAll();

        $repository = $this->getDoctrine()->getManager()->getRepository('SwarminfoUserBundle:Fieldsregister');
        $freg=$repository->findAll();


        usort($fusr, array($this, "cmpid"));

//        sort status
        $status_sort=$_GET['sortpremium'];
        if ($status_sort)
            usort($fusr, array($this, "cmpStatusMember"));

        $professions=$this->getextarafield("prof");
        $organizationtype=$this->getextarafield("ortype");
        $countries=$this->getextarafield("country");
        $regions=$this->getextarafield("regions");

        //users plans
        $repository_plan = $this->getDoctrine()->getManager()->getRepository('SwarminfoUserBundle:Plandevice');
        $plans=$repository_plan->findAll();

        $message="User ".$deluser." was deleted";
//
        $filterlist=array("username",'email',"firstname","lastname","phone");

        return $this->render('SwarminfoImagescriptBundle::usermemberlist.html.twig',
            array("users"=>$fusr, "message"=>$message,"freg"=>$freg,"professions"=>$professions,"organizationtype"=>$organizationtype,
                "searchvalue"=>null,"filtertag"=>null,
                "countries"=>$countries, "regions"=>$regions, "plans"=>$plans,'status_sort'=>$status_sort, "filterlist"=>$filterlist));
    }

//    create_user_from_admin
    public function create_user_from_adminAction(){
        $message=null;
        $professions=$this->getextarafield("prof");
        $organizationtype=$this->getextarafield("ortype");
        $countries=$this->getextarafield("country");
        $regions=$this->getextarafield("regions");
        $title=array("","Dr.","Prof.","Mr.","Mrs.","Ms.");
        $isvalid=False;

        if ($_POST["_username"] && !empty($_POST["_username"]))
        {
            $userManager = $this->get('fos_user.user_manager');
            $usr_name=$userManager->findUserBy(array("username"=>$_POST["_username"]));
            $usr_email=$userManager->findUserBy(array("email"=>$_POST["_email"]));
            if ($usr_name) $message="This username is already exist";
            elseif ($usr_email) $message="This email is already exist";
            else $isvalid=True;
        }

//        print_r($_POST["_username"]);
//        die();
        if ($isvalid)
        {
            //create user
            $user = $userManager->createUser();
            $user->setEnabled(true);
            $user->setUsername($_POST["_username"]);
            $user->setPlainPassword($_POST["_userpass"]);
            $user->setEmail($_POST["_email"]);

//            print_r($user);
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            // actually executes the queries (i.e. the INSERT query)
            $em->flush();

            //create fieldreg for user
            $isphys=$_POST["_isphysician"]=="Yes" ? true:false;
            $res1=$_POST["_receive1"]=="on" ? true:false;
            $res2=$_POST["_receive2"]=="on" ? true:false;

            $fieldreg=new Fieldsregister($user->getId(),$_POST["_title"],$_POST["_firstname"],$_POST["_lastname"],
                $_POST["_profession"], $isphys,$_POST["_organization"],$_POST["_organizationtype"],
                $_POST["_country"],$_POST["_addresline1"],$_POST["_addresline2"],$_POST["_city"],
                $_POST["_postcode"],$_POST["_region"],$_POST["_phone"],$res1,$res2
            );
            //$em = $this->getDoctrine()->getManager();
            $em->persist($fieldreg);
            // actually executes the queries (i.e. the INSERT query)
            $em->flush();

            $message="User ".$user." was created";
            
            return $this->redirect( $this->generateUrl('swarminfo_imagescript_usermemberlist', array("message"=>$message )) );
        }
        else{
            return $this->render('SwarminfoImagescriptBundle::admin_create_user.html.twig',
                array("title"=>$title,"professions"=>$professions,"organizationtype"=>$organizationtype,
                    "countries"=>$countries, "regions"=>$regions,"message"=>$message));
        }

    }
    

    //for sort
    function cmp($a, $b)
    {
        if (strtoupper($a->getUsername() )==strtoupper($b->getUsername()) ) {
            return 0;
        }
        return (strtoupper($a->getUsername() ) < strtoupper($b->getUsername())) ? -1 : 1;
    }

    function cmpid($a, $b)
    {
        if (strtoupper($a->getId() )==strtoupper($b->getId()) ) {
            return 0;
        }
        return (strtoupper($a->getId() ) > strtoupper($b->getId())) ? -1 : 1;
    }

    function cmpStatusMember($a, $b)
    {
        $expire_a = ( !empty($a->getExpire()) && $a->getExpire() > time() ) ? $a->getExpire() : time();
//        $expire_a = ( !empty($a->getExpire())  ) ? 1 : 0;

        $expire_b = ( !empty($b->getExpire()) && $b->getExpire() > time() ) ? $b->getExpire() : time();
//        $expire_b = ( !empty($b->getExpire()) ) ? 1 : 0;


//        print_r($a->getUsername().' ');
//        print_r($expire_a.'; ');

//        if ($expire_a ==$expire_a ) {
//            return 0;
//        }
        return ($expire_a  > $expire_b) ? -1 : 1;
    }

    public function translate_legendsAction()
    {
        if ($_GET['back']) return $this->render('SwarminfoImagescriptBundle::adminIframe.html.twig', array("id"=>$_GET['id'] ));

        $data_json = $this->getData($_GET['id']);
        $data = json_decode($data_json,true);
        $legends=$data["legends"];
        $languages=$data["languages"];

        $langfortranslate=$_GET['langtranslate'];

        $message=null;

        if (isset($_POST['desk_lang']) && strlen($_POST['desk_lang'])==2){
//            Translation description
//            print_r('translatearr='+$_POST['Text1_name']);
            $langfortranslate=$_POST['langtr_desc'];
            $transl=$_POST['desk_lang'];
            $translatearr = preg_split('/\n|\r\n?/', $_POST['TextTranslate_desc']);
//            print_r('lang='+$_POST['desk_lang']);

//            print_r($translatearr);
//            die();

//            echo '<br/>';
            $i=0;

            foreach($legends as $key => $value){
//                if (is_array($value['description']))
//                {
//                    print_r($value['description'][$transl].' after '.$translatearr[$i]);
//                }
//                else{
//                    print_r($value['description'].' after '.$translatearr[$i]);
//                }
                if (!is_array($value['description'])) {
                    $tmp=$legends[$key]['description'];
                    $legends[$key]['description']=array();
                    $legends[$key]['description']['en']=$tmp;
                }

//                print_r($translatearr[$i]);
//                echo '<br/>';

                $legends[$key]['description'][$transl]=$translatearr[$i];
//                print_r('legend_key_desc='+$legends[$key]['description'][$transl]);
//                echo '<br/>';
                $i+=1;
            }
            $data["legends"]=$legends;
//            print_r('legend_key_desc='+$legends[$key]['description'][$transl]);
//            print_r('legend_key_desc=');
//            die();
            $em = $this->getDoctrine()->getManager();
            $project = $this->getProject($_GET['id']);
//            print_r($project);
//            die();
            //save the json data
            $project->setData(json_encode($data));
////            persist the project
            $em->persist($project);
            $em->flush($project);
//
            $message="Translation description to ".$langfortranslate. " was successful";
        }
        elseif (isset($_POST['_languagefortraslate'])){
            //Translation
            $transl=$_POST['_languagefortraslate'];

            $translatearr = preg_split('/\n|\r\n?/', $_POST['TextTranslate']);
//            print_r($translatearr);
//            die('test');
            $i=0;

            foreach($legends as $key => $value){
                $legends[$key]['text'][$transl]=$translatearr[$i];
//                print_r($value['text'][$transl].' after '.$translatearr[$i]);
//                echo '<br/>';

                $i+=1;
            }

              $data["legends"]=$legends;
//              print_r( json_encode($data));


            $em = $this->getDoctrine()->getManager();
            $project = $this->getProject($_GET['id']);
//            print_r($project);

            //save the json data
            $project->setData(json_encode($data));
////            persist the project
            $em->persist($project);
            $em->flush($project);

            $message="Translation to ".$transl. " was successful";
            $langfortranslate=$transl;
        }


        if (!isset($_GET['langtranslate']) && count($languages)>1 && !isset($_POST['_languagefortraslate']))
        {
            foreach ( $languages as $l){
                if (strtoupper($l)!='ENGLISH'){
                    $langfortranslate=$l;
                    break;
                }

            }
        }

//        foreach($legends as $key => $value){
//            print_r($value['description']);
//            echo '<br/>';
//
//        }

//        die();

        return $this->render('SwarminfoImagescriptBundle::translatelegends.html.twig',array("id"=>$_GET['id'],"languages"=>$languages,"legends"=>$legends,
            "langfortranslate"=>$langfortranslate,"message"=>$message));
    }

    public function lock_user_from_adminAction(){
        $userManager = $this->get('fos_user.user_manager');
        $user=$userManager->findUserBy(array("id"=>$_POST["id"]));
        if ($user)
        {
            $user->setLocked(!$user->isLocked());
            $user->setPassLeftAttempt($this->container->getParameter('pass_countpossible'));
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
        }
        
        if (!$user->isLocked())
        {
//            send email to user
            $dear = $this->container->getParameter('message_unlock_head');
            $body = $this->container->getParameter('message_unlock_body');
            $subject = $this->container->getParameter('message_unlock_subject');

            $text = $dear.' <strong>'.$user->getUsername().'</strong>,<br>'.$body;

            $emailFrom=$this->container->getParameter('mailer_user');
            $message = \Swift_Message::newInstance()
                ->setSubject($subject)
                ->setFrom($emailFrom)
                ->setTo($user->getEmail())
//                ->setTo('kydim1990@rambler.ru')
                ->setBody($text, 'text/html')
            ;

            $this->get('mailer')->send($message);

            $text = "un";
        }

        $message_json = $user." has been ".$text."locked";
        
        $response = new Response(json_encode(array('result' => $message_json)));
        return $response;
//        exit();
    }

    public function testAction(){
        $hidefirst_labels = $this->container->getParameter('hidefirst_labels');
        header('Content-Type: application/json');
        header("Cache-control: public");
        echo json_encode($hidefirst_labels);
        exit();

        die();
        try{
            $id=13639174336;
            $data = $this->getData($id);

        }
        catch(Exception $e){
            $data = "{}";
        }

        header('Content-Type: application/json');
        header("Cache-control: public");
        echo $data;
        exit();

        die();
        $userManager = $this->get('fos_user.user_manager');

        $usr=$userManager->findUserBy(array("id"=>207));
        $emailFrom=$this->container->getParameter('mailer_user');
        $emailFromName=$this->container->getParameter('mailer_name');

        $twig = clone $this->get('twig');
        $template = $twig->loadTemplate('SwarminfoImagescriptBundle::email.txt.twig');
        $subject = $template->renderBlock('subject',  array('user' => $usr));
        $body_text = $template->renderBlock('body_text', array('user' => $usr));

        $message = \Swift_Message::newInstance()
            ->setSubject( $subject)
            ->setFrom(array($emailFrom => $emailFromName))
            ->setTo('kydim1312@yandex.ru')
            ->setBody( $body_text )
        ;
        $this->get('mailer')->send($message);
        $response = new Response(json_encode(array('result' => 'ok')));
        return $response;

        
        $userManager = $this->getDoctrine()->getManager()->getRepository('SwarminfoUserBundle:User');
        $removedusers=$userManager->findBy(array("status"=>'removed'));
        if (count($removedusers) > 0){
            $repository = $this->getDoctrine()->getManager()->getRepository('SwarminfoUserBundle:Fieldsregister');
            $em = $this->getDoctrine()->getManager();
            foreach ( $removedusers as $ruser){
                print_r($ruser->getid());

                $fieldsreg=$repository->findOneBy(array('id' => $ruser->getId()));
                if ($fieldsreg){
                    $em->remove($fieldsreg);
                }
                $em->remove($ruser);

            }
            $em->flush();
        }



        die();

        $promo = $this->getDoctrine()->getManager()->getRepository('SwarminfoUserBundle:Promo');
        $promo_code = $promo->findOneBy(array('promocode'=> 'NAC0001'));
//        print_r($promo_code);
        $is_used_promo = false;
        if (!is_null($promo_code) and $promo_code->getExperied()>time() and ($promo_code->getIsunlimited() or !$promo_code->getIsused())
            and (!is_null($promo_code->getStartfrom()) and $promo_code->getStartfrom() > time() or is_null($promo_code->getStartfrom()) )
        )
        {
//            print_r('test55');
            $is_used_promo = true;
        }
        if ($this->container->getParameter('is_use_paypall') == 1 and !$is_used_promo)
        {
            print_r('paypall');
        }
        print_r('promo');
        die();



        $datetime1 = new \DateTime('2017-03-05 06:03:00');
        $datetime2 = new \DateTime('2017-03-08 06:05:05');
        $interval = $datetime1->diff($datetime2);
        $minutes = $interval->days * 24 * 60;
        $minutes += $interval->h * 60;
        $minutes += $interval->i;

        $promo = $this->getDoctrine()->getManager()->getRepository('SwarminfoUserBundle:Promo');
        $promo_code = $promo->findOneBy(array('promocode'=> 'TSQ000001'));
        $is_used_promo = false;
        if (!is_null($promo_code) and $promo_code->getExperied()>time() and
            ($promo_code->getIsunlimited() or !$promo_code->getIsused())){
            $is_used_promo = true;
        }
        
        print_r($is_used_promo);
        die();

        $emailFrom=$this->container->getParameter('mailer_user');
        $emailFromName=$this->container->getParameter('mailer_name');
        $subscription_expered=$this->container->getParameter('subscription_expered');
        $subscription_subject=$this->container->getParameter('subscription_subject');
        $diff=4;
        $message = \Swift_Message::newInstance()
            ->setSubject($subscription_subject)
//            ->setFrom($emailFrom)
            ->setFrom(array($emailFrom => $emailFromName))
//            ->setTo($user->getEmail())
                ->setTo('kydim1990@rambler.ru')
            ->setBody($subscription_expered[0].' '.$diff.' '.$subscription_expered[1], 'text/html')
        ;
        $this->get('mailer')->send($message);
        $response = new Response(json_encode(array('result' => 'ok')));
        return $response;
    }

    public function loadlegendsAction(){
        $all_legs = array_keys($this->getAllProjectsData(null, null, true));
        echo json_encode($all_legs);
//        foreach ($all_legs as $l)
//        {
//            print_r($l);
//            echo '<br>';
//        }
//        print_r($all_legs);
        exit();
    }

    public function getlocaleAction(){
        $locale = $this->get('request')->getLocale();
        echo $locale;
        exit();
    }

    private function sendemailregistration($usr){
//        $userManager = $this->get('fos_user.user_manager');
//        $usr=$userManager->findUserBy(array("id"=>207));

        $emailFrom=$this->container->getParameter('mailer_user');
        $emailFromName=$this->container->getParameter('mailer_name');

        $twig = clone $this->get('twig');
        $template = $twig->loadTemplate('SwarminfoImagescriptBundle::email.txt.twig');
        $subject = $template->renderBlock('subject',  array('user' => $usr));
        $body_text = $template->renderBlock('body_text', array('user' => $usr));

        $message = \Swift_Message::newInstance()
            ->setSubject( $subject)
            ->setFrom(array($emailFrom => $emailFromName))
//            ->setTo('kydim1312@yandex.ru')
            ->setTo($usr->getEmail())
            ->setBody( $body_text )
        ;
        $this->get('mailer')->send($message);
    }

    public function gethidefirst_labelsAction()
    {
        $hidefirst_labels = $this->container->getParameter('hidefirst_labels');
        header('Content-Type: application/json');
        header("Cache-control: public");
        echo json_encode($hidefirst_labels);
        exit(); 
    }
}
