<?php
/**
 * Created by PhpStorm.
 * User: Diman
 * Date: 06.04.2017
 * Time: 1:22
 */

namespace Swarminfo\ImagescriptBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class IframeController extends Controller
{
    public function getcbparamsAction(){
        $contrast_min = $this->container->getParameter('contrast_min');
        $contrast_max = $this->container->getParameter('contrast_max');
        $contrast_mouse_sensetve = $this->container->getParameter('contrast_mouse_sensetve');

        $brightness_min = $this->container->getParameter('brightness_min');
        $brightness_max = $this->container->getParameter('brightness_max');
        $brightness_mouse_sensetve= $this->container->getParameter('brightness_mouse_sensetve');
        
        $cb_params = array('contrast_min' => $contrast_min, 'contrast_max' => $contrast_max, 'contrast_mouse_sensetve' => $contrast_mouse_sensetve,
            'brightness_min' => $brightness_min, 'brightness_max' => $brightness_max, 'brightness_mouse_sensetve' => $brightness_mouse_sensetve);
        echo json_encode($cb_params);
        exit();
    }
}