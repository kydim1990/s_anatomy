<?php
/**
 * Created by PhpStorm.
 * User: Diman
 * Date: 06.12.2016
 * Time: 20:14
 */

namespace Swarminfo\ImagescriptBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class OmcsaController extends Controller
{
    public function tipsAction(){
        return $this->render('SwarminfoImagescriptBundle::tips.html.twig');
    }		
	
    public function policyAction(){
        return $this->render('SwarminfoImagescriptBundle::policy.html.twig');
    }		
	
    public function disclaimerAction(){
        return $this->render('SwarminfoImagescriptBundle::disclaimer.html.twig');
    }
}