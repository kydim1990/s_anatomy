<?php
/**
 * Created by PhpStorm.
 * User: Diman
 * Date: 19.11.2016
 * Time: 15:08
 */

namespace Swarminfo\ImagescriptBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Swarminfo\UserBundle\Entity\Promo;
use Swarminfo\UserBundle\Entity\Plandevice;


class PromoController extends Controller
{
    private $Devices_plan=array('A'=>1,'B'=>3,'C'=>0);

    public function promoAction(){
        if (!$this->isAdmin())
        {
            return $this->redirect($this->generateUrl('swarminfo_imagescript_page1'));
        }


        $message = null;
        if ($_SERVER['REQUEST_METHOD'] == "POST"){
            $em = $this->getDoctrine()->getManager();
            $count_digits = $this->container->getParameter('promo_count_digits');
            $cd_str = '%0'.$count_digits.'d';
            for ($i = 1; $i <= intval($_POST['_number_of_code']); $i++) {
//                echo sprintf( '%06d', $i );
                $unlimited=false;
                if ($_POST["_unlimited"]=="on") $unlimited=true;
                $newpromo = new Promo($_POST['_prefix'], sprintf( $cd_str, $i ), strtotime($_POST['_experied_date']), $unlimited, strtotime($_POST['_from_date']));
                $em->persist($newpromo);
//                echo "<br>";
            }
            $em->flush();

            $message = date('M d, Y', time()).' - generated '.$_POST['_number_of_code'].
                ' promo code of "'.$_POST['_prefix'].'", expired on '.date('M d, Y', strtotime($_POST['_experied_date']));

        }

        $em = $this->getDoctrine();
        $connection = $em->getConnection();
        $statement = $connection->prepare("SELECT DISTINCT created, prefix, code, startfrom, experied, isunlimited, count(id) count_promo 
                                          FROM promo 
                                          GROUP BY prefix ORDER by created desc");
        $statement->execute();
        $all_promo = $statement->fetchAll();

        return $this->render('SwarminfoImagescriptBundle::promo.html.twig', array('all_promo'=>$all_promo, 'message'=>$message));

    }


    public function isAdmin(){
        $adminusers = $this->container->getParameter('adminusers');
        $usr = $this->get('security.context')->getToken()->getUser();
        $security = $this->get('security.context');
        $isauthenticated = $security->isGranted('IS_AUTHENTICATED_FULLY') || $security->isGranted('IS_AUTHENTICATED_REMEMBERED');
        $isadmin = $isauthenticated && isset($usr) && in_array($usr->getUsername(), $adminusers) ;
        return $isadmin;
    }

    public function usepromocodeAction(){
        $usr= $this->get('security.context')->getToken()->getUser();
        if ($usr=='anon.')
            return $this->redirect( $this->generateUrl('fos_user_security_login', array( )) );
        $message=null;
        $msgerr=null;
        if (isset($_POST["_promocode"]) && !empty($_POST["_promocode"]))
        {
            $repository = $this->getDoctrine()->getManager()->getRepository('SwarminfoUserBundle:Fieldsregister');
            $freg=$repository->find($usr->getId());
            if ($freg)
            {
                $promo = $this->getDoctrine()->getManager()->getRepository('SwarminfoUserBundle:Promo');
                $promo_code = $promo->findOneBy(array('promocode'=> $_POST["_promocode"]));
                if (!is_null($promo_code)){
                    $msgerr = true;
                    if ( $promo_code->getExperied()<time()){
                        $message='Your promo code is no longer active';
                    }
                    elseif (!is_null($promo_code->getStartfrom()) and $promo_code->getStartfrom() < time()){
                        $message = $this->container->getParameter('can_active_before');
                    }
                    elseif ($promo_code->getIsused() == '1' and $promo_code->getIsunlimited() == 0){
                        $message='Your promo code is no valid';
                    }
                    else{
                        $usr->setExpire($promo_code->getExperied());
                        $userManager = $this->get('fos_user.user_manager');
                        $userManager->updateUser($usr);

                        $promo_code->setIsused(1);
                        $em = $this->getDoctrine()->getManager();
                        $em->persist($promo_code);
                        //$em->flush();

                        //plandevice
                        $request = $this->get('request');
                        $locale = $request->getLocale();
                        $repository_plan = $this->getDoctrine()->getManager()->getRepository('SwarminfoUserBundle:Plandevice');
                        $userplan=$repository_plan->findOneBy(array('id' => $usr->getId(),'lang'=>$locale));
                        if ($userplan)
                        {
                            $userplan->setCountdevice($this->Devices_plan[$_GET['plan']]);
                        }
                        else
                            $userplan=new Plandevice($usr->getId(), 1, $locale);
                        $em->persist($userplan);
                        //$em->flush();

                        $repository = $this->getDoctrine()->getManager()->getRepository('SwarminfoUserBundle:Fieldsregister');
                        $freg=$repository->find($usr->getId());
                        $freg->setPromocode($_POST["_promocode"]);
                        $em->persist($freg);
                        $em->flush();
                        
                        $message='Promo code has been activated';
                        $msgerr = false;
                    }
                }
                else{
                    $message='Promo code is not found';
                }
            }

        }
        return $this->render('SwarminfoImagescriptBundle::usepromocode.html.twig', array("message"=>$message, "msgerr"=>$msgerr));
    }

    public function checkpromoAction(){
        $prom=$_POST["_promocode"];
        $promo = $this->getDoctrine()->getManager()->getRepository('SwarminfoUserBundle:Promo');
//        $promo_code = $promo->findOneBy(array('promocode'=> $_POST["_promocode"]));
        $promo_code = $promo->findOneBy(array('promocode'=> $prom));
        $message=null;
        if (!is_null($promo_code)){
            if ($promo_code->getExperied()<time()){
                $message='Your promo code is no longer active';
            }
            elseif (!is_null($promo_code->getStartfrom()) and $promo_code->getStartfrom() < time()){
                $message = $this->container->getParameter('can_active_before');
            }
            elseif ($promo_code->getIsused() == '1' and $promo_code->getIsunlimited() == 0){
                $message='Your promo code is no valid';
            }
            else {
                $message='OK';
            }
        }
        else
            $message='error';
        echo json_encode($message);
//        echo json_encode('ok');
        exit();
    }
}