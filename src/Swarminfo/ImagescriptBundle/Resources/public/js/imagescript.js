//the whole library is contained in the isl object (image script library)
var isl = {};

//main functions are store in the f object
isl.f = {};

//global parameters are stored in t : most of those parameters should be defined before executing anything
isl.p = {};

//this is the object containing data of the group of images displayed :
//no modification should be brought to it unless it needs to be persisted
isl.current = {}
//this variable should contain the id of the image currently displayed as the main image
isl.currentImage = null;
//the current series beeing watch : if null, set to all
isl.p.currentSeries = null;

//a simple array listing  available colors :
isl.colors = [ "white" , "DarkOrange", "green", "red", "RoyalBlue", "SandyBrown", "Gray", "Gold"];

//the id of the global container
isl.p.globalContainer = "imagescriptcontainer";
//the id of the div containing the mainImageContainer
isl.p.mainImageContainer = "mainimage";
//the id of the canvas inside the container
isl.p.mainImageCanvas = "maincanvas";
//the id of the top admin pannel div, if it exist (where the vategory of actions are displayed)
isl.p.topAdminPannel = "topadminpannel";
//the id of the top user pannel (where various actions are available for the user)
isl.p.topUserPannel = "topuserpannel";
//the id of the admin pannel where actions are actually triggered
isl.p.mainAdminPannel = "mainadminpannel";

//zoom parameters
isl.p.zoomStartPercentWidth = 0;
isl.p.zoomEndPercentWidth = 1;
isl.p.zoomStartPercentHeight = 0;
isl.p.zoomEndPercentHeight = 1;

//parameters to know if the main image should be printed in reverse mode
isl.p.horizontalSymetry = false;
isl.p.verticalSymetry = false;

//the mode to use to display legend : print, hide, square, or quiz
isl.p.legendMode = "print";
isl.p.legendSquareSize = 100;
isl.p.legendSquareCenterPercentWidth = 0.5;
isl.p.legendSquareCenterPercentHeight = 0.5;
isl.p.squareColor = "blue";
isl.p.squareLineWidth = 2;
isl.p.squareUpdateCounter = 0; //update the square image only from time to time to prevent lag
isl.p.squareUpdateCounterReset = 10;

//in case it is quiz mode, remember the list of legends that have already been displayed
isl.p.displayedQuizLegends = [];
isl.p.quizImageId = null; //the id of the image thequi is going on
//position of the quiz legends
isl.p.legendsHeightOnCanvas = {};
isl.p.legendsSideOnCanvas = {};



//set the current language
isl.p.currentLanguage = null;

//remember if we are in full screen or not
isl.p.screenmode = "normal"; //can be normal or full

//parameters of the main image container
isl.p.mainImageContainerWidth = 650;

//fix the width of the canvas main image
isl.p.setImageWidth = 500;

//various dimensions inside the canvas
isl.p.mainImageWidth = 0;
isl.p.mainImageHeight = 0;
isl.p.mainImageCanvasWidth = 0;
isl.p.mainImageCanvasHeight = 0;
isl.p.legendLeftWidth = 180;
isl.p.legendRightWidth = 180;


//possible legend size,one value chosen among this array for isl.p.fontSize
isl.p.possibleFontSize = [15 , 13 , 11 , 9, 7];
//legend parameters
isl.p.fontSize = 14;
isl.p.font = "Arial";
isl.p.verticalLinePadding = 10;
isl.p.verticalLinePaddingToWord = 6;
isl.p.wordPaddingTop = 2;
isl.p.lineWidth = 1;
isl.p.targetRadius = 2;
isl.p.legendPaddingTop = 8;
//remember if  the mouse i s over a legend
isl.p.mouseOverLegend = null;

// the height at wich to start printing the legends : those can be reseted when the image change
isl.p.legendLeftStartHeight = isl.p.fontSize;
isl.p.legendRightStartHeight = isl.p.fontSize;

//strcutre paramaters
isl.p.drawingOpacity = 0.5; //the opacity used to draw anatomical strcutres on canvas

//functions to be called on start
isl.p.onStart = [];
//functions that are called during various hook
isl.p.onResetAll = ["isl.f.selectDefautSeries", "isl.f.resetPVars", "isl.f.displayMainImage", "isl.f.displayTopMenu"];

//an object keeping in memory the anatomical structures displayed
isl.p.displayedStructures = [];

//function to initialize the project
isl.f.start = function(){
    isl.p.callFunctionsFromArray(isl.p.onStart);
}

//reset all :
isl.f.resetAll = function(){

    var id = isl.f.generateId(); 
    isl.p.resetid = id;
    if( ! isl.f.resetAllPossible) return;
    if (id != isl.p.resetid) return;
    isl.f.resetAllPossible = false;
    try{
        isl.p.callFunctionsFromArray(isl.p.onResetAll);
    }
    catch(e){}
    isl.f.resetAllPossible = true;
    //scroll to the top
    //window.scrollTo(0, 0);
}

//a small variable to know if a resetall is ongoing
isl.f.resetAllPossible = true;

//reset the top user menu
isl.f.displayTopMenu = function(){
    $("#"+isl.p.topUserPannel).html("");
    
    //switch to catalog mode
    isl.f.resetCatalogModeButton();
    
    //symetry buttons
    isl.f.symetryButtons();
    
    //download button
    isl.f.downloadButton();
    
    //print button
    isl.f.printButton();
    
    //zoom buttons
    isl.f.resetZoomButtons();
    
    //change the legend mode
    isl.f.legendModeButton()
    
    //display the choice of structures
    isl.f.resetStructureSelection();
    
    //display the choice of language
    isl.f.resetLanguageSelectionButton();
    
    //reset the selection of the series to display
    isl.f.resetSeriesSelection();

    //a form to search legends and access related image
    isl.f.resetLegendSearch();
    
    //a button to display in full screen
    isl.f.fullScreenButton();
    
}

isl.f.resetSeriesSelection = function(){
    var html = "";
    
    //get the list of languages available
    var series = isl.current.series;
    
    //print some data to choose the language to modify
    html += "<select id=serieschooseselectoruser>";
    //html += "<option value='' "+( null == isl.p.currentSeries ? " selected" : " " )+">all series</option>";
    
    //push all elements to sort in an array
    var sortarr = [];
    for(sid in series){ sortarr.push([series[sid].text, sid ]) }
    sortarr.sort( function(a,b){return a[0].localeCompare(b[0]);} );
    
    for(var i in sortarr){
        var sid = sortarr[i][1];
        html += "<option value='"+sid+"' "+( sid == isl.p.currentSeries ? " selected" : " " )+">"+series[sid].text+"</option>";
    }
    
    //for(var sid in series){
    //    html += "<option value='"+sid+"' "+( sid == isl.p.currentSeries ? " selected" : " " )+">"+series[sid].text+"</option>";
    //}
    html += "</select>";
    $("#"+isl.p.topUserPannel).append(html);
    
    //when selecting an language, it is displayed a s the main language
    $("#serieschooseselectoruser").change( function(e){
        var series = $(this).val();
        isl.p.currentSeries = series == '' ? null : series;
        //reload the bottom band images
        isl.f.loadBottomBandImages();
        
        //retrieve the first available image belonging to the series
        var images = isl.p.getSortedImage();
        isl.currentImage = images.length > 0 ? images[0].id : null;
        
        isl.f.resetAll();
    });
}


isl.f.resetLanguageSelectionButton = function(){
    var html = "";
    
    //get the list of languages available
    var languages = isl.current.languages;
    
    //print some data to choose the language to modify
    html += "<select id=languagechooseselector>";
    
    //push all languages to sort in an array
    var sortarr = [];
    for(l in languages){ sortarr.push([l, languages[i] ]) }
    sortarr.sort( function(a,b){return a[0].localeCompare(b[0]);} );
    
    for(var l in sortarr){
        var i = sortarr[l][0];
        html += "<option value='"+languages[i]+"' "+( languages[i] == isl.p.currentLanguage ? " selected" : " " )+">"+i+"</option>";
    }
    //for(var i in languages){
    //    html += "<option value='"+languages[i]+"' "+( languages[i] == isl.p.currentLanguage ? " selected" : " " )+">"+i+"</option>";
    //}
    html += "</select>";
    $("#"+isl.p.topUserPannel).append(html);
    
    //when selecting an language, it is displayed a s the main language
    $("#languagechooseselector").change( function(e){
        var language = $(this).val();
        isl.p.currentLanguage = language;
        isl.f.resetAll();
    });
    
    
}


//display a button to enter fullscreen mode
isl.f.fullScreenButton = function(){
    var html = "<a href=# id=fullScreenButton style='text-decoration:none;color:black;' title='click to use full screen mode' >full screen</a>";
    
    $("#"+isl.p.topUserPannel).append(html);
    
    $("#fullScreenButton").on("click", function(e){
        var screenMode = isl.f.screenMode();
        if (screenMode == "normal") {
            console.log("enter full screen");
            //enter full screen ...
            var container = document.getElementById(isl.p.globalContainer);
            
            $(document).fullScreen(true);
            console.log(done);
            
            //if (container.requestFullscreen) {
            //    console.log(11111111);
            //    container.requestFullscreen();
            //}
            //else if (container.webkitRequestFullscreen) {
            //    console.log(22222222222220);
            //    container.webkitRequestFullscreen();
            //}
            //else if (container.mozRequestFullScreen) {
            //    console.log(33333333333333333333333);
            //    container.mozRequestFullScreen();
            //}
            //else if (container.msRequestFullscreen) {
            //    console.log(4444444444444444444444444444444444444);
            //    container.msRequestFullscreen();
            //}
            //else{
            //    console.log("fullscreen failure");
            //}
        }
        else{
            //or exit full screen
            if (document.exitFullscreen) {
                document.exitFullscreen();
            } else if (document.webkitExitFullscreen) {
                document.webkitExitFullscreen();
            } else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if (document.msExitFullscreen) {
                document.msExitFullscreen();
            }
        }

    });
    
}

//display a button to search for legends
isl.f.resetLegendSearch = function(){
    var html = "<input placeholder='search structure' type=text id=legendSearchInput style='' title='search for a structure' value='"+("legendSearchValue" in isl.p ? isl.p.legendSearchValue : "" )+"'>";
    
    $("#"+isl.p.topUserPannel).append(html);
    
    //add an event to display a div to propose legends
    $("#legendSearchInput").on("change", isl.f.searchLegendInputModificationEvent );
    $("#legendSearchInput").on("keyup", isl.f.searchLegendInputModificationEvent );
    
    //add an event to close the legend selection when clicking outside
    $("body").unbind(".closeLegendSearchOnClick").bind("click.closeLegendSearchOnClick",function(e){
        if( ! $(this).parents('#legendSearchDiv').length ) $("#legendSearchDiv").remove();
    });

}

//function to trigger when the searchLegendInput has been modified
isl.f.searchLegendInputModificationEvent = function(e){
    //save the value of the input
    isl.p.legendSearchValue = $(this).val();
    var shouldContain = isl.p.legendSearchValue.toLowerCase();
    var legendsHtml = "";
    //if the container element does not exist, create it
    if ( ! $('#legendSearchDiv').length ){
        //get the position of the search element
        var position = $("#legendSearchInput").offset();
        //get the position of the main canvas
        var canvasPosition = $("#"+isl.p.mainImageCanvas).offset();
        var height = $("#legendSearchInput").height() + 15; 
        var width = $("#legendSearchInput").width();
        $("body").append("<div id=legendSearchDiv style='z-index:999;background-color:white;position:absolute;left:"+position.left+"px;top:"+(canvasPosition.top)+"px;width:"+width+"px;max-height:"+isl.p.mainImageHeight+"px;'></div> ")
        //$("body").append("<div id=legendSearchDiv style='z-index:999;position:fixed;left:500px;top:10px;width:"+width+";height:500;max-height:"+isl.p.mainImageHeight+";'></div> ")
    }
    
    //search for the legends with the right text
    var legends = isl.current.legends;
    var interestingLegends = [];
    for(var lid in legends){
        var txt = isl.f.getLegendText(lid);
        if( txt.toLowerCase().indexOf( shouldContain ) > -1){
            interestingLegends.push(legends[lid]);
        }
    }
    //build the html to display inside the legendSearchDiv
    var legendSearchDivHtml = "";
    if (interestingLegends.length == 0) legendSearchDivHtml += "no match found";
    for (var l in interestingLegends) {
        legendSearchDivHtml += "<a href=# class=legendSearchChoice lid="+interestingLegends[l].id+">"+ isl.f.getLegendText(interestingLegends[l].id) +"</a><br>";
        
    }
    $("#legendSearchDiv").html(legendSearchDivHtml);
    
    $("#legendSearchDiv *").on("hover",function(e){
        $("#legendSearchInput") . focusout();
        $(this).focus();
    } );


    //bind events on click on those options
    $(".legendSearchChoice").bind("click",isl.f.eventLegendSearchClick);
}

//event when clicking on a legend in the search display
isl.f.eventLegendSearchClick = function(e){
    var lid = $(this).attr("lid");
    //set the search form to the legend name
    isl.p.legendSearchValue = $(this).text();
    //remove the search div
    $("#legendSearchDiv").remove();
    //add the structure of the legend to the list of structure to display
    var legend = isl.current.legends[lid];
    if ("structure" in legend && legend.structure != "") {
        isl.f.makeStructureVisible(legend.structure);
    }
    //search the first image displaying the legend
    var images = isl.p.getSortedImage();
    for (var i in images) {
        if (lid in images[i].legends) {
            isl.currentImage = images[i].id;
            break;
        }
    }
    //reset all to display the right image with the legend on it
    isl.f.resetAll();
}

//display a button to change the mode legend are displayed
isl.f.legendModeButton = function(){
    switch(isl.p.legendMode){
        case "print" :
            nextMode = "quiz";
            var title = "click to use the quiz mode";
            var name = "displayed";
            break;
        case "quiz" :
            nextMode = "square";
            var title = "click to hide the legends";
            var name = "quiz";
            break;
        case "square" :
            nextMode = "hide";
            var title = "click to target legends by area";
            var name = "square";
            break;
        case "hide" :
            nextMode = "print";
            var title = "click to print the legends";
            var name = "hidden";
            break;
    }
    
    var html = "<a href=# id=legendModeButton style='text-decoration:none;color:black;' title='"+title+"' >"+name+"</a>";

    $("#"+isl.p.topUserPannel).append(html);
    
    $("#legendModeButton").bind("click",function(e){
        isl.p.legendMode = nextMode;
        if (nextMode == "square") {
            //reset parameters about legends :
            isl.p.legendSquareCenterPercentWidth = 0.5;
            isl.p.legendSquareCenterPercentHeight = 0.5;
        }
        isl.f.resetAll();
    });
    
}


//display a print button
isl.f.printButton = function(){
    var html = "<a href=# id=printButton style='text-decoration:none;' title=print  class='userbutton' >&#8865;</a>";

    $("#"+isl.p.topUserPannel).append(html);
    
    $("#printButton").bind("click",function(e){
        var dataUrl = document.getElementById(isl.p.mainImageCanvas).toDataURL("image/jpeg" , 1);
        var ctx = isl.f.getMainContext();
        var width = ctx.canvas.clientWidth;
        var height = ctx.canvas.clientHeight;
        var windowContent = '<!DOCTYPE html>';
        windowContent += '<html>'
        windowContent += '<head><title>Print canvas</title></head>';
        windowContent += '<body>'
        windowContent += '<img src="' + dataUrl + '">';
        windowContent += '</body>';
        windowContent += '</html>';
        var printWin = window.open('','','width='+width+',height='+height+'');
        printWin.document.open();
        printWin.document.write(windowContent);
        printWin.document.close();
        printWin.focus();
        printWin.print();
        printWin.close();
    });
    
    
}


//display a button that allow to download the current image as displayed on the screen
isl.f.downloadButton = function(){
    var html = "<a href=# id=downloadButton style='text-decoration:none;color:green;'  class='userbutton' title='download the current image' >&#8627;</a>";
    

    $("#"+isl.p.topUserPannel).append(html);
    
    $("#downloadButton").bind("click",function(e){
        var downloadButton = document.getElementById("downloadButton");
        var canvas = document.getElementById(isl.p.mainImageCanvas) ;
        downloadButton.href = canvas.toDataURL("image/jpeg" , 1);
        downloadButton.download = isl.currentImage in isl.current.images ? isl.current.images[isl.currentImage].url : "";
    });
}

//display buttons to perform symetry
isl.f.symetryButtons = function(){
    var html = "<a href=# class='userbutton' title='horizontal symetry' style='text-decoration:none;' id=horizontalSymetryButton>&#x21C5;</a> <a  class='userbutton' title='vertical symetry' style='text-decoration:none;' href=# id=verticalSymetryButton>&#x21C6;</a>";
    
    $("#"+isl.p.topUserPannel).append(html);
    
    $("#horizontalSymetryButton").bind("click",function(e){
        e.preventDefault();
        isl.p.horizontalSymetry = !isl.p.horizontalSymetry;
        isl.f.resetAll();
    });
    $("#verticalSymetryButton").bind("click",function(e){
        e.preventDefault();
        isl.p.verticalSymetry = !isl.p.verticalSymetry;
        isl.f.resetAll();
    });
}

//display a button that switch to catalog mode on click
isl.f.resetZoomButtons = function(){
    var html = "<a href=# id=zoomButton style='text-decoration:none;' title='zoom +'  class='userbutton' >&#8853;</a> <a href=# id=unzoomButton style='text-decoration:none;' title='zoom -' class='userbutton' >&#8854;</a>";
    
    $("#"+isl.p.topUserPannel).append(html);
    
    $("#zoomButton").bind("click",function(e){ e.preventDefault(); isl.f.zoom(100); });
    $("#unzoomButton").bind("click",function(e){ e.preventDefault(); isl.f.zoom(-100); });
    
    //$("#zoomButton").bind("click",function(e){ e.preventDefault(); isl.f.addCanvasZoomEvent(-0.21); });
    //$("#unzoomButton").bind("click",function(e){ e.preventDefault(); isl.f.addCanvasZoomEvent(+0.21); });
    
    
}

//add a zoom event zo the canvas
isl.f.addCanvasZoomEvent = function(percentGap){
    ////change the cursor to a zoom
    //$("#"+isl.p.mainImageCanvas+",body").css( 'cursor', percentGap < 0 ? "zoom-in" : "zoom-out" )
    ////unbind and bind event
    //$("#"+isl.p.mainImageContainer).unbind(".zoom")
    //.bind("click.zoom", isl.f.createZoomFunction(percentGap) );
    
    var imageDimension = isl.p.zoomEndPercentWidth - isl.p.zoomStartPercentWidth;
    var imageDimension = isl.p.zoomEndPercentWidth - isl.p.zoomStartPercentWidth;
    if ( (percentGap<0 && imageDimension <= Math.abs(percentGap)) || (percentGap>0 && imageDimension >=1) ) return;
    
    //build the new image dimension
    var newDimension = Math.max( 0.2 , Math.min(1, imageDimension + percentGap) ) ;
    var targetPercentWidth = 0.5;
    var targetPercentHeight = 0.5;
    
    //zoom :
    isl.f.zoomAt(newDimension , targetPercentWidth, targetPercentHeight);
    
}

//function to zoom at a given position
isl.f.zoomAt = function(newDimension , percentWidth, percentHeight){
    if( isl.p.verticalSymetry ) {
            var newEndWidth = Math.min( 1, percentWidth - newDimension/2 < 0  ? newDimension : percentWidth + newDimension/2 );
            var newStartWidth = newEndWidth - newDimension;
        }
        else{
            var newStartWidth = Math.max( 0, percentWidth + newDimension/2 > 1 ? 1 - newDimension : percentWidth - newDimension/2 );
            var newEndWidth = newStartWidth + newDimension;
        }
        if(isl.p.horizontalSymetry) {
            var newEndHeight = Math.min( 1, percentHeight - newDimension/2 < 0 ? newDimension : percentHeight + newDimension/2 );
            var newStartHeight = newEndHeight - newDimension;
        }
        else{
            var newStartHeight = Math.max( 0, percentHeight + newDimension/2 > 1 ? 1 - newDimension : percentHeight - newDimension/2 );
            var newEndHeight = newStartHeight + newDimension;
        }
        
        
        
        isl.p.zoomStartPercentHeight = newStartHeight;
        isl.p.zoomEndPercentHeight = newEndHeight;
        isl.p.zoomEndPercentWidth = newEndWidth;
        isl.p.zoomStartPercentWidth = newStartWidth;
        
        //lastly, reset all
        isl.f.resetAll();
}


//create a function to add as zoom event
isl.f.createZoomFunction = function(percentGap){
    var gap = percentGap;
    return function(e){
        //change the cursor back to origin
        $("#"+isl.p.mainImageCanvas+",body").css( 'cursor', 'auto' )
        //unbind zoom event
        $("#"+isl.p.mainImageContainer).unbind(".zoom");
        //if the gap is superior or equal to the image gap, don't do anything
        var imageDimension = isl.p.zoomEndPercentWidth - isl.p.zoomStartPercentWidth;
        if ( (gap<0 && imageDimension <= Math.abs(gap)) || (gap>0 && imageDimension >=1) ) return;
        //get the percentWidth and Height of the click
        
        var canvasX = e.pageX - $(this).offset().left;
        var canvasY = e.pageY - $(this).offset().top;
        var percentWidth  = isl.f.percentWidthFromWidth(canvasX - isl.p.legendLeftWidth) ;
        var percentHeight = isl.f.percentHeightFromHeight(canvasY );
        
        //build the new image dimension
        var newDimension = Math.max( 0.2 , Math.min(1, imageDimension + gap) ) ;
        
        //zoom :
        isl.f.zoomAt(newDimension , percentWidth, percentHeight);
    }
}

//increase or decrease by shift the size of the main image, then reset all
isl.f.zoom = function(shift){
    isl.p.setImageWidth += shift;
    if (isl.p.setImageWidth> 2000) isl.p.setImageWidth = 2000;
    if (isl.p.setImageWidth< 400) isl.p.setImageWidth = 400;
    isl.f.resetAll();
    //reload the bottom band
    isl.f.loadBottomBandImages();
}

//display a button that switch to catalog mode on click
isl.f.resetCatalogModeButton = function(){
    
    var html = "<a href=# id=switchToCatalogMode>catalog</a>";
    
    $("#"+isl.p.topUserPannel).append(html);
    
    $("#switchToCatalogMode").bind("click",function(e){
        isl.f.openCatalog();
        //display the possible series
        isl.f.displayCatalogSeries();
    })
    
    
}

//open the catalog with all the images
isl.f.openCatalog = function(){
    //add a window to display the description
    var $canvas = $("#"+isl.p.mainImageCanvas);
    var ctx = isl.f.getMainContext();
    var position = $canvas.offset();
    var html = "";
    html += "<div id=catalogPopup style='overflow:hidden;display:none;position:absolute;z-index:100000;top:"+position.top+"px;left:"+position.left+"px;width:"+ctx.canvas.clientWidth+"px;height:"+ctx.canvas.clientHeight+"px;background-color:rgba(255, 255, 255, 0.85);'>"+
                "<span id='close' onclick='this.parentNode.parentNode.removeChild(this.parentNode); return false;' style='float:right;padding:10px;cursor:pointer;' title=close >X</span>"+
                "<div id=catalogLeft style='width:200px;float:left'></div><div style='overflow:scroll;height:"+ctx.canvas.clientHeight+"px;' id=catalogRight></div>"+
                "</div>";
    $("body").append(html);
    $("#catalogPopup").show("slow");
}

//on the left part of the catalog, display the possible series
isl.f.displayCatalogSeries = function(){
    var html = "";
    //display an option to print the images of all series
    var iid = isl.f.findImageBelongingToSeries(null);
    if (iid == null) return;
    var image = isl.current.images[iid];
    html += "<div class=catalogSeries style='pointer:cursor;' sid=''>all series";
    html += "<br><img style='width:100px;cursor:pointer;' src='"+image.url+"' >";
    html += "</div><br>";
    //display each series
    for(var sid in isl.current.series){
        var iid = isl.f.findImageBelongingToSeries(sid);
        if (iid == null) continue;
        var image = isl.current.images[iid];
        html += "<div class=catalogSeries style='pointer:cursor;' sid='"+sid+"'>"+isl.current.series[sid].text;
        html += "<br><img style='width:100px;cursor:pointer;' src='"+image.url+"' >";
        html += "</div><br>";
    }
    $("#catalogLeft").html(html);
    //bind an event on click
    $(".catalogSeries").bind("click",function(e){
        var sid = $(this).attr("sid");
        isl.f.displayCatalogImages(sid);
    })
}

//inside the catalog, display the images of the given series
isl.f.displayCatalogImages = function(sid){
    if (sid == "") sid = null;
    var images = isl.p.getSortedImage(sid);
    var html = "";
    for (var i in images) {
        html += "<div class=catalogImage iid="+images[i].id+" style='display:inline-block;padding:10px;'>";
        html += "<img src='"+images[i].url+"' style='width:100px;cursor:pointer;'>";
        html += "</div>";
    }
    
    $("#catalogRight").html(html);
    
    //bind an event on click
    $(".catalogImage").bind("click",function(e){
        var iid = $(this).attr("iid");
        isl.currentImage = iid;
        $("#catalogPopup").remove();
        isl.currentSeries = null; //display all the series
        isl.f.resetAll();
    })
    
}

//for a given series, find the id of the first image matching the series
isl.f.findImageBelongingToSeries = function(sid){
    var images= isl.p.getSortedImage(null);
    for (var i in images) {
        if (images[i].series == sid || sid == null) return images[i].id;
    }
    return null;
}

//display html and add events for the strcutrure selection
isl.f.resetStructureSelection = function(){
    var html = "";
    html += "<select id=selectVisibleStructures> <option selected disabled>Choose visible legends</option>";
    html += "<option value=all>all</option><option value=none>none</option>";
    
    // sort the elements of the select
    var sortarr = [];
    for(s in isl.current.structures){ sortarr.push([isl.f.getStructureText( s ), s ]) }
    sortarr.sort( function(a,b){return a[0].localeCompare(b[0]);} );
    
    for(var i in sortarr){
        var s = sortarr[i][1];
        var visibleYet = isl.f.isStructureVisible(s);
        html += "<option value="+s+" "+(visibleYet ? "" : "style='text-decoration:line-through;'" )+">"+ isl.f.getStructureText( s ) +"</option>";
    }
    //for(var s in isl.current.structures){
    //    var visibleYet = isl.f.isStructureVisible(s);
    //    html += "<option value="+s+" "+(visibleYet ? "" : "style='text-decoration:line-through;'" )+">"+ isl.f.getStructureText( s ) +"</option>";
    //}
    
    $("#"+isl.p.topUserPannel).append(html);
    
    //on change, change the visible anatomic structures
    $("#selectVisibleStructures").change(function(e){
        var sid = $(this).val();
        //if all, build an array containing all the sid
        if (sid == "all") {
            var newAvailable = []
            for(var s in isl.current.structures) newAvailable.push(s);
            isl.p.displayedStructures = newAvailable;
        }
        else if (sid == "none") {
            isl.p.displayedStructures = [];
        }
        else{
            //toggle the structure
            if (isl.f.isStructureVisible(sid))  isl.f.makeStructureInvisible(sid);
            else isl.f.makeStructureVisible(sid);
        }
        //reset all
        isl.f.resetAll();
    });
}


//function to make a structure visible
isl.f.makeStructureVisible = function(sid){
    if (! isl.f.isStructureVisible(sid))  isl.p.displayedStructures.push(sid);
    
}

//function to make a structure invisible
isl.f.makeStructureInvisible = function(sid){
    var index = isl.p.displayedStructures.indexOf(sid);
    if (index > -1)  isl.p.displayedStructures.splice( index , 1);
}

//check if a structure is visible
isl.f.isStructureVisible = function(sid){
    return isl.p.displayedStructures.indexOf(sid) > -1;
}

//function to display an image, given an image data
isl.f.displayMainImage = function(image){
    image = isl.f.retrieveImageToDisplay();
    //if there is no url in the image object, there is nothing to display
    if (!("url" in image)) {
        $("#"+isl.p.mainImageContainer).html("no image selected");
    }
    var url = image.url;
    isl.currentImage = image.id;
    //load the image to display
    var img = new Image();
    img.onload = function() {
        isl.f.displayMainImage2(url, this);
    }
    img.src = url;
}

//retrieve the image to display
isl.f.retrieveImageToDisplay = function(image){
    //if no image is provided, find the first image
    if (image != null && (image != undefined) ) return image;
    if (isl.currentImage != null && (isl.currentImage in isl.current.images) ) {
        return  isl.current.images[isl.currentImage];
    }
    else{
        var images = isl.p.getSortedImage();
        if (0 in images) return images[0];
    }
    //by default, return an empty object
    return {};
}



//function to display an image, once the image has been loaded
isl.f.displayMainImage2 = function(url,img){
    var mainImageContainer = $("#"+isl.p.mainImageContainer);
    //create a canvas inside the main container
    mainImageContainer.html("<canvas id="+isl.p.mainImageCanvas+"></canvas>");
    //set the size of the various elements of the canvas
    isl.p.mainImageWidth = isl.p.setImageWidth;
    isl.p.mainImageHeight = img.height * (isl.p.mainImageWidth / img.width );
    isl.p.mainImageCanvasWidth = isl.p.mainImageWidth + isl.p.legendLeftWidth + isl.p.legendRightWidth;
    isl.p.mainImageCanvasHeight = isl.p.mainImageHeight;
    
    //print the image
    isl.f.printImageOnCanvas(img);
    
    //display scroll event
    isl.f.addEventScrollCanvas();
    
}

//change to the previous or the next image on scroll on the main image canvas
isl.f.addEventScrollCanvas = function(){
    $("#"+isl.p.mainImageCanvas).unbind(".scrollevent").on('mousewheel.scrollevent', function(event) {
       event.preventDefault();
        if (event.deltaY < 0){
            isl.currentImage = isl.f.getNextImageId();
            isl.f.resetAll();
        } else {
            isl.currentImage = isl.f.getPreviousImageId();
            isl.f.resetAll();
        }
        //return false to prevent bubbling and avoid window scrolling
        return false; 
    });
}

//get the id of the image preceding the current one
isl.f.getPreviousImageId = function(){
    var images = isl.p.getSortedImage();
    var pos = isl.f.getRelativeImagePosition(isl.currentImage , images);
    if ( pos == 0) return isl.currentImage;
    else return images[ pos -1 ].id;
}

//get the id of the next image 
isl.f.getNextImageId = function(){
    var images = isl.p.getSortedImage();
    var pos = isl.f.getRelativeImagePosition(isl.currentImage , images);
    if ( pos == (images.length-1) ) return isl.currentImage;
    else return images[ pos + 1 ].id;
}

//get the image position relatively to the images of the current series
isl.f.getRelativeImagePosition = function(iid , images){
    if (images == null || images == undefined) {
        images = isl.p.getSortedImage();
    }
    var pos = 0;
    while( pos < images.length){
        if (iid == images[pos].id) break;
        else pos++;
    }
    return pos;
}

//print the given image on the canvas, leaving enough space for the legend
isl.f.printImageOnCanvas = function(img){
    //print the image on the canvas
    var ctx=isl.f.getMainContext();
    //resize the canvas
    ctx.canvas.width = isl.p.mainImageCanvasWidth;
    ctx.canvas.height = isl.p.mainImageCanvasHeight;
    //ctx.rect(0, 0, 150, 1000);
    ctx.rect(0,0,isl.p.mainImageCanvasWidth,isl.p.mainImageCanvasHeight);
    ctx.fillStyle="black";
    ctx.fill(); 
    
    //perform tranformation of symetry
    ctx.save();
    ctx.scale( isl.p.verticalSymetry ? -1 : 1, isl.p.horizontalSymetry ? -1 : 1);
    var imageStartWidth = img.width * isl.p.zoomStartPercentWidth;
    var imageStartHeight = img.height * isl.p.zoomStartPercentHeight;
    var captureWidth = img.width * ( isl.p.zoomEndPercentWidth -  isl.p.zoomStartPercentWidth);
    var captureHeight = img.height * ( isl.p.zoomEndPercentHeight -  isl.p.zoomStartPercentHeight);
    ctx.drawImage(img,
                  imageStartWidth,
                  imageStartHeight,
                  captureWidth,
                  captureHeight,
                  isl.p.verticalSymetry ? -isl.p.legendLeftWidth-isl.p.mainImageWidth : isl.p.legendLeftWidth ,
                  isl.p.horizontalSymetry ? -isl.p.mainImageHeight : 0 ,
                  isl.p.mainImageWidth ,
                  isl.p.mainImageHeight
                  );
    //ctx.drawImage(img,
    //              isl.p.verticalSymetry ? -isl.p.legendLeftWidth-isl.p.mainImageWidth : isl.p.legendLeftWidth ,
    //              isl.p.horizontalSymetry ? -isl.p.mainImageHeight : 0 ,
    //              isl.p.mainImageWidth ,
    //              isl.p.mainImageHeight
    //              );
    ctx.restore();
    
    isl.f.printAllLegendsOnCanvas();
    
    isl.f.printAllStructuresOnCanvas();
    
    //display the square in square mode
    if(isl.p.legendMode == "square" ) isl.f.displaySquare();
    
    //add an event to display the legend on mouse over on quiz mode
    isl.f.addCanvasEventQuizMode();
    
    //add an event in case on square mode to display and move 
    isl.f.addEventSquareMode();
    
    //add an event to make legend clickable
    isl.f.addCanvasEventLegendClick();
    
}

isl.f.addCanvasEventLegendClick = function(){
    $canvas = $("#"+isl.p.mainImageCanvas);
    //on mouse over, check which legend is targeted
    $canvas.bind("mousemove", function(e){ isl.f.canvasEventLegendMouseoverOn(e,this);} );
    //remove it when leaving
    $canvas.bind("mouseleave", function(e){ isl.f.canvasEventLegendMouseoverOff(e,this)} );
    //on click, open a window to show the description
    $canvas.bind("click",function(e){  isl.f.canvasEventLegendClickShowDescription(e,this);} );
}

isl.f.canvasEventLegendClickShowDescription =function(e, jqelement){
    //get the legend under the mouse
    var lid = isl.f.getLegendIdUnderMouse(e, jqelement);
    if (lid == null) return;
    //add a window to display the description
    var $canvas = $("#"+isl.p.mainImageCanvas);
    var ctx = isl.f.getMainContext();
    var position = $canvas.offset();
    var description = "description" in isl.current.legends[lid] ? isl.current.legends[lid].description : "";
    var title = isl.f.getLegendText ( lid );
    var html = "";
    html += "<div id=descriptionPopup style='display:none;position:absolute;z-index:100000;top:"+position.top+"px;left:"+position.left+"px;width:"+ctx.canvas.clientWidth+"px;height:"+ctx.canvas.clientHeight+"px;background-color:rgba(255, 255, 255, 0.85);'>"+
                "<span id='close' onclick='this.parentNode.parentNode.removeChild(this.parentNode); return false;' style='float:right;padding:10px;cursor:pointer;' title=close >X</span>"+
                "<h2>"+title+"</h2>"+
                "<h3>description : </h3>"+description+
                "</div>";
    $("body").append(html);
    $("#descriptionPopup").show("slow");
}

//add an event to change the position of the square during mousemove on square mode
isl.f.addEventSquareMode = function(){
    if (isl.p.legendMode != "square") return;
    $canvas = $("#"+isl.p.mainImageCanvas);
    $canvas.bind("mousemove", function(e){
        //perform an update only from time to time
        isl.p.squareUpdateCounter ++;
        if (isl.p.squareUpdateCounter == isl.p.squareUpdateCounterReset) isl.p.squareUpdateCounter = 0;
        else return;
        //update the square
        var canvasX = e.pageX - $(this).offset().left;
        var canvasY = e.pageY - $(this).offset().top;
        isl.p.legendSquareCenterPercentWidth = isl.f.percentWidthFromWidth(canvasX - isl.p.legendLeftWidth) ;
        isl.p.legendSquareCenterPercentHeight = isl.f.percentHeightFromHeight(canvasY );
        isl.f.resetAll();
    } );
}

//add an event to display the legend on mouse over on quiz mode
isl.f.addCanvasEventQuizMode = function(){
    //add an event just in case of quiz mode
    if (isl.p.legendMode != "quiz") return;
    $canvas = $("#"+isl.p.mainImageCanvas);
    $canvas.bind("mousemove", function(e){ isl.f.canvasEventQuizModeMouseover(e,this)} );
}

//event to check if new legends must be displayed on quiz mode on mouse
isl.f.canvasEventQuizModeMouseover = function(e,jqelement){
    var lid = isl.f.getLegendIdUnderMouse(e, jqelement);
    //dispay it unless it is already done
    if( isl.p.displayedQuizLegends.indexOf( lid ) == -1) {
        isl.p.displayedQuizLegends.push(lid);
        isl.f.resetAll();
    }
}

//check which legend is under the mouse
isl.f.getLegendIdUnderMouse = function(e, jqelement){
    var canvasX = e.pageX - $(jqelement).offset().left;
    var canvasY = e.pageY - $(jqelement).offset().top;
    if (canvasX < isl.p.legendLeftWidth) var side = "left";
    else if (canvasX > isl.p.legendLeftWidth + isl.p.mainImageWidth ) side = "right";
    else return null; //don't do anything if the cursor is not on top of a legend
    var legends = isl.p.getSortedLegends();
    //calculate the position of the cursor on the real image, without zoom 
    var cursorHeightOnRealImage = isl.f.realHeightFromHeight(canvasY); 
    //if the cursor is on top of a legend 
    for(var l in isl.p.legendsHeightOnCanvas){ 
        if( isl.p.legendsSideOnCanvas[ l ] != side ) continue; 
        var gap = Math.abs( isl.p.legendsHeightOnCanvas[l] -  cursorHeightOnRealImage ); 
        if ( gap < (isl.p.fontSize /2) ) { 
            return l; 
        } 
    } 
    return null; 
} 

//print the legend targeted by the mouse in bold
isl.f.canvasEventLegendMouseoverOn = function(e, jqelement){ 
    var previousLegend = isl.p.mouseOverLegend;
    isl.p.mouseOverLegend = isl.f.getLegendIdUnderMouse(e, jqelement); //this way the legend will be printed in bold
    if (isl.p.mouseOverLegend != previousLegend) {
        //set the mouse cursor 
        $("#"+isl.p.mainImageContainer).css( 'cursor', isl.p.mouseOverLegend == null ? "default" : 'pointer' );
        //reset all
        isl.f.resetAll();
    } 
}
//remove this event
isl.f.canvasEventLegendMouseoverOff = function(e){
    isl.p.mouseOverLegend = null; 
}

isl.f.printAllStructuresOnCanvas = function(){
    var ctx = isl.f.getMainContext(); 
    for(var s in isl.current.structures){
        var structure = isl.current.structures[s];
        if( !isl.f.isStructureVisible(s) ) continue;
        if (isl.currentImage in structure.draw ) { 
            var points = [];
            var displayed = true;
            for(var p in structure.draw[isl.currentImage] ){
                if (! isl.f.isDisplayed(structure.draw[isl.currentImage][p][0] , structure.draw[isl.currentImage][p][1])) {
                    displayed = false; 
                    break;
                }
                //if one point is outside the canvas, don't display it
                points.push([isl.f.getWidthCoordinate( structure.draw[isl.currentImage][p][0] )+ isl.p.legendLeftWidth ,
                             isl.f.getHeightCoordinate( structure.draw[isl.currentImage][p][1] ) ]);
                //points.push([ structure.draw[isl.currentImage][p][0]*isl.p.mainImageWidth + isl.p.legendLeftWidth ,  structure.draw[isl.currentImage][p][1]*isl.p.mainImageHeight ]);
            }
            if(displayed) isl.f.printAndFillCurve(ctx, points, structure.color , isl.p.drawingOpacity );
        }
    }
}

//function to print and fill a curve on a canvas
isl.f.printAndFillCurve = function(ctx, points , color, opacity){ 
    if (points.length < 3) return;
    ctx.save();
    ctx.beginPath();
    ctx.moveTo(points[0][0], points[0][1]);
    points.push(points[0]);
    for (i = 1; i < points.length - 2; i ++)
    {
       var xc = (points[i][0] + points[i + 1][0]) / 2;
       var yc = (points[i][1] + points[i + 1][1]) / 2;
       ctx.quadraticCurveTo(points[i][0], points[i][1], xc, yc);
    }
    // curve through the last two points
    ctx.quadraticCurveTo(points[i][0], points[i][1], points[i+1][0],points[i+1][1]);
    ctx.lineWidth = 1;
    ctx.globalAlpha=opacity;
    ctx.fillStyle = color;
    ctx.fill();
    ctx.strokeStyle = color;
    ctx.closePath();
    ctx.stroke();
    ctx.restore();
}

//set the legend size on the left and on the right
isl.f.setLegendSize = function(legends){
    //calculate the number of legends that will displayed on the left or on the right
    var left = 0;
    var right = 0;
    for (var i in legends) {
        if (legends[i].percentWidth > 0.5) right++;
        else left ++;
    }
    //calculate the maximum legend size allowed
    var maxLegendSizeLeft = isl.p.mainImageHeight/(1+left);
    var maxLegendSizeRight = isl.p.mainImageHeight/(1+right);
    var maxSize = Math.min(maxLegendSizeLeft, maxLegendSizeRight) * 0.5; // the 0.7 coeff is assuming about 1.5 lines per legend
    if (maxSize > 13) {
        isl.p.fontSize = 13;
    }
    else if(maxSize > 11) {
        isl.p.fontSize =  11;
    }
    else if (maxSize > 10) {
        isl.p.fontSize =  10;
    }
    else if (maxSize > 10) {
        isl.p.fontSize =  10;
    }
    else if (maxSize > 10) {
        isl.p.fontSize =  10;
    }
    else isl.p.fontSize =  10;
}

//print all legends of the current image
isl.f.printAllLegendsOnCanvas = function(){
    //don't print anything if it is in hide mode
    if (isl.p.legendMode == "hide") return;
    //if the quiz mode is enabled but the imageId of the current quiz don't match with the current image, reset the quiz
    if (isl.p.quizImageId != isl.currentImage) {
        isl.p.quizImageId = isl.currentImage;
        isl.p.displayedQuizLegends = [];
        isl.p.legendsHeightOnCanvas = {};
        isl.p.legendsSideOnCanvas = {};
    }
    var legends = isl.p.getSortedLegends();
    //count the number of legends and set the right legend size depending on it
    isl.f.setLegendSize(legends);
    //set an object to remember where to start printing the legends
    var startPrintingHeight = {left:isl.p.fontSize , right:isl.p.fontSize};
    for(var l in legends){
        var lid = legends[l].id;
        //don't display the legend if it isn't inside the canvas
        if ( ! isl.f.isDisplayed( legends[l].percentWidth , legends[l].percentHeight ) ) continue;
        //don't display the legend if it isn't inside the square
        if (isl.p.legendMode == "square" && ! isl.f.isLegendInsideSquare(legends[l])) continue;
        var structure = isl.current.legends[ lid ].structure;
        //if the structure is not visible, skip it
        if (structure != "" && !isl.f.isStructureVisible(structure) ) continue;
        //the text is the one specified in the
        var text = isl.p.legendMode != "quiz" || isl.p.displayedQuizLegends.indexOf( lid ) > -1 ? isl.f.getLegendText(lid) : "";
        var color = structure != "" ? isl.current.structures[ structure ].color : "white";
        //the target width and height are saved as percentage of the image size
        var targetWidth = isl.f.getWidthCoordinate( legends[l].percentWidth );
        var targetHeight = isl.f.getHeightCoordinate( legends[l].percentHeight );
        //print the legend, only if the process hasn't been started already
        isl.f.printLegendOnCanvas( text, color, targetWidth, targetHeight , legends[l].id , startPrintingHeight);
    }
}

//check if a given legend object is inside the square foccusing the legends
isl.f.isLegendInsideSquare = function(legend){
    //the position of the legend
    var targetWidth = isl.f.getWidthCoordinate( legend.percentWidth )+ isl.p.legendLeftWidth ;
    var targetHeight = isl.f.getHeightCoordinate( legend.percentHeight );
    //the limit of the square
    var minX = isl.f.getWidthCoordinate ( isl.p.legendSquareCenterPercentWidth ) - isl.p.legendSquareSize/2 + isl.p.legendLeftWidth ;
    var minY = isl.f.getHeightCoordinate( isl.p.legendSquareCenterPercentHeight ) - isl.p.legendSquareSize/2;
    var maxX = isl.f.getWidthCoordinate( isl.p.legendSquareCenterPercentWidth) + isl.p.legendSquareSize/2 + isl.p.legendLeftWidth;
    var maxY = isl.f.getHeightCoordinate( isl.p.legendSquareCenterPercentHeight) + isl.p.legendSquareSize/2;
    
    //var minX = ( isl.p.legendSquareCenterPercentWidth - isl.p.legendSquareSize/2) * isl.p.mainImageWidth + isl.p.legendLeftWidth ;
    //var minY = ( isl.p.legendSquareCenterPercentHeight - isl.p.legendSquareSize/2) * isl.p.mainImageHeight;
    //var maxX = ( isl.p.legendSquareCenterPercentWidth + isl.p.legendSquareSize/2) * isl.p.mainImageWidth + isl.p.legendLeftWidth;
    //var maxY = ( isl.p.legendSquareCenterPercentHeight + isl.p.legendSquareSize/2) * isl.p.mainImageHeight;
    
    return (targetWidth > minX) &&(targetWidth <maxX) &&(targetHeight > minY) &&(targetHeight < maxY); 
}


//function to get the coordinates on the main images given the width and height percent
isl.f.getWidthCoordinate = function(percentWidth){
    return (parseFloat( isl.p.verticalSymetry ? isl.p.zoomEndPercentWidth - percentWidth : percentWidth - isl.p.zoomStartPercentWidth ) ) * isl.p.mainImageWidth / (isl.p.zoomEndPercentWidth - isl.p.zoomStartPercentWidth);
}

isl.f.getHeightCoordinate = function(percentHeight){
    return ( parseFloat( isl.p.horizontalSymetry ? isl.p.zoomEndPercentHeight - percentHeight : percentHeight - isl.p.zoomStartPercentHeight ) ) * isl.p.mainImageHeight / (isl.p.zoomEndPercentHeight - isl.p.zoomStartPercentHeight);
}

//function to print the square on the canvas
isl.f.displaySquare = function(){
    //get the coordinates of the square to print
    var minX = isl.f.getWidthCoordinate ( isl.p.legendSquareCenterPercentWidth ) - isl.p.legendSquareSize/2 + isl.p.legendLeftWidth ;
    var minY = isl.f.getHeightCoordinate( isl.p.legendSquareCenterPercentHeight ) - isl.p.legendSquareSize/2;
    var maxX = isl.f.getWidthCoordinate( isl.p.legendSquareCenterPercentWidth) + isl.p.legendSquareSize/2 + isl.p.legendLeftWidth;
    var maxY = isl.f.getHeightCoordinate( isl.p.legendSquareCenterPercentHeight) + isl.p.legendSquareSize/2;
    
    var ctx=isl.f.getMainContext();
    ctx.strokeStyle = isl.p.squareColor;
    ctx.lineWidth = isl.p.squareLineWidth;
    ctx.beginPath();
    //draw the vertical line
    ctx.moveTo( minX, minY);
    ctx.lineTo(minX , maxY );
    ctx.lineTo(maxX , maxY );
    ctx.lineTo(maxX , minY );
    ctx.lineTo(minX , minY );
    ctx.stroke();
}

//check if the legend should be printed on the left or on the right
isl.f.getPrintSide = function(targetWidth){
    return targetWidth * 2 > isl.p.mainImageWidth ? "right" : "left";
}

//print a legend on the canvas
isl.f.printLegendOnCanvas = function(text , color, targetWidth, targetHeight , lid , startPrintingHeight){
    
    //chek whether the legend shoudl be printed on the left side or the right side
    var side = isl.f.getPrintSide(targetWidth);
    
    var ctx=isl.f.getMainContext();
    //increase the font size in case of mouseover
    var fontSize = isl.p.mouseOverLegend == lid && isl.p.mouseOverLegend != null ? isl.p.fontSize +0 : isl.p.fontSize;
    ctx.font = ""+fontSize+"px "+isl.p.font;
    //if the lid is the one beeing hovered by the mouse, print it in bold
    ctx.fillStyle = color;
    //set the maximum width a text can take
    if(side == "left") var maxWidth = isl.p.legendLeftWidth - isl.p.verticalLinePadding - isl.p.verticalLinePaddingToWord;
    else var maxWidth = isl.p.legendRightWidth - isl.p.verticalLinePadding - isl.p.verticalLinePaddingToWord;
    //broke the text into group of words smaller than the max width
    var words = isl.f.breakText(text, maxWidth, fontSize);
    var heightIncrement = 0;
    for(var w in words) {
        var size =  ctx.measureText(words[w]).width;
        var textWidthPosition = side == "left" ? maxWidth - size : isl.p.legendLeftWidth + isl.p.mainImageWidth + isl.p.verticalLinePadding + isl.p.verticalLinePaddingToWord;
        var textHeightPosition = side == "left" ? startPrintingHeight.left + heightIncrement : startPrintingHeight.right + heightIncrement;
        ctx.fillText(  words[w], textWidthPosition , textHeightPosition);
        //increment the next height to print legend
        heightIncrement += isl.p.wordPaddingTop + isl.p.fontSize;
    }
    
    //compute useful coordinates to draw the lines
    var verticalLineTopHeight = (side == "left" ? startPrintingHeight.left :  startPrintingHeight.right ) - isl.p.fontSize + 2 ;
    var verticalLineBottomHeight = (side == "left" ? startPrintingHeight.left : startPrintingHeight.right ) -isl.p.fontSize + heightIncrement - 2;
    var verticalLineWidth = side == "left" ? isl.p.legendLeftWidth - isl.p.verticalLinePadding : isl.p.legendLeftWidth + isl.p.mainImageWidth + isl.p.verticalLinePadding ;
    var horizontalLineHeight = Math.floor(verticalLineTopHeight + verticalLineBottomHeight)/2;
    var horizontalLineLeft = verticalLineWidth;
    var horizontalLineRight = side == "left" ? verticalLineWidth + isl.p.verticalLinePadding : verticalLineWidth - isl.p.verticalLinePadding;
    var targetWidthOnCanvas = targetWidth + isl.p.legendLeftWidth;
    var targetHeightOnCanvas = targetHeight ;
    
    //remember the position where to print the legend
    isl.p.legendsHeightOnCanvas[lid] = ( verticalLineTopHeight + verticalLineBottomHeight)/2;
    isl.p.legendsSideOnCanvas[lid] = side;
    
        
    ctx.strokeStyle = color;
    ctx.lineWidth = isl.p.lineWidth;
    ctx.beginPath();
    //draw the vertical line
    ctx.moveTo( verticalLineWidth, verticalLineTopHeight);
    ctx.lineTo(verticalLineWidth , verticalLineBottomHeight );
    //draw the horizontal line
    ctx.moveTo( horizontalLineLeft , horizontalLineHeight);
    ctx.lineTo( horizontalLineRight , horizontalLineHeight);
    //draw the line to target
    ctx.lineTo( targetWidthOnCanvas  , targetHeightOnCanvas);
    //end the line
    ctx.stroke();
    
    //draw the target circle
    ctx.beginPath();
    ctx.arc(targetWidthOnCanvas, targetHeightOnCanvas, isl.p.targetRadius, 0, 2 * Math.PI, false);
    ctx.fillStyle = color;
    ctx.fill();
    
    
    //for the next legends, increment the start height
    if (side=="left") startPrintingHeight.left +=  heightIncrement + isl.p.legendPaddingTop ;
    else startPrintingHeight.right +=  heightIncrement + isl.p.legendPaddingTop ;
    
}


//break a text in several lines so that each line doesn't exceed a given width
isl.f.breakText = function(text, maxWidth, fontSize){
    var ctx=isl.f.getMainContext();
    ctx.font = fontSize+"px "+isl.p.font;
    var result = [];
    var words = text.split(" ");
    var str = "";
    for(var w in words){
        var word = words[w];
        if (str == "") str = word;
        else if ( ctx.measureText(str+" "+word).width <= maxWidth) str += " "+word;
        else{
            result.push(str);
            str = word;
        }
    }
    //in the end, append the last piece of str
    result.push(str);
    return result;
}

//retrieve the context of the main canvas image
isl.f.getMainContext = function(){
    var c=document.getElementById(isl.p.mainImageCanvas);
    var ctx=c.getContext("2d");
    return ctx;
}

//function to create a new data object
isl.f.createNewObject = function(){
    var data = {};
    
    //create a new random id
    data.id = isl.f.generateId() ;
    
    //a data should have a list of images
    data.images = {};
    
    //a data should have a list of legends
    data.legends = {};
    
    //a data should have a list of anatomical structures
    data.structures = {};
    
    //a data has general informations
    data.general = {};
    
    //a data object has one or several languages
    data.languages = {
        "english" : "english"
    };
    
    //a data object has one or several series
    data.series = {
        //0 : { text : "default" }
    };
    
    return data;
}

//function to save load a piece of data as the current object
isl.f.openProject = function(id){
    if (id != null && id != undefined) {
        var jqxhr = $.ajax({
            url: "/loaddata",
            method: "POST",
            data: {id:id},
            dataType: "json",
            type: "POST",
        })
        .done(function(res) {
            //reset all
            isl.current = res != null && "id" in res ? res : isl.f.createNewObject();
            isl.current.id = id;
            //by default, all structure are visible
            for(sid in isl.current.structures) isl.p.displayedStructures.push(sid);
            isl.f.start();
            isl.f.resetAll();
        })
        .fail(function() {
            alert( "error during loading" );
        })
    }
    else{
        //trigger directly the display all event
        isl.current = isl.f.createNewObject();
        isl.f.resetAll();
    }
}

//get an array of images sorted by appearance order
isl.p.getSortedImage = function(series){
    if (series == undefined) {
        if (isl.p.currentSeries == null) {
            isl.f.selectDefautSeries();
        }
        series = isl.p.currentSeries;
    }
    var sortable = [];
    if (!("images" in isl.current)) return sortable;
    for (var i in isl.current.images)
        if(series == null || series == isl.current.images[i].series) sortable.push(isl.current.images[i])
    //sortable.sort(function(a, b) {return a.position - b.position})
    sortable.sort(function(a, b) {return 0+(a.position) - (b.position)})
    return sortable;
}

//get an array of legends sorted by height order
isl.p.getSortedLegends = function(){
    var sortable = [];
    if (!("legends" in isl.current)) return sortable;
    for (var i in isl.current.images[ isl.currentImage].legends )
        sortable.push(isl.current.images[ isl.currentImage].legends[i])
    //sortable.sort(function(a, b) {return isl.f.getHeightCoordinate( a.percentHeight )  - isl.f.getHeightCoordinate(b.percentHeight) });
    
    //sortable = [sortable[0],sortable[1],sortable[2],sortable[3],sortable[4],sortable[5],sortable[6],sortable[7],sortable[8],sortable[9],sortable[10],sortable[11]];
    var result = [];
    left = [];
    right = [];
    c = 0;
    while(sortable.length>0){
        c++;
        sortable.sort(function(a, b) {
            var aph = isl.p.horizontalSymetry ? 1-a.percentHeight : a.percentHeight;
            var bph = isl.p.horizontalSymetry ? 1-b.percentHeight : b.percentHeight;
            var apw = isl.p.verticalSymetry ? 1-a.percentWidth : a.percentWidth;
            var bpw = isl.p.verticalSymetry ? 1-b.percentWidth : b.percentWidth;
            var ah = isl.f.getHeightCoordinate( aph );
            var bh = isl.f.getHeightCoordinate( bph );
            var aw = isl.f.getWidthCoordinate( apw );
            var bw = isl.f.getWidthCoordinate( bpw );
            var aside = apw>0.5 ? right.length : left.length;
            var bside = bpw>0.5 ? right.length : left.length;
            var horizontalLineLength = isl.p.verticalLinePadding ;
            var lineHeight = isl.p.wordPaddingTop + isl.p.fontSize + isl.p.legendPaddingTop + 2 ;
            var aslope = (aph-(aside+0.5)*( lineHeight )/isl.p.mainImageHeight )/( (apw>0.5 ? 1-apw : apw) +0.0001);
            var bslope = (bph-(bside+0.5)*( lineHeight )/isl.p.mainImageHeight )/( (bpw>0.5 ? 1-bpw : bpw) +0.0001);
            //if(c==3) console.log(a,aslope,aph-(aside+0.5)*( lineHeight )/isl.p.mainImageHeight);
            //if(c==3) console.log(b,bslope,bph-(bside+0.5)*( lineHeight )/isl.p.mainImageHeight);
            return 1000*(bslope - aslope);
        });
        var el = sortable.pop();
        if( (el.percentWidth>0.5 && isl.p.verticalSymetry)||(el.percentWidth<0.5 && !isl.p.verticalSymetry) ) left.push(el);
        else right.push(el);
        //console.log(right.length);
    }

    return left.concat(right);
}

//function to play all functions on a given array
isl.p.callFunctionsFromArray = function(arr){
    for(var i in arr){
        var fname = arr[i];
        var parts = fname.split(".");
        var f = window;
        for(var j in parts) {
            f = f[ parts[j] ];
        }
        f();
    }
}

//return the screen mode of the web browser : full or normal
isl.f.screenMode = function(){
    if (!window.screenTop && !window.screenY) {
        return "full";
    }
    else return "normal";
}

//reset some p vars
isl.f.resetPVars = function(){
    isl.p.legendLeftStartHeight = isl.p.fontSize;
    isl.p.legendRightStartHeight = isl.p.fontSize;
    isl.p.currentLanguage = isl.f.retrieveCurrentLanguage();
}

//simple function to generate a random integer as unique ID
isl.f.generateId = function(){
    return Math.floor( 1000000000000000 * Math.random() );
}
isl.f.generateIds = function(n){
    var ids = [];
    for(var i=0;i<n;i++) ids.push(isl.f.generateId());
    return ids;
}

//get the text of the given legend
isl.f.getLegendText = function(lid, language){
    if (language == null) {
        language = isl.p.currentLanguage;
    }
    
    if (!(lid in isl.current.legends)) return "";
    return language in isl.current.legends[lid].text ? isl.current.legends[lid].text[language] : "";
}


//get the text of the given structure
isl.f.getStructureText = function(sid, language){
    if (language == null) {
        language = isl.p.currentLanguage;
    }
    if (!(sid in isl.current.structures)) return "";
    return isl.current.structures[sid].text;
    //return language in isl.current.structures[sid].text ? isl.current.structures[sid].text[language] : "";
}



//retrieve the current legend
isl.f.retrieveCurrentLanguage = function(){
    if (isl.p.currentLanguage != null && isl.p.currentLanguage in isl.current.languages ) return isl.p.currentLanguage;
    //else, if the default language has been defined :
    if (isl.current.general.defaultLanguage in isl.current.languages) return  isl.current.general.defaultLanguage;
    //else return the first language found
    for(var l in isl.current.languages) return l;
    //else there is no language
    return null;
}

//check that a point of the main image is displayed
isl.f.isDisplayed = function (percentWidth,percentHeight){
    return (percentWidth >= isl.p.zoomStartPercentWidth) && (percentWidth <= isl.p.zoomEndPercentWidth) && (percentHeight <= isl.p.zoomEndPercentHeight) && (percentHeight >= isl.p.zoomStartPercentHeight);
}

//calculate the percentWidth of the total image from the position on the canvas
isl.f.percentWidthFromWidth = function(width){
    if (isl.p.verticalSymetry) {
        return (1-isl.p.zoomEndPercentWidth) + (1-(width )*(isl.p.zoomEndPercentWidth - isl.p.zoomStartPercentWidth) / isl.p.mainImageWidth);
    }
    else return isl.p.zoomStartPercentWidth + (width )*(isl.p.zoomEndPercentWidth - isl.p.zoomStartPercentWidth) / isl.p.mainImageWidth;
}
isl.f.percentHeightFromHeight = function(height){
    if (isl.p.horizontalSymetry) {
        return (1-isl.p.zoomEndPercentHeight) + (1-height * (isl.p.zoomEndPercentHeight - isl.p.zoomStartPercentHeight) / isl.p.mainImageHeight);
    }
    else return isl.p.zoomStartPercentHeight + height * (isl.p.zoomEndPercentHeight - isl.p.zoomStartPercentHeight) / isl.p.mainImageHeight;
} 



//calculate the coordinates ont the original canvas without zoom
isl.f.realHeightFromHeight = function(height){
    return isl.p.mainImageHeight * isl.f.percentHeightFromHeight(height);
}
isl.f.realWidthFromWidth = function(width){
    return isl.p.legendLeftWidth + isl.p.mainImageWidth * isl.f.percentWidthFromWidth(width);
}






//select a default series if isl.p.currentSeries is null
isl.f.selectDefautSeries = function(){
    if (isl.p.currentSeries == null) {
        for(var sid in isl.current.series){
            isl.p.currentSeries = sid;
            break;
        }
    }
}