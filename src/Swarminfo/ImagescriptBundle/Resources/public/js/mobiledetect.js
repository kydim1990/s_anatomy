/**
 * Created by dima on 14.07.16.
 */
(function ($) {
    //detect the width on page load
    $(document).ready(function () {
        var current_width=$(window).width();
        //do something with the value here!
        if (current_width<788){
            jQuery('body').addClass("probably-mobile");
        }
    });

    //update the width value when the browser is resized (usefull foe devices which from portrait to landscape)
    $(window).resize(function (){
        var current_width=$(window).width();
        //do something with the value here!
        if (current_width<788){
            jQuery('body').addClass("probably-mobile");
        }
    });

    $(window).resize(function(){
        var current_width=$(window).width();
        //do something with the value here!
        if (current_width>788){
            jQuery('body').removeClass("probably-mobile");
        }
    });
})(jQuery);
