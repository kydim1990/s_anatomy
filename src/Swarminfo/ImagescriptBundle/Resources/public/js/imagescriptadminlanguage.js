//script to use several laguages with isl






//transform the main admin pannel into an language admin pannel
isl.a.languageadminpannel = function(){
    isl.a.languageadminpannelAdd();
    isl.a.languageadminpannelSelection();
    isl.a.languageadminpannelModify();
}



//append html on the language admin pannel to choose the language to modify
isl.a.languageadminpannelSelection = function(){
    var html = "";
    
    //get the list of languages available
    var languages = isl.current.languages;
    
    //print some data to choose the language to modify
    html += "<div id=languageadminchoose><h4>Select a language :</h4> <select id=languageadminchooseselector>";
    
    //sort the languages 
    var sortarr = [];
    for(l in languages){ sortarr.push([l, languages[l] ]) }
    sortarr.sort( function(a,b){return a[0].localeCompare(b[0]);} );
    
    for(var i in sortarr){
	var l = sortarr[i][0];
        html += "<option value='"+languages[l]+"' "+( languages[l] == isl.p.currentLanguage ? " selected" : " " )+">"+l+"</option>";
    }
    html += "</select></div>";
    $("#"+isl.p.mainAdminPannel).append(html);
    
    //when selecting an language, it is displayed a s the main language
    $("#languageadminchooseselector").change( function(e){
        var language = $(this).val();
        console.log(language);
        isl.p.currentLanguage = language;
        isl.f.resetAll();
    });
}

//append html to the language admin pannel to modify the current language
isl.a.languageadminpannelModify = function(){
    var html = "";
    //a div to display a form to modify an already existing language
    var displayedImage = isl.f.retrieveCurrentLanguage();
    html += "<form id=languageadmindelete><input  class='adminpannelDangerousButton' type=submit value='delete the current language'></form>";
    
    $("#"+isl.p.mainAdminPannel).append(html);
    
    //delete an language event
    $("#languageadmindelete").submit(function(e){
        e.preventDefault();
        //ask for confirmation
	if(!isl.a.confirm("Do you really want to delete this language ? ")) return;
	var log = "delete the language "+isl.current.languages[ isl.p.currentLanguage ];
        if (isl.p.currentLanguage in isl.current.languages) {
            delete isl.current.languages[ isl.p.currentLanguage ];
            if (isl.current.general.defaultLanguage == isl.p.currentLanguage) isl.current.general.defaultLanguage = null;
            
        }
        isl.p.currentLanguage = isl.f.retrieveCurrentLanguage();
        isl.a.saveData( isl.current ,{log:log});
    });
}


//append html to the language admin pannel to add a new language
isl.a.languageadminpannelAdd = function(){
    var html = "";
    
    //get the list of languages available
    var languages = isl.current.languages;
    
    //print a form to add a new language
    html += "<div id=languageadminadd><form id=languageadminaddform> <h4>Add a new language</h4>";
    html += "Name : <input name=name type=text id=languageadminaddname value='new language'> <br> <input type=submit value='add language' />";
    html += "</form></div>";
    
    $("#"+isl.p.mainAdminPannel).append(html);
    
    //on submit, save the new language
    $("#languageadminadd").submit(function(e){
        e.preventDefault();
        var language = $("#languageadminaddname").val();
        if (language == "") return;
        isl.current.languages[language] = language;
	var log = "adding the language : "+language;
        isl.a.saveData ( isl.current , {log:log}) ;
        
    });
}


