/**
 * Created by Diman on 15.05.2017.
 */
$( document ).ready(function() {
    $( "#btToBottom" ).click(function() {
        var btimg= $('#btToBottom_img');
        var altbtToBottom_img = btimg.attr('alt') - $('#maincanvasframe').offset().top - $('#btscroll').height() - 150;
        // console.log( 'altbtToBottom_img='+altbtToBottom_img);
        window.scrollTo(0, altbtToBottom_img);
    });
});

var old_scroll = 0;

$(window).scroll(function (event) {
    var scroll = $(window).scrollTop();
    var btscrol = $('#btscroll');
    var new_margin = parseInt(btscrol.css('margin-top')) + (scroll -old_scroll);
    // console.log( 'new_margin='+new_margin);
    btscrol.css('margin-top',new_margin+'px');
    // console.log( 'scroll='+scroll);
    old_scroll = scroll;
});

