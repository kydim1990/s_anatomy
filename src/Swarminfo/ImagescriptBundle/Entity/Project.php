<?php

namespace Swarminfo\ImagescriptBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Project
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Swarminfo\ImagescriptBundle\Entity\ProjectRepository")
 */
class Project
{

    private $premiumText = "Please login as premium login"; 
    
    public function __construct(){
        //empty json by default
	$this->data = "{}";
    } 


    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="data", type="text")
     */
    private $data;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    
    /**
     * Set id
     *
     * @param string $id
     *
     * @return Project
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }
    
    
    /**
     * Set data
     *
     * @param string $data
     *
     * @return Project
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }
    
    /**
     * Get data
     *
     * @return string
     */
    public function getData()
    {
        return $this->data;
    }
    
    //same as get data but replace the legends by "subscribe to premium"
    public function getLimitedData(){
	$data = json_decode($this->getData(),true);
	foreach($data["legends"] as $lid => $ldata){
	    foreach($ldata["text"] as $language=>$tdata){
		$data["legends"][$lid]["text"][$language] = $this->premiumText;
	    }
	    $data["legends"][$lid]["description"] = $this->premiumText;
	}
	return json_encode($data);
    }
    
    //check whether the project is free or not
    public function isFree(){
        $data = json_decode($this->getData(),true);
        return isset($data["general"]["free"]) && $data["general"]["free"] == "free";
    }

    //check whether the project is publish or not
    public function isPublish(){
        $data = json_decode($this->getData(),true);
        return isset($data["general"]["publish"]) && $data["general"]["publish"] == "publish";
    }

    //check whether the project in front page or not
    public function isOnfront(){
        $data = json_decode($this->getData(),true);
        return isset($data["general"]["onfrontpage"]) && $data["general"]["onfrontpage"] == "onfrontpage";
    }
    
    //get weight
    public function getweight(){
        $data = json_decode($this->getData(),true);
        if (is_null($data["general"]["weight"]))
            return 99999;
        return $data["general"]["weight"];
    }
    
}

