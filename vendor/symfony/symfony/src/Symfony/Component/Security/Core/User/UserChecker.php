<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Symfony\Component\Security\Core\User;

use Swarminfo\ImagescriptBundle\Controller\DefaultController;
use Swarminfo\ImagescriptBundle\SwarminfoImagescriptBundle;
use Swarminfo\UserBundle\SwarminfoUserBundle;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Exception\CredentialsExpiredException;
use Symfony\Component\Security\Core\Exception\LockedException;
use Symfony\Component\Security\Core\Exception\DisabledException;
use Symfony\Component\Security\Core\Exception\AccountExpiredException;
use Symfony\Component\Security\Core\Exception\RecaptchaException;
use Symfony\Component\Security\Core\Exception\RemovedUserException;

/**
 * UserChecker checks the user account flags.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class UserChecker implements UserCheckerInterface
{
    /**
     * {@inheritdoc}
     */
    public function checkPreAuth(UserInterface $user)
    {
//        if (isset($_POST['g-recaptcha-response']) && $_POST['g-recaptcha-response'])
//            {
//                var_dump($_POST);
//                $secret="6Lf8zyETAAAAAOQMMreQkJQkz2Wps1QNbu0Q8TJY";
//                $ip=$_SERVER['REMOTE_ADDR'];
//                $captcha=$_POST["g-recaptcha-response"];
//                $rsp=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$secret&responce=$captcha&remoteip=$ip");
//                var_dump($rsp);
//                $arr=json_decode($rsp,TRUE);
//                die($arr);
//            }
//        $val=$user->getRoles();
//        die($val[0]);
        $adminusers=array("admin","master","kenho","cuicube");
        $isadmin=in_array($user->getUsername(), $adminusers);
//        die((string)$isadmin);
        if ($isadmin && (!isset($_POST['g-recaptcha-response']) || !$_POST['g-recaptcha-response']))
        {
//            die('captcha no');
            $ex = new RecaptchaException('Recaptcha is not checked');
            $ex->setUser($user);
            throw $ex;
        }

        if (!$user instanceof AdvancedUserInterface) {
            return;
        }

        if (!$user->isAccountNonLocked() and (is_null($user->getLockedAt()) or $user->getLockedAt()>time())) {
            $ex = new LockedException('User account is locked.');
            $ex->setUser($user);
            throw $ex;
        }

        if (!$user->isEnabled()) {
            $ex = new DisabledException('User account is disabled.');
            $ex->setUser($user);
            throw $ex;
        }

        if (!$user->isAccountNonExpired()) {
            $ex = new AccountExpiredException('User account has expired.');
            $ex->setUser($user);
            throw $ex;
        }
        
        if ($user->getStatus()=='removed')
        {
            $ex = new RemovedUserException('User is removed');
            $ex->setUser($user);
            throw $ex;
        }

//        print_r($user->getStatus());
//        die();
    }

    /**
     * {@inheritdoc}
     */
    public function checkPostAuth(UserInterface $user)
    {

        if (!$user instanceof AdvancedUserInterface) {
            return;
        }

        if (!$user->isCredentialsNonExpired()) {
            $ex = new CredentialsExpiredException('User credentials have expired.');
            $ex->setUser($user);
            throw $ex;
        }
    }
}
