<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FOS\UserBundle\Controller;

use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use FOS\UserBundle\Model\UserInterface;
use Swarminfo\UserBundle\Entity\Fieldsregister;
use Swarminfo\UserBundle\Entity\Plandevice;
use Swarminfo\UserBundle\Entity\Devices;

/**
 * Controller managing the registration
 *
 * @author Thibault Duplessis <thibault.duplessis@gmail.com>
 * @author Christophe Coevoet <stof@notk.org>
 */
class RegistrationController extends Controller
{
    private function cleare_removeduser(){
        $userManager = $this->getDoctrine()->getManager()->getRepository('SwarminfoUserBundle:User');
        $removedusers=$userManager->findBy(array("status"=>'removed'));
        if (count($removedusers) > 0){
            $repository = $this->getDoctrine()->getManager()->getRepository('SwarminfoUserBundle:Fieldsregister');
            $em = $this->getDoctrine()->getManager();
            foreach ( $removedusers as $ruser){
//                print_r($ruser->getid());

                $fieldsreg=$repository->findOneBy(array('id' => $ruser->getId()));
                if ($fieldsreg){
                    $em->remove($fieldsreg);
                }
                $em->remove($ruser);

            }
            $em->flush();
        }
    }

    private function sendemailregistration($usr){
//        $userManager = $this->get('fos_user.user_manager');
//        $usr=$userManager->findUserBy(array("id"=>207));

        $emailFrom=$this->container->getParameter('mailer_user');
        $emailFromName=$this->container->getParameter('mailer_name');

        $twig = clone $this->get('twig');
        $template = $twig->loadTemplate('SwarminfoImagescriptBundle::email.txt.twig');
        $subject = $template->renderBlock('subject',  array('user' => $usr));
        $body_text = $template->renderBlock('body_text', array('user' => $usr));

        $message = \Swift_Message::newInstance()
            ->setSubject( $subject)
            ->setFrom(array($emailFrom => $emailFromName))
//            ->setTo('kydim1312@yandex.ru')
            ->setTo($usr->getEmail())
            ->setBody( $body_text )
        ;
        $this->get('mailer')->send($message);
    }

    public function registerAction(Request $request)
    {
        $this->cleare_removeduser();

        /** @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface */
        $formFactory = $this->get('fos_user.registration.form.factory');
        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $user = $userManager->createUser();
        $user->setEnabled(true);

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        $form = $formFactory->createForm();
        $form->setData($user);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $event = new FormEvent($form, $request);
            $dispatcher->dispatch(FOSUserEvents::REGISTRATION_SUCCESS, $event);

            $userManager->updateUser($user);

            if (null === $response = $event->getResponse()) {
                $url = $this->generateUrl('fos_user_registration_confirmed');
                $response = new RedirectResponse($url);
            }

            $dispatcher->dispatch(FOSUserEvents::REGISTRATION_COMPLETED, new FilterUserResponseEvent($user, $request, $response));

//            $var=$_POST["_firstname"];
//            print_r($_POST["_receive1"]);
//            die();
            $isphys=false;
            if ($_POST["_isphysician"]=='isphysician_yes') $isphys=true;
            if ($_POST["_receive1"]=="on") $res1=true;
            else $res1=false;
            if ($_POST["_receive2"]=="on") $res2=true;
            else $res2=false;

//            $fieldreg=new Fieldsregister($user->getId(),$_POST["_title"],$_POST["_firstname"],$_POST["_lastname"]);
            $fieldreg=new Fieldsregister($user->getId(),$_POST["_title"],$_POST["_firstname"],$_POST["_lastname"],
                $_POST["_profession"], $isphys,$_POST["_organization"],$_POST["_organizationtype"],
                $_POST["_country"],$_POST["_addresline1"],$_POST["_addresline2"],$_POST["_city"],
                $_POST["_postcode"],$_POST["_region"],$_POST["_phone"],$res1,$res2, $_POST["_promo_code"]
                );

//            check promo code
            $promo = $this->getDoctrine()->getManager()->getRepository('SwarminfoUserBundle:Promo');
            $promo_code = $promo->findOneBy(array('promocode'=> $_POST["_promo_code"]));
            $is_used_promo = false;


            if (!is_null($promo_code) and $promo_code->getExperied()>time() and ($promo_code->getIsunlimited() or !$promo_code->getIsused())
                and (!is_null($promo_code->getStartfrom()) and $promo_code->getStartfrom() > time() or is_null($promo_code->getStartfrom()) )
                )
            {
                $user->setExpire($promo_code->getExperied());

                $promo_code->setIsused(1);
                $em = $this->getDoctrine()->getManager();
                $em->persist($promo_code);


                //plandevice
                $request = $this->get('request');
                $locale = $request->getLocale();
                $repository_plan = $this->getDoctrine()->getManager()->getRepository('SwarminfoUserBundle:Plandevice');
                $userplan=$repository_plan->findOneBy(array('id' => $user->getId(),'lang'=>$locale));
                if ($userplan)
                {
                    $userplan->setCountdevice($this->Devices_plan[$_GET['plan']]);
                }
                else
                    $userplan=new Plandevice($user->getId(), 1, $locale);
                $em->persist($userplan);

//                add device
                $repository_devices = $this->getDoctrine()->getManager()->getRepository('SwarminfoUserBundle:Devices');
                $session = $this->get('session');
                $sesdev=$repository_devices->findOneBy(array('device' => $session->getId()));
                if (!$sesdev)
                {
                    //insert device
                    $userdevice=new Devices($user->getId(),$session->getId(),$locale);
                    $em->persist($userdevice);
//                    $em->flush();
                }

                $em->flush();
                $is_used_promo = true;
                

            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($fieldreg);
            // actually executes the queries (i.e. the INSERT query)
            $em->flush();


            if (!$is_used_promo){
                $user->setStatus('removed');
                $em = $this->getDoctrine()->getManager();
                $em->persist($user);

                $em->flush();
            }
            else{
                $this->sendemailregistration($user);
            }

            if ($this->container->getParameter('is_use_paypall') == 1 and !$is_used_promo)
                return $this->redirect( $this->generateUrl('swarminfo_imagescript_paypalgetpremium', array("plan"=>$_POST['plan'],"amount"=>$_POST['amount'] )) );
            else
                return $this->redirect($this->generateUrl('swarminfo_imagescript_page1'));
//                return $this->redirect( $this->generateUrl('fos_user_registration_check_email', array("plan"=>$_POST['plan'],"amount"=>$_POST['amount'] )) );

        }

        return $this->render('FOSUserBundle:Registration:register.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * Tell the user to check his email provider
     */
    public function checkEmailAction()
    {
        $email = $this->get('session')->get('fos_user_send_confirmation_email/email');
        $this->get('session')->remove('fos_user_send_confirmation_email/email');
        $user = $this->get('fos_user.user_manager')->findUserByEmail($email);

        if (null === $user) {
            throw new NotFoundHttpException(sprintf('The user with email "%s" does not exist', $email));
        }

//        $userManager = $this->get('fos_user.user_manager');
//        $dispatcher = $this->get('event_dispatcher');
//
//        $user->setConfirmationToken(null);
//        $user->setEnabled(true);
//
////        $event = new GetResponseUserEvent($user, $request);
////        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_CONFIRM, $event);
//
//        $userManager->updateUser($user);

//        print_r($user);
//        die();

//        return $this->redirect( $this->generateUrl('swarminfo_imagescript_paypalgetpremium', array("plan"=>$_POST['plan'],"amount"=>$_POST['amount'] )) );

        return $this->render('FOSUserBundle:Registration:checkEmail.html.twig', array(
            'user' => $user,
        ));
    }

    /**
     * Receive the confirmation token from user email provider, login the user
     */
    public function confirmAction(Request $request, $token)
    {
        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');

        $user = $userManager->findUserByConfirmationToken($token);

        if (null === $user) {
            throw new NotFoundHttpException(sprintf('The user with confirmation token "%s" does not exist', $token));
        }

        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $user->setConfirmationToken(null);
        $user->setEnabled(true);

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_CONFIRM, $event);

        $userManager->updateUser($user);

        if (null === $response = $event->getResponse()) {
            $url = $this->generateUrl('fos_user_registration_confirmed');
            $response = new RedirectResponse($url);
        }

        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_CONFIRMED, new FilterUserResponseEvent($user, $request, $response));

        return $response;
    }

    /**
     * Tell the user his account is now confirmed
     */
    public function confirmedAction()
    {
        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        return $this->render('FOSUserBundle:Registration:confirmed.html.twig', array(
            'user' => $user,
            'targetUrl' => $this->getTargetUrlFromSession(),
//            "plan"=>$_GET['plan'],
//            "amount"=>$_GET['amount']
        ));


    }

    private function getTargetUrlFromSession()
    {
        // Set the SecurityContext for Symfony <2.6
        if (interface_exists('Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface')) {
            $tokenStorage = $this->get('security.token_storage');
        } else {
            $tokenStorage = $this->get('security.context');
        }

        $key = sprintf('_security.%s.target_path', $tokenStorage->getToken()->getProviderKey());

        if ($this->get('session')->has($key)) {
            return $this->get('session')->get($key);
        }
    }
}
