//functions to manage the images on the left trackdown

//define some parameters

isl.p.lefttrackdownContainer = "lefttrackdown";

//the width of the images on the left trackdown
isl.p.trackdownwidth = 100;

//load the trackdown images to be printed later
isl.p.trackdownimages = {};
//the number of images inside the trackdownimages
isl.p.trackdownnumber = 0;


//on start all, load the images to be displayed at the bottom band
isl.p.onStart.push("isl.f.reloadLeftTrackDownImages");
//on reset all, reset the image with the bottom band
isl.p.onResetAll.push("isl.f.resetLeftTrackDown");



//reset the left trackdown
isl.f.resetLeftTrackDown = function(){
    //create a canvas on the bottom band
    var trackdown = $("#"+isl.p.lefttrackdownContainer);
    //create a canvas inside the main container
    trackdown.html("");
    //print the images
    isl.f.printLeftTrackDownImages();

}

//print all the trackdown images
isl.f.printLeftTrackDownImages = function(){
    var images = isl.f.getTrackdownImages();
    //expect an object on the form : id => {url, name}
    for(var iid in images){
        if (!(iid in isl.p.trackdownimages)) continue;
        var img = isl.p.trackdownimages[iid];
        $("#"+isl.p.lefttrackdownContainer).append("<canvas id=trackdown"+iid+" tid="+iid+" class=trackdowncanvas></canvas>");
        var imgWidth = isl.p.trackdownwidth;
        var imgHeight = img.height * imgWidth / img.width;
        var c=document.getElementById("trackdown"+iid);
        c.width = imgWidth;
        c.height = imgHeight;
        var ctx=c.getContext("2d");
        
        ctx.drawImage(img, 0 ,0 , imgWidth , imgHeight);
        
        //try to print the position of the current image if specified
        if (!("trackdown" in isl.current.images[isl.currentImage])) continue;
        if (!(iid in isl.current.images[isl.currentImage].trackdown)) continue;
        var startWidth = 0;
        var endWidth = c.width;
        var startHeight = isl.current.images[isl.currentImage].trackdown[iid].percentHeight * c.height;
        var endHeight = startHeight;
        ctx.strokeStyle = isl.p.bottomBandCursorColor;
        ctx.lineWidth = isl.p.bottomBandCursorWidth;
        //draw
        ctx.beginPath();
        ctx.moveTo( startWidth, startHeight);
        ctx.lineTo( endWidth , endHeight );
        ctx.stroke();
    }
}


//reload and store the images of the left trackdown
isl.f.reloadLeftTrackDownImages = function(){
    isl.p.trackdownimages = {};
    var images = isl.f.getTrackdownImages();
    isl.p.trackdownnumber = 0;
    for (var iid in images) isl.p.trackdownnumber ++;
    for (var iid in images){
        var img = new Image();
        img.src = images[iid].url;
        var createFunction = function(iid){
            return function(){
                isl.p.trackdownimages[iid] = this;
                var alreadyLoaded = 0;
                for (var i in isl.p.trackdownimages) alreadyLoaded++;
                if (isl.p.trackdownnumber == alreadyLoaded) isl.f.resetAll();
            }
        }
        img.onload = createFunction(iid);
    }
}


//get the object of the trackdown images available
isl.f.getTrackdownImages = function(){
    return "trackdown" in isl.current ? isl.current.trackdown : {};
}
