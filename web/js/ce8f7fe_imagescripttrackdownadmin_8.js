//script to manage trackdown as an admin

//when reseting all, add event to change the position of the cursor on the trackdowns
isl.p.onResetAll.push("isl.f.resetTrackdownEvent");


//transform the main admin pannel into a trackdown admin pannel
isl.a.trackdownadminpannel = function(){
    isl.a.trackdownadminpannelAdd();
    isl.a.trackdownadminpannelRemove();
    
}

//append html to the image admin pannel to modify the current image
isl.a.trackdownadminpannelRemove = function(){
    var html = "";
    var trackdowns = isl.f.getTrackdownImages();
    html += "<div id=trackdowndelete>"
    html += "<form id=trackdonwdeleteform>"
    html += "<h4>delete a track-down :</h4> <select id=trackdownimageselection><option value='' >select a trackdown</option>";
    for(var tid in trackdowns){
        html += "<option value='"+tid+"'  >"+trackdowns[tid].text+"</option>";
    }
    html += "</select> <input  class='adminpannelDangerousButton' type=submit value='delete the trackdown'><br></form>";
    html += "</div>";
    
    $("#"+isl.p.mainAdminPannel).append(html);
    
    //delete an image event
    $("#trackdonwdeleteform").submit(function(e){
        e.preventDefault();
        //ask for confirmation
	if(!isl.a.confirm("do you really want to delete this track-down ? ")) return;
        //delete the current image
        var trackdowns = isl.f.getTrackdownImages();
        var tid = $("#trackdownimageselection").val();
        if (tid in trackdowns) {
            delete isl.current.trackdown[tid];
            isl.a.saveData( isl.current );
        }
    });
    
}


//append html to the image admin pannel to add a new image
isl.a.trackdownadminpannelAdd = function(){
    var html = "";
    
    //get the list of images available
    //get the images for all the series
    var images = isl.p.getSortedImage(null);
    
    //print a form to add a new image
    html += "<div id=trackdownadminadd><form id=trackdownadminaddform> <h4>add a new trackdown</h4>";
    html += "image of the new trackdown : <input name=file type=file id=trackdownadminaddfile><br> ";
    html += "name of the new trackdown  : <input type=text id=trackdownadminaddtext placeholder='name of the new trackdown'><br>";
    html += "<input type=submit value='add the trackdown' />";
    html += "</select></form></div><br>";
    
    $("#"+isl.p.mainAdminPannel).append(html);
    
    //on submit, save the new image
    $("#trackdownadminaddform").submit(function(e){
        e.preventDefault();
        var input = document.getElementById("trackdownadminaddfile");
        //if no file has been provided,don't do anything
        //same if the file isn't an image
        try{
            if(!input.files[0].type.match(/image.*/)) return;
        }
        catch(e){
            return;
        }
        
        //build a new image with a new name
        var id = isl.f.generateId();
        var names = ""+ id + isl.a.inputFileName(input);
        var name = names[0];
        var text = $("#trackdownadminaddtext").val();
        var ressource = {
            input : input,
            imagename : name,
        }
        if (!("trackdown" in isl.current)) {
            isl.current.trackdown = {};
        }
        isl.current.trackdown[id] = {
            id : id,
            url : "/bundles/swarminfoimagescript/ressources/"+isl.current.id+"/"+name,
            text : text
        };
        //change the main image to be displayed :
        isl.currentImage = id;
        isl.a.saveData( isl.current, ressource , isl.f.reloadLeftTrackDownImages);
        //reload and store the images of the left trackdown
        isl.f.reloadLeftTrackDownImages();
    });
}



//event to change the position of cursors for trackdowns
isl.f.resetTrackdownEvent = function(){  
    $(".trackdowncanvas").unbind('.cursorevent').bind("click",function(e){
        if (isl.currentImage == null) return;
        if (!("trackdown" in isl.current.images[isl.currentImage])) isl.current.images[isl.currentImage].trackdown = {};
        
        var tid = $(this).attr("tid");
        var id = $(this).attr("id");
        var c=document.getElementById( id );
        var ctx=c.getContext("2d");
        
        var canvasX = e.pageX - $(this).offset().left;
        var canvasY = e.pageY - $(this).offset().top;
	//compute the percent width and height
	var percentWidth = (canvasX) / ctx.canvas.clientWidth;
	var percentHeight = (canvasY) / ctx.canvas.clientHeight;
        
        isl.current.images[isl.currentImage].trackdown[tid]={
            percentWidth : percentWidth,
            percentHeight: percentHeight,
            id: tid
        }
        isl.a.saveData( isl.current );
    });
    
}



