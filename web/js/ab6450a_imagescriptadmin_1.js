//object containing data relative to the administration
isl.a = {};

//this variable should contain the id of the current tag beeing processed
isl.currentLegend = null;
//the current anatomical structure being modified
isl.currentStructure = null;

//when resetting everything, reset the admin pannel
isl.p.onResetAll.push("isl.a.generateAdminPannel");

//set the admin pannel to display : by default, it is the imageadminpannel
isl.a.pannelDisplayed = "imageadminpannel";


//display the pannel referenced in isl.a.pannelDisplayed
isl.a.displaySelectedPannel = function(){
    //change the css of the top admin pannel buttons
    //$(".adminPannelButton").css("color" , "black").css("font-size","auto").css("text-decoration","none");
    $(".adminPannelButtonSelected").removeClass("adminPannelButtonSelected");
    $("[fclick="+isl.a.pannelDisplayed+"]").addClass("adminPannelButtonSelected"); //.css("color" , "black").css("font-size","x-large").css("text-decoration","underline");
    //reset the html of the main pannel
    var html = "";
    //$("#"+isl.p.mainAdminPannel).html(html);
    $("#"+isl.p.mainAdminPannel).children().remove();;
    //display the main pannel
    isl.a[ isl.a.pannelDisplayed ]();
}

//generate the admin pannel
isl.a.generateAdminPannel = function(){
    //build the top menu of the admin pannel
    isl.a.buildTopAdminPannel();
    //open the image admin pannel by default
    isl.a.displaySelectedPannel();
}


//buffer containing the description about the next operation


//function to persist a given data object
isl.a.saveData = function(data , ressource , callback){ //console.log("caller is " + arguments.callee.caller.toString());
    var id = data.id;
    //console.log("savedata");
    //build the form data to send
    var formData= new FormData();
    formData.append("data",JSON.stringify(data));
    formData.append("id",id);
    
    //if a ressource has been defined, save it too
    //a ressource should contain : an input element with a file inside, a name where to store the file
    if(ressource != null && ("input" in ressource) ){
        var i=0;
        while(i in ressource.input.files){
            var file = ressource.input.files[i];
            formData.append("image"+i, file);
            formData.append("imagename"+i,ressource.imagename[i]);
            i++;
        }
    }
    
    //perform deletion actions if it has been defined too
    if(ressource != null && ("delete" in ressource) ){
        formData.append("delete", ressource.delete);
    }
    
    //append a log message if there is one
    if (ressource != null && ("log" in ressource)) {
        formData.append("log",ressource.log);
    }
    var str = JSON.stringify(data);
    
    //change the cursor
    $('html, body').css("cursor", "wait");
     
    var jqxhr = $.ajax({
        url: "/savedata",
        method: "POST",
        data: formData,
        processData: false,
        contentType: false,
        type: "POST",
        })
        .done(function(data,textStatus,jqxhr) {

            //reset the main admin pannel
            isl.f.resetAll();
            if(callback != null) callback();
        })
        .fail(function() {
            alert( "the last update failed : refresh the page to check if you are still logged." );
        })
        .always(function() {
            $('html, body').css("cursor", "auto");
        });

}


//build the top admin pannel, with all the actions available
isl.a.buildTopAdminPannel = function(){
    var html = "";
    
    //add button with a given fclick parameter
    //when clicked, the main admin pannel is built using that admin function
    
    
    //add a button for the general informations
    html += "<a href=# class=adminPannelButton fclick=generaladminpannel>general</a>&nbsp;&nbsp;";
    
    //add a button for the language informations
    html += "<a href=# class=adminPannelButton fclick=languageadminpannel>languages</a>&nbsp;&nbsp;";
    
    //add a button for the series informations
    html += "<a href=# class=adminPannelButton fclick=seriesadminpannel>series</a>&nbsp;&nbsp;";
    
    //add a button for the images
    html += "<a href=# class=adminPannelButton id=imageadminpannelbutton fclick=imageadminpannel>images</a>&nbsp;&nbsp;";

	    //add a button for the anatomical structures
    html += "<a href=# class=adminPannelButton fclick=structureadminpannel>legends</a>&nbsp;&nbsp;";

    //add a button for the legends
    html += "<a href=# class=adminPannelButton fclick=legendadminpannel>structures</a>&nbsp;&nbsp;";
    
   
    
    //add a button for the logs
    html += "<a href=# class=adminPannelButton fclick=logadminpannel>logs</a>&nbsp;&nbsp;";
    
    //add a button for the left side trackdown
    // html += "<a href=# class=adminPannelButton fclick=trackdownadminpannel>left trackdown</a>&nbsp;&nbsp;";
    
    
    
    //insert those links, and binds events on it
    $("#"+isl.p.topAdminPannel).html( html )
    .children().bind("click",function(e){
        e.preventDefault();
        //select the right pannel to display
        isl.a.pannelDisplayed = $(this).attr("fclick");
        //display the appropriate pannel
        isl.a.displaySelectedPannel();
    })
    
}


//function to demand confirmation : return true if it was accepted, false in other case
isl.a.confirm = function(message){
    var r = confirm(message);
    if (r == true) {
        return true;
    } else {
        return false;
    }
}



    
    
