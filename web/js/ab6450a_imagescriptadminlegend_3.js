//script to manage the legends as an admin

//transform the main admin pannel into a legend admin pannel
isl.a.legendadminpannel = function(){
    isl.a.legendadminpannelAdd();
    isl.a.legendadminpannelSelection();
    isl.a.legendadminpannelModify();
    isl.a.legendadminpannelAddLegendOnClick();
}

isl.a.legendadminpannelSelection = function(){
    var html = "";
    
    //get the list of images available
    var legends = isl.current.legends;
    
    //print some data to choose the image to modify
    html += "<div id=legendadminchoose><h4>Select a structure to edit</h4> Current Structure : <select id=legendadminchooseselector>";
    
    //sort the legends 
    var sortarr = [];
    for(var i in legends){ sortarr.push([isl.f.getLegendText(i), i ]) }
    sortarr.sort( function(a,b){return a[0].localeCompare(b[0]);} );
    
    for(var e in sortarr){
	var i = sortarr[e][1];
        html += "<option value="+legends[i].id+( legends[i].id == isl.currentLegend ? " selected" : " " )+">"+isl.f.getLegendText(i)+"</option>";
    }
    html += "</select></div>";
    $("#"+isl.p.mainAdminPannel).append(html);
    
    //when selecting an image, it is displayed a s the main image
    $("#legendadminchooseselector").change( function(e){
        var legendId = $(this).val();
        console.log(legendId);
        isl.currentLegend = legendId;
        isl.f.resetAll();
    });
}


//append html to the legend admin pannel to modify the current legend
isl.a.legendadminpannelModify = function(){
    var html = "";
    //display a form to affect the legend to an anatomical structure
    if (isl.currentLegend == null ) isl.currentLegend = isl.a.retrieveLegendToDisplay();
    //a div to display a form to modify an already existing image
    var displayedLegend = isl.a.retrieveLegendToDisplay();
    html += "<div id=legendadminmodify>"
    if ( "text" in displayedLegend ) {
        
        
	
        var currentStructure = isl.current.legends[isl.currentLegend].structure;
        html += "<form id=legendadminupdate><h4>Edit the current structure : </h4> Select a structure : <select id=legendadminStructureSelector><option value='' >none</option>";
        for(var s in isl.current.structures) html += "<option value="+s+" "+(currentStructure == s ? "selected" : "")+">"+isl.current.structures[s].text+"</option>";
        html +="</select><br>";
        
        html += "Structure Name : <br>";
	for(var l in isl.current.languages){
	    html += "&nbsp;&nbsp;&nbsp;&nbsp;"+l+" : <input class=LegendLanguageTranslation language='"+l+"' type=text value='"+ isl.f.getLegendText(isl.currentLegend,l) +"'><br>";
	}
	//html for the description
	html += "Structure Description : <br><textarea id=legendadminupdateDescription>"+("description" in displayedLegend ? displayedLegend.description : "")+"</textarea><br>";
	
	html += "<input type=submit value='update structure'></form><br>";
        html += "<form id=legendadminremovefromimage><input class='adminpannelDangerousButton' type=submit value='remove the structure from the current image'></form><br>";
	html += "<form id=legendadmindelete><input  class='adminpannelDangerousButton' type=submit value='delete the current structure'></form>";
    }
    
    else html += "no structure selected";
    html += "</div>";
    
    $("#"+isl.p.mainAdminPannel).append(html);
    
    //delete an legend event
    $("#legendadmindelete").submit(function(e){
        e.preventDefault();
	//ask for confirmation
	if(!isl.a.confirm("Do you really want to delete this structure ? ")) return;
	
        //delete the current image
        if (isl.currentLegend in isl.current.legends) {
	    var log = "delete the structure : "+isl.f.getLegendText(isl.currentLegend,isl.p.currentLanguage);
            delete isl.current.legends[ isl.currentLegend ];
            //remove the legend from all images
            for (var i in isl.current.images){
                if ( "legends" in isl.current.images[i] && isl.currentLegend in isl.current.images[i].legends ) {
                    delete isl.current.images[i].legends[isl.currentLegend];
                }
            }
        }
        isl.currentLegend = null;
        isl.a.saveData( isl.current , {log:log});
    });
    
    //remove a legend from the current image
    $("#legendadminremovefromimage").submit(function(e){
        e.preventDefault();
        //delete the current image
        if ("legends" in isl.current.images[ isl.currentImage ] && isl.currentLegend in  isl.current.images[ isl.currentImage ].legends ) {
	    var log = "remove the structure '"+isl.f.getLegendText(isl.currentLegend,isl.p.currentLanguage)+"' from image "+isl.currentImage;
            delete isl.current.images[ isl.currentImage ].legends[ isl.currentLegend ];
        }
        isl.a.saveData( isl.current, {log:log} );
    });
    
    //change the text of the legend
    $("#legendadminupdate").submit(function(e){
        e.preventDefault();
        //update the current image
        if (isl.currentLegend in isl.current.legends) {
	    //update all the legend title
	    $(".LegendLanguageTranslation").each(function(){
		var newText = $(this).val();
		var language = $(this).attr("language");
		console.log(language, newText);
		isl.current.legends[ isl.currentLegend ].text[language] = newText;
	    })

	    //update the structure
            var newStructure = $("#legendadminStructureSelector").val();
            isl.current.legends[ isl.currentLegend ].structure = newStructure;
	    
	    //update the description
	    var newDescription = $("#legendadminupdateDescription").val();
            isl.current.legends[ isl.currentLegend ].description = newDescription;
	    
	    var log = "update the structure : '"+isl.f.getLegendText(isl.currentLegend,isl.p.currentLanguage)+"'";
        }
        isl.a.saveData( isl.current , {log:log});
    });
}


//append html to the image admin pannel to add a new image
isl.a.legendadminpannelAdd = function(){
    var html = "";
    
    //get the list of images available
    var images = isl.p.getSortedImage();
    
    //print a form to add a new legend
    html += "<div id=legendadminadd><form id=legendadminaddform> <h4>add a new structure : </h4>";
    html += "<input type=hidden name=newLegendId value="+isl.f.generateId()+" />";
    html += "<input type=submit value='add the structure' />";
    html += "</select></form></div>";
    
    $("#"+isl.p.mainAdminPannel).append(html);
    
    //on submit, save the new image
    $("#legendadminaddform").submit(function(e){
        e.preventDefault();
        var newLegendId = $("[name=newLegendId]").val();
        //add a new legend
        isl.current.legends[ newLegendId ] = {
            text : {},
            id : newLegendId,
            structure : "",
        };
	//add a default text for all the languages
	for(var l in isl.current.languages){
	    isl.current.legends[ newLegendId ].text[l] = "default structure";
	}
        //focus the admin pannel on the nex legend
        isl.currentLegend = newLegendId;
        var log = "adding a new structure";
        isl.a.saveData( isl.current, {log:log} );
    });
}

//add a click event : on click, add the current legend on the current image
isl.a.legendadminpannelAddLegendOnClick = function(){ //alert("la  ???? ");
    $("#"+isl.p.mainImageContainer).unbind("click.legendadminpannelAddLegendOnClick")
    .bind("click.legendadminpannelAddLegendOnClick" , function(e){
        if ( isl.a.pannelDisplayed != "legendadminpannel") return;
        if (isl.currentLegend == null) return;
        var canvasX = e.pageX - $(this).offset().left;
        var canvasY = e.pageY - $(this).offset().top;
	//compute the percent width and height
	var percentWidth = isl.f.percentWidthFromWidth(canvasX - isl.p.legendLeftWidth); //(canvasX - isl.p.legendLeftWidth) / isl.p.mainImageWidth;
	var percentHeight = isl.f.percentHeightFromHeight(canvasY); //(canvasY ) / isl.p.mainImageHeight;
	//if (isl.p.verticalSymetry)  percentWidth = 1 - percentWidth;
	//if (isl.p.horizontalSymetry)  percentHeight = 1 - percentHeight;
        //add a the current legend to the current image
        if (!("legends" in isl.current.images[ isl.currentImage ])) isl.current.images[ isl.currentImage ].legends = {};
	isl.current.images[ isl.currentImage ].legends[ isl.currentLegend ] = {
            percentWidth : percentWidth,
            percentHeight : percentHeight,
            id : isl.currentLegend, 
        };
	var log = "update position of structure : '"+isl.f.getLegendText(isl.currentLegend,isl.p.currentLanguage)+"' on image "+isl.currentImage;
        //Save the current data
        isl.a.saveData( isl.current , {log:log});
    });
}



//retrieve the current legend
isl.a.retrieveLegendToDisplay = function(legend){
    //if no image is provided, find the first image
    if (legend != null && (legend != undefined) ) return legend;
    if (isl.currentLegend != null && (isl.currentLegend in isl.current.legends) ) {
        return  isl.current.legends[isl.currentLegend];
    }
    else{
        //pick a random legend
        for(var l in isl.current.legends) return l;
    }
    //by default, return an empty object
    return {};
}

