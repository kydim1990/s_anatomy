//script to administrate the anatomical structures



//transform the main admin pannel into a structures admin pannel
isl.a.structureadminpannel = function(){
    //reset the current structure if null
    if(isl.currentStructure == null) {
	isl.currentStructure = isl.a.retrieveStructureToDisplay();
	if (isl.currentStructure != null)  isl.currentStructure = isl.currentStructure.id;
    }
    
    isl.a.structureadminpannelAdd();
    isl.a.strucutreadminpannelSelection();
    isl.a.structureadminpannelDraw();
    isl.a.structureadminpannelModify();
    
}


isl.a.strucutreadminpannelSelection = function() { 
    var html = "";
    
    //get the list of images available
    var structures = isl.current.structures;
    
    //print some data to choose the image to modify
    html += "<div id=structureadminchoose>select an anatomical structure : <select id=structureadminchooseselector>";
    for(var i in structures){
        html += "<option value="+structures[i].id+( structures[i].id == isl.currentStructure ? " selected" : " " )+">"+structures[i].text+"</option>";
    }
    html += "</select></div>";
    $("#"+isl.p.mainAdminPannel).append(html);
    
    //when selecting a structure, it is displayed  
    $("#structureadminchooseselector").change( function(e){
        var structureId = $(this).val();
        isl.currentStructure = structureId;
        isl.f.resetAll();
    });
}

//enable the admin to draw on the admin pannel
isl.a.structureadminpannelDraw = function(){
    var html = "you can draw on the canvas by clicking and moving the mouse : ";
    html += "<form id=removeDrawingStructure><input type=submit value='click here to remove the drawing'></form>";
    
    var $container = $("#"+isl.p.mainImageContainer);
    
    $("#"+isl.p.mainAdminPannel).append(html);
    
    $container.unbind(".drawing").bind("mousedown.drawing",function(e){
	//reset the list of points created by the user
	isl.p.createdPoints = [];
	//reset the mousemove event
	$(this).unbind(".draw");
	
	$(this).on("mousemove.draw",function(e){ 
	    var $canvas = $("#"+isl.p.mainImageCanvas);
	    var percentWidth = (e.pageX - $($canvas).offset().left - isl.p.legendLeftWidth)/isl.p.mainImageWidth;
	    var percentHeight = (e.pageY - $($canvas).offset().top) / isl.p.mainImageHeight;
	    isl.p.createdPoints.push( [percentWidth, percentHeight] );
	});
	
	//reset the mouse up event
	$(this).on("mouseup.draw",function(e){
	    if( !(isl.currentStructure in isl.current.structures)) return;
	    //save the points
	    isl.current.structures[isl.currentStructure].draw[ isl.currentImage ]  = isl.f.filterDrawingPoints( isl.p.createdPoints );
	    console.log(isl.p.createdPoints);
	    console.log(isl.current.structures[isl.currentStructure]);
	    isl.a.saveData( isl.current );
	    isl.f.resetAll();
	    isl.p.createdPoints = [];
	});
    });
}

//function to filter the points to save when saving the drawing of the user
//remove points that are too close of one another
isl.f.filterDrawingPoints = function(points){
    var result = [];
    var npoints = 20;
    if (points.length<3) return result;
    if (points.length<npoints) return points;
    
    for(var p = 0 ; p<npoints; p++ ){
	result.push( points[ Math.floor(p*points.length/npoints) ] );
    }
    return result;
}


isl.a.structureadminpannelModify = function(){
    var html = "";
    //a div to display a form to modify an already existing image
    var displayedStructure = isl.a.retrieveStructureToDisplay();
    html += "<div id=structureadminmodify>"
    if ( "text" in displayedStructure ) {
        html += "<form id=structureadmindelete><input type=submit value='delete the current anatomical structure'></form>";
        html += "<form id=structureadminupdate>name : <input id=structureadminupdateText type=text value='"+ displayedStructure.text +"'><br>";
        //add a form to pick the color of the anatomical group
        html += "color : <select id=structureadminupdateColor><option disabled>choose a color</option>";
        var previousColor = displayedStructure.color;
        for(var c in isl.colors){ 
            html += "<option value="+isl.colors[c]+" style='color:"+isl.colors[c]+"' "+(previousColor == isl.colors[c] ? "selected" : "")+">"+isl.colors[c]+"</option>";
        }
        html += "</select><input type=submit value='update the structure'></form>";
    }
    else html += "no structure selected";
    html += "</div>";
    
    $("#"+isl.p.mainAdminPannel).append(html);
    
    //delete an legend event
    $("#structureadmindelete").submit(function(e){
        e.preventDefault();
        //delete the current image
        if (isl.currentStructure in isl.current.structures) {
            delete isl.current.structures[ isl.currentStructure ];
            //remove the structure from all legends
            for (var i in isl.current.legends){
                if ( "structure" in isl.current.legends[i] && isl.current.legends[i].structure == isl.currentStrcture ) {
                    isl.current.legends[i].structure = "";
                }
            }
        }
        isl.currentStructure = null;
        isl.a.saveData( isl.current );
    });
    
    //change the text of the legend
    $("#structureadminupdate").submit(function(e){
        e.preventDefault();
	console.log(isl.currentStructure);
	console.log(isl.current.structures);
        if (isl.currentStructure in isl.current.structures) {
	    //update the text of the structure
            var newText = $("#structureadminupdateText").val();
            isl.current.structures[ isl.currentStructure ].text = newText;
	    //update the color
            var newColor = $("#structureadminupdateColor").val();
            isl.current.structures[ isl.currentStructure ].color = newColor;
        }
        isl.a.saveData( isl.current );
    });
}

isl.a.structureadminpannelAdd = function(){
    var html = "";
    
    //print a form to add a new legend
    html += "<div id=strctureadminadd><form id=structureadminaddform> add a new anatomical structure";
    html += "<input type=hidden name=newStructureId value="+isl.f.generateId()+" />";
    html += "<input type=submit value=add />";
    html += "</select></form></div><br><br>";
    
    $("#"+isl.p.mainAdminPannel).append(html);
    
    //on submit, save the new image
    $("#structureadminaddform").submit(function(e){
        e.preventDefault();
        var newStructure = $("[name=newStructureId]").val();
        //add a new legend
        isl.current.structures[ newStructure ] = {
            text : "new structure name",
            id : newStructure,
            color : "white",
	    draw: {},
        };
        //by default, display the structure
        isl.p.displayedStructures.push(newStructure);
        
        //focus the admin pannel on the nex legend
        isl.currentStructure = newStructure;
        
        isl.a.saveData( isl.current );
    });
}




//retrieve the current structure object
isl.a.retrieveStructureToDisplay = function(structure){
    //if no structure is provided, find the first structure
    if (structure != null && (structure != undefined) ) return structure;
    if (isl.currentStructure!= null && (isl.currentStructure in isl.current.structures) ) {
        return isl.current.structures[isl.currentStructure];
    }
    else{
        //pick a random legend
        for(var l in isl.current.structures) return isl.current.structures[l];
    }
    //by default, return an empty object
    return {};
}

