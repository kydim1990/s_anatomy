var canvas = document.getElementById("skeletonCanvas");
var ctx;


//There you can change parameters
// fontsize
var fontsize = 26;
// fontstyle
var fontstyle = 'arial';

var font = fontsize + 'px '+fontstyle;
//color of text of legends
var fillStyle = "#7d927f";
//color of lines
var strokeStyle = "#7d927f";
// height of link
var linkHeight = fontsize;
// deference betweeen line and legend axis Y
var diff_y_line = 10;

var inLink = false;
var redirect_link = 'projects_group/';

var tags = {};

/*
There you can add, update or delete legends
sturccture:
tags['name'] = {'text': 'value_text', 'x':value_x, y: value_y, 'len':value_len, 'side':'left', 'link':'value_link'}
where:
    name - name of the tag, should be unique;
    value_text - text, which will be shows
    value_x - position x left corner of legend
    value_y - position y left corner of legend
    value_len - length of the line, which related with legend
    'side':'left' - if side exists and side == 'left' then legend is located left of the line
    value_link - link for redirect 


 */

// left
tags['head'] = {'text': 'HEAD', 'x':10, y: 50, 'len':265, 'side':'left', 'link':'head'};
tags['thorax'] = {'text': 'THORAX', 'x':10, y: 180, 'len':200, 'side':'left', 'link':'thorax' };
tags['spine'] = {'text': 'SPINE', 'x':10, y: 300, 'len':250, 'side':'left', 'link':'spine' };
tags['lowerlimb_1'] = {'text': 'LOWER', 'x':10, y: 550, 'len':210, 'side':'left', 'link':'lowerlimb' };
tags['lowerlimb_2'] = {'text': 'LIMB', 'x':10, y: 575, 'len':0, 'side':'left', 'link':'lowerlimb' };

//right
tags['neck'] = {'text': 'NECK', 'x':580, y: 120, 'len':225, 'link':'neck' };
tags['upperlimb_1'] = {'text': 'UPPER', 'x':580, y: 250, 'len':150, 'link':'upperlimb' };
tags['upperlimb_2'] = {'text': 'LIMB', 'x':580, y: 275, 'len':0, 'link':'upperlimb' };
tags['adbomen_pelvis_1'] = {'text': 'ABDOMEN', 'x':580, y: 340, 'len':200, 'link':'abdomen&pelvis' };
tags['adbomen_pelvis_2'] = {'text': '&PELVIS', 'x':580, y: 365, 'len':0, 'link':'abdomen&pelvis' };


//-------------------------------------------------------------------------------------------

// draw the balls on the canvas
function draw(){
    canvas = document.getElementById("skeletonCanvas");
    // check if supported

    // console.log(tags['head']['text']);

    if(canvas.getContext){
        ctx=canvas.getContext("2d");
        //clear canvas
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        var img = new Image();

        img.onload = function() {
            ctx.drawImage(img, 0, 0);

            //draw the link
            ctx.font = font;
            ctx.fillStyle = fillStyle;
            ctx.strokeStyle = strokeStyle;

            for (var key in tags) {
                // ctx.fillText(linkText,linkX,linkY);
                ctx.fillText(tags[key]['text'], tags[key]['x'], tags[key]['y']);
                var linkWidth = ctx.measureText(tags[key]['text']).width;

                ctx.beginPath();
                var start_line, end_line, pos_y;
                // left
                if (tags[key]['side'] == 'left')
                {
                    // draw line

                    start_line = tags[key]['x'] + linkWidth;
                    end_line = start_line + tags[key]['len'];
                    pos_y = tags[key]['y'] - diff_y_line;

                }
                else{    // right
                    start_line = tags[key]['x'];
                    end_line = start_line - tags[key]['len'];
                    pos_y = tags[key]['y'] - diff_y_line;
                }


                ctx.moveTo(start_line, pos_y);
                ctx.lineTo(end_line, pos_y);
                ctx.stroke();

            }

            // -----------------------
            //add mouse listeners
            // canvas.addEventListener("mousemove", on_mousemove, false);
            canvas.addEventListener("mousemove", on_mousemove_new, false);

            canvas.addEventListener("click", on_click, false);
        };

        img.src = '\/bundles\/swarminfoimagescript\/ressources\/7body.jpg';

    }
}

//check if the mouse is over the link and change cursor style
function on_mousemove_new (ev) {
    var x, y;

    // Get the mouse position relative to the canvas element.
    if (ev.layerX || ev.layerX == 0) { //for firefox
        x = ev.layerX;
        y = ev.layerY;
    }
    x-=canvas.offsetLeft;
    y-=canvas.offsetTop;

    //is the mouse over the link?
    for (var key in tags) {
        var legend_x = tags[key]['x'];
        var legend_y = tags[key]['y'];

        if(x>=legend_x && x <= (legend_x + ctx.measureText(tags[key]['text']).width) &&
            y <= legend_y && y >= (legend_y-linkHeight)){

            document.body.style.cursor = "pointer";
            inLink=true;
            linkText = tags[key]['link'];
            break
        }
        else{
            document.body.style.cursor = "";
            inLink=false;
        }
    }


}

//if the link has been clicked, go to link
function on_click(e) {
    if (inLink)  {
        console.log(linkText);
        top.location.href = redirect_link + linkText;
    }
}

//ie9
function on_mousemove_ie(ev)
{
    var e = window.event.srcElement;
    /*txtName.value = e.tagName;
    txtType.value = e.type;*/
    var x = window.event.clientX;
    var y = window.event.clientY;
    // console.log('x= '+x+'; y='+y);
    //alert(x+' '+y);

    x -= canvas.offsetLeft;
    y -= canvas.offsetTop;

    //document.body.style.cursor = "pointer";
    //is the mouse over the link?
    for (var key in tags) {
        var legend_x = tags[key]['x'];
        var legend_y = tags[key]['y'];

        //console.log('legend_x= '+legend_x+'; legend_y= '+legend_y);

        if(x>=legend_x && x <= (legend_x + ctx.measureText(tags[key]['text']).width) &&
            y <= legend_y && y >= (legend_y-linkHeight)){
            //console.log('legend_x= '+legend_x+'; legend_y= '+legend_y);
            //document.body.style.cursor = "pointer";
            inLink=true;
            //console.log('inLink=true');
            linkText = tags[key]['link'];
            //console.log(linkText);
            top.location.href = redirect_link + linkText;
            break
        }
        else{
            //document.body.style.cursor = "";
            inLink=false;
            //console.log('inLink=false');
        }
    }
}

function onmouseover(e){
    //console.log('onmouseover');
}

