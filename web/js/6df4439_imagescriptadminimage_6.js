//script to manage images as an admin


//transform the main admin pannel into an image admin pannel
isl.a.imageadminpannel = function(){
    isl.a.imageadminpannelAdd();
    isl.a.imageadminpannelSelection();
    isl.a.imageadminpannelModify();
    
    
}



//append html on the image admin pannel to choose the image to modify
isl.a.imageadminpannelSelection = function(){
    var html = "";
    
    //get the list of images available
    var images = isl.p.getSortedImage();
    
    //print some data to choose the image to modify
    html += "<div id=imageadminchoose><h4>Select an image to edit : </h4> Current Image : <select id=imageadminchooseselector>";
    for(var i in images){
        html += "<option value="+images[i].id+( images[i].id == isl.currentImage ? " selected" : " " )+">"+i+"</option>";
    }
    html += "</select></div>";
    $("#"+isl.p.mainAdminPannel).append(html);
    
    //when selecting an image, it is displayed a s the main image
    $("#imageadminchooseselector").change( function(e){
        var imageId = $(this).val();
        console.log(imageId);
        isl.currentImage = imageId;
        isl.f.resetAll();
    });
}

//append html to the image admin pannel to modify the current image
isl.a.imageadminpannelModify = function(){
    var html = "";
    //a div to display a form to modify an already existing image
    var displayedImage = isl.f.retrieveImageToDisplay();
    html += "<div id=imageadminmodify>"
    if ("url" in displayedImage && displayedImage.url != "") {
        html += "<form id=imageadminmodiy>"
        html += "<h4>Affect the image to a series : </h4>Current Series : <select id=seriesadminchooseselector><option value='' >Select a series</option>";
        var currentSeries = "series" in displayedImage ? displayedImage.series : "";
	
	//sort the series 
	var sortarr = [];
	for(sid in isl.current.series){ sortarr.push([isl.current.series[sid].text, sid ]) }
	sortarr.sort( function(a,b){return a[0].localeCompare(b[0]);} );
	   
	for(var i in sortarr){
	    var sid = sortarr[i][1];
	    html += "<option value='"+sid+"' "+( sid == currentSeries ? " selected" : " " )+">"+isl.current.series[sid].text+"</option>";
	}
        //for(var sid in isl.current.series){
        //    html += "<option value='"+sid+"' "+( sid == currentSeries ? " selected" : " " )+">"+isl.current.series[sid].text+"</option>";
        //}
        html += "</select><br>";
        html += "</form><br>";

    
        html += "<form id=imageadmindelete><input  class='adminpannelDangerousButton' type=submit value='delete the current image'></form>";
    }
    else html += "no image selected";
    html += "</div>";
    
    $("#"+isl.p.mainAdminPannel).append(html);
    
    //delete an image event
    $("#imageadmindelete").submit(function(e){
        e.preventDefault();
	//ask for confirmation
	if(!isl.a.confirm("Do you really want to delete this image ? ")) return;
	
	var log = "deleting the image"+isl.currentImage;
	
        //delete the current image
        var ressource = { delete : isl.current.images[isl.currentImage].url, }
        var nearestImage = isl.a.deleteImage( isl.currentImage );
        //change the main image to be displayed :
        isl.currentImage = nearestImage;
	
	ressource.log = log;
        isl.a.saveData( isl.current, ressource );
    });
    
    //when selecting an series, it is displayed a s the main series
    $("#seriesadminchooseselector").change( function(e){
        var series = $(this).val();
        isl.current.images[ isl.currentImage ].series = series;
	var log = "changing the image "+isl.currentImage+" to series "+isl.current.series[series].text;
        isl.a.saveData( isl.current, {log:log} );
        isl.f.resetAll();
    });
}


//append html to the image admin pannel to add a new image
isl.a.imageadminpannelAdd = function(){
    var html = "";
    
    //get the list of images available
    //get the images for all the series
    var images = isl.p.getSortedImage(null);
    
    //print a form to add a new image
    html += "<div id=imageadminadd><form id=imageadminaddform> <h4>Add a new image</h4>";
    html += "Add images : <input name=file type=file id=imageadminaddfile multiple><br> Position of the new image : <select name=position id=imageadminaddposition>";
    for(var i in images) html += "<option value="+i+">"+(parseInt(i)+1)+"</option>";
    html += "<option value="+(images.length+1)+">"+(images.length+1)+"</option>";
    html += "</select><br>";
    
    html += "Affect the image to a series : <select id=newimageseries>";
    var currentSeries = isl.p.currentSeries;
    
    //sort the series 
    var sortarr = [];
    for(sid in isl.current.series){ sortarr.push([isl.current.series[sid].text, sid ]) }
    sortarr.sort( function(a,b){return a[0].localeCompare(b[0]);} );
	
    for(var i in sortarr){
	var sid = sortarr[i][1];
        html += "<option value='"+sid+"' "+( sid == currentSeries ? " selected" : " " )+">"+isl.current.series[sid].text+"</option>";
    }
    html += "</select><br>";
    
    html += "<input type=submit value='add the image' />";
    html += "</form></div>";
    
    $("#"+isl.p.mainAdminPannel).append(html);
    
    //on submit, save the new image
    $("#imageadminadd").submit(function(e){
        e.preventDefault();
        var input = document.getElementById("imageadminaddfile");
        console.log("ici");
        //if no file has been provided,don't do anything
        //same if the file isn't an image
        try{
            if(!input.files[0].type.match(/image.*/)) return;
        }
        catch(e){
            return;
        }
        
        //build a new image with a new name
	var series = $("#newimageseries").val();
        var names = isl.a.inputFileName(input);
	var ids = isl.f.generateIds(names.length);
        var initialPosition = $("#imageadminaddposition").val();
        positions = [initialPosition];
	while(positions.length < ids.length) {
	    positions.push( positions[positions.length-1]+1);
	}
	
        var ressource = {
            input : input,
            imagename : names,
        }
	for (var i in ids) {
	    isl.a.modifyImage(ids[i], {
                id : ids[i],
                url : "/bundles/swarminfoimagescript/ressources/"+isl.current.id+"/"+names[i],
                position : positions[i],
		series : series
            });
	}
        
        //change the main image to be displayed :
        isl.currentImage = ids[0];
	var log = "adding "+ids.length+" images";
	ressource.log = log;
        isl.a.saveData( isl.current, ressource );
    });
}







//modify or create an image
isl.a.modifyImage = function(id , data){
    console.log(id);
    if (!(id in isl.current.images)) isl.current.images[id] = {};
    for(var attr in data)
    {
        if (attr == "position" ) {
            isl.current.images[id][attr] = parseInt(data[attr]) -0.5;
            isl.a.resetImagesPosition();
        }
        else isl.current.images[id][attr] = data[attr];
    }
    
}

//remove an image
isl.a.deleteImage = function(id){
    //get the nearest image
    var images = isl.p.getSortedImage(null);
    if (isl.current.images[id].position == 0 ) {
        var nearestImage = images.length > 1 ? 0 : null;
    }
    else var nearestImage = isl.current.images[id].position -1;
    
    delete isl.current.images[id];
    isl.a.resetImagesPosition();
    return nearestImage;
}

//reset the positions of the images
isl.a.resetImagesPosition = function(){
    
    var images = isl.p.getSortedImage(null);
    for(var i in images){
        images[i].position = i;
    }
}



//retrieve the name of a file inside an input element
isl.a.inputFileName = function(input){
    var names = [];
    for (var i = 0; i < input.files.length; ++i) {
	names.push( "" + isl.f.generateId() + input.files.item(i).name );
    }
//
//    var fullPath = input.value;
//    if (fullPath) {
//	var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
//	var filename = fullPath.substring(startIndex);
//	if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
//		filename = filename.substring(1);
//	}
//	return filename;
//    }
//    return "0";

    return names;

}
