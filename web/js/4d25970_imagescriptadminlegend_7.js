//script to manage the legends as an admin

//transform the main admin pannel into a legend admin pannel
isl.a.legendadminpannel = function(){
    isl.a.legendadminpannelAdd();
    isl.a.legendadminpannelSelection();
    isl.a.legendadminpannelModify();
    isl.a.legendadminpannelAddLegendOnClick();
}

isl.a.legendadminpannelSelection = function(){
    var html = "";
    
    //get the list of images available
    var legends = isl.current.legends;
    
    //print some data to choose the image to modify
    html += "<div id=legendadminchoose>select a legend : <select id=legendadminchooseselector>";
    for(var i in legends){
        html += "<option value="+legends[i].id+( legends[i].id == isl.currentLegend ? " selected" : " " )+">"+legends[i].text+"</option>";
    }
    html += "</select></div>";
    $("#"+isl.p.mainAdminPannel).append(html);
    
    //when selecting an image, it is displayed a s the main image
    $("#legendadminchooseselector").change( function(e){
        var legendId = $(this).val();
        console.log(legendId);
        isl.currentLegend = legendId;
        isl.f.resetAll();
    });
}


//append html to the legend admin pannel to modify the current legend
isl.a.legendadminpannelModify = function(){
    var html = "";
    //display a form to affect the legend to an anatomical structure
    if (isl.currentLegend == null ) isl.currentLegend = isl.a.retrieveLegendToDisplay();
    //a div to display a form to modify an already existing image
    var displayedLegend = isl.a.retrieveLegendToDisplay();
    html += "<div id=legendadminmodify>"
    if ( "text" in displayedLegend ) {
        html += "<form id=legendadmindelete><input type=submit value='delete the current legend'></form>";
        html += "<form id=legendadminremovefromimage><input type=submit value='remove the legend from the current image'></form><br><br>";
	
        var currentStructure = isl.current.legends[isl.currentLegend].structure;
        html += "<form id=legendadminupdate>select anatomical structure : <select id=legendadminStructureSelector><option value='' >none</option>";
        for(var s in isl.current.structures) html += "<option value="+s+" "+(currentStructure == s ? "selected" : "")+">"+isl.current.structures[s].text+"</option>";
        html +="</select><br>";
        
        html += "change legend text : <br>";
	for(var l in isl.current.languages){
	    html += "&nbsp;&nbsp;&nbsp;&nbsp;"+l+" : <input class=languageTranslation language='"+l+"' type=text value='"+ displayedLegend.text +"'><br>";
	}
	//html for the description
	html += "change legend description : <textarea id=legendadminupdateDescription>"+("description" in displayedLegend ? displayedLegend.description : "")+"</textarea><br>";
	
	html += "<input type=submit value='update legend'></form>";
        
    
    }
    
    else html += "no legend selected";
    html += "</div>";
    
    $("#"+isl.p.mainAdminPannel).append(html);
    
    //delete an legend event
    $("#legendadmindelete").submit(function(e){
        e.preventDefault();
        //delete the current image
        if (isl.currentLegend in isl.current.legends) {
            delete isl.current.legends[ isl.currentLegend ];
            //remove the legend from all images
            for (var i in isl.current.images){
                if ( "legends" in isl.current.images[i] && isl.currentLegend in isl.current.images[i].legends ) {
                    delete isl.current.images[i].legends[isl.currentLegend];
                }
            }
        }
        isl.currentLegend = null;
        isl.a.saveData( isl.current );
    });
    
    //remove a legend from the current image
    $("#legendadminremovefromimage").submit(function(e){
        e.preventDefault();
        //delete the current image
        if ("legends" in isl.current.images[ isl.currentImage ] && isl.currentLegend in  isl.current.images[ isl.currentImage ].legends ) {
            delete isl.current.images[ isl.currentImage ].legends[ isl.currentLegend ];
        }
        isl.a.saveData( isl.current );
    });
    
    //change the text of the legend
    $("#legendadminupdate").submit(function(e){
        e.preventDefault();
        //update the current image
        if (isl.currentLegend in isl.current.legends) {
	    //update the legend title
            var newText = $("#legendadminupdateText").val();
            isl.current.legends[ isl.currentLegend ].text = newText;

	    //update the structure
            var newStructure = $("#legendadminStructureSelector").val();
            isl.current.legends[ isl.currentLegend ].structure = newStructure;
	    
	    //update the description
	    var newDescription = $("#legendadminupdateDescription").val();
            isl.current.legends[ isl.currentLegend ].description = newDescription;
        console.log('test');
        }
        isl.a.saveData( isl.current );
    });
}


//append html to the image admin pannel to add a new image
isl.a.legendadminpannelAdd = function(){
    var html = "";
    
    //get the list of images available
    var images = isl.p.getSortedImage();
    
    //print a form to add a new legend
    html += "<div id=legendadminadd><form id=legendadminaddform> add a new legend";
    html += "<input type=hidden name=newLegendId value="+isl.f.generateId()+" />";
    html += "<input type=submit value=add />";
    html += "</select></form></div><br>";
    
    $("#"+isl.p.mainAdminPannel).append(html);
    
    //on submit, save the new image
    $("#legendadminaddform").submit(function(e){
        e.preventDefault();
        var newLegendId = $("[name=newLegendId]").val();
        //add a new legend
        isl.current.legends[ newLegendId ] = {
            text : "new legend text",
            id : newLegendId,
            structure : "",
        };
        //focus the admin pannel on the nex legend
        isl.currentLegend = newLegendId;
        
        isl.a.saveData( isl.current );
    });
}

//add a click event : on click, add the current legend on the current image
isl.a.legendadminpannelAddLegendOnClick = function(){ //alert("la  ???? ");
    $("#"+isl.p.mainImageContainer).unbind("click.legendadminpannelAddLegendOnClick")
    .bind("click.legendadminpannelAddLegendOnClick" , function(e){
        if ( isl.a.pannelDisplayed != "legendadminpannel") return;
        if (isl.currentLegend == null) return;
        var canvasX = e.pageX - $(this).offset().left;
        var canvasY = e.pageY - $(this).offset().top;
	//compute the percent width and height
	var percentWidth = (canvasX - isl.p.legendLeftWidth) / isl.p.mainImageWidth;
	var percentHeight = (canvasY ) / isl.p.mainImageHeight;
	if (isl.p.verticalSymetry)  percentWidth = 1 - percentWidth;
	if (isl.p.horizontalSymetry)  percentHeight = 1 - percentHeight;
        //add a the current legend to the current image
        if (!("legends" in isl.current.images[ isl.currentImage ])) isl.current.images[ isl.currentImage ].legends = {};
	isl.current.images[ isl.currentImage ].legends[ isl.currentLegend ] = {
            percentWidth : percentWidth,
            percentHeight : percentHeight,
            id : isl.currentLegend, 
        };
        //Save the current data
        isl.a.saveData( isl.current );
    });
}



//retrieve the current legend
isl.a.retrieveLegendToDisplay = function(legend){
    //if no image is provided, find the first image
    if (legend != null && (legend != undefined) ) return legend;
    if (isl.currentLegend != null && (isl.currentLegend in isl.current.legends) ) {
        return  isl.current.legends[isl.currentLegend];
    }
    else{
        //pick a random legend
        for(var l in isl.current.legends) return l;
    }
    //by default, return an empty object
    return {};
}

