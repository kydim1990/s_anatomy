//object containing data relative to the administration
isl.a = {};

//this variable should contain the id of the current tag beeing processed
isl.currentLegend = null;
//the current anatomical structure being modified
isl.currentStructure = null;

//when resetting everything, reset the admin pannel
isl.p.onResetAll.push("isl.a.generateAdminPannel");

//set the admin pannel to display : by default, it is the imageadminpannel
isl.a.pannelDisplayed = "imageadminpannel";


//display the pannel referenced in isl.a.pannelDisplayed
isl.a.displaySelectedPannel = function(){
    //change the css of the top admin pannel buttons
    $(".adminPannelButton").css("color" , "black").css("font-size","small").css("text-decoration","none");
    $("[fclick="+isl.a.pannelDisplayed+"]").css("color" , "black").css("font-size","large").css("text-decoration","underline");
    //reset the html of the main pannel
    var html = "";
    $("#"+isl.p.mainAdminPannel).html(html);
    //display the main pannel
    isl.a[ isl.a.pannelDisplayed ]();
}

//generate the admin pannel
isl.a.generateAdminPannel = function(){
    //build the top menu of the admin pannel
    isl.a.buildTopAdminPannel();
    //open the image admin pannel by default
    isl.a.displaySelectedPannel();
}



//function to persist a given data object
isl.a.saveData = function(data , ressource){
    var id = data.id;
    //console.log("savedata");
    //build the form data to send
    var formData= new FormData();
    formData.append("data",JSON.stringify(data));
    formData.append("id",id);
    
    //if a ressource has been defined, save it too
    //a ressource should contain : an input element with a file inside, a name where to store the file
    if(ressource != null && ("input" in ressource) ){
        var file = ressource.input.files[0];
        formData.append("image", file);
        formData.append("imagename",ressource.imagename);
    }
    
    //perform deletion actions if it has been defined too
    if(ressource != null && ("delete" in ressource) ){
        formData.append("delete", ressource.delete);
    }
    
    var str = JSON.stringify(data);
    
    var jqxhr = $.ajax({
        url: "savedata",
        method: "POST",
        data: formData,
        processData: false,
        contentType: false,
        type: "POST",
        })
        .done(function() {
            //reset the main admin pannel
            isl.f.resetAll();
        })
        .fail(function() {
            alert( "error" );
        })
        .always(function() {
        });

}


//build the top admin pannel, with all the actions available
isl.a.buildTopAdminPannel = function(){
    var html = "";
    
    //add button with a given fclick parameter
    //when clicked, the main admin pannel is built using that admin function
    
    //add a button for the general informations
    html += "<a href=# class=adminPannelButton fclick=languageadminpannel>images</a>&nbsp;&nbsp;";
    
    //add a button for the images
    html += "<a href=# class=adminPannelButton id=imageadminpannelbutton fclick=imageadminpannel>images</a>&nbsp;&nbsp;";
    
    //add a button for the legends
    html += "<a href=# class=adminPannelButton fclick=legendadminpannel>legends</a>&nbsp;&nbsp;";
    
    //add a button for the anatomical structures
    html += "<a href=# class=adminPannelButton fclick=structureadminpannel>anatomical structures</a>&nbsp;&nbsp;";
    
    //insert those links, and binds events on it
    $("#"+isl.p.topAdminPannel).html( html )
    .children().bind("click",function(e){
        //select the right pannel to display
        isl.a.pannelDisplayed = $(this).attr("fclick");
        //display the appropriate pannel
        isl.a.displaySelectedPannel();
    })
    
}




    
    
