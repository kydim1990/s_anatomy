//functions to manage the images on the bottom and left bands

//define some parameters
isl.p.bottomBandContainer = "bottomband";
isl.p.bottomBandCanvas = "bottombandcanvas";
isl.p.bottomBandArrows = "bottombandarrows";
isl.p.bottomBandWidth = isl.p.setImageWidth + isl.p.legendLeftWidth;
isl.p.bottomBandHeight = 50;
isl.p.bottomBandImages = []; //an array to store the image objects of the bottom band
isl.p.bottomBandImagesCumulatedWidth = []; //an array to store the cumulated width of the bottom images
isl.p.bottomBandImagesNumber = 8; //the number of image there should be in the bottom band
isl.p.bottomBandImagesLoaded = 0; //remember the number of images already loaded
isl.p.bottomBandCursorWidth = 2;
isl.p.bottomBandCursorColor = "red";
isl.p.bottomBandArrowsSize = Math.floor(isl.p.bottomBandHeight * 0.2 );
isl.p.bottomBandArrowsColor = "black";

isl.p.leftBandContainer = "leftband";

//on start all, load the images to be displayed at the bottom band
isl.p.onStart.push("isl.f.loadBottomBandImages");
//on reset all, reset the image with the bottom band
isl.p.onResetAll.push("isl.f.resetBottomBand");
////on reset all, reset the left band
//isl.p.onResetAll.push("isl.f.resetLeftBand");


//reset the bottom band
isl.f.resetBottomBand = function(){
    //create a canvas on the bottom band
    var bottomBandContainer = $("#"+isl.p.bottomBandContainer);
    //create a canvas inside the main container
    bottomBandContainer.html("<canvas id="+isl.p.bottomBandCanvas+" style='cursor:pointer;'></canvas>");
    //create the arrows inside the bottom band
    bottomBandContainer.append("<div id="+isl.p.bottomBandArrows+" style='vertical-align:top;text-align:center;display:inline-block;' ></div>");
    //reset the content of the bottom band arrows
    isl.f.resetBottomBandArrows();
    //set the size of the various elements of the canvas
    isl.p.bottomBandWidth = isl.p.setImageWidth + isl.p.legendLeftWidth;
    //fill the bottom band with black
    var ctx=isl.f.getBottomContext();
    ctx.canvas.width = isl.p.bottomBandWidth;
    ctx.canvas.height = isl.p.bottomBandHeight;
    //ctx.rect(0,0,isl.p.bottomBandWidth,isl.p.bottomBandHeight);
    //ctx.fillStyle="black";
    //ctx.fill(); 
    
    
    isl.f.printBottomBandImages();
    isl.f.printBottomCursor();
    isl.f.addBottomBandEvent();
}

//print the images of the bottom band
isl.f.printBottomBandImages = function(){ //alert("icii")
    var cumulatedWidth = isl.f.getCumulatedWidthBottomBand();
    var ctx=isl.f.getBottomContext();
    for (var i in isl.p.bottomBandImages) {
        var img = isl.p.bottomBandImages[i];
        var width = isl.f.getBottomBandImageWidth(img);
        //print the image
        ctx.drawImage(img, isl.p.bottomBandImagesCumulatedWidth[i] - width ,0 , width , isl.p.bottomBandHeight);
    }
    
}

//print the vertical band on the bottom band
isl.f.printBottomCursor = function(){
    var startWidth = isl.f.calculateBottomBandCursorPosition();
    var endWidth = startWidth;
    var startHeight = 0;
    var endHeight = isl.p.bottomBandHeight;
    var ctx = isl.f.getBottomContext();
    ctx.strokeStyle = isl.p.bottomBandCursorColor;
    ctx.lineWidth = isl.p.bottomBandCursorWidth;
    //draw
    ctx.beginPath();
    ctx.moveTo( startWidth, startHeight);
    ctx.lineTo( endWidth , endHeight );
    ctx.stroke();
}

//Calculate the position of the bottom band cursor
isl.f.calculateBottomBandCursorPosition = function(){
    var images = isl.p.getSortedImage();
    var currentImagePosition = isl.f.getRelativeImagePosition( isl.currentImage);
    return Math.floor( isl.f.getCumulatedWidthBottomBand() * (  ( currentImagePosition) / images.length  ) );    
}

//add events on the bottom band
isl.f.addBottomBandEvent = function(){
    var $canvas = $("#"+isl.p.bottomBandCanvas);
    //on click, change the image to the one matching the cursor position
    $canvas.bind("click" , function(e){
        var canvasX = e.pageX - $(this).offset().left;
        var canvasY = e.pageY - $(this).offset().top;
        //compute the position of the image to display
        var images = isl.p.getSortedImage();
        var cumulatedWidth = isl.f.getCumulatedWidthBottomBand();
        var newPosition = Math.floor( images.length * canvasX / cumulatedWidth );
        isl.currentImage = images[newPosition].id;
        isl.f.resetAll();
    });
}


//reset the html content of the bottom band arrows
isl.f.resetBottomBandArrows = function(){
    if (!(isl.currentImage in isl.current.images)) return;
    $container = $("#"+isl.p.bottomBandArrows);
    var images = isl.p.getSortedImage();
    var currentImage = isl.current.images[isl.currentImage];
    var style = "style='text-decoration:none;font-size:"+isl.p.bottomBandArrowsSize+"px;color:"+isl.p.bottomBandArrowsColor+";'";
    //var style = "style='text-decoration:none;text-align:center;display:inline-block;font-size="+isl.p.bottomBandArrowsSize+"px;color="+isl.p.bottomBandArrowsColor+";'";
    $container.html("<a "+style+" href=# id=bottomBandLeftArrow  > &lt; </a>");
    $container.append(" <span "+style+ " > "+ ( parseInt( isl.f.getRelativeImagePosition( currentImage.id )) +1)+"/"+images.length+" </span>");
    $container.append("<a "+style+" href=# id=bottomBandRightArrow  > &gt; </a>");
    
    //add the events on the link
    $("#bottomBandLeftArrow").bind("click",function(e){
        e.preventDefault();
        isl.currentImage = isl.f.getPreviousImageId();
        isl.f.resetAll();
    });
    $("#bottomBandRightArrow").bind("click",function(e){
        e.preventDefault();
        isl.currentImage = isl.f.getNextImageId();
        isl.f.resetAll();
    });
}



//function to load the images to display on the bottom band
isl.f.loadBottomBandImages = function(){
    isl.p.bottomBandImages = [];
    isl.p.bottomBandImagesCumulatedWidth = [];
    isl.f.loadOneBottomImage();
}

//function to load one bottom image
//once it is loaded, check how much width it take, and orderthe loading of the next image
isl.f.loadOneBottomImage = function(){
    var images = isl.p.getSortedImage();
    //take isl.p.bottomBandImagesNumber to display
    var cumulatedWidth = isl.f.getCumulatedWidthBottomBand();
    var nextImageId = Math.floor( cumulatedWidth/isl.p.bottomBandWidth * images.length );
    if( nextImageId >= images.length) {
        isl.f.resetBottomBand(); //display the bottom band when we can
        return;
    }
    var img = new Image();
    img.src = images[nextImageId].url;
    img.onload = function() {
        var newpush = isl.f.addOneBottomImage( this);
        if (newpush) isl.f.loadOneBottomImage();
    }
}


//get the cumulated width of the bottom band images
isl.f.getCumulatedWidthBottomBand = function(){
    return isl.p.bottomBandImagesCumulatedWidth.length == 0 ? 0 : isl.p.bottomBandImagesCumulatedWidth[ isl.p.bottomBandImagesCumulatedWidth.length - 1 ];
}

//function to add one bottom image once it is loaded
isl.f.addOneBottomImage = function(img){
    var newBottomBandImageWidth = isl.f.getBottomBandImageWidth(img);
    //if the bottom band is fulle, don't do anything
    if ( newBottomBandImageWidth > isl.p.bottomBandWidth - isl.f.getCumulatedWidthBottomBand()  ) {
        isl.f.resetBottomBand(); //display the bottom band when we can
        return false;
    }
    //push the new image
    isl.p.bottomBandImages.push(img);
    isl.p.bottomBandImagesCumulatedWidth.push( newBottomBandImageWidth + isl.f.getCumulatedWidthBottomBand()  );
    //an image has been added, return true
    return true;
}

//function to get the context of the bottom canvas
isl.f.getBottomContext = function(){
    var c=document.getElementById(isl.p.bottomBandCanvas);
    var ctx=c.getContext("2d");
    return ctx;
}

//get the width of an img once displayed on the bottom band
isl.f.getBottomBandImageWidth = function(img){
    return img.width * ( isl.p.bottomBandHeight / (img.height+1) );
}