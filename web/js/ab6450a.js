//object containing data relative to the administration
isl.a = {};

//this variable should contain the id of the current tag beeing processed
isl.currentLegend = null;
//the current anatomical structure being modified
isl.currentStructure = null;

//when resetting everything, reset the admin pannel
isl.p.onResetAll.push("isl.a.generateAdminPannel");

//set the admin pannel to display : by default, it is the imageadminpannel
isl.a.pannelDisplayed = "imageadminpannel";


//display the pannel referenced in isl.a.pannelDisplayed
isl.a.displaySelectedPannel = function(){
    //change the css of the top admin pannel buttons
    //$(".adminPannelButton").css("color" , "black").css("font-size","auto").css("text-decoration","none");
    $(".adminPannelButtonSelected").removeClass("adminPannelButtonSelected");
    $("[fclick="+isl.a.pannelDisplayed+"]").addClass("adminPannelButtonSelected"); //.css("color" , "black").css("font-size","x-large").css("text-decoration","underline");
    //reset the html of the main pannel
    var html = "";
    //$("#"+isl.p.mainAdminPannel).html(html);
    $("#"+isl.p.mainAdminPannel).children().remove();;
    //display the main pannel
    isl.a[ isl.a.pannelDisplayed ]();
}

//generate the admin pannel
isl.a.generateAdminPannel = function(){
    //build the top menu of the admin pannel
    isl.a.buildTopAdminPannel();
    //open the image admin pannel by default
    isl.a.displaySelectedPannel();
}


//buffer containing the description about the next operation


//function to persist a given data object
isl.a.saveData = function(data , ressource , callback){ //console.log("caller is " + arguments.callee.caller.toString());
    var id = data.id;
    //console.log("savedata");
    //build the form data to send
    var formData= new FormData();
    formData.append("data",JSON.stringify(data));
    formData.append("id",id);
    
    //if a ressource has been defined, save it too
    //a ressource should contain : an input element with a file inside, a name where to store the file
    if(ressource != null && ("input" in ressource) ){
        var i=0;
        while(i in ressource.input.files){
            var file = ressource.input.files[i];
            formData.append("image"+i, file);
            formData.append("imagename"+i,ressource.imagename[i]);
            i++;
        }
    }
    
    //perform deletion actions if it has been defined too
    if(ressource != null && ("delete" in ressource) ){
        formData.append("delete", ressource.delete);
    }
    
    //append a log message if there is one
    if (ressource != null && ("log" in ressource)) {
        formData.append("log",ressource.log);
    }
    var str = JSON.stringify(data);
    
    //change the cursor
    $('html, body').css("cursor", "wait");
     
    var jqxhr = $.ajax({
        url: "/savedata",
        method: "POST",
        data: formData,
        processData: false,
        contentType: false,
        type: "POST",
        })
        .done(function(data,textStatus,jqxhr) {

            //reset the main admin pannel
            isl.f.resetAll();
            if(callback != null) callback();
        })
        .fail(function() {
            alert( "the last update failed : refresh the page to check if you are still logged." );
        })
        .always(function() {
            $('html, body').css("cursor", "auto");
        });

}


//build the top admin pannel, with all the actions available
isl.a.buildTopAdminPannel = function(){
    var html = "";
    
    //add button with a given fclick parameter
    //when clicked, the main admin pannel is built using that admin function
    
    
    //add a button for the general informations
    html += "<a href=# class=adminPannelButton fclick=generaladminpannel>general</a>&nbsp;&nbsp;";
    
    //add a button for the language informations
    html += "<a href=# class=adminPannelButton fclick=languageadminpannel>languages</a>&nbsp;&nbsp;";
    
    //add a button for the series informations
    html += "<a href=# class=adminPannelButton fclick=seriesadminpannel>series</a>&nbsp;&nbsp;";
    
    //add a button for the images
    html += "<a href=# class=adminPannelButton id=imageadminpannelbutton fclick=imageadminpannel>images</a>&nbsp;&nbsp;";

	    //add a button for the anatomical structures
    html += "<a href=# class=adminPannelButton fclick=structureadminpannel>legends</a>&nbsp;&nbsp;";

    //add a button for the legends
    html += "<a href=# class=adminPannelButton fclick=legendadminpannel>structures</a>&nbsp;&nbsp;";
    
   
    
    //add a button for the logs
    html += "<a href=# class=adminPannelButton fclick=logadminpannel>logs</a>&nbsp;&nbsp;";
    
    //add a button for the left side trackdown
    // html += "<a href=# class=adminPannelButton fclick=trackdownadminpannel>left trackdown</a>&nbsp;&nbsp;";
    
    
    
    //insert those links, and binds events on it
    $("#"+isl.p.topAdminPannel).html( html )
    .children().bind("click",function(e){
        e.preventDefault();
        //select the right pannel to display
        isl.a.pannelDisplayed = $(this).attr("fclick");
        //display the appropriate pannel
        isl.a.displaySelectedPannel();
    })
    
}


//function to demand confirmation : return true if it was accepted, false in other case
isl.a.confirm = function(message){
    var r = confirm(message);
    if (r == true) {
        return true;
    } else {
        return false;
    }
}



    
    

//script to manage images as an admin


//transform the main admin pannel into an image admin pannel
isl.a.imageadminpannel = function(){
    isl.a.imageadminpannelAdd();
    isl.a.imageadminpannelSelection();
    isl.a.imageadminpannelModify();
    
    
}



//append html on the image admin pannel to choose the image to modify
isl.a.imageadminpannelSelection = function(){
    var html = "";
    
    //get the list of images available
    var images = isl.p.getSortedImage();
    
    //print some data to choose the image to modify
    html += "<div id=imageadminchoose><h4>Select an image to edit : </h4> Current Image : <select id=imageadminchooseselector>";
    for(var i in images){
        html += "<option value="+images[i].id+( images[i].id == isl.currentImage ? " selected" : " " )+">"+i+"</option>";
    }
    html += "</select></div>";
    $("#"+isl.p.mainAdminPannel).append(html);
    
    //when selecting an image, it is displayed a s the main image
    $("#imageadminchooseselector").change( function(e){
        var imageId = $(this).val();
        console.log(imageId);
        isl.currentImage = imageId;
        isl.f.resetAll();
    });
}

//append html to the image admin pannel to modify the current image
isl.a.imageadminpannelModify = function(){
    var html = "";
    //a div to display a form to modify an already existing image
    var displayedImage = isl.f.retrieveImageToDisplay();
    html += "<div id=imageadminmodify>"
    if ("url" in displayedImage && displayedImage.url != "") {
        html += "<form id=imageadminmodiy>"
        html += "<h4>Affect the image to a series : </h4>Current Series : <select id=seriesadminchooseselector><option value='' >Select a series</option>";
        var currentSeries = "series" in displayedImage ? displayedImage.series : "";
	
	//sort the series 
	var sortarr = [];
	for(sid in isl.current.series){ sortarr.push([isl.current.series[sid].text, sid ]) }
	sortarr.sort( function(a,b){return a[0].localeCompare(b[0]);} );
	   
	for(var i in sortarr){
	    var sid = sortarr[i][1];
	    html += "<option value='"+sid+"' "+( sid == currentSeries ? " selected" : " " )+">"+isl.current.series[sid].text+"</option>";
	}
        //for(var sid in isl.current.series){
        //    html += "<option value='"+sid+"' "+( sid == currentSeries ? " selected" : " " )+">"+isl.current.series[sid].text+"</option>";
        //}
        html += "</select><br>";
        html += "</form><br>";

    
        html += "<form id=imageadmindelete><input  class='adminpannelDangerousButton' type=submit value='delete the current image'></form>";
    }
    else html += "no image selected";
    html += "</div>";
    
    $("#"+isl.p.mainAdminPannel).append(html);
    
    //delete an image event
    $("#imageadmindelete").submit(function(e){
        e.preventDefault();
	//ask for confirmation
	if(!isl.a.confirm("Do you really want to delete this image ? ")) return;
	
	var log = "deleting the image"+isl.currentImage;
	
        //delete the current image
        var ressource = { delete : isl.current.images[isl.currentImage].url, }
        var nearestImage = isl.a.deleteImage( isl.currentImage );
        //change the main image to be displayed :
        isl.currentImage = nearestImage;
	
	ressource.log = log;
        isl.a.saveData( isl.current, ressource );
    });
    
    //when selecting an series, it is displayed a s the main series
    $("#seriesadminchooseselector").change( function(e){
        var series = $(this).val();
        isl.current.images[ isl.currentImage ].series = series;
	var log = "changing the image "+isl.currentImage+" to series "+isl.current.series[series].text;
        isl.a.saveData( isl.current, {log:log} );
        isl.f.resetAll();
    });
}


//append html to the image admin pannel to add a new image
isl.a.imageadminpannelAdd = function(){
    var html = "";
    
    //get the list of images available
    //get the images for all the series
    var images = isl.p.getSortedImage(null);
    
    //print a form to add a new image
    html += "<div id=imageadminadd><form id=imageadminaddform> <h4>Add a new image</h4>";
    html += "Add images : <input name=file type=file id=imageadminaddfile multiple><br> Position of the new image : <select name=position id=imageadminaddposition>";
    for(var i in images) html += "<option value="+i+">"+(parseInt(i)+1)+"</option>";
    html += "<option value="+(images.length+1)+">"+(images.length+1)+"</option>";
    html += "</select><br>";
    
    html += "Affect the image to a series : <select id=newimageseries>";
    var currentSeries = isl.p.currentSeries;
    
    //sort the series 
    var sortarr = [];
    for(sid in isl.current.series){ sortarr.push([isl.current.series[sid].text, sid ]) }
    sortarr.sort( function(a,b){return a[0].localeCompare(b[0]);} );
	
    for(var i in sortarr){
	var sid = sortarr[i][1];
        html += "<option value='"+sid+"' "+( sid == currentSeries ? " selected" : " " )+">"+isl.current.series[sid].text+"</option>";
    }
    html += "</select><br>";
    
    html += "<input type=submit value='add the image' />";
    html += "</form></div>";
    
    $("#"+isl.p.mainAdminPannel).append(html);
    
    //on submit, save the new image
    $("#imageadminadd").submit(function(e){
        e.preventDefault();
        var input = document.getElementById("imageadminaddfile");
        console.log("ici");
        //if no file has been provided,don't do anything
        //same if the file isn't an image
        try{
            if(!input.files[0].type.match(/image.*/)) return;
        }
        catch(e){
            return;
        }
        
        //build a new image with a new name
	var series = $("#newimageseries").val();
        var names = isl.a.inputFileName(input);
	var ids = isl.f.generateIds(names.length);
        var initialPosition = $("#imageadminaddposition").val();
        positions = [initialPosition];
	while(positions.length < ids.length) {
	    positions.push( positions[positions.length-1]+1);
	}
	
        var ressource = {
            input : input,
            imagename : names,
        }
	for (var i in ids) {
	    isl.a.modifyImage(ids[i], {
                id : ids[i],
                url : "/bundles/swarminfoimagescript/ressources/"+isl.current.id+"/"+names[i],
                position : positions[i],
		series : series
            });
	}
        
        //change the main image to be displayed :
        isl.currentImage = ids[0];
	var log = "adding "+ids.length+" images";
	ressource.log = log;
        isl.a.saveData( isl.current, ressource );
    });
}







//modify or create an image
isl.a.modifyImage = function(id , data){
    console.log(id);
    if (!(id in isl.current.images)) isl.current.images[id] = {};
    for(var attr in data)
    {
        if (attr == "position" ) {
            isl.current.images[id][attr] = parseInt(data[attr]) -0.5;
            isl.a.resetImagesPosition();
        }
        else isl.current.images[id][attr] = data[attr];
    }
    
}

//remove an image
isl.a.deleteImage = function(id){
    //get the nearest image
    var images = isl.p.getSortedImage(null);
    if (isl.current.images[id].position == 0 ) {
        var nearestImage = images.length > 1 ? 0 : null;
    }
    else var nearestImage = isl.current.images[id].position -1;
    
    delete isl.current.images[id];
    isl.a.resetImagesPosition();
    return nearestImage;
}

//reset the positions of the images
isl.a.resetImagesPosition = function(){
    
    var images = isl.p.getSortedImage(null);
    for(var i in images){
        images[i].position = i;
    }
}



//retrieve the name of a file inside an input element
isl.a.inputFileName = function(input){
    var names = [];
    for (var i = 0; i < input.files.length; ++i) {
	names.push( "" + isl.f.generateId() + input.files.item(i).name );
    }
//
//    var fullPath = input.value;
//    if (fullPath) {
//	var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
//	var filename = fullPath.substring(startIndex);
//	if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
//		filename = filename.substring(1);
//	}
//	return filename;
//    }
//    return "0";

    return names;

}

//script to manage the legends as an admin

//transform the main admin pannel into a legend admin pannel
isl.a.legendadminpannel = function(){
    isl.a.legendadminpannelAdd();
    isl.a.legendadminpannelSelection();
    isl.a.legendadminpannelModify();
    isl.a.legendadminpannelAddLegendOnClick();
}

isl.a.legendadminpannelSelection = function(){
    var html = "";
    
    //get the list of images available
    var legends = isl.current.legends;
    
    //print some data to choose the image to modify
    html += "<div id=legendadminchoose><h4>Select a structure to edit</h4> Current Structure : <select id=legendadminchooseselector>";
    
    //sort the legends 
    var sortarr = [];
    for(var i in legends){ sortarr.push([isl.f.getLegendText(i), i ]) }
    sortarr.sort( function(a,b){return a[0].localeCompare(b[0]);} );
    
    for(var e in sortarr){
	var i = sortarr[e][1];
        html += "<option value="+legends[i].id+( legends[i].id == isl.currentLegend ? " selected" : " " )+">"+isl.f.getLegendText(i)+"</option>";
    }
    html += "</select></div>";
    $("#"+isl.p.mainAdminPannel).append(html);
    
    //when selecting an image, it is displayed a s the main image
    $("#legendadminchooseselector").change( function(e){
        var legendId = $(this).val();
        console.log(legendId);
        isl.currentLegend = legendId;
        isl.f.resetAll();
    });
}


//append html to the legend admin pannel to modify the current legend
isl.a.legendadminpannelModify = function(){
    var html = "";
    //display a form to affect the legend to an anatomical structure
    if (isl.currentLegend == null ) isl.currentLegend = isl.a.retrieveLegendToDisplay();
    //a div to display a form to modify an already existing image
    var displayedLegend = isl.a.retrieveLegendToDisplay();
    html += "<div id=legendadminmodify>"
    if ( "text" in displayedLegend ) {
        
        
	
        var currentStructure = isl.current.legends[isl.currentLegend].structure;
        html += "<form id=legendadminupdate><h4>Edit the current structure : </h4> Select a structure : <select id=legendadminStructureSelector><option value='' >none</option>";
        for(var s in isl.current.structures) html += "<option value="+s+" "+(currentStructure == s ? "selected" : "")+">"+isl.current.structures[s].text+"</option>";
        html +="</select><br>";
        
        html += "Structure Name : <br>";
	for(var l in isl.current.languages){
	    html += "&nbsp;&nbsp;&nbsp;&nbsp;"+l+" : <input class=LegendLanguageTranslation language='"+l+"' type=text value='"+ isl.f.getLegendText(isl.currentLegend,l) +"'><br>";
	}
	//html for the description
	html += "Structure Description : <br><textarea id=legendadminupdateDescription>"+("description" in displayedLegend ? displayedLegend.description : "")+"</textarea><br>";
	
	html += "<input type=submit value='update structure'></form><br>";
        html += "<form id=legendadminremovefromimage><input class='adminpannelDangerousButton' type=submit value='remove the structure from the current image'></form><br>";
	html += "<form id=legendadmindelete><input  class='adminpannelDangerousButton' type=submit value='delete the current structure'></form>";
    }
    
    else html += "no structure selected";
    html += "</div>";
    
    $("#"+isl.p.mainAdminPannel).append(html);
    
    //delete an legend event
    $("#legendadmindelete").submit(function(e){
        e.preventDefault();
	//ask for confirmation
	if(!isl.a.confirm("Do you really want to delete this structure ? ")) return;
	
        //delete the current image
        if (isl.currentLegend in isl.current.legends) {
	    var log = "delete the structure : "+isl.f.getLegendText(isl.currentLegend,isl.p.currentLanguage);
            delete isl.current.legends[ isl.currentLegend ];
            //remove the legend from all images
            for (var i in isl.current.images){
                if ( "legends" in isl.current.images[i] && isl.currentLegend in isl.current.images[i].legends ) {
                    delete isl.current.images[i].legends[isl.currentLegend];
                }
            }
        }
        isl.currentLegend = null;
        isl.a.saveData( isl.current , {log:log});
    });
    
    //remove a legend from the current image
    $("#legendadminremovefromimage").submit(function(e){
        e.preventDefault();
        //delete the current image
        if ("legends" in isl.current.images[ isl.currentImage ] && isl.currentLegend in  isl.current.images[ isl.currentImage ].legends ) {
	    var log = "remove the structure '"+isl.f.getLegendText(isl.currentLegend,isl.p.currentLanguage)+"' from image "+isl.currentImage;
            delete isl.current.images[ isl.currentImage ].legends[ isl.currentLegend ];
        }
        isl.a.saveData( isl.current, {log:log} );
    });
    
    //change the text of the legend
    $("#legendadminupdate").submit(function(e){
        e.preventDefault();
        //update the current image
        if (isl.currentLegend in isl.current.legends) {
	    //update all the legend title
	    $(".LegendLanguageTranslation").each(function(){
		var newText = $(this).val();
		var language = $(this).attr("language");
		console.log(language, newText);
		isl.current.legends[ isl.currentLegend ].text[language] = newText;
	    })

	    //update the structure
            var newStructure = $("#legendadminStructureSelector").val();
            isl.current.legends[ isl.currentLegend ].structure = newStructure;
	    
	    //update the description
	    var newDescription = $("#legendadminupdateDescription").val();
            isl.current.legends[ isl.currentLegend ].description = newDescription;
	    
	    var log = "update the structure : '"+isl.f.getLegendText(isl.currentLegend,isl.p.currentLanguage)+"'";
        }
        isl.a.saveData( isl.current , {log:log});
    });
}


//append html to the image admin pannel to add a new image
isl.a.legendadminpannelAdd = function(){
    var html = "";
    
    //get the list of images available
    var images = isl.p.getSortedImage();
    
    //print a form to add a new legend
    html += "<div id=legendadminadd><form id=legendadminaddform> <h4>add a new structure : </h4>";
    html += "<input type=hidden name=newLegendId value="+isl.f.generateId()+" />";
    html += "<input type=submit value='add the structure' />";
    html += "</select></form></div>";
    
    $("#"+isl.p.mainAdminPannel).append(html);
    
    //on submit, save the new image
    $("#legendadminaddform").submit(function(e){
        e.preventDefault();
        var newLegendId = $("[name=newLegendId]").val();
        //add a new legend
        isl.current.legends[ newLegendId ] = {
            text : {},
            id : newLegendId,
            structure : "",
        };
	//add a default text for all the languages
	for(var l in isl.current.languages){
	    isl.current.legends[ newLegendId ].text[l] = "default structure";
	}
        //focus the admin pannel on the nex legend
        isl.currentLegend = newLegendId;
        var log = "adding a new structure";
        isl.a.saveData( isl.current, {log:log} );
    });
}

//add a click event : on click, add the current legend on the current image
isl.a.legendadminpannelAddLegendOnClick = function(){ //alert("la  ???? ");
    $("#"+isl.p.mainImageContainer).unbind("click.legendadminpannelAddLegendOnClick")
    .bind("click.legendadminpannelAddLegendOnClick" , function(e){
        if ( isl.a.pannelDisplayed != "legendadminpannel") return;
        if (isl.currentLegend == null) return;
        var canvasX = e.pageX - $(this).offset().left;
        var canvasY = e.pageY - $(this).offset().top;
	//compute the percent width and height
	var percentWidth = isl.f.percentWidthFromWidth(canvasX - isl.p.legendLeftWidth); //(canvasX - isl.p.legendLeftWidth) / isl.p.mainImageWidth;
	var percentHeight = isl.f.percentHeightFromHeight(canvasY); //(canvasY ) / isl.p.mainImageHeight;
	//if (isl.p.verticalSymetry)  percentWidth = 1 - percentWidth;
	//if (isl.p.horizontalSymetry)  percentHeight = 1 - percentHeight;
        //add a the current legend to the current image
        if (!("legends" in isl.current.images[ isl.currentImage ])) isl.current.images[ isl.currentImage ].legends = {};
	isl.current.images[ isl.currentImage ].legends[ isl.currentLegend ] = {
            percentWidth : percentWidth,
            percentHeight : percentHeight,
            id : isl.currentLegend, 
        };
	var log = "update position of structure : '"+isl.f.getLegendText(isl.currentLegend,isl.p.currentLanguage)+"' on image "+isl.currentImage;
        //Save the current data
        isl.a.saveData( isl.current , {log:log});
    });
}



//retrieve the current legend
isl.a.retrieveLegendToDisplay = function(legend){
    //if no image is provided, find the first image
    if (legend != null && (legend != undefined) ) return legend;
    if (isl.currentLegend != null && (isl.currentLegend in isl.current.legends) ) {
        return  isl.current.legends[isl.currentLegend];
    }
    else{
        //pick a random legend
        for(var l in isl.current.legends) return l;
    }
    //by default, return an empty object
    return {};
}


//script to administrate the anatomical structures



//transform the main admin pannel into a structures admin pannel
isl.a.structureadminpannel = function(){
    //reset the current structure if null
    if(isl.currentStructure == null) {
	isl.currentStructure = isl.a.retrieveStructureToDisplay();
	if (isl.currentStructure != null)  isl.currentStructure = isl.currentStructure.id;
    }
    
    isl.a.structureadminpannelAdd();
    isl.a.strucutreadminpannelSelection();
    isl.a.structureadminpannelDraw();
    isl.a.structureadminpannelModify();
    
}


isl.a.strucutreadminpannelSelection = function() { 
    var html = "";
    
    //get the list of images available
    var structures = isl.current.structures;
    
    //print some data to choose the image to modify
    html += "<div id=structureadminchoose><h4>Select a legend to edit</h4> Current Legend	: <select id=structureadminchooseselector>";
    
    //push all elements to sort in an array
    var sortarr = [];
    for(var i in structures){ sortarr.push([structures[i].text, i ]) }
    sortarr.sort( function(a,b){return a[0].localeCompare(b[0]);} );
    
    for(var e in sortarr){
	var i = sortarr[e][1];
        html += "<option value="+structures[i].id+( structures[i].id == isl.currentStructure ? " selected" : " " )+">"+structures[i].text+"</option>";
    }
    html += "</select></div>";
    $("#"+isl.p.mainAdminPannel).append(html);
    
    //when selecting a structure, it is displayed
    $("#structureadminchooseselector").change( function(e){
        var structureId = $(this).val();
        isl.currentStructure = structureId;
        isl.f.resetAll();
    });
}

//enable the admin to draw on the admin pannel
isl.a.structureadminpannelDraw = function(){
    var html = "";
    html += "<form id=removeDrawingStructure>You can draw on the canvas by clicking and moving the mouse : <br>"+
	    "<input  class='adminpannelDangerousButton' type=submit value='click here to remove the drawing'></form><br>";
    
    var $container = $("#"+isl.p.mainImageContainer);
    
    $("#"+isl.p.mainAdminPannel).append(html);
    
    $container.unbind(".drawing").bind("mousedown.drawing",function(e){
	//reset the list of points created by the user
	isl.p.createdPoints = [];
	//reset the mousemove event
	$(this).unbind(".draw");
	
	$(this).on("mousemove.draw",function(e){ 
	    var $canvas = $("#"+isl.p.mainImageCanvas);
	    var percentWidth = isl.f.percentWidthFromWidth(e.pageX - $($canvas).offset().left - isl.p.legendLeftWidth) ; //(e.pageX - $($canvas).offset().left - isl.p.legendLeftWidth)/isl.p.mainImageWidth;
	    var percentHeight = isl.f.percentHeightFromHeight(e.pageY - $($canvas).offset().top) ; //(e.pageY - $($canvas).offset().top) / isl.p.mainImageHeight;
	    //if (isl.p.verticalSymetry) percentWidth = 1-percentWidth;
	    //if (isl.p.horizontalSymetry) percentHeight =  1-percentHeight;
	    isl.p.createdPoints.push( [percentWidth, percentHeight] );
	    //console.log([percentWidth, percentHeight]);
	});
	
	//reset the mouse up event
	$(this).on("mouseup.draw",function(e){
	    if (isl.a.pannelDisplayed != "structureadminpannel") return;
	    if( !(isl.currentStructure in isl.current.structures)) return;
	    //save the points
	    isl.current.structures[isl.currentStructure].draw[ isl.currentImage ]  = isl.f.filterDrawingPoints( isl.p.createdPoints );
	    //console.log("saved points", isl.current.structures[isl.currentStructure].draw[ isl.currentImage ])
	    var log = "drawing the structure "+isl.current.structures[isl.currentStructure].text+" on image "+isl.currentImage;
	    isl.a.saveData( isl.current , {log:log} );
	    isl.f.resetAll();
	    isl.p.createdPoints = [];
	});
	
	//remove the drawing from the current image
	
    });
    $("#removeDrawingStructure").bind("submit",function(e){
	e.preventDefault();
	if(!isl.a.confirm("Do you really want to remove the drawing from this image ? ")) return;
	//save the points
	if (isl.currentImage != null && isl.currentImage in isl.current.structures[isl.currentStructure].draw) {
	    delete isl.current.structures[isl.currentStructure].draw[ isl.currentImage ];
	    var log = "removing the structure "+isl.current.structures[isl.currentStructure].text+" on image "+isl.currentImage;
	    isl.a.saveData( isl.current , {log:log} );
	}
    });
    
}

//function to filter the points to save when saving the drawing of the user
//remove points that are too close of one another
isl.f.filterDrawingPoints = function(points){
    var result = [];
    var npoints = 20;
    if (points.length<3) return result;
    if (points.length<npoints) return points;
    
    for(var p = 0 ; p<npoints; p++ ){
	result.push( points[ Math.floor(p*points.length/npoints) ] );
    }
    return result;
}


isl.a.structureadminpannelModify = function(){
    var html = "";
    //a div to display a form to modify an already existing image
    var displayedStructure = isl.a.retrieveStructureToDisplay();
    html += "<div id=structureadminmodify>";
    
    if ( "text" in displayedStructure ) {
        
        html += "<form id=structureadminupdate>"+
		"<h4>Edit legend parameters</h4>"+
		"Name : <input id=structureadminupdateText type=text value='"+ displayedStructure.text +"'><br>";
        //add a form to pick the color of the anatomical group
        html += "Color : <select id=structureadminupdateColor><option disabled>Choose a color</option>";
        var previousColor = displayedStructure.color;
	
	//push all elements to sort in an array
	var sortarr = [];
	for(var c in isl.colors){ sortarr.push([isl.colors[c], c ]) }
	sortarr.sort( function(a,b){return a[0].localeCompare(b[0]);} );
    
        for(var e in sortarr){
	    var c = sortarr[e][1];
            html += "<option value="+isl.colors[c]+" style='color:"+isl.colors[c]+"' "+(previousColor == isl.colors[c] ? "selected" : "")+">"+isl.colors[c]+"</option>";
        }
        html += "</select><br><input type=submit value='update the legend'></form><br>";
	html += "<form id=structureadmindelete><h4>Delete : </h4><input  class='adminpannelDangerousButton' type=submit value='delete the current legend'></form><br>";
    }
    else html += "no legend selected";
    html += "</div>";
    
    $("#"+isl.p.mainAdminPannel).append(html);
    
    //delete an structure event
    $("#structureadmindelete").submit(function(e){
        e.preventDefault();
	//ask for confirmation
	if(!isl.a.confirm("Do you really want to delete this legend? ")) return;
        //delete the current image
        if (isl.currentStructure in isl.current.structures) {
	    var log = "deleting the structure "+isl.current.structures[isl.currentStructure].text;
            delete isl.current.structures[ isl.currentStructure ];
            //remove the structure from all legends
            for (var i in isl.current.legends){
                if ( "structure" in isl.current.legends[i] && isl.current.legends[i].structure == isl.currentStrcture ) {
                    isl.current.legends[i].structure = "";
                }
            }
        }
        isl.currentStructure = null;
        isl.a.saveData( isl.current, {log:log} );
    });
    
    //change the text of the legend
    $("#structureadminupdate").submit(function(e){
        e.preventDefault();
	console.log(isl.currentStructure);
	console.log(isl.current.structures);
        if (isl.currentStructure in isl.current.structures) {
	    //update the text of the structure
            var newText = $("#structureadminupdateText").val();
            isl.current.structures[ isl.currentStructure ].text = newText;
	    //update the color
            var newColor = $("#structureadminupdateColor").val();
            isl.current.structures[ isl.currentStructure ].color = newColor;
	    var log = "changing color and text of legend : "+isl.current.structures[isl.currentStructure].text;
        }
        isl.a.saveData( isl.current, {log:log} );
    });
}

isl.a.structureadminpannelAdd = function(){
    var html = "";
    
    //print a form to add a new legend
    html += "<div id=strctureadminadd><form id=structureadminaddform><h4> add a new legend</h4>";
    html += "<input type=hidden name=newStructureId value="+isl.f.generateId()+" />";
    html += "<input type=submit value='add the legend' />";
    html += "</select></form></div>";
    
    $("#"+isl.p.mainAdminPannel).append(html);
    
    //on submit, save the new image
    $("#structureadminaddform").submit(function(e){
        e.preventDefault();
        var newStructure = $("[name=newStructureId]").val();
        //add a new legend
        isl.current.structures[ newStructure ] = {
            text : "new legend name",
            id : newStructure,
            color : "white",
	    draw: {},
        };
        //by default, display the structure
        isl.p.displayedStructures.push(newStructure);
	        
        //focus the admin pannel on the nex legend
        isl.currentStructure = newStructure;
        var log = "adding a new legend : "+isl.current.structures[isl.currentStructure].text;
	
        isl.a.saveData( isl.current, {log:log} );
    });
}




//retrieve the current structure object
isl.a.retrieveStructureToDisplay = function(structure){
    //if no structure is provided, find the first structure
    if (structure != null && (structure != undefined) ) return structure;
    if (isl.currentStructure!= null && (isl.currentStructure in isl.current.structures) ) {
        return isl.current.structures[isl.currentStructure];
    }
    else{
        //pick a random legend
        for(var l in isl.current.structures) return isl.current.structures[l];
    }
    //by default, return an empty object
    return {};
}


//script to use several laguages with isl






//transform the main admin pannel into an language admin pannel
isl.a.languageadminpannel = function(){
    isl.a.languageadminpannelAdd();
    isl.a.languageadminpannelSelection();
    isl.a.languageadminpannelModify();
}



//append html on the language admin pannel to choose the language to modify
isl.a.languageadminpannelSelection = function(){
    var html = "";
    
    //get the list of languages available
    var languages = isl.current.languages;
    
    //print some data to choose the language to modify
    html += "<div id=languageadminchoose><h4>Select a language :</h4> <select id=languageadminchooseselector>";
    
    //sort the languages 
    var sortarr = [];
    for(l in languages){ sortarr.push([l, languages[l] ]) }
    sortarr.sort( function(a,b){return a[0].localeCompare(b[0]);} );
    
    for(var i in sortarr){
	var l = sortarr[i][0];
        html += "<option value='"+languages[l]+"' "+( languages[l] == isl.p.currentLanguage ? " selected" : " " )+">"+l+"</option>";
    }
    html += "</select></div>";
    $("#"+isl.p.mainAdminPannel).append(html);
    
    //when selecting an language, it is displayed a s the main language
    $("#languageadminchooseselector").change( function(e){
        var language = $(this).val();
        console.log(language);
        isl.p.currentLanguage = language;
        isl.f.resetAll();
    });
}

//append html to the language admin pannel to modify the current language
isl.a.languageadminpannelModify = function(){
    var html = "";
    //a div to display a form to modify an already existing language
    var displayedImage = isl.f.retrieveCurrentLanguage();
    html += "<form id=languageadmindelete><input  class='adminpannelDangerousButton' type=submit value='delete the current language'></form>";
    
    $("#"+isl.p.mainAdminPannel).append(html);
    
    //delete an language event
    $("#languageadmindelete").submit(function(e){
        e.preventDefault();
        //ask for confirmation
	if(!isl.a.confirm("Do you really want to delete this language ? ")) return;
	var log = "delete the language "+isl.current.languages[ isl.p.currentLanguage ];
        if (isl.p.currentLanguage in isl.current.languages) {
            delete isl.current.languages[ isl.p.currentLanguage ];
            if (isl.current.general.defaultLanguage == isl.p.currentLanguage) isl.current.general.defaultLanguage = null;
            
        }
        isl.p.currentLanguage = isl.f.retrieveCurrentLanguage();
        isl.a.saveData( isl.current ,{log:log});
    });
}


//append html to the language admin pannel to add a new language
isl.a.languageadminpannelAdd = function(){
    var html = "";
    
    //get the list of languages available
    var languages = isl.current.languages;
    
    //print a form to add a new language
    html += "<div id=languageadminadd><form id=languageadminaddform> <h4>Add a new language</h4>";
    html += "Name : <input name=name type=text id=languageadminaddname value='new language'> <br> <input type=submit value='add language' />";
    html += "</form></div>";
    
    $("#"+isl.p.mainAdminPannel).append(html);
    
    //on submit, save the new language
    $("#languageadminadd").submit(function(e){
        e.preventDefault();
        var language = $("#languageadminaddname").val();
        if (language == "") return;
        isl.current.languages[language] = language;
	var log = "adding the language : "+language;
        isl.a.saveData ( isl.current , {log:log}) ;
        
    });
}



//script to use several series with isl

//transform the main admin pannel into an series admin pannel
isl.a.seriesadminpannel = function(){
    isl.a.seriesadminpannelAdd();
    isl.a.seriesadminpannelSelection();
    isl.a.seriesadminpannelModify();
}



//append html on the series admin pannel to choose the series to modify
isl.a.seriesadminpannelSelection = function(){
    var html = "";
    
    //get the list of series available
    var series = isl.current.series;
    
    //print some data to choose the series to modify
    html += "<div id=seriesadminchoose><h4>Select a series : </h4><select id=seriesadminchooseselector>";
    
    //push all elements to sort in an array
    var sortarr = [];
    for(sid in series){ sortarr.push([series[sid].text, sid ]) }
    sortarr.sort( function(a,b){return a[0].localeCompare(b[0]);} );
    
    for(var e in sortarr){
	var i = sortarr[e][1];
        html += "<option value='"+i+"' "+( i == isl.p.currentSeries ? " selected" : " " )+">"+series[i].text+"</option>";
    }
    html += "</select></div>";
    $("#"+isl.p.mainAdminPannel).append(html);
    
    //when selecting an series, it is displayed a s the main series
    $("#seriesadminchooseselector").change( function(e){
        var series = $(this).val();
        console.log(series);
        isl.p.currentSeries = series;
        isl.f.resetAll();
    });
}

//append html to the series admin pannel to modify the current series
isl.a.seriesadminpannelModify = function(){
    var html = "";
    //a div to display a form to modify an already existing series
    html += "<form id=seriesadmindelete><input class='adminpannelDangerousButton' type=submit value='delete the current series'></form>";
    
    $("#"+isl.p.mainAdminPannel).append(html);
    
    //delete an series event
    $("#seriesadmindelete").submit(function(e){
        //ask for confirmation
	if(!isl.a.confirm("do you really want to delete this series ? ")) return;
	var log = "deleting the series "+isl.current.series[ isl.p.currentSeries ].text;
        e.preventDefault();
        if (isl.p.currentSeries in isl.current.series) {
            delete isl.current.series[ isl.p.currentSeries ];
        }
        isl.p.currentSeries = null;
        isl.a.saveData( isl.current ,{log:log});
    });
}


//append html to the series admin pannel to add a new series
isl.a.seriesadminpannelAdd = function(){
    var html = "";
    
    //get the list of series available
    var series = isl.current.series;
    
    //print a form to add a new series
    html += "<div id=seriesadminadd><form id=seriesadminaddform> <h4>Add a new series</h4>";
    html += "Name : <input name=name type=text id=seriesadminaddname value='new series'><br> <input type=submit value='add series' />";
    html += "</form></div>";
    
    $("#"+isl.p.mainAdminPannel).append(html);
    
    //on submit, save the new series
    $("#seriesadminadd").submit(function(e){
        e.preventDefault();
        var series = $("#seriesadminaddname").val();
        var newSeriesId = isl.f.generateId();
        if (series == "") return;
        isl.current.series[newSeriesId] = {text: series};
        isl.p.currentSeries = newSeriesId;
	var log = "adding the new series "+series;
        isl.a.saveData ( isl.current ,{log:log}) ;
        
    });
}



//edit general informations about the project

//transform the main admin pannel into an general admin pannel
isl.a.generaladminpannel = function(){
    isl.a.generaladminpannelFree();
    //isl.a.changeCurrentMainImage();
    isl.a.generaladminpannelAddlogo();
    isl.a.generaladminpannelModify();
    isl.a.generalAdminpannelDelete();
}

//append html to delete the current project
isl.a.generalAdminpannelDelete = function(){
    var html = "";
    
    //print some data to choose the image to modify
    html += "<form id=adminpanneldelete>";
    html += "<input class='adminpannelDangerousButton' type=submit value='delete the current project'>";
    html += "</form><br><br>";
    
    $("#"+isl.p.mainAdminPannel).append(html);
    
    //when selecting an image, it is displayed a s the main image
    $("#adminpanneldelete").submit( function(e){
        e.preventDefault();
        
        if(!isl.a.confirm("Do you really want to delete this project ? ")) return;
        
        var formData= new FormData();
        formData.append("id",isl.current.id);
        //change the cursor
        $('html, body').css("cursor", "wait");
        var jqxhr = $.ajax({
            url: "/delete",
            method: "POST",
            data: formData,
            processData: false,
            contentType: false,
            type: "POST",
            })
            .done(function() {
                //window.location.replace("/userpannel");
		window.top.location.href = "/userpannel";
            })
            .fail(function() {
                alert( "error while deleting the project" );
            })
            .always(function() {
                $('html, body').css("cursor", "auto");
            });
    });
}


//append some html to change the logo of the logo of the project
isl.a.changeCurrentMainImage = function(){
    var html = "";
    
    //print some data to choose the image to modify
    html += "<form id=generaladminpannelformdefaultimage>";
    html += "<input type=submit value='use the current image as logo for the project'>";
    html += "</form><br>";
    
    $("#"+isl.p.mainAdminPannel).append(html);
    
    //when selecting an image, it is displayed a s the main image
    $("#generaladminpannelformdefaultimage").submit( function(e){
        e.preventDefault();
        isl.current.general.logo = isl.currentImage;
	var log = "modification of the default image";
        isl.a.saveData( isl.current ,{log:log});
    });
}

//append html on the image admin pannel to choose the image to modify
isl.a.generaladminpannelModify = function(){
    var html = "";
    
    //print some data to choose the image to modify
    html += "<form id=generaladminpannelform>";
    html += "<h4>General informations of the project</h4>"
    html += "Project Name : <input id=generaladminpannelformname value='"+("name" in isl.current.general ? isl.current.general.name : "")+"'><br>";
    html += "Project Tag &nbsp;&nbsp;&nbsp;: <input id=generaladminpannelformtag value='"+("tag" in isl.current.general ? isl.current.general.tag : "")+"'><br>";
    html += "<input type=submit value='change general information'>";
    html += "</form><br>";
    
    $("#"+isl.p.mainAdminPannel).append(html);
    
    //when selecting an image, it is displayed a s the main image
    $("#generaladminpannelform").submit( function(e){
        e.preventDefault();
        var name = $("#generaladminpannelformname").val();
        var tag = $("#generaladminpannelformtag").val();
        isl.current.general.name = name;
        isl.current.general.tag = tag;
        var log = "modification of the tag("+tag+") and name("+name+") of the project";
        isl.a.saveData( isl.current , {log:log} );
    });
}

//append html on the general admin pannel to change the free/premium variable of the project
isl.a.generaladminpannelFree = function(){
    var html = "";
    
    //print some data to choose the image to modify
    html += "<form id=premiumadminpannelform>";
    html += "<h4>Set the access to FREE or Premium</h4>"
    html += "Set project access to FREE : <input type=checkbox id=freeAccess "+("free" in isl.current.general && isl.current.general.free == "free" ?  "checked" : "")+"><br>";
    html += "</form><br>";
    
    $("#"+isl.p.mainAdminPannel).append(html);
    
    //change the free and premium value
    $("#freeAccess").change( function(e){
        e.preventDefault();
        isl.current.general.free = $(this).is(':checked') ? "free" : "premium";
        console.log(isl.current.general.free);
	var log = "change the access mode to "+isl.current.general.free;
        isl.a.saveData( isl.current , {log:log});
    });
}

//propose the user to add an image as the logo
isl.a.generaladminpannelAddlogo = function(){
    var html = "";
    
    //get the list of images available
    //get the images for all the series
    var images = isl.p.getSortedImage(null);
    
    //print a form to add a new image
    html += "<div id=imageadminadd><form id=imageadminaddlogo> <h4>Add image as the project logo</h4>";
    if("logo" in isl.current.general) html += "current logo : <img src='/bundles/swarminfoimagescript/ressources/"+isl.current.id+"/"+isl.current.general.logo+"' style='width:85px;' ><br>";
    html += "Add logo file : <input name=file type=file id=imageadminaddfile >";
    html += "<input type=submit value='add the image' />";
    html += "</form></div>";
    
    $("#"+isl.p.mainAdminPannel).append(html);
    
    //on submit, save the new image
    $("#imageadminaddlogo").submit(function(e){
        e.preventDefault();
        var input = document.getElementById("imageadminaddfile");
        //if no file has been provided,don't do anything
        //same if the file isn't an image
        try{
            if(!input.files[0].type.match(/image.*/)) return;
        }
        catch(e){
            return;
        }
        
        //build a new image with a new name
        
        var names = isl.a.inputFileName(input);
	var ids = isl.f.generateIds(names.length);
	
        var ressource = {
            input : input,
            imagename : names,
        }
        isl.current.general.logo = names[0];
	var log = "changing the logo of the project";
	ressource.log = log;
        //change the main image to be displayed :
        isl.a.saveData( isl.current, ressource );
    });
}


//script to manage trackdown as an admin

//when reseting all, add event to change the position of the cursor on the trackdowns
isl.p.onResetAll.push("isl.f.resetTrackdownEvent");


//transform the main admin pannel into a trackdown admin pannel
isl.a.trackdownadminpannel = function(){
    isl.a.trackdownadminpannelAdd();
    isl.a.trackdownadminpannelRemove();
    
}

//append html to the image admin pannel to modify the current image
isl.a.trackdownadminpannelRemove = function(){
    var html = "";
    var trackdowns = isl.f.getTrackdownImages();
    html += "<div id=trackdowndelete>"
    html += "<form id=trackdonwdeleteform>"
    html += "<h4>delete a track-down :</h4> <select id=trackdownimageselection><option value='' >select a trackdown</option>";
    for(var tid in trackdowns){
        html += "<option value='"+tid+"'  >"+trackdowns[tid].text+"</option>";
    }
    html += "</select> <input  class='adminpannelDangerousButton' type=submit value='delete the trackdown'><br></form>";
    html += "</div>";
    
    $("#"+isl.p.mainAdminPannel).append(html);
    
    //delete an image event
    $("#trackdonwdeleteform").submit(function(e){
        e.preventDefault();
        //ask for confirmation
	if(!isl.a.confirm("do you really want to delete this track-down ? ")) return;
        //delete the current image
        var trackdowns = isl.f.getTrackdownImages();
        var tid = $("#trackdownimageselection").val();
        if (tid in trackdowns) {
            delete isl.current.trackdown[tid];
            isl.a.saveData( isl.current );
        }
    });
    
}


//append html to the image admin pannel to add a new image
isl.a.trackdownadminpannelAdd = function(){
    var html = "";
    
    //get the list of images available
    //get the images for all the series
    var images = isl.p.getSortedImage(null);
    
    //print a form to add a new image
    html += "<div id=trackdownadminadd><form id=trackdownadminaddform> <h4>add a new trackdown</h4>";
    html += "image of the new trackdown : <input name=file type=file id=trackdownadminaddfile><br> ";
    html += "name of the new trackdown  : <input type=text id=trackdownadminaddtext placeholder='name of the new trackdown'><br>";
    html += "<input type=submit value='add the trackdown' />";
    html += "</select></form></div><br>";
    
    $("#"+isl.p.mainAdminPannel).append(html);
    
    //on submit, save the new image
    $("#trackdownadminaddform").submit(function(e){
        e.preventDefault();
        var input = document.getElementById("trackdownadminaddfile");
        //if no file has been provided,don't do anything
        //same if the file isn't an image
        try{
            if(!input.files[0].type.match(/image.*/)) return;
        }
        catch(e){
            return;
        }
        
        //build a new image with a new name
        var id = isl.f.generateId();
        var names = ""+ id + isl.a.inputFileName(input);
        var name = names[0];
        var text = $("#trackdownadminaddtext").val();
        var ressource = {
            input : input,
            imagename : name,
        }
        if (!("trackdown" in isl.current)) {
            isl.current.trackdown = {};
        }
        isl.current.trackdown[id] = {
            id : id,
            url : "/bundles/swarminfoimagescript/ressources/"+isl.current.id+"/"+name,
            text : text
        };
        //change the main image to be displayed :
        isl.currentImage = id;
        isl.a.saveData( isl.current, ressource , isl.f.reloadLeftTrackDownImages);
        //reload and store the images of the left trackdown
        isl.f.reloadLeftTrackDownImages();
    });
}



//event to change the position of cursors for trackdowns
isl.f.resetTrackdownEvent = function(){  
    $(".trackdowncanvas").unbind('.cursorevent').bind("click",function(e){
        if (isl.currentImage == null) return;
        if (!("trackdown" in isl.current.images[isl.currentImage])) isl.current.images[isl.currentImage].trackdown = {};
        
        var tid = $(this).attr("tid");
        var id = $(this).attr("id");
        var c=document.getElementById( id );
        var ctx=c.getContext("2d");
        
        var canvasX = e.pageX - $(this).offset().left;
        var canvasY = e.pageY - $(this).offset().top;
	//compute the percent width and height
	var percentWidth = (canvasX) / ctx.canvas.clientWidth;
	var percentHeight = (canvasY) / ctx.canvas.clientHeight;
        
        isl.current.images[isl.currentImage].trackdown[tid]={
            percentWidth : percentWidth,
            percentHeight: percentHeight,
            id: tid
        }
        isl.a.saveData( isl.current );
    });
    
}



