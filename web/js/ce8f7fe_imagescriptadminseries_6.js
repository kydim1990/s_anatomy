//script to use several series with isl

//transform the main admin pannel into an series admin pannel
isl.a.seriesadminpannel = function(){
    isl.a.seriesadminpannelAdd();
    isl.a.seriesadminpannelSelection();
    isl.a.seriesadminpannelModify();
}



//append html on the series admin pannel to choose the series to modify
isl.a.seriesadminpannelSelection = function(){
    var html = "";
    
    //get the list of series available
    var series = isl.current.series;
    
    //print some data to choose the series to modify
    html += "<div id=seriesadminchoose><h4>Select a series : </h4><select id=seriesadminchooseselector>";
    
    //push all elements to sort in an array
    var sortarr = [];
    for(sid in series){ sortarr.push([series[sid].text, sid ]) }
    sortarr.sort( function(a,b){return a[0].localeCompare(b[0]);} );
    
    for(var e in sortarr){
	var i = sortarr[e][1];
        html += "<option value='"+i+"' "+( i == isl.p.currentSeries ? " selected" : " " )+">"+series[i].text+"</option>";
    }
    html += "</select></div>";
    $("#"+isl.p.mainAdminPannel).append(html);
    
    //when selecting an series, it is displayed a s the main series
    $("#seriesadminchooseselector").change( function(e){
        var series = $(this).val();
        console.log(series);
        isl.p.currentSeries = series;
        isl.f.resetAll();
    });
}

//append html to the series admin pannel to modify the current series
isl.a.seriesadminpannelModify = function(){
    var html = "";
    //a div to display a form to modify an already existing series
    html += "<form id=seriesadmindelete><input class='adminpannelDangerousButton' type=submit value='delete the current series'></form>";
    
    $("#"+isl.p.mainAdminPannel).append(html);
    
    //delete an series event
    $("#seriesadmindelete").submit(function(e){
        //ask for confirmation
	if(!isl.a.confirm("do you really want to delete this series ? ")) return;
	var log = "deleting the series "+isl.current.series[ isl.p.currentSeries ].text;
        e.preventDefault();
        if (isl.p.currentSeries in isl.current.series) {
            delete isl.current.series[ isl.p.currentSeries ];
        }
        isl.p.currentSeries = null;
        isl.a.saveData( isl.current ,{log:log});
    });
}


//append html to the series admin pannel to add a new series
isl.a.seriesadminpannelAdd = function(){
    var html = "";
    
    //get the list of series available
    var series = isl.current.series;
    
    //print a form to add a new series
    html += "<div id=seriesadminadd><form id=seriesadminaddform> <h4>Add a new series</h4>";
    html += "Name : <input name=name type=text id=seriesadminaddname value='new series'><br> <input type=submit value='add series' />";
    html += "</form></div>";
    
    $("#"+isl.p.mainAdminPannel).append(html);
    
    //on submit, save the new series
    $("#seriesadminadd").submit(function(e){
        e.preventDefault();
        var series = $("#seriesadminaddname").val();
        var newSeriesId = isl.f.generateId();
        if (series == "") return;
        isl.current.series[newSeriesId] = {text: series};
        isl.p.currentSeries = newSeriesId;
	var log = "adding the new series "+series;
        isl.a.saveData ( isl.current ,{log:log}) ;
        
    });
}


