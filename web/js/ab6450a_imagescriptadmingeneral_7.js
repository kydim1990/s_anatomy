//edit general informations about the project

//transform the main admin pannel into an general admin pannel
isl.a.generaladminpannel = function(){
    isl.a.generaladminpannelFree();
    //isl.a.changeCurrentMainImage();
    isl.a.generaladminpannelAddlogo();
    isl.a.generaladminpannelModify();
    isl.a.generalAdminpannelDelete();
}

//append html to delete the current project
isl.a.generalAdminpannelDelete = function(){
    var html = "";
    
    //print some data to choose the image to modify
    html += "<form id=adminpanneldelete>";
    html += "<input class='adminpannelDangerousButton' type=submit value='delete the current project'>";
    html += "</form><br><br>";
    
    $("#"+isl.p.mainAdminPannel).append(html);
    
    //when selecting an image, it is displayed a s the main image
    $("#adminpanneldelete").submit( function(e){
        e.preventDefault();
        
        if(!isl.a.confirm("Do you really want to delete this project ? ")) return;
        
        var formData= new FormData();
        formData.append("id",isl.current.id);
        //change the cursor
        $('html, body').css("cursor", "wait");
        var jqxhr = $.ajax({
            url: "/delete",
            method: "POST",
            data: formData,
            processData: false,
            contentType: false,
            type: "POST",
            })
            .done(function() {
                //window.location.replace("/userpannel");
		window.top.location.href = "/userpannel";
            })
            .fail(function() {
                alert( "error while deleting the project" );
            })
            .always(function() {
                $('html, body').css("cursor", "auto");
            });
    });
}


//append some html to change the logo of the logo of the project
isl.a.changeCurrentMainImage = function(){
    var html = "";
    
    //print some data to choose the image to modify
    html += "<form id=generaladminpannelformdefaultimage>";
    html += "<input type=submit value='use the current image as logo for the project'>";
    html += "</form><br>";
    
    $("#"+isl.p.mainAdminPannel).append(html);
    
    //when selecting an image, it is displayed a s the main image
    $("#generaladminpannelformdefaultimage").submit( function(e){
        e.preventDefault();
        isl.current.general.logo = isl.currentImage;
	var log = "modification of the default image";
        isl.a.saveData( isl.current ,{log:log});
    });
}

//append html on the image admin pannel to choose the image to modify
isl.a.generaladminpannelModify = function(){
    var html = "";
    
    //print some data to choose the image to modify
    html += "<form id=generaladminpannelform>";
    html += "<h4>General informations of the project</h4>"
    html += "Project Name : <input id=generaladminpannelformname value='"+("name" in isl.current.general ? isl.current.general.name : "")+"'><br>";
    html += "Project Tag &nbsp;&nbsp;&nbsp;: <input id=generaladminpannelformtag value='"+("tag" in isl.current.general ? isl.current.general.tag : "")+"'><br>";
    html += "<input type=submit value='change general information'>";
    html += "</form><br>";
    
    $("#"+isl.p.mainAdminPannel).append(html);
    
    //when selecting an image, it is displayed a s the main image
    $("#generaladminpannelform").submit( function(e){
        e.preventDefault();
        var name = $("#generaladminpannelformname").val();
        var tag = $("#generaladminpannelformtag").val();
        isl.current.general.name = name;
        isl.current.general.tag = tag;
        var log = "modification of the tag("+tag+") and name("+name+") of the project";
        isl.a.saveData( isl.current , {log:log} );
    });
}

//append html on the general admin pannel to change the free/premium variable of the project
isl.a.generaladminpannelFree = function(){
    var html = "";
    
    //print some data to choose the image to modify
    html += "<form id=premiumadminpannelform>";
    html += "<h4>Set the access to FREE or Premium</h4>"
    html += "Set project access to FREE : <input type=checkbox id=freeAccess "+("free" in isl.current.general && isl.current.general.free == "free" ?  "checked" : "")+"><br>";
    html += "</form><br>";
    
    $("#"+isl.p.mainAdminPannel).append(html);
    
    //change the free and premium value
    $("#freeAccess").change( function(e){
        e.preventDefault();
        isl.current.general.free = $(this).is(':checked') ? "free" : "premium";
        console.log(isl.current.general.free);
	var log = "change the access mode to "+isl.current.general.free;
        isl.a.saveData( isl.current , {log:log});
    });
}

//propose the user to add an image as the logo
isl.a.generaladminpannelAddlogo = function(){
    var html = "";
    
    //get the list of images available
    //get the images for all the series
    var images = isl.p.getSortedImage(null);
    
    //print a form to add a new image
    html += "<div id=imageadminadd><form id=imageadminaddlogo> <h4>Add image as the project logo</h4>";
    if("logo" in isl.current.general) html += "current logo : <img src='/bundles/swarminfoimagescript/ressources/"+isl.current.id+"/"+isl.current.general.logo+"' style='width:85px;' ><br>";
    html += "Add logo file : <input name=file type=file id=imageadminaddfile >";
    html += "<input type=submit value='add the image' />";
    html += "</form></div>";
    
    $("#"+isl.p.mainAdminPannel).append(html);
    
    //on submit, save the new image
    $("#imageadminaddlogo").submit(function(e){
        e.preventDefault();
        var input = document.getElementById("imageadminaddfile");
        //if no file has been provided,don't do anything
        //same if the file isn't an image
        try{
            if(!input.files[0].type.match(/image.*/)) return;
        }
        catch(e){
            return;
        }
        
        //build a new image with a new name
        
        var names = isl.a.inputFileName(input);
	var ids = isl.f.generateIds(names.length);
	
        var ressource = {
            input : input,
            imagename : names,
        }
        isl.current.general.logo = names[0];
	var log = "changing the logo of the project";
	ressource.log = log;
        //change the main image to be displayed :
        isl.a.saveData( isl.current, ressource );
    });
}

