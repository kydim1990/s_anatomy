
// console.log(findlocate);
// console.log(var currentLanguage);

var TranslateDictRu={
    'Password must meet the following requirements': 'Пароль должен отвечать следующим требованиям',
    'At least': 'Как минимум',
    'one capital letter': 'одну заглавную букву',
    'one number': 'одну цифру',
    'one special chars': 'один специальный символ',
    'characters': 'символов'
};

// dict for translate menu bar SC
var TranslateDictSC={
    'Password must meet the following requirements': 'Password must meet the following requirements',
    'At least': 'At least',
    'one capital letter': 'one capital letter',
    'one number': 'one number',
    'one special chars': 'one special chars',
    'characters': 'characters'
};

// dict for translate menu bar TC
var TranslateDictTC={
    'Password must meet the following requirements': 'Password must meet the following requirements',
    'At least': 'At least',
    'one capital letter': 'one capital letter',
    'one number': 'one number',
    'one special chars': 'one special chars',
    'characters': 'characters'
};

// dict for translate menu bar JA 
var TranslateDictJA={
    'Password must meet the following requirements': 'Password must meet the following requirements',
    'At least': 'At least',
    'one capital letter': 'one capital letter',
    'one number': 'one number',
    'one special chars': 'one special chars',
    'characters': 'characters'
};

// dict for translate menu bar FR 
var TranslateDictFR={
    'Password must meet the following requirements': 'Password must meet the following requirements',
    'At least': 'At least',
    'one capital letter': 'one capital letter',
    'one number': 'one number',
    'one special chars': 'one special chars',
    'characters': 'characters'
};

// dict for translate menu bar ES
var TranslateDictES={
    'Password must meet the following requirements': 'Password must meet the following requirements',
    'At least': 'At least',
    'one capital letter': 'one capital letter',
    'one number': 'one number',
    'one special chars': 'one special chars',
    'characters': 'characters'
};

// dict for translate menu bar DE 
var TranslateDictDE={
    'Password must meet the following requirements': 'Password must meet the following requirements',
    'At least': 'At least',
    'one capital letter': 'one capital letter',
    'one number': 'one number',
    'one special chars': 'one special chars',
    'characters': 'characters'
};

// dict for translate menu bar KE
var TranslateDictKE={
    'Password must meet the following requirements': 'Password must meet the following requirements',
    'At least': 'At least',
    'one capital letter': 'one capital letter',
    'one number': 'one number',
    'one special chars': 'one special chars',
    'characters': 'characters'
};

// dict for translate menu bar PT 
var TranslateDictPT={
    'Password must meet the following requirements': 'Password must meet the following requirements',
    'At least': 'At least',
    'one capital letter': 'one capital letter',
    'one number': 'one number',
    'one special chars': 'one special chars',
    'characters': 'characters'
};

// dict for translate menu bar AR
var TranslateDictAR={
    'Password must meet the following requirements': 'Password must meet the following requirements',
    'At least': 'At least',
    'one capital letter': 'one capital letter',
    'one number': 'one number',
    'one special chars': 'one special chars',
    'characters': 'characters'
};

var currentlocale;

function strength(){
    var str = document.URL;
    var n = str.search(/\/[a-z][a-z]\//);
    currentlocale=str.substring(n+1, n+3);
    // console.log(str);
    var passstrong = "<div id='pswd_info'> <h4>"+translate('Password must meet the following requirements')+":</h4>";
    passstrong += "<ul>";
    passstrong += "<li id='capital' class='invalid'>"+translate('At least')+ "<strong> "+translate('one capital letter')+"</strong></li>";
    //passstrong += "<li id='number' class='invalid'>"+translate('At least')+ "<strong> "+translate('one number')+"</strong></li>";
    //passstrong += "<li id='letter' class='invalid'>"+translate('At least')+ "<strong> "+translate('one special chars')+"</strong></li> ";
    passstrong += "<li id='length' class='invalid'>"+translate('At least')+ "<strong> 8 "+translate('characters')+" </strong></li>";
    passstrong += "</ul> </div>";
    $( "#fos_user_registration_form_plainPassword_first" ).after(passstrong);

    $('input[id=fos_user_registration_form_plainPassword_first]').keyup(function() {
        // keyup event code here
    });
    $('input[id=fos_user_registration_form_plainPassword_first]').focus(function() {
        // focus code here
    });
    $('input[id=fos_user_registration_form_plainPassword_first]').blur(function() {
        // blur code here
    });


    $('input[id=fos_user_registration_form_plainPassword_first]').keyup(function() {
        var pswd = $(this).val();

        if ( pswd.length < 8 ) {
            $('#length').removeClass('valid').addClass('invalid');
        } else {
            $('#length').removeClass('invalid').addClass('valid');
        }

        // one special char
        /*if (pswd.match(/([!,@,#,$,%,^,&,*,?,_,~])/) ) {
            $('#letter').removeClass('invalid').addClass('valid');
        } else {
            $('#letter').removeClass('valid').addClass('invalid');
        }*/

        //validate capital letter
        if ( pswd.match(/[A-Z]/) ) {
            $('#capital').removeClass('invalid').addClass('valid');
        } else {
            $('#capital').removeClass('valid').addClass('invalid');
        }

        //validate number
        /*if ( pswd.match(/\d/) ) {
            $('#number').removeClass('invalid').addClass('valid');
            $('input[type="submit"]').prop('disabled', false);
        } else {
            $('#number').removeClass('valid').addClass('invalid');
        }*/

    }).focus(function() {
        $('#pswd_info').show();
    }).blur(function() {
        $('#pswd_info').hide();
    });


}

function  checkpass() {
    if ($('#length').hasClass("valid") /*&& $('#letter').hasClass("valid")*/ && $('#capital').hasClass("valid") /*&& $('#number').hasClass("valid")*/ )
    {
        return true;
    }
    return false
}



function translate(text) {
    // console.log(currentlocale);
    switch(currentlocale){
        case 'ru':return TranslateDictRu[text];
        case 'sc':return TranslateDictSC[text];
        case 'tc':return TranslateDictTC[text];
        case 'ja':return TranslateDictJA[text];
        case 'fr':return TranslateDictFR[text];
        case 'es':return TranslateDictES[text];
        case 'de':return TranslateDictDE[text];
        case 'ke':return TranslateDictKE[text];
        case 'pt':return TranslateDictPT[text];
        case 'ar':return TranslateDictAR[text];
        default: return text;
    }
    // console.log(var TranslateDictRu['displayed']);
}
