/**
 * Created by Diman on 02.03.2017.
 */
$( document ).ready(function() {

    // console.log('admin_security_ready');
    $('#username').change(function() {
        isadmin($(this).val());
    });

    $('#security_code').change(function() {
        checksecuritycode($(this).val());
    });

});

var security_code=null;

isadmin = function (username) {
    $.ajax({
        type: 'POST',
        url: '/isadmin',
        data: {
            username: username
        },
        cache: false,
        success: function(result) {
            if(result){
                var resultObj = JSON.parse(result);
                if (resultObj['result']=='ok'){
                    $(".admin_security").css("display","block");
                    $("#security_code").prop('required',true);
                    security_code=resultObj['code'];
                    // console.log('security_code='+security_code);

                    $(':input[type="submit"]').prop('disabled', true);

                }
                else{
                    $(".admin_security").css("display","none");
                    $("#security_code").prop('required',false);
                    $(':input[type="submit"]').prop('disabled', false);

                }
                // console.log('username_result='+result);

            }else{
                $(".admin_security").css("display","none");
                $("#security_code").prop('required',false);
                $(':input[type="submit"]').prop('disabled', false);
                console.log("is not admin");
            }
        }
    });
};

checksecuritycode = function (code) {
    $.ajax({
        type: 'POST',
        url: '/isadmin',
        data: {
            _code:code
        },
        cache: false,
        success: function(result) {
            if(result){
                var resultObj = JSON.parse(result);
                if (resultObj['result']=='ok'){
                    console.log('code is good!!');
                    $('#security_error').text('OK');
                    $(':input[type="submit"]').prop('disabled', false);
                    $("#security_error").css("color", "green");
                }
                else{
                    $('#security_error').text('Fail Code');
                    $(':input[type="submit"]').prop('disabled', true);
                    $("#security_error").css("color", "red");
                }
                // console.log('checksecuritycode='+result);

            }else{
                $('#security_error').text('Fail Code');
                console.log("error");
            }
        }
    });
};