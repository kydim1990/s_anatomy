//script to manage images as an admin


//transform the main admin pannel into an image admin pannel
isl.a.imageadminpannel = function(){
    isl.a.imageadminpannelSelection();
    isl.a.imageadminpannelModify();
    isl.a.imageadminpannelAdd();
    
}



//append html on the image admin pannel to choose the image to modify
isl.a.imageadminpannelSelection = function(){
    var html = "";
    
    //get the list of images available
    var images = isl.p.getSortedImage();
    
    //print some data to choose the image to modify
    html += "<div id=imageadminchoose>select an image : <select id=imageadminchooseselector>";
    for(var i in images){
        html += "<option value="+images[i].id+( images[i].id == isl.currentImage ? " selected" : " " )+">"+i+"</option>";
    }
    html += "</select></div>";
    $("#"+isl.p.mainAdminPannel).append(html);
    
    //when selecting an image, it is displayed a s the main image
    $("#imageadminchooseselector").change( function(e){
        var imageId = $(this).val();
        console.log(imageId);
        isl.currentImage = imageId;
        isl.f.resetAll();
    });
}

//append html to the image admin pannel to modify the current image
isl.a.imageadminpannelModify = function(){
    var html = "";
    //a div to display a form to modify an already existing image
    var displayedImage = isl.f.retrieveImageToDisplay();
    html += "<div id=imageadminmodify>"
    if (displayedImage.url != "") {
        html += "<form id=imageadmindelete><input type=submit value='delete the current image'></form>";
    }
    else html += "no image selected";
    html += "</div>";
    
    $("#"+isl.p.mainAdminPannel).append(html);
    
    //delete an image event
    $("#imageadmindelete").submit(function(e){
        e.preventDefault();
        //delete the current image
        var ressource = { delete : isl.current.images[isl.currentImage].url, }
        var nearestImage = isl.a.deleteImage( isl.currentImage );
        //change the main image to be displayed :
        isl.currentImage = nearestImage;
        isl.a.saveData( isl.current, ressource );
    });
}


//append html to the image admin pannel to add a new image
isl.a.imageadminpannelAdd = function(){
    var html = "";
    
    //get the list of images available
    var images = isl.p.getSortedImage();
    
    //print a form to add a new image
    html += "<div id=imageadminadd><form id=imageadminaddform> add a new image";
    html += "<input name=file type=file id=imageadminaddfile> position of the new image <select name=position id=imageadminaddposition>";
    for(var i in images) html += "<option value="+i+">"+(parseInt(i)+1)+"</option>";
    html += "<option value="+(images.length+1)+">"+(images.length+1)+"</option>";
    html += "<input type=submit value=add />";
    html += "</select></form></div>";
    
    $("#"+isl.p.mainAdminPannel).append(html);
    
    //on submit, save the new image
    $("#imageadminadd").submit(function(e){
        e.preventDefault();
        var input = document.getElementById("imageadminaddfile");
        console.log("ici");
        //if no file has been provided,don't do anything
        //same if the file isn't an image
        try{
            if(!input.files[0].type.match(/image.*/)) return;
        }
        catch(e){
            return;
        }
        console.log(222);
        //build a new image with a new name
        var id = isl.f.generateId();
        var name = ""+ id + isl.a.inputFileName(input);
        var position = $("#imageadminaddposition").val();
        
        var ressource = {
            input : input,
            imagename : name,
        }
        
        isl.a.modifyImage(id, {
                id : id,
                url : "/bundles/swarminfoimagescript/ressources/"+isl.current.id+"/"+name,
                position : position 
            });
        //change the main image to be displayed :
        isl.currentImage = id;
        isl.a.saveData( isl.current, ressource );
        console.log(333);
    });
}







//modify or create an image
isl.a.modifyImage = function(id , data){
    console.log(isl);
    if (!(id in isl.current.images)) isl.current.images[id] = {};
    for(var attr in data)
    {
        if (attr == "position" ) {
            isl.current.images[id][attr] = data[attr] -0.5;
            isl.a.resetImagesPosition();
            //console.log(isl.current);
            //alert("reset !!! ");
        }
        else isl.current.images[id][attr] = data[attr];
    }
}

//remove an image
isl.a.deleteImage = function(id){
    //get the nearest image
    var images = isl.p.getSortedImage();
    if (isl.current.images[id].position == 0 ) {
        var nearestImage = images.length > 1 ? 0 : null;
    }
    else var nearestImage = isl.current.images[id].position -1;
    
    delete isl.current.images[id];
    isl.a.resetImagesPosition();
    return nearestImage;
}

//reset the positions of the images
isl.a.resetImagesPosition = function(){
    var images = isl.p.getSortedImage();
    for(var i in images){
        images[i].position = i;
    }
}



//retrieve the name of a file inside an input element
isl.a.inputFileName = function(input){
    var fullPath = input.value;
    if (fullPath) {
	var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
	var filename = fullPath.substring(startIndex);
	if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
		filename = filename.substring(1);
	}
	return filename;
    }
    return "0";
}
