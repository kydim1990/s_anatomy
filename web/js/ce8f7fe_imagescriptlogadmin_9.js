//edit general informations about the project

//save the number of log file to use
isl.p.logFileNumber = 0;


//add a button to watch the logs
isl.a.logadminpannel = function(){
    isl.p.logFileNumber = 0;
    var html = "<div id=logcontainer style='max-height:700px;white-space: pre-line;overflow: auto;'></div><button value='view more logs' id=morelogs>view more logs</button>";
    
    $("#"+isl.p.mainAdminPannel).append(html);
    
    //when selecting an image, it is displayed a s the main image
    $("#morelogs").click( function(e){
        e.preventDefault();
        //load and display the text
        $.ajax({
            url : "/bundles/swarminfoimagescript/ressources/"+isl.current.id+"/logs/log"+(isl.p.logFileNumber == 0 ? "" : isl.p.logFileNumber),
            dataType: "text",
	    cache: false,
            success : function (data) {
                $("#logcontainer").append(data);
                isl.p.logFileNumber++;
            }
        });
    });
    //load the last log files
    $("#morelogs").trigger("click");
    
}

// add a button to transalte
isl.a.translateadminpannel = function(){
    isl.p.logFileNumber = 0;
    var html = "<button class='btn-filled ' value='Translate legends' id=translatelegends>Tool for translate legends</button>";

    $("#"+isl.p.mainAdminPannel).append(html);

    $("#translatelegends").click( function(e){
        e.preventDefault();
        //go to translate
        // console.log('test');
        url="/translate_legends?id="+isl.current.id;
        $(location).attr("href", url);
        // window.location.href = "http://130.211.243.117/usermemberlist";
        // $.ajax({
        //     // url : "/bundles/swarminfoimagescript/ressources/"+isl.current.id+"/logs/log"+(isl.p.logFileNumber == 0 ? "" : isl.p.logFileNumber),
        //     url : "http://130.211.243.117/usermemberlist",
        //     dataType: "text",
        //     cache: false,
        //     success : function (data) {
        //         console.log('Good');
        //         // $("#logcontainer").append(data);
        //         // isl.p.logFileNumber++;
        //     }
        // });
    });
 

}