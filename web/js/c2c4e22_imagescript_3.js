//the whole library is contained in the isl object (image script library)
var isl = {};

//main functions are store in the f object
isl.f = {};

//global parameters are stored in t : most of those parameters should be defined before executing anything
isl.p = {};

//this is the object containing data of the group of images displayed :
//no modification should be brought to it unless it needs to be persisted
isl.current = {}
//this variable should contain the id of the image currently displayed as the main image
isl.currentImage = null;

//a simple array listing  available colors :
isl.colors = [ "white" , "yellow", "green", "red", "blue", "Chartreuse", "black", "Cornsilk"];

//the id of the global container
isl.p.globalContainer = "imagescriptcontainer";
//the id of the div containing the mainImageContainer
isl.p.mainImageContainer = "mainimage";
//the id of the canvas inside the container
isl.p.mainImageCanvas = "maincanvas";
//the id of the top admin pannel div, if it exist (where the vategory of actions are displayed)
isl.p.topAdminPannel = "topadminpannel";
//the id of the top user pannel (where various actions are available for the user)
isl.p.topUserPannel = "topuserpannel";
//the id of the admin pannel where actions are actually triggered
isl.p.mainAdminPannel = "mainadminpannel";


//parameters to know if the main image should be printed in reverse mode
isl.p.horizontalSymetry = false;
isl.p.verticalSymetry = false;

//the mode to use to display legend : print, hide or quiz
isl.p.legendMode = "print";
//in case it is quiz mode, remember the list of legends that have already been displayed
isl.p.displayedQuizLegends = [];
isl.p.quizImageId = null; //the id of the image thequi is going on
//position of the quiz legends
isl.p.legendsHeightOnCanvas = {};
isl.p.legendsSideOnCanvas = {};

//set the current language
isl.p.currentLanguage = null;

//remember if we are in full screen or not
isl.p.screenmode = "normal"; //can be normal or full

//parameters of the main image container
isl.p.mainImageContainerWidth = 800;

//fix the width of the canvas main image
isl.p.setImageWidth = 800;

//various dimensions inside the canvas
isl.p.mainImageWidth = 0;
isl.p.mainImageHeight = 0;
isl.p.mainImageCanvasWidth = 0;
isl.p.mainImageCanvasHeight = 0;
isl.p.legendLeftWidth = 200;
isl.p.legendRightWidth = 200;


//legend parameters
isl.p.fontSize = 14;
isl.p.font = "Arial";
isl.p.verticalLinePadding = 10;
isl.p.verticalLinePaddingToWord = 2;
isl.p.wordPaddingTop = 2;
isl.p.lineWidth = 1;
isl.p.targetRadius = 2;
isl.p.legendPaddingTop = 4;
//remember if  the mouse i s over a legend
isl.p.mouseOverLegend = null;

// the height at wich to start printing the legends : those can be reseted when the image change
isl.p.legendLeftStartHeight = isl.p.fontSize;
isl.p.legendRightStartHeight = isl.p.fontSize;

//strcutre paramaters
isl.p.drawingOpacity = 0.5; //the opacity used to draw anatomical strcutres on canvas

//functions to be called on start
isl.p.onStart = [];
//functions that are called during various hook
isl.p.onResetAll = ["isl.f.resetPVars", "isl.f.displayMainImage", "isl.f.displayTopMenu"];

//an object keeping in memory the anatomical structures displayed
isl.p.displayedStructures = [];

//function to initialize the project
isl.f.start = function(){
    isl.p.callFunctionsFromArray(isl.p.onStart);
}

//reset all :
isl.f.resetAll = function(){ console.log("resetall");
    var id = isl.f.generateId(); 
    isl.p.resetid = id;
    if( ! isl.f.resetAllPossible) return;
    if (id != isl.p.resetid) return;
    isl.f.resetAllPossible = false;
    try{
        isl.p.callFunctionsFromArray(isl.p.onResetAll);
    }
    catch(e){}
    isl.f.resetAllPossible = true;
}

//a small variable to know if a resetall is ongoing
isl.f.resetAllPossible = true;

//reset the top user menu
isl.f.displayTopMenu = function(){
    $("#"+isl.p.topUserPannel).html("");
    
    //switch to catalog mode
    isl.f.resetCatalogModeButton();
    
    //zoom buttons
    isl.f.resetZoomButtons();
    
    //symetry buttons
    isl.f.symetryButtons();
    
    //download button
    isl.f.downloadButton();
    
    //print button
    isl.f.printButton();
    
    //change the legend mode
    isl.f.legendModeButton()
    
    //display the choice of structures
    isl.f.resetStructureSelection();
    
    //a form to search legends and access related image
    isl.f.resetLegendSearch();
    
    //a button to display in full screen
    isl.f.fullScreenButton();
    
}

//display a button to enter fullscreen mode
isl.f.fullScreenButton = function(){
    var html = "<a href=# id=fullScreenButton style='text-decoration:none;color:black;' title='click to use full screen mode' >full screen</a>";
    
    $("#"+isl.p.topUserPannel).append(html);
    
    $("#fullScreenButton").on("click", function(e){
        var screenMode = isl.f.screenMode();
        if (screenMode == "normal") {
            //enter full screen ...
            var container = document.getElementById(isl.p.globalContainer);
            if (container.requestFullscreen) {
                container.requestFullscreen();
            }
            else if (container.webkitRequestFullscreen) {
                container.webkitRequestFullscreen();
            }
            else if (container.mozRequestFullScreen) {
                container.mozRequestFullScreen();
            }
            else if (container.msRequestFullscreen) {
                container.msRequestFullscreen();
            }
            else{
                console.log("nooooo");
                console.log(container);
            }
        }
        else{
            //or exit full screen
            if (document.exitFullscreen) {
                document.exitFullscreen();
            } else if (document.webkitExitFullscreen) {
                document.webkitExitFullscreen();
            } else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if (document.msExitFullscreen) {
                document.msExitFullscreen();
            }
        }

    });

    
    
}

//display a button to search for legends
isl.f.resetLegendSearch = function(){
    var html = "<input type=text id=legendSearchInput style='' title='search for a legend' value='"+("legendSearchValue" in isl.p ? isl.p.legendSearchValue : "" )+"'>";
    
    $("#"+isl.p.topUserPannel).append(html);
    
    //add an event to display a div to propose legends
    $("#legendSearchInput").on("change", isl.f.searchLegendInputModificationEvent );
    $("#legendSearchInput").on("keyup", isl.f.searchLegendInputModificationEvent );
    
    //add an event to close the legend selection when clicking outside
    $("body").unbind(".closeLegendSearchOnClick").bind("click.closeLegendSearchOnClick",function(e){
        if( ! $(this).parents('#legendSearchDiv').length ) $("#legendSearchDiv").remove();
    });

}

//function to trigger when the searchLegendInput has been modified
isl.f.searchLegendInputModificationEvent = function(e){
    //save the value of the input
    isl.p.legendSearchValue = $(this).val();
    var shouldContain = isl.p.legendSearchValue.toLowerCase();
    var legendsHtml = "";
    //if the container element does not exist, create it
    if ( ! $('#legendSearchDiv').length ){
        //get the position of the search element
        var position = $("#legendSearchInput").offset();
        //get the position of the main canvas
        var canvasPosition = $("#"+isl.p.mainImageCanvas).offset();
        var height = $("#legendSearchInput").height() + 15; 
        var width = $("#legendSearchInput").width();
        $("body").append("<div id=legendSearchDiv style='z-index:999;background-color:white;position:absolute;left:"+position.left+"px;top:"+(canvasPosition.top)+"px;width:"+width+"px;max-height:"+isl.p.mainImageHeight+"px;'></div> ")
        //$("body").append("<div id=legendSearchDiv style='z-index:999;position:fixed;left:500px;top:10px;width:"+width+";height:500;max-height:"+isl.p.mainImageHeight+";'></div> ")
    }
    
    //search for the legends with the right text
    var legends = isl.current.legends;
    var interestingLegends = [];
    for(var lid in legends){
        var txt = isl.f.getLegendText(lid);
        if( txt.toLowerCase().indexOf( shouldContain ) > -1){
            interestingLegends.push(legends[lid]);
        }
    }
    //build the html to display inside the legendSearchDiv
    var legendSearchDivHtml = "";
    if (interestingLegends.length == 0) legendSearchDivHtml += "no match found";
    for (var l in interestingLegends) {
        legendSearchDivHtml += "<a href=# class=legendSearchChoice lid="+interestingLegends[l].id+">"+ isl.f.getLegendText(interestingLegends[l].id) +"</a><br>";
        
    }
    $("#legendSearchDiv").html(legendSearchDivHtml);
    
    //bind events on click on those options
    $(".legendSearchChoice").bind("click",isl.f.eventLegendSearchClick);
}

//event when clicking on a legend in the search display
isl.f.eventLegendSearchClick = function(e){
    var lid = $(this).attr("lid");
    //remove the search div
    $("#legendSearchDiv").remove();
    //add the structure of the legend to the list of structure to display
    var legend = isl.current.legends[lid];
    if ("structure" in legend && legend.structure != "") {
        isl.f.makeStructureVisible(legend.structure);
    }
    //search the first image displaying the legend
    var images = isl.p.getSortedImage();
    for (var i in images) {
        if (lid in images[i].legends) {
            isl.currentImage = images[i].id;
            break;
        }
    }
    //reset all to display the right image with the legend on it
    isl.f.resetAll();
}

//display a button to change the mode legend are displayed
isl.f.legendModeButton = function(){
    switch(isl.p.legendMode){
        case "print" :
            nextMode = "quiz";
            var title = "click to use the quiz mode";
            var name = "displayed";
            break;
        case "quiz" :
            nextMode = "hide";
            var title = "click to hide the legends";
            var name = "quiz";
            break;
        case "hide" :
            nextMode = "print";
            var title = "click to print the legends";
            var name = "hidden";
            break;
    }
    
    var html = "<a href=# id=legendModeButton style='text-decoration:none;color:black;' title='"+title+"' >"+name+"</a>";

    $("#"+isl.p.topUserPannel).append(html);
    
    $("#legendModeButton").bind("click",function(e){
        isl.p.legendMode = nextMode;
        isl.f.resetAll();
    });
    
}


//display a print button
isl.f.printButton = function(){
    var html = "<a href=# id=printButton style='text-decoration:none;color:black;'>&#8865;</a>";

    $("#"+isl.p.topUserPannel).append(html);
    
    $("#printButton").bind("click",function(e){
        var dataUrl = document.getElementById(isl.p.mainImageCanvas).toDataURL("image/jpeg" , 1);
        var ctx = isl.f.getMainContext();
        var width = ctx.canvas.clientWidth;
        var height = ctx.canvas.clientHeight;
        var windowContent = '<!DOCTYPE html>';
        windowContent += '<html>'
        windowContent += '<head><title>Print canvas</title></head>';
        windowContent += '<body>'
        windowContent += '<img src="' + dataUrl + '">';
        windowContent += '</body>';
        windowContent += '</html>';
        var printWin = window.open('','','width='+width+',height='+height+'');
        printWin.document.open();
        printWin.document.write(windowContent);
        printWin.document.close();
        printWin.focus();
        printWin.print();
        printWin.close();
    });
    
    
}


//display a button that allow to download the current image as displayed on the screen
isl.f.downloadButton = function(){
    var html = "<a href=# id=downloadButton style='text-decoration:none;color:green;'>&#8627;</a>";
    

    $("#"+isl.p.topUserPannel).append(html);
    
    $("#downloadButton").bind("click",function(e){
        var downloadButton = document.getElementById("downloadButton");
        var canvas = document.getElementById(isl.p.mainImageCanvas) ;
        downloadButton.href = canvas.toDataURL("image/jpeg" , 1);
        downloadButton.download = isl.currentImage in isl.current.images ? isl.current.images[isl.currentImage].url : "";
    });
}

//display buttons to perform symetry
isl.f.symetryButtons = function(){
    var html = "<a href=# title='horizontal symetry' style='text-decoration:none;' id=horizontalSymetryButton>&#x21C5;</a> <a title='vertical symetry' style='text-decoration:none;' href=# id=verticalSymetryButton>&#x21C6;</a>";
    
    $("#"+isl.p.topUserPannel).append(html);
    
    $("#horizontalSymetryButton").bind("click",function(e){
        e.preventDefault();
        isl.p.horizontalSymetry = !isl.p.horizontalSymetry;
        isl.f.resetAll();
    });
    $("#verticalSymetryButton").bind("click",function(e){
        e.preventDefault();
        isl.p.verticalSymetry = !isl.p.verticalSymetry;
        isl.f.resetAll();
    });
}

//display a button that switch to catalog mode on click
isl.f.resetZoomButtons = function(){
    var html = "<a href=# id=zoomButton style='text-decoration:none;'>&#8853;</a> <a href=# id=unzoomButton style='text-decoration:none;'>&#8854;</a>";
    
    $("#"+isl.p.topUserPannel).append(html);
    
    $("#zoomButton").bind("click",function(e){ e.preventDefault(); isl.f.zoom(100); });
    $("#unzoomButton").bind("click",function(e){ e.preventDefault(); isl.f.zoom(-100); });
}

//increase or decrease by shift the size of the main image, then reset all
isl.f.zoom = function(shift){
    isl.p.setImageWidth += shift;
    if (isl.p.setImageWidth> 2000) isl.p.setImageWidth = 2000;
    if (isl.p.setImageWidth< 400) isl.p.setImageWidth = 400;
    isl.f.resetAll();
}

//display a button that switch to catalog mode on click
isl.f.resetCatalogModeButton = function(){
    var html = "<a href=# id=switchToCatalogMode>catalog</a>";
    
    $("#"+isl.p.topUserPannel).append(html);
    
    
}


//display html and add events for the strcutrure selection
isl.f.resetStructureSelection = function(){
    var html = "";
    html += "<select id=selectVisibleStructures> <option selected disabled>Choose visible structures</option>";
    html += "<option value=all>all</option><option value=none>none</option>";
    for(var s in isl.current.structures){
        var visibleYet = isl.f.isStructureVisible(s);
        html += "<option value="+s+" "+(visibleYet ? "" : "style='text-decoration:line-through;'" )+">"+isl.current.structures[s].text+"</option>";
    }
    
    $("#"+isl.p.topUserPannel).append(html);
    
    //on change, change the visible anatomic structures
    $("#selectVisibleStructures").change(function(e){
        var sid = $(this).val();
        //if all, build an array containing all the sid
        if (sid == "all") {
            var newAvailable = []
            for(var s in isl.current.structures) newAvailable.push(s);
            isl.p.displayedStructures = newAvailable;
        }
        else if (sid == "none") {
            isl.p.displayedStructures = [];
        }
        else{
            //toggle the structure
            if (isl.f.isStructureVisible(sid))  isl.f.makeStructureInvisible(sid);
            else isl.f.makeStructureVisible(sid);
        }
        //reset all
        isl.f.resetAll();
    });
}


//function to make a structure visible
isl.f.makeStructureVisible = function(sid){
    if (! isl.f.isStructureVisible(sid))  isl.p.displayedStructures.push(sid);
    
}

//function to make a structure invisible
isl.f.makeStructureInvisible = function(sid){
    var index = isl.p.displayedStructures.indexOf(sid);
    if (index > -1)  isl.p.displayedStructures.splice( index , 1);
}

//check if a structure is visible
isl.f.isStructureVisible = function(sid){
    return isl.p.displayedStructures.indexOf(sid) > -1;
}

//function to display an image, given an image data
isl.f.displayMainImage = function(image){
    image = isl.f.retrieveImageToDisplay();
    //if there is no url in the image object, there is nothing to display
    if (!("url" in image)) {
        $("#"+isl.p.mainImageContainer).html("no image selected");
    }
    var url = image.url;
    isl.currentImage = image.id;
    //load the image to display
    var img = new Image();
    img.onload = function() {
        isl.f.displayMainImage2(url, this);
    }
    img.src = url;
}

//retrieve the image to display
isl.f.retrieveImageToDisplay = function(image){
    //if no image is provided, find the first image
    if (image != null && (image != undefined) ) return image;
    if (isl.currentImage != null && (isl.currentImage in isl.current.images) ) {
        return  isl.current.images[isl.currentImage];
    }
    else{
        var images = isl.p.getSortedImage();
        if (0 in images) return images[0];
    }
    //by default, return an empty object
    return {};
}



//function to display an image, once the image has been loaded
isl.f.displayMainImage2 = function(url,img){
    var mainImageContainer = $("#"+isl.p.mainImageContainer);
    //create a canvas inside the main container
    mainImageContainer.html("<canvas id="+isl.p.mainImageCanvas+"></canvas>");
    //set the size of the various elements of the canvas
    isl.p.mainImageWidth = isl.p.setImageWidth;
    isl.p.mainImageHeight = img.height * (isl.p.setImageWidth / img.width );
    isl.p.mainImageCanvasWidth = isl.p.mainImageWidth + isl.p.legendLeftWidth + isl.p.legendRightWidth;
    isl.p.mainImageCanvasHeight = isl.p.mainImageHeight;
    
    //print the image
    isl.f.printImageOnCanvas(img);
    
    //display scroll event
    isl.f.addEventScrollCanvas();
    
}

//change to the previous or the next image on scroll on the main image canvas
isl.f.addEventScrollCanvas = function(){
    $("#"+isl.p.mainImageCanvas).on('mousewheel', function(event) {
       event.preventDefault();
        if (event.deltaY < 0){
            isl.currentImage = isl.f.getNextImageId();
            isl.f.resetAll();
        } else {
            isl.currentImage = isl.f.getPreviousImageId();
            isl.f.resetAll();
        }
    });
}

//get the id of the image preceding the current one
isl.f.getPreviousImageId = function(){
    var images = isl.p.getSortedImage();
    if ( isl.current.images[ isl.currentImage ].position == 0) return isl.currentImage;
    else return images[ parseInt(isl.current.images[ isl.currentImage ].position) -1 ].id;
}

//get the id of the next image 
isl.f.getNextImageId = function(){
    var images = isl.p.getSortedImage();
    if ( isl.current.images[ isl.currentImage ].position == images.length-1) return isl.currentImage;
    else return images[ parseInt(isl.current.images[ isl.currentImage ].position) + 1 ].id;
}

//print the given image on the canvas, leaving enough space for the legend
isl.f.printImageOnCanvas = function(img){
    //print the image on the canvas
    var ctx=isl.f.getMainContext();
    //resize the canvas
    ctx.canvas.width = isl.p.mainImageCanvasWidth;
    ctx.canvas.height = isl.p.mainImageCanvasHeight;
    //ctx.rect(0, 0, 150, 1000);
    ctx.rect(0,0,isl.p.mainImageCanvasWidth,isl.p.mainImageCanvasHeight);
    ctx.fillStyle="black";
    ctx.fill(); 
    
    //perform tranformation of symetry
    ctx.save();
    ctx.scale( isl.p.verticalSymetry ? -1 : 1, isl.p.horizontalSymetry ? -1 : 1);
    ctx.drawImage(img,
                  isl.p.verticalSymetry ? -isl.p.legendLeftWidth-isl.p.mainImageWidth : isl.p.legendLeftWidth ,
                  isl.p.horizontalSymetry ? -isl.p.mainImageHeight : 0 ,
                  isl.p.mainImageWidth ,
                  isl.p.mainImageHeight
                  );
    ctx.restore();
    
    isl.f.printAllLegendsOnCanvas();
    
    isl.f.printAllStructuresOnCanvas();
    
    //add an event to display the legend on mouse over on quiz mode
    isl.f.addCanvasEventQuizMode();
    
    //add an event to make legend clickable
    isl.f.addCanvasEventLegendClick();
    
}

isl.f.addCanvasEventLegendClick = function(){
    $canvas = $("#"+isl.p.mainImageCanvas);
    //on mouse over, check which legend is targeted
    $canvas.bind("mousemove", function(e){ isl.f.canvasEventLegendMouseoverOn(e,this);} );
    //remove it when leaving
    $canvas.bind("mouseleave", function(e){ isl.f.canvasEventLegendMouseoverOff(e,this)} );
    //on click, open a window to show the description
    $canvas.bind("click",function(e){  isl.f.canvasEventLegendClickShowDescription(e,this);} );
}

isl.f.canvasEventLegendClickShowDescription =function(e, jqelement){
    //get the legend under the mouse
    var lid = isl.f.getLegendIdUnderMouse(e, jqelement);
    if (lid == null) return;
    //add a window to display the description
    var $canvas = $("#"+isl.p.mainImageCanvas);
    var ctx = isl.f.getMainContext();
    var position = $canvas.offset();
    var description = "description" in isl.current.legends[lid] ? isl.current.legends[lid].description : "";
    var title = isl.current.legends[lid].text;
    var html = "";
    html += "<div id=descriptionPopup style='display:none;position:absolute;z-index:100000;top:"+position.top+"px;left:"+position.left+"px;width:"+ctx.canvas.clientWidth+"px;height:"+ctx.canvas.clientWidth+"px;background-color:rgba(255, 255, 255, 0.85);'>"+
                "<span id='close' onclick='this.parentNode.parentNode.removeChild(this.parentNode); return false;' style='float:right;padding:10px;cursor:pointer;' title=close >X</span>"+
                "<h2>"+title+"</h2>"+
                "<h3>description : </h3>"+description+
                "</div>";
    $("body").append(html);
    console.log(html);
    $("#descriptionPopup").show("slow");
}

//add an event to display the legend on mouse over on quiz mode
isl.f.addCanvasEventQuizMode = function(){
    //add an event just in case of quiz mode
    if (isl.p.legendMode != "quiz") return;
    $canvas = $("#"+isl.p.mainImageCanvas);
    $canvas.bind("mousemove", function(e){ isl.f.canvasEventQuizModeMouseover(e,this)} );
}

//event to check if new legends must be displayed on quiz mode on mouse
isl.f.canvasEventQuizModeMouseover = function(e,jqelement){
    var lid = isl.f.getLegendIdUnderMouse(e, jqelement);
    //dispay it unless it is already done
    if( isl.p.displayedQuizLegends.indexOf( lid ) == -1) {
        isl.p.displayedQuizLegends.push(lid);
        isl.f.resetAll();
    }
}

//check which legend is under the mouse
isl.f.getLegendIdUnderMouse = function(e, jqelement){
    var canvasX = e.pageX - $(jqelement).offset().left;
    var canvasY = e.pageY - $(jqelement).offset().top;
    if (canvasX < isl.p.legendLeftWidth) var side = "left";
    else if (canvasX > isl.p.legendLeftWidth + isl.p.mainImageWidth ) side = "right";
    else return null; //don't do anything if the cursor is not on top of a legend
    var legends = isl.p.getSortedLegends();
    //if the cursor is on top of a legend
    for(var l in isl.p.legendsHeightOnCanvas){
        if( isl.p.legendsSideOnCanvas[ l ] != side ) continue;
        //var gap = Math.abs( isl.p.legendsHeightOnCanvas[l] - (isl.p.mainImageHeight - canvasY) );
        var gap = Math.abs( isl.p.legendsHeightOnCanvas[l] -  canvasY );
        //console.log( gap );
        if ( gap < (isl.p.fontSize /2) ) {
            return l;
        }
    }
    return null;
}

//print the legend targeted by the mouse in bold
isl.f.canvasEventLegendMouseoverOn = function(e, jqelement){
    var previousLegend = isl.p.mouseOverLegend;
    isl.p.mouseOverLegend = isl.f.getLegendIdUnderMouse(e, jqelement); //this way the legend will be printed in bold
    if (isl.p.mouseOverLegend != previousLegend) {
        //set the mouse cursor
        $("#"+isl.p.mainImageContainer).css( 'cursor', isl.p.mouseOverLegend == null ? "default" : 'pointer' );
        //reset all
        isl.f.resetAll();
    }
}
//remove this event
isl.f.canvasEventLegendMouseoverOff = function(e){
    isl.p.mouseOverLegend = null;
}

isl.f.printAllStructuresOnCanvas = function(){
    var ctx = isl.f.getMainContext();
    for(var s in isl.current.structures){
        var structure = isl.current.structures[s];
        if (isl.currentImage in structure.draw ) {
            var points = [];
            console.log(structure.draw[isl.currentImage] ); 
            for(var p in structure.draw[isl.currentImage] ){
                points.push([ structure.draw[isl.currentImage][p][0]*isl.p.mainImageWidth + isl.p.legendLeftWidth ,  structure.draw[isl.currentImage][p][1]*isl.p.mainImageHeight ]);
            }
            isl.f.printAndFillCurve(ctx, points, structure.color , isl.p.drawingOpacity );
        }
    }
}

//function to print and fill a curve on a canvas
isl.f.printAndFillCurve = function(ctx, points , color, opacity){ //console.log(points);
    if (points.length < 3) return;
    ctx.save();
    ctx.beginPath();
    ctx.moveTo(points[0][0], points[0][1]);
    points.push(points[0]);
    console.log("laaaaa");
    for (i = 1; i < points.length - 2; i ++)
    {
       var xc = (points[i][0] + points[i + 1][0]) / 2;
       var yc = (points[i][1] + points[i + 1][1]) / 2;
       ctx.quadraticCurveTo(points[i][0], points[i][1], xc, yc);
    }
    // curve through the last two points
    ctx.quadraticCurveTo(points[i][0], points[i][1], points[i+1][0],points[i+1][1]);
    ctx.lineWidth = 1;
    ctx.globalAlpha=opacity;
    ctx.fillStyle = color;
    ctx.fill();
    ctx.strokeStyle = color;
    ctx.closePath();
    ctx.stroke();
    ctx.restore();
}

//print all legends of the current image
isl.f.printAllLegendsOnCanvas = function(){
    //don't print anything if it is in hide mode
    if (isl.p.legendMode == "hide") return;
    //if the quiz mode is enabled but the imageId of the current quiz don't match with the current image, reset the quiz
    if (isl.p.quizImageId != isl.currentImage) {
        isl.p.quizImageId = isl.currentImage;
        isl.p.displayedQuizLegends = [];
        isl.p.legendsHeightOnCanvas = {};
        isl.p.legendsSideOnCanvas = {};
    }
    var legends = isl.p.getSortedLegends();
    //set an object to remember where to start printing the legends
    var startPrintingHeight = {left:isl.p.legendLeftStartHeight , right:isl.p.legendRightStartHeight};
    for(var l in legends){
        var lid = legends[l].id;
        var structure = isl.current.legends[ lid ].structure;
        //if the structure is not visible, skip it
        if (structure != "" && !isl.f.isStructureVisible(structure) ) continue;
        //the text is the one specified in the
        var text = isl.p.legendMode != "quiz" || isl.p.displayedQuizLegends.indexOf( lid ) > -1 ? isl.current.legends[ lid ].text : "";
        var color = structure != "" ? isl.current.structures[ structure ].color : "white";
        //the target width and height are saved as percentage of the image size
        var targetWidth = isl.f.getWidthCoordinate( legends[l].percentWidth );
        var targetHeight = isl.f.getHeightCoordinate( legends[l].percentHeight );
        //print the legend, only if the process hasn't been started already
        isl.f.printLegendOnCanvas( text, color, targetWidth, targetHeight , legends[l].id , startPrintingHeight);
    }
}


//function to get the coordinates on the main images given the width and height percent
isl.f.getWidthCoordinate = function(percentWidth){
    return parseFloat( isl.p.verticalSymetry ? 1-percentWidth : percentWidth ) * isl.p.mainImageWidth
}

isl.f.getHeightCoordinate = function(percentHeight){
    return parseFloat( isl.p.horizontalSymetry ? 1-percentHeight : percentHeight ) * isl.p.mainImageHeight;
}

//print a legend on the canvas
isl.f.printLegendOnCanvas = function(text , color, targetWidth, targetHeight , lid , startPrintingHeight){
    
    //chek whether the legend shoudl be printed on the left side or the right side
    var side = targetWidth * 2 > isl.p.mainImageWidth ? "right" : "left";
    
    var ctx=isl.f.getMainContext();
    //increase the font size in case of mouseover
    var fontSize = isl.p.mouseOverLegend == lid && isl.p.mouseOverLegend != null ? isl.p.fontSize +3 : isl.p.fontSize;
    ctx.font = ""+fontSize+"px "+isl.p.font;
    //if the lid is the one beeing hovered by the mouse, print it in bold
    ctx.fillStyle = color;
    //set the maximum width a text can take
    if(side == "left") var maxWidth = isl.p.legendLeftWidth - isl.p.verticalLinePadding - isl.p.verticalLinePaddingToWord - isl.p.verticalLinePaddingToWord;
    else var maxWidth = isl.p.legendRightWidth - isl.p.verticalLinePadding - isl.p.verticalLinePaddingToWord - isl.p.verticalLinePaddingToWord;
    //broke the text into group of words smaller than the max width
    var words = isl.f.breakText(text, maxWidth, fontSize);
    var heightIncrement = 0;
    for(var w in words) {
        var size =  ctx.measureText(words[w]).width;
        var textWidthPosition = side == "left" ? maxWidth - size : isl.p.legendLeftWidth + isl.p.mainImageWidth + isl.p.verticalLinePadding + isl.p.verticalLinePaddingToWord;
        var textHeightPosition = side == "left" ? startPrintingHeight.left + heightIncrement : startPrintingHeight.right + heightIncrement;
        ctx.fillText(  words[w], textWidthPosition , textHeightPosition);
        //increment the next height to print legend
        heightIncrement += isl.p.wordPaddingTop + isl.p.fontSize;
    }
    
    //compute useful coordinates to draw the lines
    var verticalLineTopHeight = (side == "left" ? startPrintingHeight.left :  startPrintingHeight.right ) - isl.p.fontSize + 2 ;
    var verticalLineBottomHeight = (side == "left" ? startPrintingHeight.left : startPrintingHeight.right ) -isl.p.fontSize + heightIncrement - 2;
    var verticalLineWidth = side == "left" ? isl.p.legendLeftWidth - isl.p.verticalLinePadding : isl.p.legendLeftWidth + isl.p.mainImageWidth + isl.p.verticalLinePadding ;
    var horizontalLineHeight = Math.floor(verticalLineTopHeight + verticalLineBottomHeight)/2;
    var horizontalLineLeft = verticalLineWidth;
    var horizontalLineRight = side == "left" ? verticalLineWidth + isl.p.verticalLinePadding : verticalLineWidth - isl.p.verticalLinePadding;
    var targetWidthOnCanvas = targetWidth + isl.p.legendLeftWidth;
    var targetHeightOnCanvas = targetHeight ;
    
    //remember the position where to print the legend
    isl.p.legendsHeightOnCanvas[lid] = ( verticalLineTopHeight + verticalLineBottomHeight)/2;
    isl.p.legendsSideOnCanvas[lid] = side;
    
        
    ctx.strokeStyle = color;
    ctx.lineWidth = isl.p.lineWidth;
    ctx.beginPath();
    //draw the vertical line
    ctx.moveTo( verticalLineWidth, verticalLineTopHeight);
    ctx.lineTo(verticalLineWidth , verticalLineBottomHeight );
    //draw the horizontal line
    ctx.moveTo( horizontalLineLeft , horizontalLineHeight);
    ctx.lineTo( horizontalLineRight , horizontalLineHeight);
    //draw the line to target
    ctx.lineTo( targetWidthOnCanvas  , targetHeightOnCanvas);
    //end the line
    ctx.stroke();
    
    //draw the target circle
    ctx.beginPath();
    ctx.arc(targetWidthOnCanvas, targetHeightOnCanvas, isl.p.targetRadius, 0, 2 * Math.PI, false);
    ctx.fillStyle = color;
    ctx.fill();
    
    
    //for the next legends, increment the start height
    if (side=="left") startPrintingHeight.left +=  heightIncrement + isl.p.legendPaddingTop ;
    else startPrintingHeight.right +=  heightIncrement + isl.p.legendPaddingTop ;
    
}


//break a text in several lines so that each line doesn't exceed a given width
isl.f.breakText = function(text, maxWidth, fontSize){
    var ctx=isl.f.getMainContext();
    ctx.font = fontSize+"px "+isl.p.font;
    var result = [];
    var words = text.split(" ");
    var str = "";
    for(var w in words){
        var word = words[w];
        if (str == "") str = word;
        else if ( ctx.measureText(str+" "+word).width <= maxWidth) str += " "+word;
        else{
            result.push(str);
            str = word;
        }
    }
    //in the end, append the last piece of str
    result.push(str);
    return result;
}

//retrieve the context of the main canvas image
isl.f.getMainContext = function(){
    var c=document.getElementById(isl.p.mainImageCanvas);
    var ctx=c.getContext("2d");
    return ctx;
}

//function to create a new data object
isl.f.createNewObject = function(){
    var data = {};
    
    //create a new random id
    data.id = isl.f.generateId() ;
    
    //a data should have a list of images
    data.images = {};
    
    //a data should have a list of legends
    data.legends = {};
    
    //a data should have a list of anatomical structures
    data.structures = {};
    
    //a data has general informations
    data.general = {};
    
    //a data object has one or several languages
    data.languages = {
        "english" : "english"
    };
    
    return data;
}

//function to save load a piece of data as the current object
isl.f.openProject = function(id){
    if (id != null && id != undefined) {
        var jqxhr = $.ajax({
            url: "loaddata",
            method: "POST",
            data: {id:id},
            dataType: "json",
            type: "POST",
        })
        .done(function(res) {
            //reset all
            isl.current = res != null && "id" in res ? res : isl.f.createNewObject();
            isl.current.id = id;
            //by default, all structure are visible
            for(sid in isl.current.structures) isl.p.displayedStructures.push(sid);
            isl.f.start();
            isl.f.resetAll();
        })
        .fail(function() {
            alert( "error during loading" );
        })
    }
    else{
        //trigger directly the display all event
        console.log("new project");
        isl.current = isl.f.createNewObject();
        isl.f.resetAll();
    }
}

//get an array of images sorted by appearance order
isl.p.getSortedImage = function(){
    var sortable = [];
    if (!("images" in isl.current)) return sortable;
    for (var i in isl.current.images)
        sortable.push(isl.current.images[i])
    //sortable.sort(function(a, b) {return a.position - b.position})
    sortable.sort(function(a, b) {return (a.percentHeight/a.percentWidth) - (b.percentHeight/b.percentWidth)})
    return sortable;
}

//get an array of legends sorted by height order
isl.p.getSortedLegends = function(){
    var sortable = [];
    if (!("legends" in isl.current)) return sortable;
    for (var i in isl.current.images[ isl.currentImage].legends )
        sortable.push(isl.current.images[ isl.currentImage].legends[i])
    sortable.sort(function(a, b) {return isl.f.getHeightCoordinate( a.percentHeight )  - isl.f.getHeightCoordinate(b.percentHeight) })
    return sortable;
}

//function to play all functions on a given array
isl.p.callFunctionsFromArray = function(arr){
    for(var i in arr){
        var fname = arr[i];
        var parts = fname.split(".");
        var f = window;
        for(var j in parts) {
            f = f[ parts[j] ];
        }
        f();
    }
}

//return the screen mode of the web browser : full or normal
isl.f.screenMode = function(){
    if (!window.screenTop && !window.screenY) {
        return "full";
    }
    else return "normal";
}

//reset some p vars
isl.f.resetPVars = function(){
    isl.p.legendLeftStartHeight = isl.p.fontSize;
    isl.p.legendRightStartHeight = isl.p.fontSize;
}

//simple function to generate a random integer as unique ID
isl.f.generateId = function(){
    return Math.floor( 1000000000000000 * Math.random() );
}

//get the text of the given legend
isl.f.getLegendText = function(lid, language){
    if (language == null) {
        language = isl.f.currentLanguage;
    }
    if (!(lid in isl.current.legends)) return "";
    return language in isl.current.legends[lid].text ? isl.current.legends[lid].text[language] : "";
}