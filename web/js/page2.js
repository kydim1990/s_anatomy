/**
 * Created by Diman on 21.11.2016.
 */

var all_legends=[];

loadall_legends = function () {
    $.ajax({
        type: 'POST',
        // url: '/test',
        url: '/loadlegends',
        cache: false,
        success: function(result) {
            if(result){
                resultObj = JSON.parse(result);
                // console.log(resultObj);
                for (var i = 0; i < resultObj.length; i++) {
                    // console.log(resultObj[i]);
                    all_legends.push(resultObj[i]);
                }
                // console.log(all_legends.length);
            }else{
                console.error("loadall_legends error");
            }
        }
    });
}


loadall_legends();

//
search_change = function (e) {
    // console.log('search_change');

    var current_val  = $(this).val();
    // console.log('current_val= '+current_val);

    var position = $("#search_legend").offset();

    // var top_ = (position.top).toString() + 'px'

        // $('#legendSearchDiv').css({ top: position.top+'px' });
    $('#legendSearchDiv').css({ top: position.top.toString()+'px' });

    $('#legendSearchDiv').addClass('search-active');
    $('#legendSearchDiv').removeClass('search-hide');

    if (current_val.length){
        $('#clear-input').css({ display: 'inherit'});
    }
    else
    {
        $('#clear-input').css({ display: 'none'});
    }

    //search for the legends with the right text
    var interestingLegends = [];

    all_legends.forEach(function(entry){

        if (entry.toString().toUpperCase().indexOf(current_val.toUpperCase()) > -1)
        {

            interestingLegends.push(entry);
        }
    });

    var legendSearchDivHtml = "";
    
    for (var l in interestingLegends) {
        // console.log(interestingLegends[l]);
        legendSearchDivHtml += "<a href=/anatomy?search="+interestingLegends[l].toString().replace(/\s/g,"%20")+" class=legendSearchChoice lid="+interestingLegends[l]+">"+ interestingLegends[l] +"</a><br>";
        // legendSearchDivHtml += "<a href=# class=legendSearchChoice lid="+interestingLegends[l]+">"+ interestingLegends[l] +"</a><br>";

    }
    $("#legendSearchDiv").html(legendSearchDivHtml);

}



$( document ).ready(function() {
    // console.log( "ready!" );
    $('a').on('mousedown', function(e) {
        console.log('mousedown');
    });

    $( ".legendSearchChoice" ).mousedown(function() {
        console.log('mousedown');
    });

    $('#search_legend').focusout(function(){
//            console.log($(document.activeElement).text());
        window.setTimeout(function() { $('#legendSearchDiv').removeClass('search-active')}, 200);
        window.setTimeout(function() { $('#legendSearchDiv').addClass('search-hide')}, 200);
        // $('#legendSearchDiv').removeClass('search-active');
        // $('#legendSearchDiv').addClass('search-hide');
    });

    $( "#clear-input" ).click(function() {
        $('#search_legend').val('');
    });

    $('#search_legend').on('input',search_change);

    // console.log($('#search_legend').val());

    /*if (document.all && (!document.documentMode || (document.documentMode && document.documentMode < 8))) {
        //alert('1111');
        
        document.getElementById("skeletonCanvas").onclick = function(e){
            alert('test');
        };
    }*/

    console.log('Testset');
});
