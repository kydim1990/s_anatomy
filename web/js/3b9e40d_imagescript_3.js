//the whole library is contained in the isl object (image script library)
var isl = {};

//main functions are store in the f object
isl.f = {};

//global parameters are stored in t : most of those parameters should be defined before executing anything
isl.p = {};

//this is the object containing data of the group of images displayed :
//no modification should be brought to it unless it needs to be persisted
isl.current = {}
//this variable should contain the id of the image currently displayed as the main image
isl.currentImage = null;
//the current series beeing watch : if null, set to all
isl.p.currentSeries = null;

//a simple array listing  available colors :
isl.colors = [ "white" , "DarkOrange", "green", "red", "RoyalBlue", "SandyBrown", "Gray", "Gold", "PeachPuff", "Magenta", "Aqua", "Tan", "LawnGreen", "Plum", "CornflowerBlue", "Yellow", "YellowGreen"];

isl.defaulthidelegends =["bone", "joint", "joint/ligament", "lymph node", "bursa", "others"];

//the id of the global container
isl.p.globalContainer = "imagescriptcontainer";
//the id of the div containing the mainImageContainer
isl.p.mainImageContainer = "mainimage";
//the id of the canvas inside the container
isl.p.mainImageCanvas = "maincanvas";
//the id of the top admin pannel div, if it exist (where the vategory of actions are displayed)
isl.p.topAdminPannel = "topadminpannel";
//the id of the top user pannel (where various actions are available for the user)
isl.p.topUserPannel = "topuserpannel";
//the id of the admin pannel where actions are actually triggered
isl.p.mainAdminPannel = "mainadminpannel";

//flipswitch
isl.p.flipswitch_test = 'ON';
isl.p.flipswitch_marleft = 10;
isl.p.flipswitch_checked = 'checked';

//count_view_legends
isl.p.count_view_legends = 0;

//view_legends_dict
isl.p.view_legends_dict = null;

// search default
isl.p.default_serch = null;

//maxcount_side_legend
maxdiff_side_legend = 80;

//is_right_mouseactive_down
isl.p.is_right_mouse_down = false;
isl.p.is_right_mouse_down_pos = {};
isl.p.is_right_mouse_up_pos = {};

//is_left_mouseactive_down
isl.p.is_left_mouse_down = false;
isl.p.is_left_mouse_down_old = null;
isl.p.is_left_mouse_down_old_y = null;
isl.p.is_left_mouse_down_sevsetive= 0.01;
isl.p.is_left_mouse_down_sevsetive_y= 0.01;


//zoom parameters
isl.p.zoomStartPercentWidth = 0;
isl.p.zoomEndPercentWidth = 1;
isl.p.zoomStartPercentHeight = 0;
isl.p.zoomEndPercentHeight = 1;

//zoom default
isl.p.zoom_default = 0;
isl.p.zoom_isfirst = true;


//parameters to know if the main image should be printed in reverse mode
isl.p.horizontalSymetry = false;
isl.p.verticalSymetry = false;

//the mode to use to display legend : print, hide, square, or quiz
isl.p.legendMode = "print";
isl.p.legendSquareSize = 100;
isl.p.legendSquareLargeSize = 200;
isl.p.legendSquareSmallSize = 100;
isl.p.legendSquareCenterPercentWidth = 0.5;
isl.p.legendSquareCenterPercentHeight = 0.5;
isl.p.squareColor = "blue";
isl.p.squareLineWidth = 2;
isl.p.squareUpdateCounter = 0; //update the square image only from time to time to prevent lag
isl.p.squareUpdateCounterReset = 10;
isl.p.squareMultyIncrement= 5; //coeff of spacing between labels in square

//in case it is quiz mode, remember the list of legends that have already been displayed
isl.p.displayedQuizLegends = [];
isl.p.quizImageId = null; //the id of the image thequi is going on
//position of the quiz legends
isl.p.legendsHeightOnCanvas = {};
isl.p.legendsSideOnCanvas = {};

//set the current language
isl.p.currentLanguage = null;

//languagepage
isl.p.languagepage = null;

// current locale
isl.p.Currentlocale = null;

//remember if we are in full screen or not
isl.p.screenmode = "normal"; //can be normal or full

//parameters of the main image container
isl.p.mainImageContainerWidth = 1170;

//fix the width of the canvas main image
isl.p.setImageWidth = 700;

//various dimensions inside the canvas
isl.p.mainImageWidth = 0;
isl.p.mainImageHeight = 0;
isl.p.mainImageCanvasWidth = 0;
isl.p.mainImageCanvasHeight = 0;
isl.p.legendLeftWidth = 260;
isl.p.legendRightWidth = 260;


//possible legend size,one value chosen among this array for isl.p.fontSize
isl.p.possibleFontSize = [15 , 13 , 11 , 9, 7];
//legend parameters
isl.p.fontSize = 10;
isl.p.squarefontSize = 18;
isl.p.font = "Arial";
isl.p.verticalLinePadding = 10;
isl.p.verticalLinePaddingToWord = 16;
isl.p.wordPaddingTop = 6;
isl.p.lineWidth = 1;
isl.p.targetRadius = 2;
isl.p.legendPaddingTop = 6;
//remember if  the mouse i s over a legend
isl.p.mouseOverLegend = null;

isl.p.contrast_active = false;
var contrast_value = 0;
var contrast_value_prev = 0;
var contrast_value_form = 0;
isl.p.contrast_min = -100;
isl.p.contrast_max = 100;
isl.p.contrast_alpha = 0.1;
isl.p.contrast_default = (isl.p.contrast_max + isl.p.contrast_min ) / 2;

//zomm step
isl.p.zoom_mouse = 1;

isl.p.brightness_active = false;
var brightness_value = 0;
var brightness_value_prev = 0;
var brightness_value_form = 0;
isl.p.brightness_min = -50;
isl.p.brightness_max = 100;
isl.p.brightness_alpha = 0.1;
isl.p.brightness_default = (isl.p.brightness_max + isl.p.brightness_min ) / 2;

isl.p.contrast_preview = true;

//modal window contol c-t br-ss
isl.p.contrast_modal = 'none';

// the height at wich to start printing the legends : those can be reseted when the image change
isl.p.legendLeftStartHeight = isl.p.fontSize;
isl.p.legendRightStartHeight = isl.p.fontSize;

//strcutre paramaters
isl.p.drawingOpacity = 0.5; //the opacity used to draw anatomical strcutres on canvas

//functions to be called on start
isl.p.onStart = [];
//functions that are called during various hook
isl.p.onResetAll = ["isl.f.selectDefautSeries", "isl.f.resetPVars", "isl.f.displayMainImage", "isl.f.displayTopMenu"];

//an object keeping in memory the anatomical structures displayed
isl.p.displayedStructures = [];

isl.p.TranslateDictEN={
    // choose the language
    "english":"English","JAPANESE":"日本語","Chinese_sc":"中文(简体)","Chinese_tc":"中文(繁體)","Français":"Français","Español":"Español","Deutsch":"Deutsch","Português":"Português","Русский":"Русский","Arabic":"اللُغَة العَرَبِيَّة",
    //description
    "description":"Suggest further reading at the following external link/links that are unrelated to OMCSA.",
    //description_default popup
    "description_default":"The inclusion of the above link/links that can be freely obtained on Google search is provided merely as a convenience for reference and does not imply endorsement or association of the website/websites by us and these website/websites are not under the control or responsibility of OMCSA. Members and visitors are suggested to seek further references through the internet, journals and books."
};

// dict for translate menu bar RU
isl.p.TranslateDictRu={
    // display legend
    'displayed':'викторина', 'quiz':'скрыть','hidden':'отображать', "click to use the quiz mode":'нажмите чтобы использовать викторину',
    "click to hide the legends":"нажмите, чтобы скрыть надписи", "click to print the legends":"нажмите, чтобы показать надписи",
    // Structures
    "Structures":"Выбрать надписи", "all":"все", "none":"ничего","arteries":"артерии","Bone":"кости","brain":"мозги",
    "Cisterns/Sulci/Ventricles":"Цистерны /борозд / Желудочки", "dural sinuses/veins":"дуральные пазух / вены","nerves":"нервы","others":"другое",
    // choose the language
    "english":"английский","japanese":"японский",
    // display a button to search for legends
    "search for a structure":'поиск для структуры', "search anatomy":'поиск структуры',
    // full screen
    "full screen":"полный экран",
    // rainbow
    "Rainbow":"радуга","click to use rainbow":"нажмите, чтобы использовать радугу",
    //description
    "description":"описание",
    //description_default popup
    "description_default":"описание по умолчанию"
};

// dict for translate menu bar SC
isl.p.TranslateDictSC={
    // display legend
    'displayed':'CH displayed', 'quiz':'CH quiz','hidden':'CH hidden', "click to use the quiz mode":'CH click to use the quiz mode',
    "click to hide the legends":"CH click to hide the legends", "click to print the legends":"CH click to print the legends",
    // Structures
    "Structures":"CH Structures", "all":"CH all", "none":"CH none","arteries":"CH arteries","Bone":"CH Bone","brain":"CH brain",
    "Cisterns/Sulci/Ventricles":"CH Cisterns/Sulci/Ventricles", "dural sinuses/veins":"CH dural sinuses/veins","nerves":"CH nerves","others":"CH others",
    // choose the language
    "english":"CH english","JAPANESE":"CH japanese",
    // display a button to search for legends
    "search for a structure":'CH search for a structure', "search anatomy":'CH search anatomy',
    // full screen
    "full screen":"CH full screen",
    // rainbow
    "Rainbow":"CH Rainbow","click to use rainbow":"CH click to use rainbow",
    //description
    "description":"建议在以下与OMCSA无关的外部链接/链接上进一步阅读。",
    //description_default popup
    "description_default":"包括可以在Google搜索上自由获得的上述链接/链接仅仅是为了方便参考，并不意味着我们对网站/网站的认可或关联，并且这些网站/网站不受控制或责任 的OMCSA。 建议会员和访客通过互联网，期刊和书籍寻求进一步的参考。"
};

// dict for translate menu bar TC
isl.p.TranslateDictTC={
    // display legend
    'displayed':'CH displayed', 'quiz':'CH quiz','hidden':'CH hidden', "click to use the quiz mode":'CH click to use the quiz mode',
    "click to hide the legends":"CH click to hide the legends", "click to print the legends":"CH click to print the legends",
    // Structures
    "Structures":"CH Structures", "all":"CH all", "none":"CH none","arteries":"CH arteries","Bone":"CH Bone","brain":"CH brain",
    "Cisterns/Sulci/Ventricles":"CH Cisterns/Sulci/Ventricles", "dural sinuses/veins":"CH dural sinuses/veins","nerves":"CH nerves","others":"CH others",
    // choose the language
    "english":"CH english","JAPANESE":"CH japanese",
    // display a button to search for legends
    "search for a structure":'CH search for a structure', "search anatomy":'CH search anatomy',
    // full screen
    "full screen":"CH full screen",
    // rainbow
    "Rainbow":"CH Rainbow","click to use rainbow":"CH click to use rainbow",
    //description
    "description":"建議在以下與OMCSA無關的外部鏈接/鏈接上進一步閱讀。",
    //description_default popup
    "description_default":"包括可以在Google搜索上自由獲得的上述鏈接/鏈接僅僅是為了方便參考，並不意味著我們對網站/網站的認可或關聯，並且這些網站/網站不受控製或責任 的OMCSA。 建議會員和訪客通過互聯網，期刊和書籍尋求進一步的參考。"
};

// dict for translate menu bar JA
isl.p.TranslateDictJA={
    // display legend
   'displayed':'JA displayed', 'quiz':'JA quiz','hidden':'JA hidden', "click to use the quiz mode":'JA click to use the quiz mode',
    "click to hide the legends":"JA click to hide the legends", "click to print the legends":"JA click to print the legends",
    // Structures
    "Structures":"JA Structures", "all":"JA all", "none":"JA none","arteries":"JA arteries","Bone":"JA Bone","brain":"JA brain",
    "Cisterns/Sulci/Ventricles":"JA Cisterns/Sulci/Ventricles", "dural sinuses/veins":"JA dural sinuses/veins","nerves":"JA nerves","others":"JA others",
    // choose the language
    "english":"JA english","JAPANESE":"JA japanese",
    // display a button to searJA for legends
    "search for a structure":'JA search for a structure', "search anatomy":'JA search anatomy',
    // full screen
    "full screen":"JA full screen",
    // rainbow
    "Rainbow":"JA Rainbow","click to use rainbow":"JA click to use rainbow",
    //description
    "description":"さらにOMCSAとは無関係な、次の外部リンク/リンクで読んで提案します。",
    //description_default popup
    "description_default":"自由にGoogle検索で取得することができる上記のリンク/リンクを含むことは、参照のための便宜としてのみ提供されており、コントロールまたは責任の下ではありません私たちとこれらのウェブサイト/ウェブサイトによって承認またはウェブサイト/ウェブサイトの関連性を意味するものではありません。OMCSAの。メンバーと訪問者は、インターネット、雑誌や書籍を通じ、さらに参照を追求することが示唆されています。"
};

// dict for translate menu bar FR
isl.p.TranslateDictFR={
    // display legend
    'displayed':'FR displayed', 'quiz':'FR quiz','hidden':'FR hidden', "click to use the quiz mode":'FR click to use the quiz mode',
    "click to hide the legends":"FR click to hide the legends", "click to print the legends":"FR click to print the legends",
    // Structures
    "Structures":"FR Structures", "all":"FR all", "none":"FR none","arteries":"FR arteries","Bone":"FR Bone","brain":"FR brain",
    "Cisterns/Sulci/Ventricles":"FR Cisterns/Sulci/Ventricles", "dural sinuses/veins":"FR dural sinuses/veins","nerves":"FR nerves","others":"FR others",
    // choose the language
    "english":"FR english","JAPANESE":"FR japanese",
    // display a button to searFR for legends
    "search for a structure":'FR search for a structure', "search anatomy":'FR search anatomy',
    // full screen
    "full screen":"FR full screen",
    // rainbow
    "Rainbow":"FR Rainbow","click to use rainbow":"FR click to use rainbow",
    //description
    "description":"FR description",
    //description_default popup
    "description_default":"Suggestions de lectures complémentaires et des liens externes vers les OMCSA / liens indépendants suivants. Y compris l'accès gratuit à la recherche Google sur les liens suivants pour plus de commodité seulement et ne signifie pas que nous approuvons ou site affilié / sites et ces sites / sites ne sont pas sous le contrôle ou la responsabilité OMCSA. Membres et visiteurs des recommandations pour référence ultérieure via Internet, de périodiques ou de livres."
};

// dict for translate menu bar ES
isl.p.TranslateDictES={
    // display legend
    'displayed':'ES displayed', 'quiz':'ES quiz','hidden':'ES hidden', "click to use the quiz mode":'ES click to use the quiz mode',
    "click to hide the legends":"ES click to hide the legends", "click to print the legends":"ES click to print the legends",
    // Structures
    "Structures":"ES Structures", "all":"ES all", "none":"ES none","arteries":"ES arteries","Bone":"ES Bone","brain":"ES brain",
    "Cisterns/Sulci/Ventricles":"ES Cisterns/Sulci/Ventricles", "dural sinuses/veins":"ES dural sinuses/veins","nerves":"ES nerves","others":"ES others",
    // choose the language
    "english":"ES english","JAPANESE":"ES japanese",
    // display a button to searES for legends
    "search for a structure":'ES search for a structure', "search anatomy":'ES search anatomy',
    // full screen
    "full screen":"ES full screen",
    // rainbow
    "Rainbow":"ES Rainbow","click to use rainbow":"ES click to use rainbow",
    //description
    "description":"ES description",
    //description_default popup
    "description_default":"description default ES"
};

// dict for translate menu bar DE
isl.p.TranslateDictDE={
    // display legend
    'displayed':'DE displayed', 'quiz':'DE quiz','hidden':'DE hidden', "click to use the quiz mode":'DE click to use the quiz mode',
    "click to hide the legends":"DE click to hide the legends", "click to print the legends":"DE click to print the legends",
    // Structures
    "Structures":"DE Structures", "all":"DE all", "none":"DE none","arteries":"DE arteries","Bone":"DE Bone","brain":"DE brain",
    "Cisterns/Sulci/Ventricles":"DE Cisterns/Sulci/Ventricles", "dural sinuses/veins":"DE dural sinuses/veins","nerves":"DE nerves","others":"DE others",
    // choose the language
    "english":"DE english","JAPANESE":"DE japanese",
    // display a button to searDE for legends
    "search for a structure":'DE search for a structure', "search anatomy":'DE search anatomy',
    // full screen
    "full screen":"DE full screen",
    // rainbow
    "Rainbow":"DE Rainbow","click to use rainbow":"DE click to use rainbow",
    //description
    "description":"DE description",
    //description_default popup
    "description_default":"description default DE"
};

// dict for translate menu bar KE
isl.p.TranslateDictKE={
    // display legend
    'displayed':'KE displayed', 'quiz':'KE quiz','hidden':'KE hidden', "click to use the quiz mode":'KE click to use the quiz mode',
    "click to hide the legends":"KE click to hide the legends", "click to print the legends":"KE click to print the legends",
    // Structures
    "Structures":"KE Structures", "all":"KE all", "none":"KE none","arteries":"KE arteries","Bone":"KE Bone","brain":"KE brain",
    "Cisterns/Sulci/Ventricles":"KE Cisterns/Sulci/Ventricles", "dural sinuses/veins":"KE dural sinuses/veins","nerves":"KE nerves","others":"KE others",
    // choose the language
    "english":"KE english","JAPANESE":"KE japanese",
    // display a button to searKE for legends
    "search for a structure":'KE search for a structure', "search anatomy":'KE search anatomy',
    // full screen
    "full screen":"KE full screen",
    // rainbow
    "Rainbow":"KE Rainbow","click to use rainbow":"KE click to use rainbow",
    //description
    "description":"KE description",
    //description_default popup
    "description_default":"description default KE"
};

// dict for translate menu bar PT
isl.p.TranslateDictPT={
    // display legend
    'displayed':'PT displayed', 'quiz':'PT quiz','hidden':'PT hidden', "click to use the quiz mode":'PT click to use the quiz mode',
    "click to hide the legends":"PT click to hide the legends", "click to print the legends":"PT click to print the legends",
    // Structures
    "Structures":"PT Structures", "all":"PT all", "none":"PT none","arteries":"PT arteries","Bone":"PT Bone","brain":"PT brain",
    "Cisterns/Sulci/Ventricles":"PT Cisterns/Sulci/Ventricles", "dural sinuses/veins":"PT dural sinuses/veins","nerves":"PT nerves","others":"PT others",
    // choose the language
    "english":"PT english","JAPANESE":"PT japanese",
    // display a button to searPT for legends
    "search for a structure":'PT search for a structure', "search anatomy":'PT search anatomy',
    // full screen
    "full screen":"PT full screen",
    // rainbow
    "Rainbow":"PT Rainbow","click to use rainbow":"PT click to use rainbow",
    //description
    "description":"PT description",
    //description_default popup
    "description_default":"description default PT"
};

// dict for translate menu bar AR
isl.p.TranslateDictAR={
    // display legend
    'displayed':'AR displayed', 'quiz':'AR quiz','hidden':'AR hidden', "click to use the quiz mode":'AR click to use the quiz mode',
    "click to hide the legends":"AR click to hide the legends", "click to print the legends":"AR click to print the legends",
    // Structures
    "Structures":"AR Structures", "all":"AR all", "none":"AR none","arteries":"AR arteries","Bone":"AR Bone","brain":"AR brain",
    "Cisterns/Sulci/Ventricles":"AR Cisterns/Sulci/Ventricles", "dural sinuses/veins":"AR dural sinuses/veins","nerves":"AR nerves","others":"AR others",
    // choose the language
    "english":"AR english","JAPANESE":"AR japanese",
    // display a button to searAR for legends
    "search for a structure":'AR search for a structure', "search anatomy":'AR search anatomy',
    // full screen
    "full screen":"AR full screen",
    // rainbow
    "Rainbow":"AR Rainbow","click to use rainbow":"AR click to use rainbow",
    //description
    "description":"AR description",
    //description_default popup
    "description_default":"description default AR"
};

function getParameterByName(name, url) {
    if (!url) {
        var url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}


//function to initialize the project
isl.f.start = function(){
    isl.p.callFunctionsFromArray(isl.p.onStart);
}

$.urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null){
        return null;
    }
    else{
        var res = results[1].replace(/\_/g, ' ');
        // console.log(res);
        return res;
    }
}

//reset all :
isl.f.resetAll = function(){

    var id = isl.f.generateId(); 
    isl.p.resetid = id;
    if( ! isl.f.resetAllPossible) return;
    if (id != isl.p.resetid) return;
    isl.f.resetAllPossible = false;
    try{
        isl.p.callFunctionsFromArray(isl.p.onResetAll);
    }
    catch(e){}
    isl.f.resetAllPossible = true;
    //scroll to the top
    //window.scrollTo(0, 0);
}

// translate menu frame
isl.f.TranslateMenuBar = function(text) {
    // console.log(isl.p.Currentlocale);
    switch(isl.p.Currentlocale){
        case 'ru':return isl.p.TranslateDictRu[text];
        case 'sc':return isl.p.TranslateDictSC[text];
        case 'tc':return isl.p.TranslateDictTC[text];
        case 'ja':return isl.p.TranslateDictJA[text];
        case 'fr':return isl.p.TranslateDictFR[text];
        case 'es':return isl.p.TranslateDictES[text];
        case 'de':return isl.p.TranslateDictDE[text];
        case 'ke':return isl.p.TranslateDictKE[text];
        case 'pt':return isl.p.TranslateDictPT[text];
        case 'ar':return isl.p.TranslateDictAR[text];
        case 'en': return text in isl.p.TranslateDictEN ? isl.p.TranslateDictEN[text] : text;
        default: return text;
    }

    // console.log(isl.p.TranslateDictRu['displayed']);
}

//a small variable to know if a resetall is ongoing
isl.f.resetAllPossible = true;

isl.f.isFirst = true;

//reset the top user menu
isl.f.displayTopMenu = function(){
    $("#"+isl.p.topUserPannel).html("");
    
    //switch to catalog mode
 // isl.f.resetCatalogModeButton();
    
    //symetry buttons
    isl.f.symetryButtons();
    
    //download button
 //   isl.f.downloadButton();
    
    //print button
 //   isl.f.printButton();
    
    //zoom buttons
    isl.f.resetZoomButtons();

    //scuare mode button
    isl.f.squareMode()

    //scuare mode large button
    isl.f.squareModelarge()

    //display contrast slider
    // isl.f.conrastslider();

    //display brightness slider
    // isl.f.brightnessslider();

    // modal window for control contrast and brigthness
    isl.f.constr_br_modal();

    //change the legend mode
    isl.f.legendModeButton();

    //display the choice of structures
    isl.f.resetStructureSelection();

    //display count view legend
    isl.f.resetCountViewLegendSelectionButton();

    //display the choice of language
    isl.f.resetLanguageSelectionButton();
    
    //reset the selection of the series to display
    isl.f.resetSeriesSelection();
    
    //a form to search legends and access related image
    isl.f.resetLegendSearch();

    //a button to display flipswitch
    isl.f.flipswitchButton();

    //a button to display in full screen
    isl.f.fullScreenButton();

    //a button to display inrainbow
    isl.f.rainbow();

    //display copy current image
    isl.f.CountImages();

    if (isl.p.zoom_isfirst){
        isl.f.zoom(isl.p.zoom_default);
        isl.p.zoom_isfirst = false;
    }


}

isl.f.GoTosearch = function(){
    if ($.urlParam('search') != null && $.urlParam('search') !='' && isl.f.isFirst)
    {
        var legends = isl.current.legends;
        // console.log("isl.p.default_serch= "+isl.f.getLegendText(legends[isl.p.default_serch].id));

        isl.f.select_search_default(isl.p.default_serch);

        isl.f.isFirst = false;

    }
}


isl.f.resetSeriesSelection = function(){
    var html = "";
    
    //get the list of languages available
    var series = isl.current.series;
    
    //print some data to choose the language to modify
    html += "<select id=serieschooseselectoruser>";
    //html += "<option value='' "+( null == isl.p.currentSeries ? " selected" : " " )+">all series</option>";
    
    //push all elements to sort in an array
    var sortarr = [];
    for(sid in series){ sortarr.push([series[sid].text, sid ]) }
    sortarr.sort( function(a,b){return a[0].localeCompare(b[0]);} );
    
    for(var i in sortarr){
        var sid = sortarr[i][1];
        html += "<option value='"+sid+"' "+( sid == isl.p.currentSeries ? " selected" : " " )+">"+series[sid].text+"</option>";
    }
    
    //for(var sid in series){
    //    html += "<option value='"+sid+"' "+( sid == isl.p.currentSeries ? " selected" : " " )+">"+series[sid].text+"</option>";
    //}
    html += "</select>";
    $("#"+isl.p.topUserPannel).append(html);

    //when selecting an language, it is displayed a s the main language
    $("#serieschooseselectoruser").change( function(e){

        var series = $(this).val();
        isl.p.currentSeries = series == '' ? null : series;
        //reload the bottom band images
        isl.f.loadBottomBandImages();
        
        //retrieve the first available image belonging to the series
        var images = isl.p.getSortedImage();
        isl.currentImage = images.length > 0 ? images[0].id : null;
        
        isl.f.resetAll();
    });
}

var smallmodelegendpercentsize=0.5;
var largemodelegendpercentsize=1;

//display a square small
isl.f.squareMode = function(){
    var html = "<a class=userbutton href=# id=squareMode style='text-decoration:none;color:white;'>&#9633</a>";

    $("#"+isl.p.topUserPannel).append(html);

    $("#squareMode").bind("click",function(e){
        if (isl.p.legendMode != "square")
        {
            // console.log(isl.p.legendMode);
            isl.p.legendMode = "square";
            isl.p.legendSquareCenterPercentWidth = smallmodelegendpercentsize;
            isl.p.legendSquareCenterPercentHeight = smallmodelegendpercentsize;
            isl.p.legendSquareSize=isl.p.legendSquareSmallSize;
        }
        else
        {
            isl.p.legendMode = 'print';
        }
        isl.f.resetAll();
    });

}

//display a square large
isl.f.squareModelarge = function(){
    var html = "<a class=userbutton href=# id=squareModelarge style='text-decoration:none;color:white;'>&#9633</a>";

    $("#"+isl.p.topUserPannel).append(html);

    $("#squareModelarge").bind("click",function(e){
        if (isl.p.legendMode != "largesquare")
        {
            // console.log(isl.p.legendMode);
            isl.p.legendMode = "largesquare";
            isl.p.legendSquareCenterPercentWidth = 2;
            isl.p.legendSquareCenterPercentHeight = 2;
            isl.p.legendSquareSize=isl.p.legendSquareLargeSize;
        }
        else
        {
            isl.p.legendMode = 'print';
        }
        isl.f.resetAll();
    });

}

//display a conrast
isl.f.conrastslider = function(){
    var html = "<a class=userbutton href=# id=conrastbutton title=contrast style='text-decoration:none;color:white; font-size: 20px;display:";
    if (!isl.p.contrast_active) html += "inline-block";
    else html += "none";
    html += "'>&#9680</a>";

    html += "<input class=userbutton title=contrast type=range orient=vertical id=conrast_slider min ="+isl.p.contrast_min+" max="+isl.p.contrast_max+" ";
    html += "value="+contrast_value;
    html +=   " style='text-decoration:none;color:white;  writing-mode: bt-lr;" +
        "  -webkit-appearance: slider-vertical;   width: 8px; " +
        "height: 50px;  padding: 0 5px;display: ";

    if (isl.p.contrast_active) html += "inline-block";
    else html += "none";

    html +=  "'>&#9637</input>";

    $("#"+isl.p.topUserPannel).append(html);

    $("#conrastbutton").bind("click",function(e){

        isl.p.contrast_active=!isl.p.contrast_active;
        // console.log(isl.p.contrast_active);
        isl.f.resetAll();
    });

    $("#conrast_slider").bind("focusout",function(e){
        isl.p.contrast_active=!isl.p.contrast_active;
        // console.log('conrast_slider los focus');
        isl.f.resetAll();
    });

    $("#conrast_slider").bind("change",function(e){
        contrast_value=$('#conrast_slider').val();
        // console.log('brightness_slider value='+contrast_value);
        isl.f.resetAll();
    });

}


//display a brightness
isl.f.brightnessslider = function(){
    var html = "<a class=userbutton href=# id=brightnessbutton title=brightness style='text-decoration:none;color:white; font-size: 20px;display:";
    if (!isl.p.brightness_active) html += "inline-block";
    else html += "none";
    html += "'>&#9965</a>";

    html += "<input class=userbutton title=brightness type=range min ="+isl.p.brightness_min+" max="+isl.p.brightness_max+" ";
    html += "value="+brightness_value;
    html +=   " orient=vertical id=brightness_slider " +
        "style='text-decoration:none;color:white;  writing-mode: bt-lr;" +
        "  -webkit-appearance: slider-vertical;   width: 8px; " +
        "height: 50px;  padding: 0 5px;display: ";

    // console.log('html='+html);

    if (isl.p.brightness_active) html += "inline-block";
    else html += "none";

    html +=  "'>&#9637</input>";

    $("#"+isl.p.topUserPannel).append(html);

    $("#brightnessbutton").bind("click",function(e){
        isl.p.brightness_active=!isl.p.brightness_active;
        // console.log(isl.p.brightness_active);
        isl.f.resetAll();
    });

    $("#brightness_slider").bind("focusout",function(e){
        isl.p.brightness_active=!isl.p.brightness_active;
        isl.f.resetAll();
    });

    $("#brightness_slider").bind("change",function(e){
        brightness_value=$('#brightness_slider').val();
        // console.log('brightness_slider value='+brightness_value);
        isl.f.resetAll();
    });

}

// modal window for control contrast and brigthness
isl.f.constr_br_modal = function(){
    var html = "<a class=userbutton href=# id=constr_br_modal title=Brightness/Contrast style='text-decoration:none;color:white; font-size: 20px;display:";
    if (!isl.p.brightness_active) html += "inline-block";
    else html += "none";
    html += "'>&#9680</a>";

    html+='<div id="contast-window" class="modal" style="';
    html+='display: '+isl.p.contrast_modal;
    html+= '">    <div class="modal-content" ';
    html+=  '">  <span class="close-contrast" ';
    html+='>&times;</span>    <p class="header_modal">Brightness/Contrast</p>   ';

    // body
    //Brightness
    html+='<div class="cb_slider" ><div><div style="overflow: hidden;float: left;  width: 70%; "><div style="overflow: hidden; height: 70px">';
    html+='<p style="float:left; color:#C2C2C2; font-size: 14px;">Brightness:</p><p style="float:right; font-size: 14px; color:white;' +
        'text-decoration: underline">'+brightness_value_form+'</p>';
    html += '<input style="height: 2px; -webkit-appearance: none !important; background-color: #C2C2C2' +
        '" type=range id=brightness_slider_mod min='+isl.p.brightness_min+' max='+isl.p.brightness_max+' value='+brightness_value_form+'></div>';

    //Conrast
    html+='<div>';
    html+='<p style="float:left; color:#C2C2C2; font-size: 14px;">Contrast:</p><p style="float:right; font-size: 14px; color:white;' +
        ' text-decoration: underline">'+contrast_value_form+'</p>';
    html += '<input style="height: 10px; height: 2px; -webkit-appearance: none !important; background-color: #C2C2C2' +
        '" type=range id=contrast_slider_mod min='+isl.p.contrast_min+' max='+isl.p.contrast_max+' value='+contrast_value_form+'></div>';

    //Use legacy
    // html += '<div id=use_legacy  style="height: 30px;   font-size: 14px; color:white;"';
    // html +=  '<p><input type="checkbox" id=use_legacy value="0">Use legacy</p>' ;
    // html += '</div>';

    html += '</div>  ';

    //right panel
    html += '<div id=ok_contarst style="float: right;  width: 30%;  height: 100px; padding-left: 30px;">';

    //button ok
    html += '<div id=ok_contarst style="height: 30px;" ';
    html +=  '<p><input type="button" id="bt_ok" value="Ok" style="width: 100px;  border-radius: 25px;  border: none; height: 30px;' +
        ' color:white; background-color: #535353; border: solid white 1px; font-size: 14;"></p>' ;
    html += '</div>';

    //button Cancel
    html += '<div id=cancel_contarst  style="height: 30px;"';
    html +=  '<p><input type="button" id="bt_cancel" value="Cancel" style="width: 100px;  border-radius: 25px;  border: none;  height: 30px; ' +
        'color:white; background-color: #535353; border: solid white 1px; font-size: 14;' +
        'margin-top: 10px;"></p>' ;
    html += '</div>';

    //button auto
    html += '<div id=auto_contarst  style="height: 30px;"';
    html +=  '<p><input type="button" id="bt_default" value="Default" style="width: 100px;  border-radius: 25px; border: none; height: 30px; background-color: #535353;' +
        ' margin-top: 20px; border: solid white 1px; font-size: 14;' +
        'color:white"></p>' ;
    html += '</div>';

    //Preview
    html += '<div id=preview_contarst  style="height: 30px; font-size: 14px; margin-top: 30px; color:white;"';
    html +=  '<p><input type="checkbox" id="Preview" value="0" ';
    if (isl.p.contrast_preview){
        html += 'checked';
    }

    html +=  '>Preview</p>' ;
    html += '</div>';

    html +=  '</div>';
    html += '</div></div> </div></div> ';

    $("#"+isl.p.topUserPannel).append(html);

    $("#constr_br_modal").bind("click",function(e){
        isl.p.contrast_modal = 'block';
        contrast_value_prev = contrast_value;
        brightness_value_prev = brightness_value;
        isl.f.resetAll();
    });

    $(".close-contrast").bind("click",function(e){
        isl.p.contrast_modal = 'none';
        isl.f.resetAll();
    });

    $("#brightness_slider_mod").bind("change",function(e){
        if ($('#Preview').is(':checked')){
            brightness_value=$('#brightness_slider_mod').val();
            // console.log('brightness_slider value='+brightness_value);

        }
        brightness_value_form = $('#brightness_slider_mod').val();
        isl.f.resetAll();
    });

    $("#contrast_slider_mod").bind("change",function(e){
        if ($('#Preview').is(':checked')){
            contrast_value=$('#contrast_slider_mod').val();
            // console.log('brightness_slider value='+brightness_value);
        }
        contrast_value_form = $('#contrast_slider_mod').val();
        isl.f.resetAll();
    });

    $("#Preview").change(function() {
        isl.p.contrast_preview = this.checked;
    });

    $("#bt_default").click(function() {
        brightness_value = 0; //isl.p.brightness_default;
        contrast_value = 0; //isl.p.contrast_default;
        contrast_value_form = contrast_value;
        brightness_value_form = brightness_value;
        isl.f.resetAll();
    });

    $("#bt_cancel").click(function() {
        isl.p.contrast_modal = 'none';
        contrast_value= contrast_value_prev ;
        brightness_value = brightness_value_prev ;
        isl.f.resetAll();
    });

    $("#bt_ok").click(function() {
        brightness_value=$('#brightness_slider_mod').val();
        brightness_value_form=$('#brightness_slider_mod').val();
        contrast_value=$('#contrast_slider_mod').val();
        contrast_value_form=$('#contrast_slider_mod').val();
        isl.p.contrast_modal = 'none';
        isl.f.resetAll();
    });

}

isl.f.getparamsCB = function() {
    $.ajax({
        type: "post",
        url: "/getcbparams",
        dataType: 'json',
        success: function (getpar) {
            isl.p.contrast_min = getpar.contrast_min;
            isl.p.contrast_max = getpar.contrast_max;
            isl.p.contrast_alpha = getpar.contrast_mouse_sensetve;
            isl.p.contrast_default = (isl.p.contrast_max + isl.p.contrast_min ) / 2;

            isl.p.brightness_min = getpar.brightness_min;
            isl.p.brightness_max = getpar.brightness_max;
            isl.p.brightness_alpha = getpar.brightness_mouse_sensetve;
            isl.p.brightness_default = (isl.p.brightness_max + isl.p.brightness_min ) / 2;

            // console.log(getpar.brightness_mouse_sensetve);
        },
        error: function () {
            // alert(data);
            alert("Something went wrong. Please, try again.");
        }
    });
};

//gethidefirst_labels
isl.f.gethidefirst_labels = function() {
    $.ajax({
        type: "post",
        url: "/gethidefirst_labels",
        dataType: 'json',
        success: function (getpar) {
            // console.log('gethidefirst_labels='+getpar);
            isl.p.view_legends_dict = getpar;
        },
        error: function () {
            // alert(data); 
            alert("Something went wrong. Please, try again.");
        }
    });
};

//after loaded page
$( document ).ready(function() {
    isl.f.getparamsCB();
    isl.f.gethidefirst_labels();
    // console.log(document.referrer);
    $.ajax({
        type: "post",
        url: "/getlocale",
        dataType: 'text',
        // data: {},
        success: function (data) {
            isl.p.Currentlocale = data;
            // alert(data);
            // console.log(data);
        },
        error: function () {
            // alert(data);
            alert("Something went wrong. Please, try again.");
        }
    });

    // console.log(isl.p.Currentlocale);

    // isl.p.Currentlocale=findlocate;
    switch(isl.p.Currentlocale)
    {
        case 'en':
            isl.p.languagepage='english';
            break;
        case 'ja': isl.p.languagepage='Japanese';
            break;
        case 'sc': isl.p.languagepage='Chinese_sc';
            break;
        case 'tc': isl.p.languagepage='Chinese_tc';
            break;
        case 'fr': isl.p.languagepage='Français';
            break;
        case 'ec': isl.p.languagepage='Español';
            break;
        case 'de': isl.p.languagepage='Deutsch';
            break;
        case 'pt': isl.p.languagepage='Português';
            break;
        case 'ru': isl.p.languagepage='Русский';
            // isl.p.languagepage='Русский';
            break;
        case 'ar': isl.p.languagepage='Arabic';
            break;
        default: isl.p.languagepage='english';
    }

    isl.f.resetAll();
});

// return code locale by name
isl.f.GetCodelocaleByName = function(text) {
    var cur_locale='en';
    switch(text.toLowerCase())
    {
        case 'english':
            cur_locale='en';
            break;
        case 'japanese': cur_locale='ja';
            break;
        case 'chinese_sc': cur_locale='sc';
            break;
        case 'chinese_tc': cur_locale='tc';
            break;
        case 'français': cur_locale='fr';
            break;
        case 'español': cur_locale='ec';
            break;
        case 'deutsch': cur_locale='de';
            break;
        case 'português': cur_locale='pt';
            break;
        case 'Русский': cur_locale='ru';
            // isl.p.languagepage='Русский';
            break;
        case 'arabic': cur_locale='ar';
            break;
        default: cur_locale='english';

    }
    return cur_locale;
    // console.log(isl.p.Currentlocale);
    // isl.f.resetAll();
};


isl.f.resetLanguageSelectionButton = function(){
    var html = "";
    
    //get the list of languages available
    var languages = isl.current.languages;
    // console.log(isl.current.languages);
    //print some data to choose the language to modify
    html += "<select id=languagechooseselector>";
    
    //push all languages to sort in an array
    var sortarr = [];
    for(l in languages){ sortarr.push([l, languages[i] ]) }
    sortarr.sort( function(a,b){return a[0].localeCompare(b[0]);} );
    
    for(var l in sortarr){
        var i = sortarr[l][0];
        // html += "<option value='"+languages[i]+"' "+( languages[i] == isl.p.currentLanguage ? " selected" : " " )+">"+i+"</option>";
        // html += "<option value='"+languages[i]+"' "+( languages[i] == isl.p.languagepage ? " selected" : " " )+">"+i+"</option>";
        html += "<option value='"+languages[i]+"' "+( languages[i] == isl.p.languagepage ? " selected" : " " )+">"+isl.f.TranslateMenuBar(i)+"</option>";
        // console.log(languages[i]);
    }
    //for(var i in languages){
    //    html += "<option value='"+languages[i]+"' "+( languages[i] == isl.p.currentLanguage ? " selected" : " " )+">"+i+"</option>";
    //}
    html += "</select>";
    $("#"+isl.p.topUserPannel).append(html);
    // console.log(isl.p.currentLanguage);
    //when selecting an language, it is displayed a s the main language
    $("#languagechooseselector").change( function(e){
        // alert('Test');
        // console.log(document.referrer);
        var language = $(this).val();
        isl.p.languagepage = language;
        isl.p.currentLanguage =isl.p.languagepage;
        console.log('isl.p.languagepage='+isl.p.languagepage);
        console.log('isl.p.Currentlocale='+isl.p.Currentlocale);
        isl.f.resetAll();
    });
    
    
}

var raincolors=["white", "PeachPuff", "Magenta", "Red", "Aqua", "Tan", "LawnGreen", "Plum", "CornflowerBlue", "Yellow", "YellowGreen"];
var isRain=false;
//display a buttom to enter rainbow
isl.f.rainbow = function() {
    // var html = "<a href=# id=rainbow style='text-decoration:none;color:black;' title='click to use rainbow' >Rainbow</a>";
    var html = "<a href=# id=rainbow style='text-decoration:none;color:black;' title='"+isl.f.TranslateMenuBar('click to use rainbow')+"' >"+isl.f.TranslateMenuBar('Rainbow')+"</a>";

    $("#" + isl.p.topUserPannel).append(html);

    $("#rainbow").on("click", function(e){
        //console.log(isRain);
        isRain=!isRain;
        isl.f.resetAll();
        isl.f.printAllLegendsOnCanvas();
    });
}

//display a buttom to enter rainbow
isl.f.flipswitchButton = function() {
    // var html = "<a href=# id=rainbow style='text-decoration:none;color:black;' title='click to use rainbow' >Rainbow</a>";
    var html = "<a href=# id=flipswitch>"
        +"<label class='switch'>  <input id='flipswitchcheckbox' type='checkbox' " + isl.p.flipswitch_checked +">  <div class='slider round'><div class='textflip' style='margin-left: "
        + isl.p.flipswitch_marleft + "px;"
        +"'>"
        + isl.p.flipswitch_test
        + "</div></div></label>"
        +"</a>";

    $("#" + isl.p.topUserPannel).append(html);

    $("#flipswitchcheckbox").change(function() {
        if(this.checked) {
            // console.log('flipswitchButton on');
            isl.p.flipswitch_test = "ON";
            isl.p.flipswitch_marleft = 10;
            isl.p.flipswitch_checked = "checked";
        }
        else{
            // console.log('flipswitchButton off');
            isl.p.flipswitch_test = "OFF";
            isl.p.flipswitch_marleft = 30;
            isl.p.flipswitch_checked = '';
        }
        isl.f.resetAll();
    });
}

//display copy current image
isl.f.CountImages = function() {
    var currentImage = isl.current.images[isl.currentImage];
    // console.log(currentImage);
    if (currentImage){
        var html = "<div id=CountImages style='vertical-align:top;text-align:center;display:inline-block; color:white' ></div>";
        $container = $("#CountImages");
        var images = isl.p.getSortedImage();

        var bottomBandHeight=50;
        var bottomBandArrowsSize = Math.floor(bottomBandHeight* 0.5 );
        var bottomBandArrowsColor = "white";
        var style = "style='text-decoration:none;font-size:"+bottomBandArrowsSize+"px;color:"+bottomBandArrowsColor+";'";
        // console.log($container);
        $("#" + isl.p.topUserPannel).append(html);
        // $("#imagescriptcontainer").append("<div id=CountImages style='vertical-align:top;text-align:center;display:inline-block; color:white' >Test</div>");

        $("#CountImages").append("<a "+style+" href=# id=bottomBandLeftArrow1  > &lt;&lt; </a>");
        $("#CountImages").append(" <span "+style+ " > "+ ( parseInt( isl.f.getRelativeImagePosition( currentImage.id )) +1)+"/"+images.length+" </span>");
        $("#CountImages").append("<a "+style+" href=# id=bottomBandRightArrow1  > &gt;&gt; </a>");

        // console.log($("#CountImages").html());

        // $("#" + isl.p.topUserPannel).append($("#CountImages").html());
        //add the events on the link
        $("#bottomBandLeftArrow1").bind("click",function(e){
            e.preventDefault();
            isl.currentImage = isl.f.getPreviousImageId();
            isl.f.resetAll();
        });
        $("#bottomBandRightArrow1").bind("click",function(e){
            e.preventDefault();
            isl.currentImage = isl.f.getNextImageId();
            isl.f.resetAll();
        });
    }

};

//display a button to enter fullscreen mode
isl.f.fullScreenButton = function(){
    // var html = "<a href=# id=fullScreenButton style='text-decoration:none;color:black;' title='click to use full screen mode' >full screen</a>";
    var html = "<a href=# id=fullScreenButton style='text-decoration:none;color:black;' title='click to use full screen mode' >"+isl.f.TranslateMenuBar('full screen')+"</a>";

    $("#"+isl.p.topUserPannel).append(html);
    
    $("#fullScreenButton").on("click", function(e){
        var screenMode = isl.f.screenMode();
        if (screenMode == "normal") {
            console.log("enter full screen");
            //enter full screen ...
            var container = document.getElementById(isl.p.globalContainer);
            
            $(document).fullScreen(true);
            console.log(done);

        }
        else{
            //or exit full screen
            if (document.exitFullscreen) {
                document.exitFullscreen();
            } else if (document.webkitExitFullscreen) {
                document.webkitExitFullscreen();
            } else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if (document.msExitFullscreen) {
                document.msExitFullscreen();
            }
        }

    });
    
}

isl.f.isSearchactive = function(){
    return "legendSearchValue" in isl.p ? true : false;
};

//display a button to search for legends
isl.f.resetLegendSearch = function(){
    // if set search value
    $search_par = $.urlParam('search');
    // console.log("$search_par= "+$search_par);

    if ( $search_par!= null && $search_par != '' && isl.p.legendSearchValue == null  && isl.f.isFirst)
    {
        isl.p.legendSearchValue = $.urlParam('search');
        console.log("isl.p.legendSearchValue= "+isl.p.legendSearchValue);
        var legends = isl.current.legends;

        for(var lid in legends){
            var txt = isl.f.getLegendText(lid);
            if( txt.toLowerCase() == isl.p.legendSearchValue.toLowerCase() ){
                // console.log("search= "+legends[lid]);
                // console.log("search= "+isl.f.getLegendText(legends[lid].id));
                // isl.f.select_search_default(legends[lid]);
                isl.p.legendSearchValue = isl.f.getLegendText(legends[lid].id);
                isl.p.default_serch = lid;
                break;
                // interestingLegends.push(legends[lid]);
            }
        }
    }

    // var html = "<input placeholder='search anatomy' type=text id=legendSearchInput style='' title='search for a structure' value='"+("legendSearchValue" in isl.p ? isl.p.legendSearchValue : "" )+"'>";
    // var html = "<input placeholder='search anatomy' type=text id=legendSearchInput style='' title="+isl.f.TranslateMenuBar('search for a structure')+" value='"+("legendSearchValue" in isl.p ? isl.p.legendSearchValue : "" )+"'>";
    var html = "<input placeholder='"+isl.f.TranslateMenuBar("search anatomy")+"' type=text id=legendSearchInput style='' title='"+isl.f.TranslateMenuBar('search for a structure')+"' value='"+("legendSearchValue" in isl.p ? isl.p.legendSearchValue : "" )+"'>";

    // console.log(html);

    $("#"+isl.p.topUserPannel).append(html);
    
    //add an event to display a div to propose legends
    $("#legendSearchInput").on("change", isl.f.searchLegendInputModificationEvent );
    $("#legendSearchInput").on("keyup", isl.f.searchLegendInputModificationEvent );
    $("#legendSearchInput").on("mouseover", isl.f.Clearsearch );
    // $("#legendSearchInput").on("focus", isl.f.Click_one);
    // $("#legendSearchInput").on("click", isl.f.Click_one );
    // $("#legendSearchInput").on("click", isl.f.Click_one);

    //add an event to close the legend selection when clicking outside
    $("body").unbind(".closeLegendSearchOnClick").bind("click.closeLegendSearchOnClick",function(e){
        if( ! $(this).parents('#legendSearchDiv').length ) $("#legendSearchDiv").remove();
    });


}

//default use search value is searc is not null
isl.f.select_search_default = function(lid){
    // console.log('run select_search_default');
    // $('#maincanvas').focus();
    // console.log('    before');
    isl.f.resetAll();
    // if ( ! $('#legendSearchDiv').length ){
        //get the position of the search element
        var position = $("#legendSearchInput").offset();
        //get the position of the main canvas
        var canvasPosition = $("#"+isl.p.mainImageCanvas).offset();
        var height = $("#legendSearchInput").height() + 15;
        var width = $("#legendSearchInput").width();
        $("body").append("<div id=legendSearchDiv style='display: none; z-index:999;background-color:white;position:absolute;left:"+position.left+"px;top:"+(canvasPosition.top)+"px;width:"+width+"px;max-height:"+isl.p.mainImageHeight+"px;'></div> ")
        //$("body").append("<div id=legendSearchDiv style='z-index:999;position:fixed;left:500px;top:10px;width:"+width+";height:500;max-height:"+isl.p.mainImageHeight+";'></div> ")
    // }
    // console.log('    after');
    var  legendSearchDivHtml = "<a href=# class=legendSearchChoice id=default_search lid="+isl.current.legends[lid].id+">"+ isl.f.getLegendText(isl.current.legends[lid].id) +"</a><br>";
    $("#legendSearchDiv").html(legendSearchDivHtml);

    isl.p.legendSearch=$('#default_search');

    // console.log("    "+isl.p.legendSearch);

    //remove the search div
    // $("#legendSearchDiv").remove();
    //add the structure of the legend to the list of structure to display

    var legend = isl.current.legends[lid];
    if ("structure" in legend && legend.structure != "") {
        isl.f.makeStructureVisible(legend.structure);
    }
    //search the first image displaying the legend
    var images = isl.p.getSortedImage();
    for (var i in images) {
        if (lid in images[i].legends) {
            isl.currentImage = images[i].id;
            break;
        }
    }
    //reset all to display the right image with the legend on it
    isl.f.resetAll();

    // console.log('run select_search_default  READY');
}

isl.f.Clearsearch =function() {
    // init plugin (with callback)
    $('#legendSearchInput').clearSearch({ callback: function() { isl.p.legendSearch=null;isl.f.resetAll(); } } );
};


isl.f.Click_one =function() {
    alert( "Handler for .click() called." );
};


//clear search
(function($) {
    $.fn.clearSearch = function(options) {
        var settings = $.extend({
            'clearClass' : 'clear_input',
            'focusAfterClear' : true,
            'linkText' : '&times;'
        }, options);
        return this.each(function() {
            var $this = $(this), btn,
                divClass = settings.clearClass + '_div';

            if (!$this.parent().hasClass(divClass)) {
                $this.wrap('<div style="position: relative; display: inline-block; margin-bottom: -40px" class="'
                    + divClass + '">' + $this.html() + '</div>');
                $this.after('<a style="position: absolute; cursor: pointer;" class="'
                    + settings.clearClass + '">' + "X" + '</a>');
            }
            btn = $this.next();

            function clearField() {
                // console.log("clearField");

                $this.val('').change();
                triggerBtn();
                if (settings.focusAfterClear) {
                    $this.focus();
                }
                if (typeof (settings.callback) === "function") {
                    settings.callback();
                }

                isl.p.legendSearch=null;
            }

            function triggerBtn() {
                if (hasText()) {
                    btn.show();
                } else {
                    btn.hide();
                }
                update();
            }

            function hasText() {
                return $this.val().replace(/^\s+|\s+$/g, '').length > 0;
            }

            function update() {
                var width = $this.outerWidth(), height = $this.outerHeight();
                btn.css({
                    top : height / 2 - btn.height() / 2,
                    left : width - height / 2 - btn.height() / 2+30
                });
            }

            btn.on('click', clearField);
            $this.on('keyup keydown change focus', triggerBtn);
            triggerBtn();
        });
    };
})(jQuery);

//function to trigger when the searchLegendInput has been modified
isl.f.searchLegendInputModificationEvent = function(e){
    // console.log('searchLegendInputModificationEvent');
    //save the value of the input
    /*$(this).keydown(function(e) {
        if (e.keyCode==46)
        {
            isl.p.legendSearch=null;
            $(this).val('');
            console.log($(this).val());
            return;
        }
        //console.log(e.keyCode);
    });*/
    isl.p.legendSearchValue = $(this).val();

    if (isl.p.legendSearchValue !== null && isl.p.legendSearchValue !== ''){
        cursid = 'all';
        Selectedstructures=[];
        var newAvailable = [];
        // for(var s in isl.current.structures) newAvailable.push(s);
        for(var s in isl.current.structures){
            newAvailable.push(s);
            if (isl.f.getStructureText( s )=='Bone' || isl.f.getStructureText( s )=='joint')
                Selectedstructures.push(s);
        }
        $("#selectVisibleStructures").val('all');
    }


    var shouldContain = isl.p.legendSearchValue.toLowerCase();
    var legendsHtml = "";
    //if the container element does not exist, create it

    if ( ! $('#legendSearchDiv').length ){
        //get the position of the search element
        var position = $("#legendSearchInput").offset();
        //get the position of the main canvas
        var canvasPosition = $("#"+isl.p.mainImageCanvas).offset();
        var height = $("#legendSearchInput").height() + 15; 
        var width = $("#legendSearchInput").width();
        $("body").append("<div id=legendSearchDiv style='z-index:999;background-color:white;position:absolute;left:"+position.left+"px;top:"+(canvasPosition.top)+"px;width:"+width+"px;max-height:"+isl.p.mainImageHeight+"px;'></div> ")
        //$("body").append("<div id=legendSearchDiv style='z-index:999;position:fixed;left:500px;top:10px;width:"+width+";height:500;max-height:"+isl.p.mainImageHeight+";'></div> ")
    }

    //search for the legends with the right text
    var legends = isl.current.legends;
    var interestingLegends = [];
    for(var lid in legends){
        var txt = isl.f.getLegendText(lid);
        if( txt.toLowerCase().indexOf( shouldContain ) > -1){
            interestingLegends.push(legends[lid]);
        }
    }
    //build the html to display inside the legendSearchDiv
    var legendSearchDivHtml = "";
    if (interestingLegends.length == 0) legendSearchDivHtml += "no match found";
    for (var l in interestingLegends) {
        legendSearchDivHtml += "<a href=# class=legendSearchChoice lid="+interestingLegends[l].id+">"+ isl.f.getLegendText(interestingLegends[l].id) +"</a><br>";
        
    }
    $("#legendSearchDiv").html(legendSearchDivHtml);
    
    $("#legendSearchDiv *").on("hover",function(e){
        $("#legendSearchInput") . focusout();
        $(this).focus();
    } );


    //bind events on click on those options
    $(".legendSearchChoice").bind("click",isl.f.eventLegendSearchClick);
}



//event when clicking on a legend in the search display
isl.f.eventLegendSearchClick = function(e){
    var lid = $(this).attr("lid");
    //set the search form to the legend name
    isl.p.legendSearchValue = $(this).text();
    isl.p.legendSearch=$(this);

    // console.log("isl.p.legendSearch= "+isl.p.legendSearch);

    //remove the search div
    $("#legendSearchDiv").remove();
    //add the structure of the legend to the list of structure to display
    var legend = isl.current.legends[lid];
    if ("structure" in legend && legend.structure != "") {
        isl.f.makeStructureVisible(legend.structure);
    }
    //search the first image displaying the legend
    var images = isl.p.getSortedImage();
    for (var i in images) {
        if (lid in images[i].legends) {
            isl.currentImage = images[i].id;
            break;
        }
    }
    //reset all to display the right image with the legend on it
    isl.f.resetAll();
}

//display a button to change the mode legend are displayed
isl.f.legendModeButton = function(){
    switch(isl.p.legendMode){
        case "print" :
            nextMode = "quiz";
            var title = "click to use the quiz mode";
            var name = "displayed";
            break;
        case "quiz" :
            // nextMode = "square";
            nextMode = "hide";
            var title = "click to hide the legends";
            var name = "quiz";
            break;
        // case "square" :
        //     nextMode = "hide";
        //     var title = "click to target legends by area";
        //     var name = "square";
        //     // var name = "largesquare";
        //     break;
        case "hide" :
            nextMode = "print";
            var title = "click to print the legends"; 
            var name = "hidden";
            break;
        default:
            nextMode = "quiz";
            var title = "click to use the quiz mode";
            var name = "displayed";
    }
    
    // var html = "<a href=# id=legendModeButton style='text-decoration:none;color:black;' title='"+title+"' >"+name+"</a>";
    var html = "<a href=# id=legendModeButton style='text-decoration:none;color:black;' title='"+isl.f.TranslateMenuBar(title)+"' >"+isl.f.TranslateMenuBar(name)+"</a>";
    // isl.f.TranslateMenuBar('displayed');
    $("#"+isl.p.topUserPannel).append(html);
    
    $("#legendModeButton").bind("click",function(e){
        isl.p.legendMode = nextMode;
        if (nextMode == "square") {
            //reset parameters about legends :
            isl.p.legendSquareCenterPercentWidth = 0.5;
            isl.p.legendSquareCenterPercentHeight = 0.5;
        }
        isl.f.resetAll();
    });
    
}


//display a print button
isl.f.printButton = function(){
    var html = "<a href=# id=printButton style='text-decoration:none;' title=print  class='userbutton' >&#8865;</a>";

    $("#"+isl.p.topUserPannel).append(html);
    
    $("#printButton").bind("click",function(e){
        var dataUrl = document.getElementById(isl.p.mainImageCanvas).toDataURL("image/jpeg" , 1);
        var ctx = isl.f.getMainContext();
        var width = ctx.canvas.clientWidth;
        var height = ctx.canvas.clientHeight;
        var windowContent = '<!DOCTYPE html>';
        windowContent += '<html>'
        windowContent += '<head><title>Print canvas</title></head>';
        windowContent += '<body>'
        windowContent += '<img src="' + dataUrl + '">';
        windowContent += '</body>';
        windowContent += '</html>';
        var printWin = window.open('','','width='+width+',height='+height+'');
        printWin.document.open();
        printWin.document.write(windowContent);
        printWin.document.close();
        printWin.focus();
        printWin.print();
        printWin.close();
    });
    
    
}


//display a button that allow to download the current image as displayed on the screen
isl.f.downloadButton = function(){
    var html = "<a href=# id=downloadButton style='text-decoration:none;color:green;'  class='userbutton' title='download the current image' >&#8627;</a>";
    

    $("#"+isl.p.topUserPannel).append(html);
    
    $("#downloadButton").bind("click",function(e){
        var downloadButton = document.getElementById("downloadButton");
        var canvas = document.getElementById(isl.p.mainImageCanvas) ;
        downloadButton.href = canvas.toDataURL("image/jpeg" , 1);
        downloadButton.download = isl.currentImage in isl.current.images ? isl.current.images[isl.currentImage].url : "";
    });
}

//display buttons to perform symetry
isl.f.symetryButtons = function(){
    var html = "<a href=# class='userbutton' title='flip vertical' style='text-decoration:none;' id=horizontalSymetryButton>&#x21C5;</a> <a  class='userbutton' title='flip horizontal' style='text-decoration:none;' href=# id=verticalSymetryButton>&#x21C6;</a>";
    
    $("#"+isl.p.topUserPannel).append(html);
    
    $("#horizontalSymetryButton").bind("click",function(e){
        e.preventDefault();
        isl.p.horizontalSymetry = !isl.p.horizontalSymetry;
        isl.f.resetAll();
    });
    $("#verticalSymetryButton").bind("click",function(e){
        e.preventDefault();
        isl.p.verticalSymetry = !isl.p.verticalSymetry;
        isl.f.resetAll();
    });
}

//display a button that switch to catalog mode on click
isl.f.resetZoomButtons = function(){
    var html = "<a href=# id=zoomButton style='text-decoration:none;' title='zoom +'  class='userbutton' >&#8853;</a> <a href=# id=unzoomButton style='text-decoration:none;' title='zoom -' class='userbutton' >&#8854;</a>";
    
    $("#"+isl.p.topUserPannel).append(html);
    
    $("#zoomButton").bind("click",function(e){ e.preventDefault(); isl.f.zoom(100); });
    $("#unzoomButton").bind("click",function(e){ e.preventDefault(); isl.f.zoom(-100); });
    
    //$("#zoomButton").bind("click",function(e){ e.preventDefault(); isl.f.addCanvasZoomEvent(-0.21); });
    //$("#unzoomButton").bind("click",function(e){ e.preventDefault(); isl.f.addCanvasZoomEvent(+0.21); });
    
    
}

//add a zoom event zo the canvas
isl.f.addCanvasZoomEvent = function(percentGap){
    ////change the cursor to a zoom
    //$("#"+isl.p.mainImageCanvas+",body").css( 'cursor', percentGap < 0 ? "zoom-in" : "zoom-out" )
    ////unbind and bind event
    //$("#"+isl.p.mainImageContainer).unbind(".zoom")
    //.bind("click.zoom", isl.f.createZoomFunction(percentGap) );
    
    var imageDimension = isl.p.zoomEndPercentWidth - isl.p.zoomStartPercentWidth;
    var imageDimension = isl.p.zoomEndPercentWidth - isl.p.zoomStartPercentWidth;
    if ( (percentGap<0 && imageDimension <= Math.abs(percentGap)) || (percentGap>0 && imageDimension >=1) ) return;
    
    //build the new image dimension
    var newDimension = Math.max( 0.2 , Math.min(1, imageDimension + percentGap) ) ;
    var targetPercentWidth = 0.5;
    var targetPercentHeight = 0.5;
    
    //zoom :
    isl.f.zoomAt(newDimension , targetPercentWidth, targetPercentHeight);
    
}

//function to zoom at a given position
isl.f.zoomAt = function(newDimension , percentWidth, percentHeight){
    if( isl.p.verticalSymetry ) {
            var newEndWidth = Math.min( 1, percentWidth - newDimension/2 < 0  ? newDimension : percentWidth + newDimension/2 );
            var newStartWidth = newEndWidth - newDimension;
        }
        else{
            var newStartWidth = Math.max( 0, percentWidth + newDimension/2 > 1 ? 1 - newDimension : percentWidth - newDimension/2 );
            var newEndWidth = newStartWidth + newDimension;
        }
        if(isl.p.horizontalSymetry) {
            var newEndHeight = Math.min( 1, percentHeight - newDimension/2 < 0 ? newDimension : percentHeight + newDimension/2 );
            var newStartHeight = newEndHeight - newDimension;
        }
        else{
            var newStartHeight = Math.max( 0, percentHeight + newDimension/2 > 1 ? 1 - newDimension : percentHeight - newDimension/2 );
            var newEndHeight = newStartHeight + newDimension;
        }
        
        
        
        isl.p.zoomStartPercentHeight = newStartHeight;
        isl.p.zoomEndPercentHeight = newEndHeight;
        isl.p.zoomEndPercentWidth = newEndWidth;
        isl.p.zoomStartPercentWidth = newStartWidth;
        
        //lastly, reset all
        isl.f.resetAll();
}


//create a function to add as zoom event
isl.f.createZoomFunction = function(percentGap){
    var gap = percentGap;
    return function(e){
        //change the cursor back to origin
        $("#"+isl.p.mainImageCanvas+",body").css( 'cursor', 'auto' )
        //unbind zoom event
        $("#"+isl.p.mainImageContainer).unbind(".zoom");
        //if the gap is superior or equal to the image gap, don't do anything
        var imageDimension = isl.p.zoomEndPercentWidth - isl.p.zoomStartPercentWidth;
        if ( (gap<0 && imageDimension <= Math.abs(gap)) || (gap>0 && imageDimension >=1) ) return;
        //get the percentWidth and Height of the click
        
        var canvasX = e.pageX - $(this).offset().left;
        var canvasY = e.pageY - $(this).offset().top;
        var percentWidth  = isl.f.percentWidthFromWidth(canvasX - isl.p.legendLeftWidth) ;
        var percentHeight = isl.f.percentHeightFromHeight(canvasY );
        
        //build the new image dimension
        var newDimension = Math.max( 0.2 , Math.min(1, imageDimension + gap) ) ;
        
        //zoom :
        isl.f.zoomAt(newDimension , percentWidth, percentHeight);
    }
}

//increase or decrease by shift the size of the main image, then reset all
isl.f.zoom = function(shift){
    isl.p.setImageWidth += shift;
    if (isl.p.setImageWidth> 2000) isl.p.setImageWidth = 2000;
    if (isl.p.setImageWidth< 400) isl.p.setImageWidth = 400;
    isl.f.resetAll();
    //reload the bottom band
    isl.f.loadBottomBandImages();
}

//display a button that switch to catalog mode on click
isl.f.resetCatalogModeButton = function(){
    
    var html = "<a href=# id=switchToCatalogMode>catalog</a>";
    
    $("#"+isl.p.topUserPannel).append(html);
    
    $("#switchToCatalogMode").bind("click",function(e){
        isl.f.openCatalog();
        //display the possible series
        isl.f.displayCatalogSeries();
    })
    
    
}

//open the catalog with all the images
isl.f.openCatalog = function(){
    //add a window to display the description
    var $canvas = $("#"+isl.p.mainImageCanvas);
    var ctx = isl.f.getMainContext();
    var position = $canvas.offset();
    var html = "";
    html += "<div id=catalogPopup style='overflow:hidden;display:none;position:absolute;z-index:100000;top:"+position.top+"px;left:"+position.left+"px;width:"+ctx.canvas.clientWidth+"px;height:"+ctx.canvas.clientHeight+"px;background-color:rgba(255, 255, 255, 0.85);'>"+
                "<span id='close' onclick='this.parentNode.parentNode.removeChild(this.parentNode); return false;' style='float:right;padding:10px;cursor:pointer;' title=close >X</span>"+
                "<div id=catalogLeft style='width:200px;float:left'></div><div style='overflow:scroll;height:"+ctx.canvas.clientHeight+"px;' id=catalogRight></div>"+
                "</div>";
    $("body").append(html);
    $("#catalogPopup").show("slow");
}

//on the left part of the catalog, display the possible series
isl.f.displayCatalogSeries = function(){
    var html = "";
    //display an option to print the images of all series
    var iid = isl.f.findImageBelongingToSeries(null);
    if (iid == null) return;
    var image = isl.current.images[iid];
    html += "<div class=catalogSeries style='pointer:cursor;' sid=''>all series";
    html += "<br><img style='width:100px;cursor:pointer;' src='"+image.url+"' >";
    html += "</div><br>";

    //display each series
    for(var sid in isl.current.series){
        var iid = isl.f.findImageBelongingToSeries(sid);
        if (iid == null) continue;
        var image = isl.current.images[iid];
        html += "<div class=catalogSeries style='pointer:cursor;' sid='"+sid+"'>"+isl.current.series[sid].text;
        html += "<br><img style='width:100px;cursor:pointer;' src='"+image.url+"' >";
        html += "</div><br>";
    }
    $("#catalogLeft").html(html);
    //bind an event on click
    $(".catalogSeries").bind("click",function(e){
        var sid = $(this).attr("sid");
        isl.f.displayCatalogImages(sid);
    })
}

//inside the catalog, display the images of the given series
isl.f.displayCatalogImages = function(sid){
    if (sid == "") sid = null;
    var images = isl.p.getSortedImage(sid);
    var html = "";
    for (var i in images) {
        html += "<div class=catalogImage iid="+images[i].id+" style='display:inline-block;padding:10px;'>";
        html += "<img src='"+images[i].url+"' style='width:100px;cursor:pointer;'>";
        html += "</div>";
    }
    
    $("#catalogRight").html(html);
    
    //bind an event on click
    $(".catalogImage").bind("click",function(e){
        var iid = $(this).attr("iid");
        isl.currentImage = iid;
        $("#catalogPopup").remove();
        isl.currentSeries = null; //display all the series
        isl.f.resetAll();
    })
    
}

//for a given series, find the id of the first image matching the series
isl.f.findImageBelongingToSeries = function(sid){
    var images= isl.p.getSortedImage(null);
    for (var i in images) {
        if (images[i].series == sid || sid == null) return images[i].id;
    }
    return null;
}

// var Choosetext='';
var Selectedstructures=[];
var cursid = '';
//display html and add events for the strcutrure selection
isl.f.resetStructureSelection = function(){
    var html = "";
    html += "<select id=selectVisibleStructures> <option selected disabled>"+isl.f.TranslateMenuBar('Structures')+"</option>";
    // var title='';
    // if (Choosetext=='') title=isl.f.TranslateMenuBar('Structures');
    // else title=Choosetext;

    // html += "<select id=selectVisibleStructures> <option selected disabled>"+title+"</option>";
    // html += "<option value=all>all</option><option value=none>none</option>";
    html += "<option value=all>"+isl.f.TranslateMenuBar('all')+"</option><option value=none>"+isl.f.TranslateMenuBar('none')+"</option>";

    // sort the elements of the select
    var sortarr = [];
    for(s in isl.current.structures){ sortarr.push([isl.f.getStructureText( s ), s ]) }
    sortarr.sort( function(a,b){return a[0].localeCompare(b[0]);} );
    
    for(var i in sortarr){
        var s = sortarr[i][1];
        var visibleYet = isl.f.isStructureVisible(s);
        // console.log(s+' '+visibleYet);
        // html += "<option value="+s+" "+(visibleYet ? "" : "style='text-decoration:line-through;'" )+">"+ isl.f.getStructureText( s ) +"</option>";
        // html += "<option value="+s+" "+(visibleYet ? "" : "style='text-decoration:line-through;'" )+">"+ isl.f.TranslateMenuBar(isl.f.getStructureText( s )) +"</option>";

        if (cursid == 'all'){
            // console.log('test');
            isl.f.makeStructureVisible(s);
            html += "<option value=" + s + ">" + isl.f.TranslateMenuBar(isl.f.getStructureText(s)) + "</option>";
            continue;
            //visibleYet = true;
        }


        if (!(Selectedstructures.length===0 && ($.inArray(isl.f.getStructureText(s).toLowerCase(), isl.defaulthidelegends) != -1) )  )
        {
            // console.log(isl.f.getStructureText( s ));
            html += "<option value="+s+" "+(visibleYet ? "" : "class=txtDecoration" )+">"+ isl.f.TranslateMenuBar(isl.f.getStructureText( s )) +"</option>";
        }
        else
        {
            isl.f.makeStructureInvisible(s);
            html += "<option value=" + s + " " + ("class=txtDecoration" ) + ">" + isl.f.TranslateMenuBar(isl.f.getStructureText(s)) + "</option>";
        }


        // html += "<option value="+s+" "+(visibleYet ? "" : "class=txtDecoration" )+">"+ isl.f.TranslateMenuBar(isl.f.getStructureText( s )) +"</option>";
    }
    //for(var s in isl.current.structures){
    //    var visibleYet = isl.f.isStructureVisible(s);
    //    html += "<option value="+s+" "+(visibleYet ? "" : "style='text-decoration:line-through;'" )+">"+ isl.f.getStructureText( s ) +"</option>";
    //}
    
    $("#"+isl.p.topUserPannel).append(html);


    // var prev_sid = null;
    //on change, change the visible anatomic structures
    $("#selectVisibleStructures").change(function(e){
        // console.log('selectVisibleStructures');
        var sid = $(this).val();
        // console.log(sid);
        // Choosetext=$("#selectVisibleStructures option:selected").text();
        //if all, build an array containing all the sid
        cursid = '';
        if (sid == "all") {
            // console.log('all');
            cursid = 'all';
            Selectedstructures=[];
            var newAvailable = [];
            // for(var s in isl.current.structures) newAvailable.push(s);
            for(var s in isl.current.structures){
                newAvailable.push(s);
                // if (isl.f.getStructureText( s )=='Bone' || isl.f.getStructureText( s )=='joint')
                //     Selectedstructures.push(s);
            }

            isl.p.displayedStructures = newAvailable;
            // Selectedstructures=[];

        }
        else if (sid == "none") {
            isl.p.displayedStructures = [];
            Selectedstructures=[];
        }
        else{
            // console.log('else');
            //toggle the structure
            // if (isl.f.isStructureVisible(sid))
            //     isl.f.makeStructureInvisible(sid);
            // else isl.f.makeStructureVisible(sid);
            // Click when show
            // if (prev_sid == 'all'){
            //     console.log('clear structure');
            //     Selectedstructures = [];
            // }

            isl.p.displayedStructures = [];
            if (jQuery.inArray(sid, Selectedstructures) === -1){
                Selectedstructures.push(sid);
                console.log('push sid= '+sid);
            }

            else {
                Selectedstructures.splice( $.inArray(sid, Selectedstructures), 1 );
                console.log('splice sid= '+sid);
            }
            // console.log(Selectedstructures);
            isl.p.displayedStructures=Selectedstructures;
        }
        //reset all
        // prev_sid = sid;
        isl.f.resetAll();
    });
}


//function to make a structure visible
isl.f.makeStructureVisible = function(sid){
    if (! isl.f.isStructureVisible(sid))  isl.p.displayedStructures.push(sid);
    
}

//function to make a structure invisible
isl.f.makeStructureInvisible = function(sid){
    var index = isl.p.displayedStructures.indexOf(sid);
    if (index > -1)  isl.p.displayedStructures.splice( index , 1);
}

//check if a structure is visible
isl.f.isStructureVisible = function(sid){
    return isl.p.displayedStructures.indexOf(sid) > -1;
}

//function to display an image, given an image data
isl.f.displayMainImage = function(image){
    image = isl.f.retrieveImageToDisplay();
    //if there is no url in the image object, there is nothing to display
    if (!("url" in image)) {
        $("#"+isl.p.mainImageContainer).html("no image selected");
    }
    var url = image.url;
    isl.currentImage = image.id;
    //load the image to display
    var img = new Image();
    img.onload = function() {
        isl.f.displayMainImage2(url, this);
        isl.f.GoTosearch();
    }
    img.src = url;
}

//retrieve the image to display
isl.f.retrieveImageToDisplay = function(image){
    //if no image is provided, find the first image
    if (image != null && (image != undefined) ) return image;
    if (isl.currentImage != null && (isl.currentImage in isl.current.images) ) {
        return  isl.current.images[isl.currentImage];
    }
    else{
        var images = isl.p.getSortedImage();
        if (0 in images) return images[0];
    }
    //by default, return an empty object
    return {};
}



//function to display an image, once the image has been loaded
isl.f.displayMainImage2 = function(url,img){
    var mainImageContainer = $("#"+isl.p.mainImageContainer);
    //create a canvas inside the main container
    mainImageContainer.html("<canvas id="+isl.p.mainImageCanvas+"></canvas>");
    //set the size of the various elements of the canvas
    isl.p.mainImageWidth = isl.p.setImageWidth;
    isl.p.mainImageHeight = img.height * (isl.p.mainImageWidth / img.width );

//    var maxside = isl.f.getMaxSide();
    // console.log('maxside=' + maxside);

//    if (maxside > isl.p.mainImageHeight - maxdiff_side_legend){
//        var maincanvas =  $('#maincanvasframe');
//        var heightplus = isl.p.mainImageHeight - maxside + maxdiff_side_legend;
//        // console.log('isl.p.mainImageHeight before=' + isl.p.mainImageHeight);
//        isl.p.mainImageHeight += heightplus;
//        var maincanvas_h_new =   maincanvas.height() + heightplus;
        // console.log('maincanvas_h_new=' + maincanvas_h_new);
//        maincanvas.height(maincanvas_h_new);
        // maincanvas.height( maincanvas.height() + heightplus);
        // console.log('heightplus=' + heightplus);
        // console.log('isl.p.mainImageHeight=' + isl.p.mainImageHeight);
        // console.log('mainImageContainer.height=' + mainImageContainer.height());
        // isl.p.mainImageCanvasHeight += (maxside / maxcount_side_legend) * 100;
//    }

    // var maincanvas =  $('#maincanvasframe');
    // var coeff_max = 15;
    // var heightplus = isl.p.mainImageHeight + (67 - 40)*coeff_max;
    // console.log('isl.p.mainImageHeight before=' + isl.p.mainImageHeight);
    // isl.p.mainImageHeight += (67 - 40)*coeff_max;
    // var maincanvas_h_new =   maincanvas.height() + heightplus;
    // console.log('maincanvas_h_new=' + maincanvas_h_new);
    // maincanvas.height(maincanvas_h_new);

    isl.p.mainImageCanvasWidth = isl.p.mainImageWidth + isl.p.legendLeftWidth + isl.p.legendRightWidth;
    isl.p.mainImageCanvasHeight = isl.p.mainImageHeight;

    //print the image
    isl.f.printImageOnCanvas(img);
    
    //display scroll event
    isl.f.addEventScrollCanvas();
    
}

//change to the previous or the next image on scroll on the main image canvas
isl.f.addEventScrollCanvas = function(){
    $("#"+isl.p.mainImageCanvas).unbind(".scrollevent").on('mousewheel.scrollevent', function(event) {
       event.preventDefault();
        if (event.deltaY < 0){
            isl.currentImage = isl.f.getNextImageId();
            isl.f.resetAll();
        } else {
            isl.currentImage = isl.f.getPreviousImageId();
            isl.f.resetAll();
        }
        //return false to prevent bubbling and avoid window scrolling
        return false; 
    });
}

//get the id of the image preceding the current one
isl.f.getPreviousImageId = function(){
    var images = isl.p.getSortedImage();
    var pos = isl.f.getRelativeImagePosition(isl.currentImage , images);
    if ( pos == 0) return isl.currentImage;
    else {
        if (!isl.p.legendSearch || isl.p.legendSearch.text()=='')
            return images[pos - 1].id;
        else {
            //if (isl.p.legendSearch) console.log(isl.p.legendSearch.text());
            var lid = $(isl.p.legendSearch).attr("lid");
            var legend = isl.current.legends[lid];
            for (var i=pos-1; i>=0;i--) {
                if (lid in images[i].legends) {
                    //console.log(i);
                    return images[i].id;
                }
            }
            return isl.currentImage;
        }
    }
    //else return images[ pos -1 ].id;
}

//get the id of the next image 
isl.f.getNextImageId = function(){
    var images = isl.p.getSortedImage();
    var pos = isl.f.getRelativeImagePosition(isl.currentImage , images);
    if ( pos == (images.length-1) ) return isl.currentImage;
    else {
        if (!isl.p.legendSearch || isl.p.legendSearch.text()=='')
            return images[pos + 1].id;
        else {
            //if (isl.p.legendSearch) console.log(isl.p.legendSearch.text());
            var lid = $(isl.p.legendSearch).attr("lid");
            var legend = isl.current.legends[lid];
            for (var i=pos+1; i<images.length;i++) {
                if (lid in images[i].legends) {
                    //console.log(i);
                    return images[i].id;
                }
            }
            return isl.currentImage;
        }
    }
    //else return images[ pos + 1 ].id;
}

//get the image position relatively to the images of the current series
isl.f.getRelativeImagePosition = function(iid , images){
    if (images == null || images == undefined) {
        images = isl.p.getSortedImage();
    }
    var pos = 0;
    while( pos < images.length){
        if (iid == images[pos].id) break;
        else pos++;
    }
    return pos;
}

//print the given image on the canvas, leaving enough space for the legend
isl.f.printImageOnCanvas = function(img){
    //print the image on the canvas
    var ctx=isl.f.getMainContext();
    //resize the canvas
    ctx.canvas.width = isl.p.mainImageCanvasWidth;

    ctx.canvas.height = isl.p.mainImageCanvasHeight;
    //ctx.rect(0, 0, 150, 1000);

    ctx.rect(0,0,isl.p.mainImageCanvasWidth,isl.p.mainImageCanvasHeight);
    // console.log('isl.p.mainImageCanvasHeight=' + isl.p.mainImageCanvasHeight);
    ctx.fillStyle="black";
    ctx.fill(); 
    
    //perform tranformation of symetry
    ctx.save();
    ctx.scale( isl.p.verticalSymetry ? -1 : 1, isl.p.horizontalSymetry ? -1 : 1);
    var imageStartWidth = img.width * isl.p.zoomStartPercentWidth;
    var imageStartHeight = img.height * isl.p.zoomStartPercentHeight;
    var captureWidth = img.width * ( isl.p.zoomEndPercentWidth -  isl.p.zoomStartPercentWidth);
    var captureHeight = img.height * ( isl.p.zoomEndPercentHeight -  isl.p.zoomStartPercentHeight);
    ctx.drawImage(img,
                  imageStartWidth,
                  imageStartHeight,
                  captureWidth,
                  captureHeight,
                  isl.p.verticalSymetry ? -isl.p.legendLeftWidth-isl.p.mainImageWidth : isl.p.legendLeftWidth ,
                  isl.p.horizontalSymetry ? -isl.p.mainImageHeight : 0 ,
                  isl.p.mainImageWidth ,
                  isl.p.mainImageHeight
                  );

    // range -100 to 100
    brightness(parseInt(brightness_value));
    // brightness(-15);

    contrast(parseInt(contrast_value));
    // contrast(50);

    // console.log('brightness='+brightness_value);

    //ctx.drawImage(img,
    //              isl.p.verticalSymetry ? -isl.p.legendLeftWidth-isl.p.mainImageWidth : isl.p.legendLeftWidth ,
    //              isl.p.horizontalSymetry ? -isl.p.mainImageHeight : 0 ,
    //              isl.p.mainImageWidth ,
    //              isl.p.mainImageHeight
    //              );

    // ctx.data=contrastImage(ctx,30);

    ctx.restore();
    
    isl.f.printAllLegendsOnCanvas();
    
    isl.f.printAllStructuresOnCanvas();
    
    //display the square in square mode
    if(isl.p.legendMode == "square" ) isl.f.displaySquare();

    //display the large square in square mode
    if(isl.p.legendMode == "largesquare" ) {
        isl.f.displaySquareLarge();
    }
    
    //add an event to display the legend on mouse over on quiz mode
    isl.f.addCanvasEventQuizMode();
    
    //add an event in case on square mode to display and move 
    isl.f.addEventSquareMode();
    
    //add an event to make legend clickable
    isl.f.addCanvasEventLegendClick();

    //mouse's functions
    var cvs = document.getElementById(isl.p.mainImageCanvas);
    var rect = cvs.getBoundingClientRect(), root = document.documentElement;

    var contrast_range = (isl.p.contrast_max - isl.p.contrast_min) / ctx.canvas.height;
    var brightness_range = (isl.p.brightness_max - isl.p.brightness_min) / ctx.canvas.width;

    $('#'+isl.p.mainImageCanvas).bind('contextmenu', function(e){
        return false;
    });

    cvs.addEventListener("mousedown", function(e) {
        // console.log('mousedown');
        if(e.button === 2){
            isl.p.is_right_mouse_down = true;
            // console.log('isl.p.is_right_mouse_down='+isl.p.is_right_mouse_down);
            isl.p.is_right_mouse_down_pos.x = e.clientX - rect.left - root.scrollLeft;
            isl.p.is_right_mouse_down_pos.y = e.clientY - rect.top - root.scrollTop;
            // console.log('mouseX ='+isl.p.is_right_mouse_down_pos.x +', mouseY='+isl.p.is_right_mouse_down_pos.y);
        }
        else if(e.button === 0){
            isl.p.is_left_mouse_down = true;
            isl.p.is_left_mouse_down_old = e.clientX;
            isl.p.is_left_mouse_down_old_y = e.clientY;
            // cvs.style.cursor = "pointer";
            // isl.p.is_left_mouse_down_old = $(window).scrollLeft();
            console.log('isl.p.is_left_mouse_down='+isl.p.is_left_mouse_down);
        }
    });

    cvs.addEventListener("mouseup", function(e) {
        // console.log('e.button='+e.button);
        if(e.button === 2){
            isl.p.is_right_mouse_down = false;
            // console.log('isl.p.is_right_mouse_up='+isl.p.is_right_mouse_down);
        }
        else if(e.button === 0){
            isl.p.is_left_mouse_down = false;
            cvs.style.cursor = "default";
            // console.log('isl.p.is_left_mouse_down='+isl.p.is_left_mouse_down);
        }
    });

    cvs.addEventListener("mouseleave", function(e) {
        // console.log('mouseleave');
        isl.p.is_left_mouse_down = false;
        isl.p.is_right_mouse_down = false;
        cvs.style.cursor = "default";
        isl.f.resetAll();
    });

    cvs.addEventListener("mousemove", function(e) {
        // console.log('mousemove canvas');
        if (isl.p.is_right_mouse_down && !isl.p.is_left_mouse_down){
            // return relative mouse position
            var m_x = e.clientX - rect.left - root.scrollLeft;
            var m_y = e.clientY - rect.top - root.scrollTop;

            var angleRadians = Math.atan2(isl.p.is_right_mouse_down_pos.y - m_y, m_x- isl.p.is_right_mouse_down_pos.x);
            var sin = Math.sin(angleRadians);
            var cos = -Math.cos(angleRadians);

            contrast_value += isl.p.contrast_alpha * contrast_range * Math.abs(isl.p.is_right_mouse_down_pos.x - m_x) * Math.sign(cos);
            contrast_value = Math.round(Math.min(Math.max(contrast_value, isl.p.contrast_min), isl.p.contrast_max));
            contrast_value_form = contrast_value;


            brightness_value += isl.p.brightness_alpha * brightness_range * Math.abs(m_y- isl.p.is_right_mouse_down_pos.y) * Math.sign(sin);
            brightness_value = Math.round(Math.min(Math.max(brightness_value, isl.p.brightness_min), isl.p.brightness_max ));
            brightness_value_form = brightness_value;
            // console.log('contrast_value ='+contrast_value);
            // console.log('brightness_value ='+brightness_value);
            isl.f.resetAll();
        }
        else if(isl.p.is_left_mouse_down && !isl.p.is_right_mouse_down){
            // cvs.style.cursor = "pointer";
            var step_scroll_x =  - (e.clientX - isl.p.is_left_mouse_down_old) * isl.p.is_left_mouse_down_sevsetive;
            var step_scroll_y =  - (e.clientY - isl.p.is_left_mouse_down_old_y) * isl.p.is_left_mouse_down_sevsetive_y;
            // console.log('left mouse step_scroll_x =' + step_scroll_x);
            // console.log('left mouse  $(window).scrollLeft=' + $(window).scrollLeft());
            window.scrollTo($(window).scrollLeft() + step_scroll_x, $(window).scrollTop() + step_scroll_y);
        }
       else if(isl.p.is_left_mouse_down && isl.p.is_left_mouse_down){
           var m_x = e.clientX - rect.left - root.scrollLeft;
           var m_y = e.clientY - rect.top - root.scrollTop;

           var angleRadians = Math.atan2(isl.p.is_right_mouse_down_pos.y - m_y, m_x- isl.p.is_right_mouse_down_pos.x);
           // var sin = Math.sin(angleRadians);
           var cos = -Math.cos(angleRadians);

           var zoom_x = -isl.p.zoom_mouse * contrast_range * Math.abs(isl.p.is_right_mouse_down_pos.x - m_x) * Math.sign(cos);
           // console.log('ZOOM_X=' + zoom_x);
           if (zoom_x != 0 && !isNaN(zoom_x)){
               // if (zoom_x > 0)
               //     cvs.style.cursor = "zoom-in";
               // else{
               //     cvs.style.cursor = "zoom-out";
               // }
               isl.f.zoom(zoom_x);
           }


       }

    });
    
}

function brightness(amount){
    var canvas=document.getElementById(isl.p.mainImageCanvas);
    var ctx=canvas.getContext("2d");
    var data = ctx.getImageData(0,0,canvas.width,canvas.height);//get pixel data
    var pixels = data.data;
    for(var i = 0; i < pixels.length; i+=4){//loop through all data
        /*
         pixels[i] is the red component
         pixels[i+1] is the green component
         pixels[i+2] is the blue component
         pixels[i+3] is the alpha component
         */
        pixels[i] += amount;
        pixels[i+1] += amount;
        pixels[i+2] += amount;
    }
    data.data = pixels;
    ctx.putImageData(data,0,0);//put the image data back
}

//contrast effect
function contrast(amount){
    var canvas=document.getElementById(isl.p.mainImageCanvas);
    var ctx=canvas.getContext("2d");
    var data = ctx.getImageData(0,0,canvas.width,canvas.height);//get pixel data
    var pixels = data.data;
    var factor = (259 * (amount + 255)) / (255 * (259 - amount));
    for(var i = 0; i < pixels.length; i+=4){//loop through all data
        /*
         pixels[i] is the red component
         pixels[i+1] is the green component
         pixels[i+2] is the blue component
         pixels[i+3] is the alpha component
         */

        //var brightness = (pixels[i]+pixels[i+1]+pixels[i+2])/3; //get the brightness

        /*pixels[i] += brightness > 127 ? amount : -amount;
        pixels[i+1] += brightness > 127 ? amount : -amount;
        pixels[i+2] += brightness > 127 ? amount : -amount;*/



        pixels[i] = factor * (pixels[i] - 128) + 128;
        pixels[i+1] = factor * (pixels[i+1] - 128) + 128;
        pixels[i+2] = factor * (pixels[i+2] - 128) + 128;

    }
    data.data = pixels;

    ctx.putImageData(data,0,0);//put the image data back
}

function contrastImage(imageData, contrast) {

    var data = imageData.data;
    var factor = (259 * (contrast + 255)) / (255 * (259 - contrast));

    for(var i=0;i<data.length;i+=4)
    {
        data[i] = factor * (data[i] - 128) + 128;
        data[i+1] = factor * (data[i+1] - 128) + 128;
        data[i+2] = factor * (data[i+2] - 128) + 128;
    }
    return imageData;
}

isl.f.addCanvasEventLegendClick = function(){
    $canvas = $("#"+isl.p.mainImageCanvas);
    //on mouse over, check which legend is targeted
    $canvas.bind("mousemove", function(e){ isl.f.canvasEventLegendMouseoverOn(e,this);} );
    //remove it when leaving
    $canvas.bind("mouseleave", function(e){ isl.f.canvasEventLegendMouseoverOff(e,this)} );
    //on click, open a window to show the description
    $canvas.bind("click",function(e){  isl.f.canvasEventLegendClickShowDescription(e,this);} );
}

isl.f.canvasEventLegendClickShowDescription =function(e, jqelement){
    //get the legend under the mouse
    var lid = isl.f.getLegendIdUnderMouse(e, jqelement);
    if (lid == null) return;
    //add a window to display the description
    var $canvas = $("#"+isl.p.mainImageCanvas);
    var ctx = isl.f.getMainContext();
    var position = $canvas.offset();
    var description='';
    if (typeof(isl.current.legends[lid].description)=='object'){
        var code_page_lang =isl.f.GetCodelocaleByName(isl.p.languagepage);
        if (code_page_lang !=isl.p.Currentlocale)
            description =isl.current.legends[lid].description[code_page_lang];
        else
            description =isl.current.legends[lid].description[isl.p.Currentlocale];
    }

    else
        description = "description" in isl.current.legends[lid] ? isl.current.legends[lid].description : "";

    // console.log('description_obj='+isl.current.legends[lid]);
    // var propValue;
    // for(var propName in isl.current.legends[lid]) {
    //     propValue = isl.current.legends[lid][propName];
    //
    //     console.log(propName,propValue);
    // }

    // console.log('description='+description);
    // console.log('Currentlocale='+isl.p.Currentlocale);
    // console.log('description_ja='+isl.current.legends[lid].description[isl.p.Currentlocale]);

    var title = isl.f.getLegendText ( lid );
    var html = "";
    html += "<div id=descriptionPopup style='display:none;position:absolute;z-index:100000;top:"+position.top+"px;left:"+position.left+"px;width:"+ctx.canvas.clientWidth+"px;height:"+ctx.canvas.clientHeight+"px;background-color:rgba(255, 255, 255, 0.85);'>"+
                "<span id='close' onclick='this.parentNode.parentNode.removeChild(this.parentNode); return false;' style='float:right;padding:10px;cursor:pointer;' title=close >X</span>"+
                "<h2>"+title+"</h2>"+
                // "<h3>description : </h3>"+description+
                "<h3>"+isl.f.TranslateMenuBar('description')+"</h3>"+description+
                "<div class='desc_popup' >"+isl.f.TranslateMenuBar('description_default')+"</div>"
                "</div>"; 
    $("body").append(html);
    $("#descriptionPopup").show("slow");
}

//add an event to change the position of the square during mousemove on square mode
isl.f.addEventSquareMode = function(){
    if (isl.p.legendMode != "square" && isl.p.legendMode != "largesquare") return;
    $canvas = $("#"+isl.p.mainImageCanvas);
    $canvas.bind("mousemove", function(e){
        //perform an update only from time to time
        isl.p.squareUpdateCounter ++;
        if (isl.p.squareUpdateCounter == isl.p.squareUpdateCounterReset) isl.p.squareUpdateCounter = 0;
        else return;
        //update the square
        var canvasX = e.pageX - $(this).offset().left;
        var canvasY = e.pageY - $(this).offset().top;
        isl.p.legendSquareCenterPercentWidth = isl.f.percentWidthFromWidth(canvasX - isl.p.legendLeftWidth) ;
        isl.p.legendSquareCenterPercentHeight = isl.f.percentHeightFromHeight(canvasY );
        isl.f.resetAll();
    } );
}

//add an event to display the legend on mouse over on quiz mode
isl.f.addCanvasEventQuizMode = function(){
    //add an event just in case of quiz mode
    if (isl.p.legendMode != "quiz") return;
    $canvas = $("#"+isl.p.mainImageCanvas);
    $canvas.bind("mousemove", function(e){ isl.f.canvasEventQuizModeMouseover(e,this)} );
}

//event to check if new legends must be displayed on quiz mode on mouse
isl.f.canvasEventQuizModeMouseover = function(e,jqelement){
    var lid = isl.f.getLegendIdUnderMouse(e, jqelement);
    //dispay it unless it is already done
    if( isl.p.displayedQuizLegends.indexOf( lid ) == -1) {
        isl.p.displayedQuizLegends.push(lid);
        isl.f.resetAll();
    }
}

//check which legend is under the mouse
isl.f.getLegendIdUnderMouse = function(e, jqelement){
    var canvasX = e.pageX - $(jqelement).offset().left;
    var canvasY = e.pageY - $(jqelement).offset().top;
    if (canvasX < isl.p.legendLeftWidth) var side = "left";
    else if (canvasX > isl.p.legendLeftWidth + isl.p.mainImageWidth ) side = "right";
    else return null; //don't do anything if the cursor is not on top of a legend
    var legends = isl.p.getSortedLegends();
    //calculate the position of the cursor on the real image, without zoom 
    var cursorHeightOnRealImage = isl.f.realHeightFromHeight(canvasY); 
    //if the cursor is on top of a legend 
    for(var l in isl.p.legendsHeightOnCanvas){ 
        if( isl.p.legendsSideOnCanvas[ l ] != side ) continue; 
        var gap = Math.abs( isl.p.legendsHeightOnCanvas[l] -  cursorHeightOnRealImage ); 
        if ( gap < (isl.p.fontSize /2) ) { 
            return l; 
        } 
    } 
    return null; 
} 

//print the legend targeted by the mouse in bold
isl.f.canvasEventLegendMouseoverOn = function(e, jqelement){ 
    var previousLegend = isl.p.mouseOverLegend;
    isl.p.mouseOverLegend = isl.f.getLegendIdUnderMouse(e, jqelement); //this way the legend will be printed in bold
    if (isl.p.mouseOverLegend != previousLegend) {
        //set the mouse cursor 
        $("#"+isl.p.mainImageContainer).css( 'cursor', isl.p.mouseOverLegend == null ? "default" : 'pointer' );
        //reset all
        isl.f.resetAll();
    } 
}
//remove this event
isl.f.canvasEventLegendMouseoverOff = function(e){
    isl.p.mouseOverLegend = null; 
}

isl.f.printAllStructuresOnCanvas = function(){
    var ctx = isl.f.getMainContext(); 
    for(var s in isl.current.structures){
        var structure = isl.current.structures[s];
        if( !isl.f.isStructureVisible(s) ) continue;
        if (isl.currentImage in structure.draw ) { 
            var points = [];
            var displayed = true;
            for(var p in structure.draw[isl.currentImage] ){
                if (! isl.f.isDisplayed(structure.draw[isl.currentImage][p][0] , structure.draw[isl.currentImage][p][1])) {
                    displayed = false; 
                    break;
                }
                //if one point is outside the canvas, don't display it
                points.push([isl.f.getWidthCoordinate( structure.draw[isl.currentImage][p][0] )+ isl.p.legendLeftWidth ,
                             isl.f.getHeightCoordinate( structure.draw[isl.currentImage][p][1] ) ]);
                //points.push([ structure.draw[isl.currentImage][p][0]*isl.p.mainImageWidth + isl.p.legendLeftWidth ,  structure.draw[isl.currentImage][p][1]*isl.p.mainImageHeight ]);
            }
            if(displayed) isl.f.printAndFillCurve(ctx, points, structure.color , isl.p.drawingOpacity );
        }
    }
}

//function to print and fill a curve on a canvas
isl.f.printAndFillCurve = function(ctx, points , color, opacity){ 
    if (points.length < 3) return;
    ctx.save();
    ctx.beginPath();
    ctx.moveTo(points[0][0], points[0][1]);
    points.push(points[0]);
    for (i = 1; i < points.length - 2; i ++)
    {
       var xc = (points[i][0] + points[i + 1][0]) / 2;
       var yc = (points[i][1] + points[i + 1][1]) / 2;
       ctx.quadraticCurveTo(points[i][0], points[i][1], xc, yc);
    }
    // curve through the last two points
    ctx.quadraticCurveTo(points[i][0], points[i][1], points[i+1][0],points[i+1][1]);
    ctx.lineWidth = 1;
    ctx.globalAlpha=opacity;
    ctx.fillStyle = color;
    ctx.fill();
    ctx.strokeStyle = color;
    ctx.closePath();
    ctx.stroke();
    ctx.restore();
}

//set the legend size on the left and on the right
isl.f.setLegendSize = function(legends){
    //calculate the number of legends that will displayed on the left or on the right
    var left = 0;
    var right = 0;
    // console.log($( "#selectVisibleStructures option:selected" ).text());
    // console.log(isl.p.displayedStructures);
    for (var i in legends) {
        // if ( ! isl.f.isDisplayed( legends[i].percentWidth , legends[i].percentHeight ) ) continue;
        var lid = legends[i].id;
        var structure = isl.current.legends[ lid ].structure;
        if (structure != "" && !isl.f.isStructureVisible(structure) ) continue;
        if (legends[i].percentWidth > 0.5) right++;
        else left ++;
    }

    left = (isl.p.count_view_legends > 0 && left > isl.p.count_view_legends) ? left - isl.p.count_view_legends : left;
    right = (isl.p.count_view_legends > 0 && right > isl.p.count_view_legends) ? right - isl.p.count_view_legends : right;

    //calculate the maximum legend size allowed
    var maxLegendSizeLeft = isl.p.mainImageHeight/(1+left);
    // console.log("Left= "+maxLegendSizeLeft);
    var maxLegendSizeRight = isl.p.mainImageHeight/(1+right);
    // console.log("Right= "+maxLegendSizeRight);
    var maxSize = Math.min(maxLegendSizeLeft, maxLegendSizeRight) * 0.5; // the 0.7 coeff is assuming about 1.5 lines per legend
    // console.log("maxSize="+maxSize);
    if (maxSize > 60) {
         isl.p.fontSize = 20;
    }
    else if(maxSize > 56) {
        isl.p.fontSize =  18;
    }
    else if(maxSize > 52) {
        isl.p.fontSize =  18;
    }
    else if(maxSize > 48) {
        isl.p.fontSize =  18;
    }
    else if(maxSize > 44) {
        isl.p.fontSize =  17;
    }
    else if(maxSize > 40) {
        isl.p.fontSize =  17;
    }
    else if(maxSize > 36) {
        isl.p.fontSize =  16;
    }
    else if(maxSize > 32) {
        isl.p.fontSize =  16;
    }
    else if(maxSize > 28) {
        isl.p.fontSize =  16;
    }
    else if(maxSize > 24) {
        isl.p.fontSize =  15;
    }
    else if(maxSize > 20) {
        isl.p.fontSize =  14;
    }
    else if(maxSize > 16) {
        isl.p.fontSize =  13;
    }
    else if(maxSize > 15) {
        isl.p.fontSize =  13;
    }
    else if(maxSize > 14) {
        isl.p.fontSize =  13;
    }
    else if(maxSize > 13) {
        isl.p.fontSize =  13;
    }
    else if (maxSize > 12) {
        isl.p.fontSize =  11;
    }
    else if (maxSize > 11) {
        isl.p.fontSize =  11;
    }
    // else if (maxSize > 10) {
    //     isl.p.fontSize =  10;
    // }
    // else isl.p.fontSize =  Math.round(maxSize);
    else isl.p.fontSize =  11;
}

//get max left and right
var countleft=0;
var countright=0;

isl.f.getMaxSide = function(){
    countleft=0;
    countright=0;

    var maxTargetHeight = 0;
    var legends = isl.p.getSortedLegends();
    for(var l in legends){
        var lid = legends[l].id;
        var structure = isl.current.legends[ lid ].structure;
        if (structure != "" && !isl.f.isStructureVisible(structure) ) continue;

        var targetWidth = isl.f.getWidthCoordinate( legends[l].percentWidth );
        var targetHeight = isl.f.getHeightCoordinate( legends[l].percentHeight );
        var side = isl.f.getPrintSide(targetWidth);
        if (side=="left"){
            countleft+=1;
        }
        else {
            countright+=1;
         }
        maxTargetHeight = Math.max(maxTargetHeight, targetHeight);
    }
    return maxTargetHeight;
    // return Math.max(countleft, countright);
};

//print all legends of the current image
isl.f.printAllLegendsOnCanvas = function(){
    //don't print anything if it is in hide mode
    if (isl.p.legendMode == "hide") return;
    //if the quiz mode is enabled but the imageId of the current quiz don't match with the current image, reset the quiz
    if (isl.p.quizImageId != isl.currentImage) {
        isl.p.quizImageId = isl.currentImage;
        isl.p.displayedQuizLegends = [];
        isl.p.legendsHeightOnCanvas = {};
        isl.p.legendsSideOnCanvas = {};
    }
    var legends = isl.p.getSortedLegends();
    //count the number of legends and set the right legend size depending on it
    isl.f.setLegendSize(legends);
    //set an object to remember where to start printing the legends
    var startPrintingHeight = {left:isl.p.fontSize , right:isl.p.fontSize};
    //****
    var most_bottom_targetHeight =0;

    isl.f.getMaxSide();
    //****
    ///current number left
    var curleft=0;
    //current number right
    var curright=0;
    if (countright > isl.p.count_view_legends ) countright -= isl.p.count_view_legends;
    if (countleft > isl.p.count_view_legends ) countleft -= isl.p.count_view_legends;
    var i_left = 0;
    var i_right = 0;

    for(var l in legends){
        var lid = legends[l].id;
        //don't display the legend if it isn't inside the canvas
        if ( ! isl.f.isDisplayed( legends[l].percentWidth , legends[l].percentHeight ) ) continue;
        //don't display the legend if it isn't inside the square
        if ((isl.p.legendMode == "square" || isl.p.legendMode == "largesquare" )  && ! isl.f.isLegendInsideSquare(legends[l])) continue;
        if (isl.f.isLegendInsideSquare(legends[l]) && (isl.p.legendMode == "square" || isl.p.legendMode == "largesquare" )){
            // console.log("isLegendInsideSquare_printAllLegendsOnCanva");
            isl.p.fontSize = isl.p.squarefontSize;
        }
        var structure = isl.current.legends[ lid ].structure;
        //if the structure is not visible, skip it
        if (structure != "" && !isl.f.isStructureVisible(structure) ) continue;
        //the text is the one specified in the
        var text = isl.p.legendMode != "quiz" || isl.p.displayedQuizLegends.indexOf( lid ) > -1 ? isl.f.getLegendText(lid) : "";

        var color = structure != "" ? isl.current.structures[ structure ].color : "white";
        //the target width and height are saved as percentage of the image size
        var targetWidth = isl.f.getWidthCoordinate( legends[l].percentWidth );
        var targetHeight = isl.f.getHeightCoordinate( legends[l].percentHeight );

        //**
        var side = isl.f.getPrintSide(targetWidth);

        {
            if (side=="left")
            {
                if (i_left < isl.p.count_view_legends){
                    i_left += 1;
                    continue;
                 }

            }
            else if(i_right <= isl.p.count_view_legends){
                  i_right += 1;
                  continue;
            }

        }

        if (isRain)
        {
            if (side=="left")
            {
                if (curleft<raincolors.length) color=raincolors[curleft];
                else {
                    var overindex=curleft-raincolors.length*Math.floor(curleft/raincolors.length);
                    color=raincolors[overindex];
                }
                curleft+=1;
            }
            else
            {
                if (curright<raincolors.length) color=raincolors[curright];
                else {
                    var overindex=curright-raincolors.length*Math.floor(curright/raincolors.length);
                    color=raincolors[overindex];
                }
                curright+=1;
            }
        }

        //**
        //print the legend, only if the process hasn't been started alread
        isl.f.printLegendOnCanvas( text, color, targetWidth, targetHeight , legends[l].id , startPrintingHeight,countleft,countright);
        most_bottom_targetHeight = Math.max(startPrintingHeight.left, startPrintingHeight.right);
    }
    // console.log('most_bottom_targetHeight='+most_bottom_targetHeight);
    var btToBottom_img = $(parent.document).find('#btToBottom_img');
    btToBottom_img.prop("alt", most_bottom_targetHeight);
    // console.log('btToBottom_img='+btToBottom_img.attr("alt"));
}

//check if a given legend object is inside the square foccusing the legends
isl.f.isLegendInsideSquare = function(legend){
    // console.log('isLegendInsideSquare');
    //the position of the legend
    var targetWidth = isl.f.getWidthCoordinate( legend.percentWidth )+ isl.p.legendLeftWidth ;
    var targetHeight = isl.f.getHeightCoordinate( legend.percentHeight );
    //the limit of the square
    var minX = isl.f.getWidthCoordinate ( isl.p.legendSquareCenterPercentWidth ) - isl.p.legendSquareSize/2 + isl.p.legendLeftWidth ;
    var minY = isl.f.getHeightCoordinate( isl.p.legendSquareCenterPercentHeight ) - isl.p.legendSquareSize/2;
    var maxX = isl.f.getWidthCoordinate( isl.p.legendSquareCenterPercentWidth) + isl.p.legendSquareSize/2 + isl.p.legendLeftWidth;
    var maxY = isl.f.getHeightCoordinate( isl.p.legendSquareCenterPercentHeight) + isl.p.legendSquareSize/2;
    
    //var minX = ( isl.p.legendSquareCenterPercentWidth - isl.p.legendSquareSize/2) * isl.p.mainImageWidth + isl.p.legendLeftWidth ;
    //var minY = ( isl.p.legendSquareCenterPercentHeight - isl.p.legendSquareSize/2) * isl.p.mainImageHeight;
    //var maxX = ( isl.p.legendSquareCenterPercentWidth + isl.p.legendSquareSize/2) * isl.p.mainImageWidth + isl.p.legendLeftWidth;
    //var maxY = ( isl.p.legendSquareCenterPercentHeight + isl.p.legendSquareSize/2) * isl.p.mainImageHeight;
    
    return (targetWidth > minX) &&(targetWidth <maxX) &&(targetHeight > minY) &&(targetHeight < maxY); 
}


//function to get the coordinates on the main images given the width and height percent
isl.f.getWidthCoordinate = function(percentWidth){
    return (parseFloat( isl.p.verticalSymetry ? isl.p.zoomEndPercentWidth - percentWidth : percentWidth - isl.p.zoomStartPercentWidth ) ) * isl.p.mainImageWidth / (isl.p.zoomEndPercentWidth - isl.p.zoomStartPercentWidth);
}

isl.f.getHeightCoordinate = function(percentHeight){
    return ( parseFloat( isl.p.horizontalSymetry ? isl.p.zoomEndPercentHeight - percentHeight : percentHeight - isl.p.zoomStartPercentHeight ) ) * isl.p.mainImageHeight / (isl.p.zoomEndPercentHeight - isl.p.zoomStartPercentHeight);
}

//function to print the square on the canvas
isl.f.displaySquare = function(){
    // console.log('show square');
    //get the coordinates of the square to print
    var minX = isl.f.getWidthCoordinate ( isl.p.legendSquareCenterPercentWidth ) - isl.p.legendSquareSize/2 + isl.p.legendLeftWidth ;
    var minY = isl.f.getHeightCoordinate( isl.p.legendSquareCenterPercentHeight ) - isl.p.legendSquareSize/2;
    var maxX = isl.f.getWidthCoordinate( isl.p.legendSquareCenterPercentWidth) + isl.p.legendSquareSize/2 + isl.p.legendLeftWidth;
    var maxY = isl.f.getHeightCoordinate( isl.p.legendSquareCenterPercentHeight) + isl.p.legendSquareSize/2;
    
    var ctx=isl.f.getMainContext();
    ctx.strokeStyle = isl.p.squareColor;
    ctx.lineWidth = isl.p.squareLineWidth;
    ctx.beginPath();
    //draw the vertical line
    ctx.moveTo( minX, minY);
    ctx.lineTo(minX , maxY );
    ctx.lineTo(maxX , maxY );
    ctx.lineTo(maxX , minY );
    ctx.lineTo(minX , minY );
    ctx.stroke();
}

//function to print the square large on the canvas
isl.f.displaySquareLarge = function(){
    //get the coordinates of the square to print
    // console.log(isl.p.legendSquareCenterPercentWidth );

    var minX = isl.f.getWidthCoordinate ( isl.p.legendSquareCenterPercentWidth ) - isl.p.legendSquareSize/2 + isl.p.legendLeftWidth ;
    var minY = isl.f.getHeightCoordinate( isl.p.legendSquareCenterPercentHeight ) - isl.p.legendSquareSize/2;
    var maxX = isl.f.getWidthCoordinate( isl.p.legendSquareCenterPercentWidth) + isl.p.legendSquareSize/2 + isl.p.legendLeftWidth;
    var maxY = isl.f.getHeightCoordinate( isl.p.legendSquareCenterPercentHeight) + isl.p.legendSquareSize/2;

    // minX=minX*2;
    // minY=minY*2;
    // maxX=maxX*2;
    // maxY=maxY*2;
    var coef=1.5;

    var ctx=isl.f.getMainContext();
    ctx.strokeStyle = isl.p.squareColor;
    ctx.lineWidth = isl.p.squareLineWidth;
    ctx.beginPath();
    //draw the vertical line
    ctx.moveTo( minX, minY);
    ctx.lineTo(minX , maxY);
    ctx.lineTo(maxX , maxY );
    ctx.lineTo(maxX , minY );
    ctx.lineTo(minX , minY );
    ctx.stroke();
}

//check if the legend should be printed on the left or on the right
isl.f.getPrintSide = function(targetWidth){
    return targetWidth * 2 > isl.p.mainImageWidth ? "right" : "left";
}

var SearchColorText='black'; //color of search text
var SearchColorBackground='white'; //color of background of search text

// return when search is active
isl.f.isSearchActive= function(isfound){
    if (isl.p.legendSearch!=null) {
        // console.log('isl.p.legendSearch.text()='+isl.p.legendSearch.text()+', isfound='+isfound );
        return isl.p.legendSearch.text()!='' && isfound;
    }

    return true;

}

//print a legend on the canvas
isl.f.printLegendOnCanvas = function(text , color, targetWidth, targetHeight , lid , startPrintingHeight,countleft, countright){
    
    //chek whether the legend shoudl be printed on the left side or the right side
    var side = isl.f.getPrintSide(targetWidth);
    
    var ctx=isl.f.getMainContext();
    //increase the font size in case of mouseover
    var fontSize = isl.p.mouseOverLegend == lid && isl.p.mouseOverLegend != null ? isl.p.fontSize +0 : isl.p.fontSize;
    // var fontSize = isl.p.fontSize;
    // console.log('fontSize= '+fontSize);
    ctx.font = ""+fontSize+"px "+isl.p.font;
    //if the lid is the one beeing hovered by the mouse, print it in bold
    ctx.fillStyle = color;
    //set the maximum width a text can take
    if(side == "left") var maxWidth = isl.p.legendLeftWidth - isl.p.verticalLinePadding - isl.p.verticalLinePaddingToWord;
    else var maxWidth = isl.p.legendRightWidth - isl.p.verticalLinePadding - isl.p.verticalLinePaddingToWord;
    //broke the text into group of words smaller than the max width
    var words = isl.f.breakText(text, maxWidth, fontSize);
    var heightIncrement = 0;
    //***********
    var clientHeight=ctx.canvas.clientHeight;
    var islongfirst=true;
    // console.log('Count legends',countleft,countright);
    //***********
    var isfound=false;
    for(var w in words) {
        var size =  ctx.measureText(words[w]).width;
        var textWidthPosition = side == "left" ? maxWidth - size : isl.p.legendLeftWidth + isl.p.mainImageWidth + isl.p.verticalLinePadding + isl.p.verticalLinePaddingToWord;
        var textHeightPosition = side == "left" ? startPrintingHeight.left + heightIncrement : startPrintingHeight.right + heightIncrement;

        if (islongfirst  ){
            if (side=="left" )
            {
                var Hplus=clientHeight/(countleft*2.5);
                if (countleft>17 && countleft<24) Hplus=clientHeight/(countleft*4);
                else if (countleft>=24) Hplus=clientHeight/(countleft*6);
                //console.log(words[w],lid);
                Hplus =isl.p.legendMode == "square" ? Hplus*=isl.p.squareMultyIncrement : Hplus;

                startPrintingHeight.left+=Hplus;
            }
            else
            {
                var Hplus=clientHeight/(countright*2.5);
                if (countright>17 && countright<24) Hplus=clientHeight/(countright*4);
                else {
                    if (countright>=24) Hplus=clientHeight/(countright*6);
                    if (countright>=30) Hplus=clientHeight/(countright*14);
                }
                //console.log(words[w],lid);
                Hplus =isl.p.legendMode == "square" ? Hplus*=isl.p.squareMultyIncrement : Hplus;
                startPrintingHeight.right+=Hplus;

            }

            textHeightPosition+=Hplus;
            islongfirst=false;

        }

        if (isl.p.legendSearch && isl.p.legendSearch.text()!='')
        {
            if (isl.p.legendSearch.text()==words[w] || (isl.p.legendSearch.text().length>words[w].length &&  isl.p.legendSearch.text().indexOf(words[w])==0 ))
            {
                //console.log(words[w].length);
                if (side=="left" )
                {
                    if (words[w].length>10) textWidthPosition-=10;
                    else if (words[w].length>20) textWidthPosition-=20;
                    else textWidthPosition-=2;
                }
                ctx.font = "bold "+fontSize+"px "+isl.p.font;
                // ctx.fillStyle = 'black'; //black
                ctx.fillStyle = SearchColorText; //black
                var h=fontSize;
                var w_str=words[w];
                if (isl.p.legendSearch.text().length>words[w].length)
                {
                    h*=2;
                    w_str=w_str.substring(0,w_str.length/2);
                }

                drawTextBG(ctx, w_str, h+"px "+isl.p.font, textWidthPosition, textHeightPosition-fontSize, side);
                // console.log(words[w]);
                // console.log(textHeightPosition);
                isfound=true;
            }
        }

        if (!isl.f.isSearchActive(isfound) && isl.p.flipswitch_checked !='') continue;

        // console.log(words[w]);
        ctx.font = ""+fontSize+"px "+isl.p.font;

        ctx.fillText(  words[w], textWidthPosition , textHeightPosition);
        if (isfound)
        {
            // ctx.fillRect(x, y, width, parseInt(font, 10));

            ctx.font = ""+fontSize+"px "+isl.p.font;
        }
        //increment the next height to print legend
        heightIncrement += isl.p.wordPaddingTop + isl.p.fontSize;
        //---Test

    }

    if (!isl.f.isSearchActive(isfound) && isl.p.flipswitch_checked !='') return;

    //compute useful coordinates to draw the lines
    var verticalLineTopHeight = (side == "left" ? startPrintingHeight.left :  startPrintingHeight.right ) - isl.p.fontSize + 2 ;
    var verticalLineBottomHeight = (side == "left" ? startPrintingHeight.left : startPrintingHeight.right ) -isl.p.fontSize + heightIncrement - 2;
    var verticalLineWidth = side == "left" ? isl.p.legendLeftWidth - isl.p.verticalLinePadding: isl.p.legendLeftWidth + isl.p.mainImageWidth + isl.p.verticalLinePadding ;
    var horizontalLineHeight = Math.floor(verticalLineTopHeight + verticalLineBottomHeight)/2;
    var horizontalLineLeft = verticalLineWidth;
    var horizontalLineRight = side == "left" ? verticalLineWidth + isl.p.verticalLinePadding : verticalLineWidth - isl.p.verticalLinePadding;
    var targetWidthOnCanvas = targetWidth + isl.p.legendLeftWidth;
    var targetHeightOnCanvas = targetHeight ;

    //******Test

    //*****

    //remember the position where to print the legend
    isl.p.legendsHeightOnCanvas[lid] = ( verticalLineTopHeight + verticalLineBottomHeight)/2;
    isl.p.legendsSideOnCanvas[lid] = side;


    ctx.strokeStyle = color;
    ctx.lineWidth = isl.p.lineWidth;
    ctx.beginPath();
    //draw the vertical line
    if (isfound) ctx.lineWidth=3;
    ctx.moveTo( verticalLineWidth, verticalLineTopHeight);
    ctx.lineTo(verticalLineWidth , verticalLineBottomHeight );
    //draw the horizontal line
    ctx.moveTo( horizontalLineLeft , horizontalLineHeight);
    ctx.lineTo( horizontalLineRight , horizontalLineHeight);
    //draw the line to target
    ctx.lineTo( targetWidthOnCanvas  , targetHeightOnCanvas);
    //end the line
    ctx.stroke();

    //draw the target circle
    ctx.beginPath();
    ctx.arc(targetWidthOnCanvas, targetHeightOnCanvas, isl.p.targetRadius, 0, 2 * Math.PI, false);
    ctx.fillStyle = color;
    ctx.fill();



    //for the next legends, increment the start height
    if (side=="left") startPrintingHeight.left +=  heightIncrement + isl.p.legendPaddingTop ;
    else startPrintingHeight.right +=  heightIncrement + isl.p.legendPaddingTop ;
    
}

//draw background for text
function drawTextBG(ctx, txt, font, x, y, side) {
    var dxl=8, dxr=3;
    ctx.save();
    ctx.font = font;
    ctx.textBaseline = 'top';
    // ctx.fillStyle = 'white';
    ctx.fillStyle = SearchColorBackground;

    var width = ctx.measureText(txt).width+25;

    (side=='left') ? x-=dxl : x+=dxr ;
    ctx.fillRect(x-5, y, width, parseInt(font, 15));
    // ctx.fillRect(x-10, y, width, heith);

    // ctx.fillStyle = '#000';
    // ctx.fillText(txt, x, y);

    ctx.restore();
}

//break a text in several lines so that each line doesn't exceed a given width
isl.f.breakText = function(text, maxWidth, fontSize){
    var ctx=isl.f.getMainContext();
    ctx.font = fontSize+"px "+isl.p.font;
    var result = [];
    var words = text.split(" ");
    var str = "";
    for(var w in words){
        var word = words[w];
        if (str == "") str = word;
        else if ( ctx.measureText(str+" "+word).width <= maxWidth) str += " "+word;
        else{
            result.push(str);
            str = word;
        }
    }
    //in the end, append the last piece of str
    result.push(str);
    return result;
}

//retrieve the context of the main canvas image
isl.f.getMainContext = function(){
    var c=document.getElementById(isl.p.mainImageCanvas);
    var ctx=c.getContext("2d");
    return ctx;
}

//function to create a new data object
isl.f.createNewObject = function(){
    var data = {};
    
    //create a new random id
    data.id = isl.f.generateId() ;
    
    //a data should have a list of images
    data.images = {};
    
    //a data should have a list of legends
    data.legends = {};
    
    //a data should have a list of anatomical structures
    data.structures = {};
    
    //a data has general informations
    data.general = {};
    
    //a data object has one or several languages
    data.languages = {
        "english" : "english",
    };
    
    //a data object has one or several series
    data.series = {
        //0 : { text : "default" }
    };
    
    return data;
}

//function to save load a piece of data as the current object
isl.f.openProject = function(id){
    if (id != null && id != undefined) {
        var jqxhr = $.ajax({
            url: "/loaddata",
            method: "POST",
            data: {id:id},
            dataType: "json",
            type: "POST",
        })
        .done(function(res) {
            //reset all
            isl.current = res != null && "id" in res ? res : isl.f.createNewObject();
            isl.current.id = id;
            //by default, all structure are visible
            for(sid in isl.current.structures) isl.p.displayedStructures.push(sid);
            isl.f.start();
            isl.f.resetAll();
        })
        .fail(function() {
            alert( "error during loading" );
        })
    }
    else{
        //trigger directly the display all event
        isl.current = isl.f.createNewObject();
        isl.f.resetAll();
    }
}

var temp=true;
//get an array of images sorted by appearance order
isl.p.getSortedImage = function(series){
    if (series == undefined) {
        if (isl.p.currentSeries == null) {
            isl.f.selectDefautSeries();
        }
        series = isl.p.currentSeries;
    }
    var sortable = [];
    if (!("images" in isl.current)) return sortable;
    var lid = $(isl.p.legendSearch).attr("lid");
    var a=1;
    for (var i in isl.current.images)
    {
        if (isl.p.legendSearch && isl.p.legendSearch.text()!='')
        {
            if (lid in isl.current.images[i].legends) {
                if(series == null || series == isl.current.images[i].series) sortable.push(isl.current.images[i])
                a+=1;

            }
        }
        else
        {
            if(series == null || series == isl.current.images[i].series) sortable.push(isl.current.images[i])
        }

    }

    //console.log(sortable.length);
    //sortable.sort(function(a, b) {return a.position - b.position})
    sortable.sort(function(a, b) {return 0+(a.position) - (b.position)})
    return sortable;
}

//get an array of legends sorted by height order
isl.p.getSortedLegends = function(){
    var sortable = [];
    if (!("legends" in isl.current)) return sortable;
    for (var i in isl.current.images[ isl.currentImage].legends )
        sortable.push(isl.current.images[ isl.currentImage].legends[i])
    //sortable.sort(function(a, b) {return isl.f.getHeightCoordinate( a.percentHeight )  - isl.f.getHeightCoordinate(b.percentHeight) });
    
    //sortable = [sortable[0],sortable[1],sortable[2],sortable[3],sortable[4],sortable[5],sortable[6],sortable[7],sortable[8],sortable[9],sortable[10],sortable[11]];
    var result = [];
    left = [];
    right = [];
    c = 0;
    while(sortable.length>0){
        c++;
        sortable.sort(function(a, b) {
            var aph = isl.p.horizontalSymetry ? 1-a.percentHeight : a.percentHeight;
            var bph = isl.p.horizontalSymetry ? 1-b.percentHeight : b.percentHeight;
            var apw = isl.p.verticalSymetry ? 1-a.percentWidth : a.percentWidth;
            var bpw = isl.p.verticalSymetry ? 1-b.percentWidth : b.percentWidth;
            var ah = isl.f.getHeightCoordinate( aph );
            var bh = isl.f.getHeightCoordinate( bph );
            var aw = isl.f.getWidthCoordinate( apw );
            var bw = isl.f.getWidthCoordinate( bpw );
            var aside = apw>0.5 ? right.length : left.length;
            var bside = bpw>0.5 ? right.length : left.length;
            var horizontalLineLength = isl.p.verticalLinePadding ;
            var lineHeight = isl.p.wordPaddingTop + isl.p.fontSize + isl.p.legendPaddingTop + 2 ;
            var aslope = (aph-(aside+0.5)*( lineHeight )/isl.p.mainImageHeight )/( (apw>0.5 ? 1-apw : apw) +0.0001);
            var bslope = (bph-(bside+0.5)*( lineHeight )/isl.p.mainImageHeight )/( (bpw>0.5 ? 1-bpw : bpw) +0.0001);
            //if(c==3) console.log(a,aslope,aph-(aside+0.5)*( lineHeight )/isl.p.mainImageHeight);
            //if(c==3) console.log(b,bslope,bph-(bside+0.5)*( lineHeight )/isl.p.mainImageHeight);
            return 1000*(bslope - aslope);
        });
        var el = sortable.pop();
        if( (el.percentWidth>0.5 && isl.p.verticalSymetry)||(el.percentWidth<0.5 && !isl.p.verticalSymetry) ) left.push(el);
        else right.push(el);
        //console.log(right.length);
    }

    return left.concat(right);
}

//function to play all functions on a given array
isl.p.callFunctionsFromArray = function(arr){
    for(var i in arr){
        var fname = arr[i];
        var parts = fname.split(".");
        var f = window;
        for(var j in parts) {
            f = f[ parts[j] ];
        }
        f();
    }
}

//return the screen mode of the web browser : full or normal
isl.f.screenMode = function(){
    if (!window.screenTop && !window.screenY) {
        return "full";
    }
    else return "normal";
}

//reset some p vars
isl.f.resetPVars = function(){
    isl.p.legendLeftStartHeight = isl.p.fontSize;
    isl.p.legendRightStartHeight = isl.p.fontSize;
    isl.p.currentLanguage = isl.f.retrieveCurrentLanguage();
    // console.log(isl.p.currentLanguage);
}

//simple function to generate a random integer as unique ID
isl.f.generateId = function(){
    return Math.floor( 1000000000000000 * Math.random() );
}
isl.f.generateIds = function(n){
    var ids = [];
    for(var i=0;i<n;i++) ids.push(isl.f.generateId());
    return ids;
}

//get the text of the given legend
isl.f.getLegendText = function(lid, language){
    if (language == null) {
        language = isl.p.currentLanguage;
    }
    
    if (!(lid in isl.current.legends)) return "";
    return language in isl.current.legends[lid].text ? isl.current.legends[lid].text[language] : "";
}


//get the text of the given structure
isl.f.getStructureText = function(sid, language){
    if (language == null) {
        language = isl.p.currentLanguage;
    }
    if (!(sid in isl.current.structures)) return "";
    return isl.current.structures[sid].text;
    //return language in isl.current.structures[sid].text ? isl.current.structures[sid].text[language] : "";
}



//retrieve the current legend
isl.f.retrieveCurrentLanguage = function(){
    if (isl.p.currentLanguage != null && isl.p.currentLanguage in isl.current.languages)
    {
        return isl.p.languagepage;
        // console.log(isl.p.currentLanguage);
        // return isl.p.currentLanguage;
    }
    //else, if the default language has been defined :
    if (isl.current.general.defaultLanguage in isl.current.languages)
    {
        // console.log(isl.p.currentLanguage);
        return isl.p.languagepage;
        // return  isl.current.general.defaultLanguage;
    }

    //else return the first language found
    for(var l in isl.current.languages)
    {
        return isl.p.languagepage;
        // return l;
    }

    //else there is no language
    return null;
}

//check that a point of the main image is displayed
isl.f.isDisplayed = function (percentWidth,percentHeight){
    return (percentWidth >= isl.p.zoomStartPercentWidth) && (percentWidth <= isl.p.zoomEndPercentWidth) && (percentHeight <= isl.p.zoomEndPercentHeight) && (percentHeight >= isl.p.zoomStartPercentHeight);
}

//calculate the percentWidth of the total image from the position on the canvas
isl.f.percentWidthFromWidth = function(width){
    if (isl.p.verticalSymetry) {
        return (1-isl.p.zoomEndPercentWidth) + (1-(width )*(isl.p.zoomEndPercentWidth - isl.p.zoomStartPercentWidth) / isl.p.mainImageWidth);
    }
    else return isl.p.zoomStartPercentWidth + (width )*(isl.p.zoomEndPercentWidth - isl.p.zoomStartPercentWidth) / isl.p.mainImageWidth;
}
isl.f.percentHeightFromHeight = function(height){
    if (isl.p.horizontalSymetry) {
        return (1-isl.p.zoomEndPercentHeight) + (1-height * (isl.p.zoomEndPercentHeight - isl.p.zoomStartPercentHeight) / isl.p.mainImageHeight);
    }
    else return isl.p.zoomStartPercentHeight + height * (isl.p.zoomEndPercentHeight - isl.p.zoomStartPercentHeight) / isl.p.mainImageHeight;
} 



//calculate the coordinates ont the original canvas without zoom
isl.f.realHeightFromHeight = function(height){
    return isl.p.mainImageHeight * isl.f.percentHeightFromHeight(height);
}
isl.f.realWidthFromWidth = function(width){
    return isl.p.legendLeftWidth + isl.p.mainImageWidth * isl.f.percentWidthFromWidth(width);
}


//select a default series if isl.p.currentSeries is null
isl.f.selectDefautSeries = function(){
    if (isl.p.currentSeries == null) {
        for(var sid in isl.current.series){
            isl.p.currentSeries = sid;
            break;
        }
    }
}

isl.f.resetCountViewLegendSelectionButton = function(){
    var html = "";

    html += "<select id=countviewlegendchooseselector>";

    for (var key in isl.p.view_legends_dict) {
        // console.log('key= '+ key+ ' = ' +isl.p.view_legends_dict[key]);
        html += "<option value=" + isl.p.view_legends_dict[key] +" "+( isl.p.count_view_legends == isl.p.view_legends_dict[key] ? " selected" : " " )+">"+ key + "</option>";
    }

    // html += "<option value='ALL' "+( isl.p.count_view_legends == 0 ? " selected" : " " )+">ALL</option>";
    // html += "<option value='20' "+( isl.p.count_view_legends == 20 ? " selected" : " " )+">20</option>";
    // html += "<option value='40' "+( isl.p.count_view_legends == 40 ? " selected" : " " )+">40</option>";
    // html += "<option value='60' "+( isl.p.count_view_legends == 60 ? " selected" : " " )+">60</option>";

    html += "</select>";
    $("#"+isl.p.topUserPannel).append(html);

    $("#countviewlegendchooseselector").change( function(e){
        isl.p.count_view_legends = $(this).val();
        // console.log('isl.p.count_view_legends= '+isl.p.count_view_legends );
        isl.f.resetAll();
    });

}